@ECHO OFF
:: Check for Python Installation
CALL .\virtual_env\Scripts\activate
if errorlevel 1 goto installPython
:: Reaching here means Python Venv was already created
goto:startServer

:installPython
CALL .\windows_installer.bat
goto:finished

:startServer

python install_requirements.py --update_venv=True
python manage.py migrate

start cmd.exe @cmd /k .\background_runner.bat

echo.
echo "Waiting 5 seconds for background_runner to set up the server."
:: hacky sleep: ping localhost for 5 tries, each try needs 1 sec
ping 127.0.0.1 -n 5 > nul

explorer "http://127.0.0.1:8000/"

:finished

@ECHO ON