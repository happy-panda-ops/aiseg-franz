@ECHO OFF
echo "Python >= 3.9 required."
echo.
pause
echo.
echo "The installation will be inside this folder. You want to uninstall? Remove the folder virtual_env or the whole project"
echo.

:: Check for Python Installation
echo.
echo "Checking if the environment exists:"
echo.
CALL .\virtual_env\Scripts\activate
if errorlevel 1 goto createPythonVenv
:: Reaching here means Python Venv was already created
echo "Found existing virtual environment"
goto:executePipInstaller

:createPythonVenv
echo.
echo "Creating a new environment."
echo.

:: Reaching here means Python 3.9 is installed.
py -3.9 -m venv virtual_env
if errorlevel 1 goto errorNoPython
goto:activateVenv


:activateVenv
:: Execute stuff...
echo "Virtual environment was created"
CALL .\virtual_env\Scripts\activate

goto:executePipInstaller

:executePipInstaller
python install_requirements.py

:: Once done, exit the batch file -- skips executing the errorNoPython section
goto:success

:errorNoPython
echo.
echo "Error: Python is not installed or not in Path or the wrong version is at the top of your environment variables."
echo "Download here: https://www.python.org/ftp/python/3.9.13/python-3.9.13-amd64.exe"
echo "Start again after installation or fix your environment variables. Leave everything on default during installation."
goto:finished

:success
echo.
echo.
echo "Finished windows installer."
echo.
echo "If everything worked, you can now run the 'start_server.bat' file"
echo.
goto:finished

:finished
pause

@ECHO ON