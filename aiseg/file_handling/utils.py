import base64
import errno
import os
import pickle
import re
import shutil

from distutils.dir_util import copy_tree
from django.utils import timezone

from glob import glob


def convert_np_array_to_binary(array):
    byte_array = pickle.dumps(array)
    encoded_array = base64.b64encode(byte_array)
    return encoded_array


def convert_binary_to_np_array(binary):
    byte_array = base64.b64decode(binary)
    decoded_array = pickle.loads(byte_array)
    return decoded_array


def create_folder(path):
    """
    Checks if a folder exists or creates the folder
    """
    if not os.path.isdir(path):
        os.makedirs(path)


def natural_sort_key(s, sort_regular_expression=re.compile('([0-9]+)')):
    """
    Helper to sort like the windows explorer

    Natural sorting like the explorer does is not doable with sorted() or list.sort(key=str.lower).
    Why: number will not be seen as numbers but as text resulting to the following standard behaviour:
        ["A1", "a10", "A11", "a2", "a22", "A3"].sort() ->                       ['A1', 'A11', 'A3', 'a10', 'a2', 'a22']
        ["A1", "a10", "A11", "a2", "a22", "A3"].sort(key=str.lower) ->          ['A1', 'a10', 'A11', 'a2', 'a22', 'A3']
    What we want:
        ["A1", "a10", "A11", "a2", "a22", "A3"].sort(key=natural_sort_key) ->   ['A1', 'a2', 'A3', 'a10', 'A11', 'a22']

    Examples:
        - your_list_containing_some_text.sort(key=natural_sort_key)
        - ["A1", "a10", "A11", "a2", "a22", "A3"].sort(key=natural_sort_key) -> ['A1', 'a2', 'A3', 'a10', 'A11', 'a22']

    Args:
        s: string to be analyzed by the regular expressions
        sort_regular_expression: regular expression of something. Defaults to numbers

    Returns: string separated into list of strings and numbers like:
        natural_sort_key('a10b11') -> ['a', 10, 'b', 11, '']
        natural_sort_key('a10b')   -> ['a', 10, 'b']

    """
    return [int(text) if text.isdigit() else text.lower()
            for text in sort_regular_expression.split(s)]


def get_files(folder, get_absolute_paths=True, extension=None, sort=True, natural_sorting=True):
    """

    Args:
        folder: folder to search in
        get_absolute_paths: if true: returns the whole relative path to the files
        extension: None, one extension as string or a list/tuple of extensions -->
         - None: all file paths are returned
         - String: only those with this extension
         - List/Tuple: any extension in the list/tuple
        sort: sorts files by name
        natural_sorting: example: a = ['a10', 'a9', 'a1'].sort() -> ['a1', 'a10', 'a9'].
                         sort like os sorts like this: ['a10', 'a9', 'a1'] -> ['a1', 'a9', 'a10']

    Returns: all file names or file paths in a given folder with the given extension(s)

    """
    if not isinstance(get_absolute_paths, bool):
        raise ValueError('get_absolute_paths must be a bool!')
    if not isinstance(sort, bool):
        raise ValueError('get_absolute_paths must be a bool!')
    if extension is None or extension == 'all':
        files = [file for file in os.listdir(folder) if os.path.isfile(os.path.join(folder, file))]
    elif isinstance(extension, list) or isinstance(extension, tuple):
        extensions = tuple(extension)
        files = [file for file in os.listdir(folder) if
                 os.path.isfile(os.path.join(folder, file)) and file.lower().endswith(extensions)]
    elif isinstance(extension, str):
        files = [file for file in os.listdir(folder) if
                 os.path.isfile(os.path.join(folder, file)) and file.lower().endswith(extension)]
    else:
        raise ValueError('The provided extension must be: None, a string or a list/tuple for multiple extensions')

    if get_absolute_paths:
        files = [os.path.join(folder, file) for file in files]

    if sort:
        if natural_sorting:
            files.sort(key=natural_sort_key)
        else:
            files.sort()
    return files


def get_sub_folders(folder, depth=None):
    """
    Given a folder, this function looks into it and returns a list of sub folders.

    Args:
        folder: Folder to look in
        depth: None for all sub folders in every sub folder,
            pass 1 to list folders in current folder and not further sub folders

    Returns: list of absolute sub folder paths

    """
    if not os.path.isdir(folder):
        raise FileNotFoundError(f'The provided folder {folder} does not exist.')
    if depth is not None:
        if not isinstance(depth, int):
            raise ValueError('Depth must be an integer.')
        if depth < 0:
            raise ValueError('Depth cannot be negative')
        elif depth < 1:
            raise ValueError('Depth must be at least 1 for current folder level')

    # workaround to os.walk. os.walk is very slow if there are a lot of sub_folders -> iterating is way faster in most
    # cases
    if depth == 1:
        # way faster if sub dirs contain a lot of data
        sub_folders = glob(os.path.join(folder, "*", ""))
        # glob returns tailing slashes --> remove tailing slashes
        sub_folders = [os.path.abspath(folder) for folder in sub_folders]

    elif depth is None:
        sub_folders = []
        current_sub_folders = glob(os.path.join(folder, "*", ""))
        current_sub_folders = [os.path.abspath(folder) for folder in current_sub_folders]
        for folder in current_sub_folders:
            sub_folders.append(folder)
            sub_folders += get_sub_folders(folder, None)

    else:
        sub_folders = []
        current_sub_folders = glob(os.path.join(folder, "*", ""))
        current_sub_folders = [os.path.abspath(folder) for folder in current_sub_folders]
        for i in range(1, depth):
            for folder in current_sub_folders:
                sub_folders.append(folder)
                sub_folders += get_sub_folders(folder, depth - i)

    return sub_folders


def folder_contains_files_or_files_in_sub_folders(folder):
    files = get_files(folder)
    sub_folders = get_sub_folders(folder)
    if files:
        return True
    for folder in sub_folders:
        files = get_files(folder, extension='all')
        if files:
            return True
    return False


def folder_contains_files(folder, extension='all'):
    files = get_files(folder, extension=extension)
    if not files:
        return False
    return True


def folder_exists(folder):
    return os.path.isdir(folder)


def folder_contains_sub_folders(folder):
    sub_folders = [x[0] for x in os.walk(folder)]
    sub_folders.pop(0)  # 0 is folder itself
    if sub_folders:
        return True
    return False


def copy_or_move_file_or_files(file_or_file_list, target_folder, base_folder=None, move=False, overwrite=False):
    """
    Args:
        file_or_file_list: absolut file path or list of absolute file paths, if only the file name is given,
            pass a base folder as well
        target_folder: folder to copy or move the file(s) to
        base_folder: if only file names are given in the list, pass the base folder of them
        move: True for moving the local_files, False for copy
        overwrite: False will add a new like 'new_your_file_name' if the file already exists
    """
    if not isinstance(file_or_file_list, list):
        file_or_file_list = [file_or_file_list]

    files_in_target_folder = get_files(target_folder, get_absolute_paths=False, extension='all')

    if base_folder:
        for index, file in enumerate(file_or_file_list):
            file_or_file_list[index] = os.path.join(base_folder, file)

    for file in file_or_file_list:
        file_name = os.path.basename(file)

        # do not overwrite data
        if not overwrite:
            while file_name in files_in_target_folder:
                file_name = 'new_' + file_name

        if move:
            shutil.move(file, os.path.join(target_folder, file_name))
        else:
            shutil.copyfile(file, os.path.join(target_folder, file_name))


def copy_file(file, target_folder, overwrite=True):
    """shortcut (shorter func name)"""
    copy_or_move_file_or_files(file, target_folder, move=False, overwrite=overwrite)


def copy_files(file_or_file_list, target_folder, base_folder=None, overwrite=False):
    """shortcut (shorter func name)"""
    copy_or_move_file_or_files(file_or_file_list, target_folder, base_folder, move=False, overwrite=overwrite)


def move_files(file_or_file_list, target_folder, base_folder=None, overwrite=False):
    """shortcut (shorter func name)"""
    copy_or_move_file_or_files(file_or_file_list, target_folder, base_folder, move=True, overwrite=overwrite)


def folder_contains_file(file_name, folder):
    files = get_files(folder, get_absolute_paths=False, extension='all')
    if file_name in files:
        return True
    return False


def get_overwritten_file_path(file_path):
    file_name = os.path.basename(file_path)
    file, file_extension = file_name.split('.')
    dir_name = os.path.dirname(file_path)
    now = timezone.now()
    human_time_string = now.strftime('_%Y_%m_%d_%H_%M_%S')
    file_path = os.path.join(dir_name, file + human_time_string + file_extension)
    if os.path.isfile(file_path):
        file_path = os.path.join(dir_name, file + human_time_string + '_new' + file_extension)
    return file_path


def remove_file(file_path):
    try:
        os.remove(file_path)
    except OSError as e:
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred


def remove_files(file_list):
    for file_path in file_list:
        remove_file(file_path)


def remove_dir(directory):
    if os.path.isdir(directory):
        shutil.rmtree(directory)


def copy_dir(source_dir, destination_dir):
    """
    Attention: moving a lot of data at one time, for example a folder with thousands and thousands of files,
    distutils writes everything in cache which can result in a os error saying that you have not enough space.

    This is a Linux issue!!!
    try to "cp super_large_files_folder folder_to_copy_to" , open htop and you can see the rising of the cache.

    If that happened to you and you want to fix your system (since cache will not be emptied after error) open
    a shell (linux) and type:

    echo 3 | sudo tee /proc/sys/vm/drop_caches
    """
    copy_tree(source_dir, destination_dir)


def move_dir(source_dir, destination_dir):
    shutil.move(source_dir, destination_dir)
