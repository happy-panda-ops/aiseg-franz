import cv2
import logging
import math
import numpy as np
import os
import random
import torch
import warnings

from torch.utils.data import DistributedSampler
from torch.utils.data.dataloader import DataLoader

from .data_sets import Dataset2D
from .data_sets import Dataset3D
from .. import constants
from ..file_handling.read import read_csv_to_list
from ..file_handling.utils import get_sub_folders, get_files
from ..file_handling.write import save_csv
from ..multicore_utils import slice_multicore_parts, multiprocess_function, get_max_process_count
from ..utils import get_memory, get_max_threads


class NotEnoughTrainingDataError(Exception):
    def __init__(self, *args, **kwargs):
        super(NotEnoughTrainingDataError, self).__init__(*args)


def multicore_get_files(folders, process_nr):
    files = []
    for i, folder in enumerate(folders):
        if process_nr == 0:
            logging.info(f'Process {process_nr}: {i + 1}/{len(folders)}')
        files += get_files(folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    if process_nr == 0:
        logging.info('Waiting for other processes to finish.')
    return files


def multicore_std_and_mean_adder(files, channels, process_nr):
    if channels == 1:
        mean = 0
        std = 0

        for i, file in enumerate(files):
            if process_nr == 0:
                logging.info(f'Process {process_nr}: {i + 1}/{len(files)}')
            img = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
            mean += np.mean(img)
            std += np.std(img)
    else:
        mean = [0, 0, 0]
        std = [0, 0, 0]

        for i, file in enumerate(files):
            if process_nr == 0:
                logging.info(f'Process {process_nr}: {i + 1}/{len(files)}')
            img = cv2.imread(file, cv2.IMREAD_COLOR)
            mean[0] += np.mean(img[:, :, 2])
            mean[1] += np.mean(img[:, :, 1])
            mean[2] += np.mean(img[:, :, 0])

            std[0] += np.std(img[:, :, 2])
            std[1] += np.std(img[:, :, 1])
            std[2] += np.std(img[:, :, 0])
    if process_nr == 0:
        logging.info('Finished, waiting for other processes to finish.')
    return mean, std, len(files)


def get_dataset_mean_and_std(dataset_name, input_size, channels, checkpoint=None, logger=None):
    """
    Calculates the preprocessed training images data and calculates the mean and std.
    """
    if checkpoint is not None:
        try:
            mean = checkpoint['dataset_mean']
            std = checkpoint['dataset_std']
            return mean, std
        except KeyError:
            pass

    dataset_folder = os.path.join(constants.DATASET_FOLDER, dataset_name, f'in_size_{input_size}')
    mean_and_std_file = os.path.join(dataset_folder, 'dataset_mean_and_std.csv')
    if os.path.isfile(mean_and_std_file):
        mean_and_std = read_csv_to_list(mean_and_std_file)
        mean = mean_and_std[0]
        std = mean_and_std[1]
        if len(mean) == channels:
            return mean, std

    images_folder = os.path.join(dataset_folder, 'images')

    folders_to_check = get_sub_folders(images_folder)

    if folders_to_check:
        folders_map = slice_multicore_parts(folders_to_check)
        process_nr_map = list(range(len(folders_map)))

        logging.info('Loading data folders')
        results = multiprocess_function(multicore_get_files, [folders_map, process_nr_map], logger=logger)

        files = []
        for result in results:
            files += result
    else:
        files = get_files(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

    files_map = slice_multicore_parts(files)
    channels_map = len(files) * [channels]
    process_nr_map = list(range(len(files)))

    logging.info(f'Calculating std and mean of dataset.')
    mean_std_file_count = multiprocess_function(
        multicore_std_and_mean_adder, [files_map, channels_map, process_nr_map], logger
    )

    if channels == 1:
        mean = 0
        std = 0
        file_count = 0

        for result in mean_std_file_count:
            mean += result[0]
            std += result[1]
            file_count += result[2]

        mean = (mean / file_count) / 255
        std = (std / file_count) / 255

        save_csv(
            mean_and_std_file, [[mean], [std]], f'mean (line 1) and std (line 2) of dataset: {dataset_name}'
        )
        mean = [mean]
        std = [std]

    else:
        # rgb!
        mean = [0, 0, 0]
        std = [0, 0, 0]
        file_count = 0

        for result in mean_std_file_count:
            mean[0] += result[0][0]
            mean[1] += result[0][1]
            mean[2] += result[0][2]

            std[0] += result[1][0]
            std[1] += result[1][1]
            std[2] += result[1][2]

            file_count += result[2]

        for i in range(3):
            mean[i] = (mean[i] / file_count) / 255
            std[i] = (std[i] / file_count) / 255

        save_csv(
            mean_and_std_file, [mean, std], f'mean (line 1) and std (line 2) of dataset: {dataset_name}'
        )

    return mean, std


def slice_sequence_or_files(sequence_or_files, training_percentage=None):
    """
    Splits up 80% training, 30% valid

    If uneven, test sequence will have one element less than valid
    """
    if training_percentage is None:
        training_percentage = 80

    train_size = int(round(training_percentage / 100, 2) * len(sequence_or_files))

    train_sequence = sequence_or_files[:train_size]
    valid_sequence = sequence_or_files[train_size:]

    if not train_sequence or not valid_sequence:
        raise NotEnoughTrainingDataError('Not enough training data to properly create train and valid datasets')

    return train_sequence, valid_sequence


def slice_data_to_sub_volumes_or_sequences(sequences, depth):
    """
    Creates as many volumes as possible out of the sequence

    Args:
        sequences: list of sub lists where each sub list contains the paths to slices of one volume/sequence
        depth: depth of the volumes

    Returns: list of sub list where each sub list represents one volume

    """
    volumes = []
    target_depth = depth  # just for convenience -> better naming

    for vol_sequence in sequences:
        if len(vol_sequence) / depth < 1:
            folder_name = os.path.basename(os.path.dirname(vol_sequence[0]))
            raise NotEnoughTrainingDataError(f'Input volume {folder_name} is too small for depth: {depth}')

        volume_depth = len(vol_sequence)
        total_volumes = math.ceil(len(vol_sequence) / target_depth)
        if total_volumes > 1:
            offset_d_per_volume = (total_volumes * target_depth - volume_depth) // (total_volumes - 1)
        else:
            offset_d_per_volume = 0

        d_coordinates = [i * (depth - offset_d_per_volume) for i in range(total_volumes)]
        d_coordinates[-1] = volume_depth - target_depth

        for start in d_coordinates:
            end = start + target_depth
            sub_volume = vol_sequence[start:end]
            volumes.append(sub_volume)

    return volumes


def split_volume_sequences(volume_sequences, percentage):
    """
    Splits list of volumes into a two parts by a given percentage.

    Args:
        volume_sequences: list of sub list where each sub list represents a single volume of a fixed depth
        percentage: where to split

    Returns: first the x percent of volumes, second the remaining volumes
    """

    total = len(volume_sequences)
    sequence_size = math.ceil(percentage / 100 * total)
    extracted_sequences = []
    current_size = 0
    idx = 0
    # realign training data
    while current_size < sequence_size:
        current_size += 1
        extracted_sequences.append(volume_sequences[idx])
        idx += 1

    # remove already added sequences
    for i in range(idx):
        volume_sequences.pop(0)

    # extracted and remaining sequences
    return extracted_sequences, volume_sequences


def slice_sub_volumes_to_train_data(sequences, train_data_percentage=80):
    input_train_volumes, input_valid_volumes = split_volume_sequences(sequences, train_data_percentage)

    try:
        if not input_valid_volumes:
            input_valid_volumes = [input_train_volumes[-1]]
            input_train_volumes.pop(-1)
        if not input_train_volumes[0]:
            raise IndexError

    except IndexError as e:
        raise NotEnoughTrainingDataError(
            'Too less data! Could not split training data into train, valid and test sequences.'
        ) from e
    return input_train_volumes, input_valid_volumes


def create_train_valid_sequences_or_volumes(inputs,
                                            targets,
                                            is_3d,
                                            depth_or_sequence_size=None,
                                            shuffle_data_split=False,
                                            train_data_percentage=80,
                                            ):
    """
    Args:
        inputs: list of sub lists where each sub list represents one volume/time sequence
        targets: list of sub lists where each sub list represents one volume/time sequence
        is_3d: if the sequences should contain 3 dimensional data or 2D --> example a 3d block or a sequence for rnn's
        depth_or_sequence_size: depth or rnn sequence size
        shuffle_data_split: if True, the initial split of the file paths into training and valid data is randomized
        train_data_percentage: defaults to 80% train data, 30% valid data if no validation data is passed
    """

    image_train_sequences = []
    image_valid_sequences = []

    mask_train_sequences = []
    mask_valid_sequences = []

    if shuffle_data_split:
        combined = list(zip(inputs, targets))
        random.shuffle(combined)
        inputs = [element[0] for element in combined]
        targets = [element[1] for element in combined]

    if is_3d:
        image_volumes = slice_data_to_sub_volumes_or_sequences(inputs, depth_or_sequence_size)
        mask_volumes = slice_data_to_sub_volumes_or_sequences(targets, depth_or_sequence_size)

        image_train_sequences, image_valid_sequences = slice_sub_volumes_to_train_data(
            image_volumes, train_data_percentage
        )
        mask_train_sequences, mask_valid_sequences = slice_sub_volumes_to_train_data(
            mask_volumes, train_data_percentage
        )
    else:
        # attention: sequence represents 3d data. Used by RNN's
        for image_sequence, mask_sequence in zip(inputs, targets):
            image_train, image_valid = slice_sequence_or_files(image_sequence, train_data_percentage)
            mask_train, mask_valid = slice_sequence_or_files(mask_sequence, train_data_percentage)

            image_train_sequences.append(image_train)
            image_valid_sequences.append(image_valid)

            mask_train_sequences.append(mask_train)
            mask_valid_sequences.append(mask_valid)

    return image_train_sequences, mask_train_sequences, image_valid_sequences, mask_valid_sequences


def seed_worker_reproducible(worker_id):
    """
    This ensures that each worker has a different "predictable" randomness so to say if you pass reproduce=True
    """
    worker_seed = constants.GLOBAL_SEED * (worker_id + 1)
    while worker_seed >= 2**32:
        worker_seed -= 2**32
    assert worker_seed >= 0
    assert worker_seed < 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


def get_hardware_aware_num_workers(input_batch_shape, online_aug_active, prefetch_factor, total_batches, num_gpus):
    """
    This function calculates the optimal num workers based on your hardware.
    Goal are (number physical cores - 1) workers.

    When to use multiple workers:
        - Your batch contains thousands of files to be loaded
        - Your batch needs to be preprocessed (e.g. augmented)

    Otherwise, using multiple workers might slow down the training!
    This is due to the fact, that each worker starts a new thread which takes a short time and needs to send
    the loaded batch back to the main thread.

    Args:
        input_batch_shape (tuple): shape of one batch, used to approximate the needed ram
        online_aug_active (bool): True if online augmentation is used, false if only thousands of files are loaded
        prefetch_factor (int): prefetch factor of the batches to load. Needed for ram approximation
        total_batches (int): total number of training batches
        num_gpus (int): number of gpus used in parallel
    """

    usable_threads, total_cpu_cores = get_max_process_count(logical=True, max_ram_in_gb=0.01)
    # data loading is allowed to use threads - main processes however should use a physical core to prevent unwanted
    # issues
    max_workers = usable_threads - num_gpus * 2 - 2  # each gpu has its own process, main thread needs to run as well

    if max_workers <= 1:
        if max_workers < 0:
            max_workers = 0
        return max_workers

    if online_aug_active:
        # calculate ram usage of all workers and reduce if not enough ram is available
        memory_used, memory_total, memory_unit = get_memory(device='cpu')  # returns MiB
        memory_avail = memory_total - memory_used

        byte_per_batch = 1
        for element in input_batch_shape:
            if element == 0 or element == 1:
                continue
            byte_per_batch = byte_per_batch * element

        mib_per_batch = byte_per_batch / (2 ** 20)
        mib_per_worker = mib_per_batch * prefetch_factor

        if memory_avail // mib_per_worker < max_workers:
            max_workers = int(memory_avail // mib_per_worker)

    # check that enough train data is available to satisfy all workers
    while max_workers * prefetch_factor > total_batches:
        max_workers = max_workers - 1

    # if multiple gpus are in use, num_workers refers to workers per gpu - reduce accordingly
    if num_gpus > 1:
        max_workers = max_workers // num_gpus

    if max_workers > constants.MAX_NUM_WORKERS:
        max_workers = constants.MAX_NUM_WORKERS

    return max_workers


def calculate_num_workers(architecture,
                          batch_size,
                          channels,
                          input_size,
                          input_depth,
                          dataset,
                          prefetch_factor,
                          onl_aug_active,
                          num_gpus):
    """Helper for: get_num_workers_and_prefetch. This is not intended to be used anywhere else."""
    if architecture in constants.TWO_D_NETWORKS:
        input_batch_shape = (batch_size, channels, input_size, input_size)
    else:
        input_batch_shape = (
            batch_size, channels, input_depth, input_size, input_size
        )
        if architecture in constants.STATEFUL_RECURRENT_UNETS + constants.STATELESS_RECURRENT_UNETS:
            raise NotImplementedError('RNNs are currently not supported')

    total_batches = len(dataset) // batch_size

    num_workers = get_hardware_aware_num_workers(
        input_batch_shape=input_batch_shape,
        online_aug_active=onl_aug_active,
        prefetch_factor=prefetch_factor,
        total_batches=total_batches,
        num_gpus=num_gpus,
    )
    return num_workers


def get_num_workers_and_prefetch(online_aug,
                                 architecture,
                                 batch_size,
                                 channels,
                                 input_size,
                                 input_depth,
                                 train_dataset,
                                 valid_dataset,
                                 is_inference=False,
                                 num_gpus=1,
                                 max_workers=None,
                                 ):
    """
    Calculates the number of workers used for the training dataset and validation dataset based on the batch size and
    input dimensions or if online augmentation is active.
    """
    prefetch_factor = 2  # pytorch default, used with two workers is fastest without online aug
    valid_prefetch_factor = 2

    num_workers = calculate_num_workers(
        architecture,
        batch_size,
        channels,
        input_size,
        input_depth,
        train_dataset,
        prefetch_factor,
        online_aug,
        num_gpus
    )
    num_validation_workers = calculate_num_workers(
        architecture,
        batch_size,
        channels,
        input_size,
        input_depth,
        valid_dataset,
        valid_prefetch_factor,
        False,
        num_gpus
    )

    if online_aug and not is_inference:
        # try to already preprocess more data
        prefetch_factor = 4
        if num_workers * prefetch_factor > len(train_dataset) // batch_size:
            # keep prefetch as it is
            prefetch_factor = 2

    else:
        # using 2 workers is beneficial for larger batch sizes, 3 is slower, 4 is ok as well but 2 was the fastest!
        # 1 is also a little faster but prefetch has to be higher
        # setting 2 workers has to be validated!
        if is_inference:
            if num_workers > 4:
                num_workers = 4
            elif num_workers == 1 and 4 <= len(train_dataset) // batch_size:
                prefetch_factor = 4
        else:
            # following values have been beneficial if no augmentation was used
            if architecture in constants.TWO_D_NETWORKS and num_workers > 2:
                num_workers = 2
            elif architecture in constants.THREE_D_NETWORKS:
                if num_workers > 4:
                    num_workers = 4
                if num_workers <= 4 and num_workers * 4 <= len(train_dataset) // batch_size:
                    prefetch_factor = 4
            elif 0 < num_workers <= 2 and num_workers * 4 <= len(train_dataset) // batch_size:
                prefetch_factor = 4

    if num_validation_workers > 2:
        num_validation_workers = 2
    elif num_validation_workers == 1 and 4 <= len(valid_dataset) // batch_size:
        valid_prefetch_factor = 4

    if max_workers is not None:
        if num_workers > max_workers:
            num_workers = max_workers
        if num_validation_workers > max_workers:
            num_validation_workers = max_workers

    return num_workers, num_validation_workers, prefetch_factor, valid_prefetch_factor


def multi_load_train_data_to_ram(inputs, targets, is_2d, channels, log_info, process_nr):
    out_inputs = []
    out_targets = []
    if is_2d:
        for i, (in_path, target_path) in enumerate(zip(inputs, targets)):
            if process_nr == 0 and log_info:
                logging.info(f'Process {process_nr}: {i+1}/{len(inputs)}')
            if channels == 1:
                img = cv2.imread(in_path, cv2.IMREAD_GRAYSCALE)
            else:
                img = cv2.imread(in_path, cv2.IMREAD_COLOR)
            mask = cv2.imread(target_path, cv2.IMREAD_GRAYSCALE)
            out_inputs.append(img)
            out_targets.append(mask)
    else:
        for i, (input_files, target_files) in enumerate(zip(inputs, targets)):
            if process_nr == 0 and log_info:
                logging.info(f'Process {process_nr}: {i+1}/{len(inputs)}')
            input_volume = []
            if channels == 1:
                for image in input_files:
                    input_volume.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
            else:
                for image in input_files:
                    input_volume.append(cv2.imread(image, cv2.IMREAD_COLOR))

            target_volume = []
            for mask in target_files:
                target_volume.append(cv2.imread(mask, cv2.IMREAD_GRAYSCALE))

            input_volume = np.array(input_volume)
            target_volume = np.array(target_volume)
            out_inputs.append(input_volume)
            out_targets.append(target_volume)
    if process_nr == 0 and log_info:
        logging.info(f'Waiting for other processes to finish.')
    return out_inputs, out_targets, process_nr


def load_images_to_ram(inputs, targets, is_2d, channels, logger, num_gpus, log_info):
    max_threads = get_max_threads(logical=True, logging_active=False)
    max_threads = max_threads // num_gpus
    if max_threads < 1:
        max_threads = 1

    if max_threads > 1:
        inputs_map = slice_multicore_parts(inputs, total_maps=max_threads)
        targets_map = slice_multicore_parts(targets, total_maps=max_threads)
        is_2d_map = len(inputs_map) * [is_2d]
        channels_map = len(inputs_map) * [channels]
        log_info_map = len(inputs_map) * [log_info]
        process_nr_map = list(range(len(inputs_map)))

        results = multiprocess_function(
            multi_load_train_data_to_ram,
            [inputs_map, targets_map, is_2d_map, channels_map, log_info_map, process_nr_map],
            logger=logger,
        )
        sorted_results = sorted(results, key=lambda x: x[2])

        out_inputs = []
        out_targets = []
        for result in sorted_results:
            out_inputs += result[0]
            out_targets += result[1]
    else:
        out_inputs, out_targets, _ = multi_load_train_data_to_ram(inputs, targets, is_2d, channels, log_info, 0)
    return out_inputs, out_targets


def set_up_loaders(image_paths,
                   mask_paths,
                   architecture,
                   channels,
                   classes,
                   batch_size,
                   reproduce,
                   online_aug,
                   input_size,
                   input_depth=None,
                   test_case=False,
                   valid_images_paths=None,
                   valid_masks_paths=None,
                   dataset_mean=None,
                   dataset_std=None,
                   train_data_percentage=80,
                   max_workers=None,
                   multi_gpu_training=False,
                   drop_last=False,
                   num_gpus=None,
                   log_info=True,
                   to_ram=True,
                   logger=None,
                   ):
    """
    Creates the train, validation and test loader

    Args:
        image_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        mask_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        architecture:
        channels:
        classes:
        batch_size:
        reproduce:
        online_aug:
        input_size:
        input_depth:
        test_case: True at Unit testing
        valid_images_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        valid_masks_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        dataset_mean: mean of the actual dataset. If None, a default mean will be used
        dataset_std: standard deviation of the dataset. If None, a default std will be used
        train_data_percentage: if no validation set is given, this is the split ratio into train and valid
        max_workers: number of parallel workers. 0 is always slower, since vram usage suddenly drops with
            num_workers > 0. Defaults are best but leave the possibility to change.
        multi_gpu_training:
        drop_last:
        num_gpus: needed to calculate correct worker count
        log_info: activates or deactivates the logging
        to_ram: if True, this loads all batches to the ram
        logger: needed if loading to ram is activated

    Returns: train_loader, valid_loader

    """
    if num_gpus is None:
        num_gpus = 1
    if valid_masks_paths is None:
        valid_masks_paths = []
    if valid_images_paths is None:
        valid_images_paths = []

    if architecture in constants.TWO_D_NETWORKS:

        if valid_images_paths:
            train_images = image_paths
            train_masks = mask_paths
            valid_images = valid_images_paths
            valid_masks = valid_masks_paths
        else:
            if not test_case:
                # shuffle data split
                combined = list(zip(image_paths, mask_paths))
                random.shuffle(combined)
                image_paths = [element[0] for element in combined]
                mask_paths = [element[1] for element in combined]

            train_images, valid_images = slice_sequence_or_files(image_paths, training_percentage=train_data_percentage)
            train_masks, valid_masks = slice_sequence_or_files(mask_paths, training_percentage=train_data_percentage)

        if to_ram:
            if log_info:
                logging.info('Loading training data to ram.')
            train_images, train_masks = load_images_to_ram(
                train_images, train_masks, True, channels, logger, num_gpus, log_info
            )
            if log_info:
                logging.info('Loading validation data to ram.')
            valid_images, valid_masks = load_images_to_ram(
                valid_images, valid_masks, True, channels, logger, num_gpus, log_info
            )
            if log_info:
                logging.info('Data loaded.')

        train_dataset = Dataset2D(
            train_images,
            train_masks,
            channels,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )
        valid_dataset = Dataset2D(
            valid_images,
            valid_masks,
            channels,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )
    else:
        if valid_images_paths:
            train_inputs = slice_data_to_sub_volumes_or_sequences(image_paths, input_depth)
            train_targets = slice_data_to_sub_volumes_or_sequences(mask_paths, input_depth)
            valid_inputs = slice_data_to_sub_volumes_or_sequences(valid_images_paths, input_depth)
            valid_targets = slice_data_to_sub_volumes_or_sequences(valid_masks_paths, input_depth)
        else:
            train_inputs, train_targets, valid_inputs, valid_targets = create_train_valid_sequences_or_volumes(
                image_paths,
                mask_paths,
                is_3d=True,
                depth_or_sequence_size=input_depth,
                train_data_percentage=train_data_percentage,
                shuffle_data_split=False if test_case else True,
            )

        if to_ram:
            if log_info:
                logging.info('Loading training data to ram.')
            train_inputs, train_targets = load_images_to_ram(
                train_inputs, train_targets, False, channels, logger, num_gpus, log_info
            )
            if log_info:
                logging.info('Loading validation data to ram.')
            valid_inputs, valid_targets = load_images_to_ram(
                valid_inputs, valid_targets, False, channels, logger, num_gpus, log_info
            )
            if log_info:
                logging.info('Data loaded.')

        train_dataset = Dataset3D(
            train_inputs,
            train_targets,
            classes=classes,
            channels=channels,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )
        valid_dataset = Dataset3D(
            valid_inputs,
            valid_targets,
            classes=classes,
            channels=channels,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )

        if architecture not in constants.THREE_D_NETWORKS:
            warnings.warn('Sequence training setup is currently not tested. If something fails, create an issue.')
            # Loading sequences resulting in:
            # inputs: [B, F, C, H, W] ... F: Frames instead of depth
            # targets: [B, F, H, W]
            # Loading volumes results in:
            # inputs: [B, C, D, H, W]
            # targets: [B, F, H, W]

    if reproduce:
        generator = torch.Generator()
        generator.manual_seed(constants.GLOBAL_SEED)
        worker_init_fn = seed_worker_reproducible
    else:
        generator = None
        worker_init_fn = None

    num_workers, num_validation_workers, prefetch_factor, valid_prefetch_factor = get_num_workers_and_prefetch(
        online_aug=online_aug,
        architecture=architecture,
        batch_size=batch_size,
        channels=channels,
        input_size=input_size,
        input_depth=input_depth,
        train_dataset=train_dataset,
        valid_dataset=valid_dataset,
        num_gpus=num_gpus,
        max_workers=max_workers,
    )

    if num_workers == 0:
        # dataloader class only allows prefetch 2 when no workers are in use
        prefetch_factor = 2
        valid_prefetch_factor = 2

    if log_info:
        logging.info(f'Using {num_workers} parallel workers during training (prefetch: {prefetch_factor}).')
        logging.info(f'Using {num_validation_workers} parallel workers during validation '
                     f'(prefetch: {valid_prefetch_factor}).')

    train_loader = DataLoader(
        dataset=train_dataset,
        batch_size=batch_size,
        shuffle=False if multi_gpu_training else True,
        num_workers=num_workers,
        drop_last=drop_last,
        worker_init_fn=worker_init_fn,
        generator=generator,
        prefetch_factor=prefetch_factor,
        sampler=DistributedSampler(train_dataset) if multi_gpu_training else None,
    )
    valid_loader = DataLoader(
        dataset=valid_dataset,
        batch_size=batch_size,
        shuffle=False if multi_gpu_training else True,
        drop_last=drop_last,
        generator=generator,
        worker_init_fn=worker_init_fn,
        num_workers=num_validation_workers,
        prefetch_factor=valid_prefetch_factor,
        sampler=DistributedSampler(valid_dataset) if multi_gpu_training else None,
    )
    return train_loader, valid_loader
