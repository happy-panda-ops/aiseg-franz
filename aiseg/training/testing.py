import cv2
import logging
import numpy as np
import os
import sys

# start fix:
# during testing view: with spawn as start method, django cannot set up correctly. Only happens here and
# during manual testing?
import torch

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(os.path.dirname(current_dir))
sys.path.extend([module_dir])

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

import django

django.setup()
# end fix

from concurrent.futures.process import BrokenProcessPool

from aiseg import constants
from aiseg.file_handling.read import load_volume_from_folder
from aiseg.file_handling.write import save_csv
from aiseg.inference.predict import predict
from aiseg.dl_models.loading import load_model_from_checkpoint
from aiseg.inference.utils import create_decoding_array
from aiseg.multicore_utils import slice_multicore_parts, multiprocess_function, get_max_process_count
from aiseg.train_data_preprocessing.image_manipulation import get_mapped_label_to_color_or_gray, \
    convert_color_masks_to_label_masks, multiprocess_enlarge_images_and_masks
from aiseg.training.metrics import MetricsCalculator, MetricStorage
from aiseg.temporary_parameters.parameters import FixedParameters
from aiseg.file_handling.utils import get_files, create_folder, \
    folder_contains_sub_folders, get_sub_folders, remove_dir
from aiseg.utils import get_max_threads, create_3d_iteration_map


def mask_gray_0_to_n_classes(mask, classes):
    unique = np.unique(mask)
    unique = list(unique)
    if len(unique) > classes:
        raise RuntimeError(
            f'mask range conversion from 0 to 255 into 0 to n-1 classes failed. Mask contains {len(unique)} '
            f'different classes, expected {classes}.'
        )

    expected_gray_value_per_class = create_decoding_array(classes)
    for class_id, value in enumerate(expected_gray_value_per_class):
        mask[mask == value] = class_id
    return mask


def multi_calculate_testing_metrics_2d(predicted_masks, target_masks, classes, process_nr):
    """
    Calculate metrics for each image
    """
    metrics_storage = MetricStorage(classes)

    if process_nr == 0:
        logging.info(f'Starting metric calculation. (Process {process_nr + 1})')

    for i, (prediction, target) in enumerate(zip(predicted_masks, target_masks)):
        if process_nr == 0:
            logging.info(f'{i + 1}/{len(predicted_masks)} Images')
        predicted_mask = cv2.imread(prediction, cv2.IMREAD_GRAYSCALE)
        target_mask = cv2.imread(target, cv2.IMREAD_GRAYSCALE)

        predicted_mask = mask_gray_0_to_n_classes(predicted_mask, classes)
        # target should already be 0 to n_classes

        calculator = MetricsCalculator(predicted_mask, target_mask, classes)
        calculator.calculate()
        metrics_storage.store_calculator(calculator)

    if not metrics_storage.all_pixels_list:
        return []

    if process_nr == 0:
        logging.info('Finished, waiting for other processes.')

    return metrics_storage


def calculate_testing_metrics_2d(predicted_masks, target_masks, classes, logger):
    storages = []
    threads, _ = get_max_process_count(False, constants.TESTING_RAM_RESERVATION)
    done = False
    while not done:
        try:
            if threads > 1:
                predicted_masks_map = slice_multicore_parts(predicted_masks, total_maps=threads)
                target_masks_map = slice_multicore_parts(target_masks, total_maps=threads)
                class_map = len(predicted_masks_map) * [classes]
                process_nr_map = list(range(len(class_map)))
                storages = multiprocess_function(
                    multi_calculate_testing_metrics_2d,
                    [predicted_masks_map, target_masks_map, class_map, process_nr_map],
                    logger
                )
            else:
                storages = [multi_calculate_testing_metrics_2d(predicted_masks, target_masks, classes, 0)]
            done = True
        except BrokenProcessPool as e:
            logging.info(
                f'Failed to use multiprocessing, most likely: too less RAM available to split into multiple processes.'
            )
            logging.info(f'Decreasing process count from {threads} to {threads - 1}')
            threads = threads - 1

    return storages


def multi_calculate_testing_metrics_3d(predicted_folders, target_masks_folders, classes, process_nr):
    metrics_storage = MetricStorage(classes)

    if process_nr == 0:
        logging.info(f'Starting metric calculation. (Process {process_nr + 1})')

    for i, (predicted_masks_folder, target_masks_folder) in enumerate(zip(predicted_folders, target_masks_folders)):
        if process_nr == 0:
            logging.info(
                f'Process {process_nr}: {i + 1}/{len(predicted_folders)}'
            )

        predicted_volume = load_volume_from_folder(predicted_masks_folder, multiprocessing=False)
        predicted_volume = mask_gray_0_to_n_classes(predicted_volume, classes)
        target_volume = load_volume_from_folder(target_masks_folder, multiprocessing=False)

        calculator = MetricsCalculator(predicted_volume, target_volume, classes)
        calculator.calculate()
        metrics_storage.store_calculator(calculator)

    if not metrics_storage.all_pixels_list:
        return []

    if process_nr == 0:
        logging.info('Finished, waiting for other processes.')

    return metrics_storage


def calculate_testing_metrics_3d(predicted_folders, target_folders, classes, logger):
    metrics_storages = []
    threads, _ = get_max_process_count(False, constants.TESTING_RAM_RESERVATION)
    done = False
    while not done:
        try:
            if threads > 1:
                predicted_folders_map = slice_multicore_parts(predicted_folders, total_maps=threads)
                target_folders_map = slice_multicore_parts(target_folders, total_maps=threads)
                classes_map = len(predicted_folders_map) * [classes]
                process_nr_map = list(range(len(target_folders_map)))
                metrics_storages = multiprocess_function(
                    multi_calculate_testing_metrics_3d,
                    [predicted_folders_map, target_folders_map, classes_map, process_nr_map],
                    logger,
                )
            else:
                metrics_storages = [multi_calculate_testing_metrics_3d(predicted_folders, target_folders, classes, 0)]
            done = True
        except BrokenProcessPool as e:
            logging.info(
                f'Failed to use multiprocessing, most likely: too less RAM available to split into multiple processes.'
            )
            logging.info(f'Decreasing process count from {threads} to {threads - 1}')
            threads = threads - 1

    return metrics_storages


def show_and_get_results(storages, predicted_folders, classes, clean_up=True):
    if not storages:
        logging.info('Testing data is not well suited!')
        return None

    storage = MetricStorage(classes)

    if len(storages) > 1:
        logging.info('Combining results of whole tested data.')
    for metric_storage in storages:
        storage.combine_storages(metric_storage)
    storage.calculate_result()

    if clean_up:
        logging.info('Removing predictions')
        for out_folder in predicted_folders:
            remove_dir(out_folder)

    storage.show_result()
    return storage


def get_in_size_reduction_list(is_3d, input_size, input_depth):
    if is_3d:
        reduction_list = create_3d_iteration_map()
        indexes_to_pop = []
        for i, (size, depth) in enumerate(reduction_list):
            if size > input_size or depth > input_depth:
                indexes_to_pop.append(i)
        for idx in reversed(indexes_to_pop):
            reduction_list.pop(idx)
    else:
        input_sizes = [choice[0] for choice in constants.IN_SIZE_CHOICES_2D if choice[0] <= input_size]
        input_sizes = list(reversed(input_sizes))
        reduction_list = list(zip(input_sizes, len(input_sizes) * [input_depth]))
    return reduction_list


def calculate_test(architecture,
                   batch_size,
                   input_size,
                   input_depth,
                   device,
                   classes,
                   model_path,
                   images_dir_to_segment,
                   ground_truth_folder,
                   logger,
                   overlap=None,
                   ):
    if overlap is None:
        overlap = 50
    if architecture in constants.TWO_D_NETWORKS:
        image_folders = [images_dir_to_segment]
    else:
        image_folders = get_sub_folders(images_dir_to_segment, depth=1)

        if not image_folders:
            raise FileNotFoundError(f'No data found for segmentation at {images_dir_to_segment}')

    predicted_folders = []
    for image_folder in image_folders:
        possible_relic_folders = [
            os.path.join(image_folder, 'original'),
            os.path.join(image_folder, 'temp'),
            os.path.join(image_folder, 'segmented'),
            os.path.join(image_folder, 'combined'),
        ]
        for old_folder in possible_relic_folders:
            logging.info(f'Found old folder: {old_folder}!')
            logging.info(f'Removing...')
            remove_dir(old_folder)
        basename = os.path.basename(image_folder)
        files = get_files(image_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        logging.info(f'Folder {basename} has {len(files)} images.')
        try:
            out_folder = predict(model_path=model_path, data_folder=image_folder, batch_size=batch_size,
                                 input_size=input_size, input_depth=input_depth, device=device,
                                 decode_to_color=False, overlap=overlap, binary_logit_threshold=0.5, combine=False,
                                 logger=logger)
            predicted_folders.append(out_folder)
        except RuntimeError as e:
            if 'CUDA out of memory' not in e.args[0]:
                raise RuntimeError(e)
            logging.info(f'Unexpected Error: CUDA out of memory. Checkpoint may was trained on a different machine.')
            logging.info(f'Reducing input parameters.')
            model = load_model_from_checkpoint(model_path, torch.device('cpu'), True)
            is_3d = True if model.__class__.__name__ in constants.THREE_D_NETWORKS else False
            reduction_list = get_in_size_reduction_list(is_3d, input_size, input_depth)
            batch_size = 1

            for in_size, depth in reduction_list:
                torch.cuda.empty_cache()
                # overwrite parameters for following loops
                logging.info(f'Testing h=w: {in_size}, depth: {depth}')
                input_size = in_size
                input_depth = depth
                try:
                    out_folder = predict(model_path=model_path, data_folder=image_folder, batch_size=batch_size,
                                         input_size=input_size, input_depth=input_depth, device=device,
                                         decode_to_color=False, overlap=overlap, binary_logit_threshold=0.5,
                                         combine=False, logger=logger)
                    predicted_folders.append(out_folder)
                    break
                except RuntimeError as e:
                    logging.info(f'Failed! (h=w: {in_size}, depth: {depth})')
                    if 'CUDA out of memory' not in e.args[0]:
                        raise RuntimeError(e)

    logging.info('Calculating metrics..')
    if architecture in constants.TWO_D_NETWORKS:
        predicted_masks = get_files(predicted_folders[0], extension=constants.SUPPORTED_MASK_TYPES)
        target_masks = get_files(ground_truth_folder, extension=constants.SUPPORTED_MASK_TYPES)

        if len(predicted_masks) != len(target_masks):
            raise RuntimeError(
                f'Unexpected Error: there are {len(predicted_masks)} predictions (in {predicted_folders[0]}) but only '
                f'{len(target_masks)} targets (in {ground_truth_folder}).'
            )

        storages = calculate_testing_metrics_2d(predicted_masks, target_masks, classes, logger)

    else:
        gt_folders = get_sub_folders(ground_truth_folder, depth=1)
        if len(gt_folders) != len(predicted_folders):
            raise RuntimeError(
                f'Unexpected Error: there are {len(predicted_folders)} predictions but '
                f'only {len(gt_folders)} targets.'
            )
        storages = calculate_testing_metrics_3d(predicted_folders, gt_folders, classes, logger)

    storages = show_and_get_results(storages, predicted_folders, classes)

    logging.info(f'Removing created data..')
    for i, image_folder in enumerate(image_folders):
        logging.info(f'{i+1}/{len(image_folders)}')
        possible_relic_folders = [
            os.path.join(image_folder, 'original'),
            os.path.join(image_folder, 'temp'),
            os.path.join(image_folder, 'segmented'),
            os.path.join(image_folder, 'combined'),
        ]
        for old_folder in possible_relic_folders:

            remove_dir(old_folder)

    return storages


def convert_test_masks_to_grayscale(mask_paths, label_count, threads=None, logger=None):
    if not mask_paths:
        raise ValueError('Masks are empty')

    if threads is None:
        threads = get_max_threads(logical=False, max_ram_in_gb=constants.TESTING_RAM_RESERVATION)

    base_dir = os.path.dirname(mask_paths[0])
    folder_is_valid_file = os.path.join(base_dir, 'masks_are_valid.txt')
    if os.path.isfile(folder_is_valid_file):
        return

    needs_conversion = False

    for path in mask_paths:
        img = cv2.imread(path, -1)
        if len(img.shape) == 3:
            needs_conversion = True
            break
        else:
            if np.max(np.unique(img)) >= label_count:  # eg.: 0 and 1 -> label count = 2 --> check for equal or greater
                needs_conversion = True
                break

    folder_is_valid_file_header = 'This file points out, that the masks are usable to test a neural network of ' \
                                  'aiseg\n' \
                                  'project by Franz Wagner.'
    if not needs_conversion:
        save_csv(folder_is_valid_file, [], folder_is_valid_file_header)
        return True

    logging.info(f'Performing color/grayscale to label correction for masks. Original data will be kept.')

    mapped_labels_to_color_or_gray = get_mapped_label_to_color_or_gray(mask_paths, label_count, logger=logger)

    # create backup folders for original data before multiprocessing
    for path in mask_paths:
        folder = os.path.dirname(path)
        backup = os.path.join(folder, 'original')
        create_folder(backup)

    logging.info(f'Preparing masks for further usage: converting to grayscale in range: 0 to {label_count - 1}.')

    if threads == 1:
        convert_color_masks_to_label_masks(mask_paths, mapped_labels_to_color_or_gray, process_nr=0)
    else:
        masks_map = slice_multicore_parts(mask_paths, total_maps=threads)
        mapped_labels_map = len(masks_map) * [mapped_labels_to_color_or_gray]
        mapped_process_nr_map = list(range(len(mapped_labels_map)))

        multiprocess_function(
            convert_color_masks_to_label_masks, [masks_map, mapped_labels_map, mapped_process_nr_map], logger
        )

    logging.info(f'Masks of folder: {os.path.dirname(mask_paths[0])} are now valid.')
    save_csv(folder_is_valid_file, [], folder_is_valid_file_header)


def prepare_testing_data(testing_dir_path, classes, is_2d, input_size, channels, logger):
    got_error = False
    if not os.path.isdir(testing_dir_path):
        logging.info(f'There is no such directory: {testing_dir_path}')
        got_error = True

    images_folder = os.path.join(testing_dir_path, 'images')

    if not os.path.isdir(images_folder):
        logging.info(f'There is no images subdirectory: {images_folder}')
        got_error = True

    gt_folder = os.path.join(testing_dir_path, 'masks')
    if not os.path.isdir(gt_folder):
        logging.info(f'There is no masks subdirectory: {gt_folder}')
        got_error = True

    if folder_contains_sub_folders(gt_folder) and not is_2d:
        gt_folders = get_sub_folders(gt_folder, depth=1)
    else:
        gt_folders = [gt_folder]

    for gt_sub_folder in gt_folders:
        if not get_files(gt_sub_folder, extension=constants.SUPPORTED_MASK_TYPES):
            supported_types = ', '.join(constants.SUPPORTED_MASK_TYPES)
            logging.info(
                f'The testing masks folder is empty or contains not supported files: {gt_sub_folder}.\n'
                f'Supported image types are: {supported_types}')
            got_error = True

    if folder_contains_sub_folders(images_folder) and not is_2d:
        images_folders = get_sub_folders(images_folder, depth=1)
    else:
        images_folders = [images_folder]

    for images_sub_folder in images_folders:
        if not get_files(images_sub_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES):
            supported_types = ', '.join(constants.SUPPORTED_INPUT_IMAGE_TYPES)
            logging.info(
                f'The testing images folder is empty or contains not supported files: {images_sub_folder}.\n'
                f'Supported image types are: {supported_types}')
            got_error = True

    if got_error:
        return None, None

    for mask_folder in gt_folders:
        mask_paths = get_files(mask_folder, extension=constants.SUPPORTED_MASK_TYPES)
        convert_test_masks_to_grayscale(mask_paths, label_count=classes, logger=logger)

    for img_folder, mask_folder in zip(images_folders, gt_folders):
        logging.info('Preprocessing images and ground truth for testing.')
        images = get_files(img_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_files(mask_folder, extension=constants.SUPPORTED_MASK_TYPES)

        threads = get_max_threads(False, max_ram_in_gb=constants.TESTING_RAM_RESERVATION)

        images_map = slice_multicore_parts(images, total_maps=threads)
        masks_map = slice_multicore_parts(masks, total_maps=threads)
        create_folder(os.path.join(os.path.dirname(images[0]), 'original'))
        create_folder(os.path.join(os.path.dirname(masks[0]), 'original'))

        load_gray_map = len(masks_map) * [True if channels == 1 else False]
        input_size_map = len(masks_map) * [input_size]
        process_nr_map = list(range(len(input_size_map)))
        logging.info(f'Using {len(process_nr_map)} processes.')
        multiprocess_function(
            multiprocess_enlarge_images_and_masks,
            [images_map, masks_map, load_gray_map, input_size_map, process_nr_map],
            logger
        )

    logging.info(f'Finished preprocessing, checking correct naming..')
    for img_folder, mask_folder in zip(images_folders, gt_folders):
        images = get_files(img_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_files(mask_folder, extension=constants.SUPPORTED_MASK_TYPES)
        for img, mask in zip(images, masks):
            img_name = os.path.basename(img).split('.')[0]
            mask_name = os.path.basename(mask).split('.')[0]
            if img_name != mask_name:
                raise RuntimeError(
                    f'Images and masks are not named correctly! Names need to be the same like:\n'
                    f'image: img_001 | mask: img_001.\n'
                    f'Yours are:\n'
                    f'image: {img_name} | mask: {mask_name}.\n'
                    f'Please rename images or masks accordingly!\n'
                    f'Images dir: {img_folder}, masks accordingly.'
                )
    logging.info('Finished.')

    return images_folder, gt_folder


def run_test_loop(params, checkpoint_path, testing_dir_path, logger):
    """
    This function is able to precisely calculate the testing accuracy - even with different sized images/volumes.
    The final results of multiple different test images or volumes is of course weighted by the size of each image or
    volume.

    Args:
        params: FixedParameters instance. Required params:
        checkpoint_path: checkpoint to test
        testing_dir_path: folder containing the testing data
        logger: needed for multiprocessing

    Returns: storage containing the results

    """
    if not isinstance(params, FixedParameters):
        raise ValueError('params must be a FixedParameters instance!')

    if not os.path.isdir(testing_dir_path):
        logging.info('No testing data, skipping analysis.')
        return

    if testing_dir_path is None:
        testing_dir = os.path.join(constants.DATASET_FOLDER, params.dataset_name, 'test')
    else:
        testing_dir = testing_dir_path

    logging.info(f"Testing checkpoint {os.path.basename(checkpoint_path).split('_')[-1].split('.')[0]}")

    model = load_model_from_checkpoint(checkpoint_path, params.device)
    out_channels = model.out_channels

    params.architecture = model.__class__.__name__
    params.channels = model.in_channels
    params.classes = 2 if out_channels == 1 else out_channels
    is_2d = params.architecture in constants.TWO_D_NETWORKS
    image_folder, gt_folder = prepare_testing_data(
        testing_dir, params.classes, is_2d, params.input_size, params.channels, logger=logger
    )
    if image_folder is None:
        return

    return calculate_test(params.architecture,
                          params.batch_size,
                          params.input_size,
                          params.input_depth,
                          params.device,
                          params.classes,
                          checkpoint_path,
                          image_folder,
                          gt_folder,
                          logger,
                          overlap=params.overlap,)
