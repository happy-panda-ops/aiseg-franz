import logging
import numpy as np
import os
import random
import time
import torch
import torch.distributed as dist

from torch.nn.parallel import DistributedDataParallel as DDP
from torch.optim import lr_scheduler
from decimal import Decimal
from django.utils import timezone

from aiseg import constants
from aiseg.dl_models.backward_compatibility import BACKWARD_COMPATIBLE_NETWORKS
from aiseg.file_handling.utils import get_files, remove_file, create_folder
from aiseg.temporary_parameters.user_information import show_best_five_stats
from aiseg.training.saving import save_history, get_os_model_name
from aiseg.utils import show_memory, get_time_spent_string, get_time_remaining_string, \
    set_up_reproducibility, get_human_time_string_from_seconds


class Trainer:

    def __init__(self, train_preparer, multi_gpu_training=False):
        """

        Args:
            train_preparer:
            multi_gpu_training:
        """

        self.train_preparer = train_preparer

        self.log_file_instance = self.train_preparer.log_file_instance
        self.logger = self.train_preparer.logger

        # User training parameters
        self.classes = self.train_preparer.classes
        self.epochs = self.train_preparer.epochs

        if multi_gpu_training:
            self.device = torch.device(f'cuda:{dist.get_rank()}')
            self.num_gpus = dist.get_world_size()
            self.gpu_id = dist.get_rank()
        else:
            self.device = self.train_preparer.device
            self.num_gpus = None
            self.gpu_id = None

        self.save_every = self.train_preparer.save_every if self.train_preparer.save_every > 0 else self.epochs

        # default parameters or parameters that will be calculated during setup
        self.start_epoch = self.train_preparer.start_epoch
        self.model_analysis = self.train_preparer.model_analysis

        # model, scaler, loss, ...
        self.model = self.train_preparer.model
        if self.gpu_id is not None:
            self.model.to(self.device)

            if self.model.__class__ in BACKWARD_COMPATIBLE_NETWORKS:
                # significantly slower
                find_unused_parameters = True
            else:
                find_unused_parameters = False

            self.model = DDP(self.model, device_ids=[self.gpu_id], find_unused_parameters=find_unused_parameters)

        # setup here - important for multi gpu training and continuation
        if self.train_preparer.lr is not None:
            self.optimizer = self._get_optimizer(**{'lr': self.train_preparer.lr})
        else:
            self.optimizer = self._get_optimizer()

        if self.train_preparer.optimizer_state is not None:
            self.optimizer.load_state_dict(self.train_preparer.optimizer_state)

            # crucial!!
            # issue: using more VRam when loading from checkpoint than starting from scratch:
            # since loading is in external func, there is no problem but the state dict is heavy!
            # https://discuss.pytorch.org/t/gpu-memory-usage-increases-by-90-after-torch-load/9213/9
            # 0/11441 MiB  --> initial
            # 6270/11441 MiB  --> loaded checkpoint
            # 6270/11441 MiB  --> set up optimizer
            # 2060/11441 MiB  --> deleted optimizer_state dict
            del self.train_preparer.optimizer_state
            torch.cuda.empty_cache()

        self.lr_scheduler_name = self.train_preparer.lr_scheduler
        if self.train_preparer.lr_scheduler is not None:
            self.lr_scheduler = self.__get_optimizer_scheduler(self.optimizer)
            if train_preparer.lr_scheduler_state is not None:
                self.lr_scheduler.load_state_dict(self.train_preparer.lr_scheduler_state)

        self.warm_up_epochs = self.train_preparer.lr_warm_up_period
        self.constant_epochs = self.train_preparer.lr_constant_period

        self.loss_func = self.train_preparer.loss_func.to(self.device)

        # deactivating amp: causing loss to get nan in some instances. (BCE with logits Loss)
        # see: https://github.com/pytorch/pytorch/issues/40497#issuecomment-764538341
        # the thread is interesting - Nvidias Apex AMP seems to work though
        if torch.device(self.device).type == 'cpu' or True:
            self.scaler = torch.cuda.amp.GradScaler(enabled=False)
            self.amp_enabled = False
        else:
            # If AMP is supported and continuation: use scaler from trainer_preparer!
            self.scaler = torch.cuda.amp.GradScaler()
            self.amp_enabled = True

        self.profiler = self.train_preparer.profiler
        self.training_started_time = self.train_preparer.training_started_time

        # DATA:
        self.training_loader = self.train_preparer.training_loader
        self.validation_loader = self.train_preparer.validation_loader
        self.fine_tuning_epoch = self.train_preparer.fine_tuning_epoch

    def _get_optimizer(self, **kwargs):
        if self.train_preparer.optimizer_name == constants.OPTIMIZER_NAME_ADAM:
            return torch.optim.Adam(self.model.parameters(), **kwargs)
        elif self.train_preparer.optimizer_name == constants.OPTIMIZER_NAME_ADAMW:
            return torch.optim.AdamW(self.model.parameters(), **kwargs)
        elif self.train_preparer.optimizer_name == constants.OPTIMIZER_NAME_SGD:
            return torch.optim.SGD(self.model.parameters(), **kwargs)
        else:
            raise NotImplementedError(f'Optimizer named {self.train_preparer.optimizer_name} is not supported.')

    def __get_optimizer_scheduler(self, optimizer):
        """Return a learning rate scheduler
        For explanation see: https://www.kaggle.com/code/billiemage/understand-lr-scheduler-with-simple-examples
        """
        if self.train_preparer.lr_scheduler == constants.LR_SCHEDULER_LINEAR:
            warm_up = self.train_preparer.lr_warm_up_period
            constant = self.train_preparer.lr_constant_period
            decay = self.train_preparer.lr_decay_period

            if warm_up > 0:
                m_warm = 1 / (warm_up + 1)  # otherwise last warm up epoch multiplier = 1
            else:
                m_warm = 0

            if decay > 0:
                m_decay = 1 / (decay + 1)  # otherwise last epoch lr = 0 -> useless
            else:
                m_decay = 0

            def lambda_rule(epoch):
                """
                1. the learning rate increases linearly for self.lr_warm_up_period epochs
                2. after 1., the learning rate will be constant for self.lr_constant_period epochs
                3. after 2., the learning rate decays to 0 for self.lr_decay_period epochs
                """
                if epoch < warm_up:
                    # warm up mode
                    x = warm_up - epoch
                    lr_l = 1 - m_warm * x
                elif epoch >= constant + warm_up:
                    # decay mode
                    x = (warm_up + constant + decay) - epoch
                    lr_l = m_decay * x
                    if lr_l < 0:
                        # if user made an error such as: total epochs > warm up + constant + decay
                        # is caught at frontend
                        lr_l = 0
                else:
                    # constant lr mode
                    lr_l = 1
                if constants.ENABLE_AMP and lr_l < 1.e-07:
                    lr_l = 1.e-07
                return lr_l

            scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda_rule)
        elif self.train_preparer.lr_scheduler == constants.LR_SCHEDULER_STEP:
            scheduler = lr_scheduler.StepLR(optimizer, step_size=self.train_preparer.lr_adjust_step_size, gamma=0.5)
        elif self.train_preparer.lr_scheduler == constants.LR_SCHEDULER_COSINE:
            scheduler = lr_scheduler.CosineAnnealingLR(
                optimizer, T_max=self.train_preparer.lr_adjust_step_size, eta_min=constants.MINIMUM_LR
            )
        elif self.train_preparer.lr_scheduler == constants.LR_SCHEDULER_COSINE_ANNEALING_WARM:
            scheduler = lr_scheduler.CosineAnnealingWarmRestarts(
                optimizer, T_0=self.train_preparer.lr_adjust_step_size, T_mult=2, eta_min=constants.MINIMUM_LR
            )
        else:
            raise NotImplementedError(f'Scheduler {self.train_preparer.lr_scheduler} is not supported.')
        return scheduler

    def reseed(self):
        """When using DDP, you need to reseed in each sub process"""
        if self.train_preparer.reproduce and self.train_preparer.multi_gpu_training:
            seed_multiplier = 1 + self.gpu_id
            # No python, torch and numpy cpu state setting - otherwise, all states are the same of process with rank=0.
            # Getting access of other processes states without communication is not easy. Therefore, only cuda states
            # are saved at the checkpoint.
            # When continuing, the cpu states differ, but now using the same seed multiplied by (1 + gpu_id).
            # Ensures reproducible randomness by the loss of continuous random states
            try:
                # different machine may have less gpus or is now running with more gpus on same machine.
                set_up_reproducibility(self.train_preparer.torch_cuda_states[self.gpu_id], self.device, seed_multiplier)
            except IndexError:
                set_up_reproducibility(seed_multiplier)

    def get_targets_and_outputs(self, inputs, targets):
        """
        Used by test loop
        Returns output batch with channels always at out_batch.shape[1]
        """
        inputs = inputs.to(self.device, dtype=torch.float)

        # long for multiclass else we could get too large numbers resulting in nan
        masks_type = torch.float32 if self.classes == 1 else torch.long
        targets = targets.to(self.device, dtype=masks_type)

        with torch.cuda.amp.autocast(enabled=constants.ENABLE_AMP):
            outputs = self.model(inputs)
        return targets, outputs

    def get_targets_outputs_and_loss(self, input_batch, target_batch):
        """
        Used by train and validation loop
        Returns output batch with channels always at out_batch.shape[1]
        """
        input_batch = input_batch.to(self.device, dtype=torch.float)

        # long for multiclass else we could get too large numbers resulting in nan
        masks_type = torch.float32 if self.classes == 2 else torch.long
        target_batch = target_batch.to(self.device, dtype=masks_type)

        with torch.cuda.amp.autocast(enabled=constants.ENABLE_AMP):
            output_batch = self.model(input_batch)

            if self.classes == 2:
                loss = self.loss_func(output_batch.squeeze(dim=1), target_batch)
            else:
                loss = self.loss_func(output_batch, target_batch)

        return target_batch, output_batch, loss

    @staticmethod
    def compute_accuracy(output_logits, targets):
        # output_logits of shape: B, C, ....
        if output_logits.size(1) == 1:
            output_probabilities = torch.sigmoid(output_logits)
            outs = (output_probabilities.squeeze(1) > 0.5).float()
            correct = outs.eq(targets.squeeze()).sum().item()
        else:
            values, predictions = torch.max(output_logits, dim=1)
            correct = predictions.eq(targets).sum().item()

        total = targets.nelement()  # sums up all pixels (batch_size x width x height)
        accuracy = (correct / total) * targets.size(0)

        return accuracy

    def reduce_loss_and_acc(self, loss, acc, dataset_length):
        # loss was multiplied by each batch size --> divide by all training samples
        if self.gpu_id is not None:
            # reduce -> sums all values up on rank 0 to get real loss and acc  -> nccl
            # all_reduce -> sums all values up on all ranks  -> gloo and nccl
            # see: https://pytorch.org/tutorials/intermediate/dist_tuto.html
            # and: https://pytorch.org/docs/stable/distributed.html#

            # tensors to be sharded have to be on gpu
            loss = torch.tensor(loss).to(self.device)
            acc = torch.tensor(acc).to(self.device)

            dist.all_reduce(loss, op=dist.ReduceOp.SUM)  # default op is SUM
            dist.all_reduce(acc, op=dist.ReduceOp.SUM)

            loss = loss.item()
            acc = acc.item()

        acc = acc / dataset_length
        loss = loss / dataset_length
        return loss, acc

    def train_loop(self, epoch):
        self.model.train()

        train_loss = 0.0
        train_acc = 0.0

        if self.gpu_id == 0 or self.gpu_id is None:
            if self.lr_scheduler_name == constants.LR_SCHEDULER_LINEAR:
                if epoch < self.warm_up_epochs:
                    mode = f' (mode: lr warm up)'
                elif epoch >= self.warm_up_epochs + self.constant_epochs:
                    mode = f' (mode: lr decay)'
                else:
                    mode = f' (mode: lr constant)'
            else:
                mode = ''
            logging.info(f'Learning rate epoch {epoch + 1}{mode}: lr={self.optimizer.param_groups[0]["lr"]}')

            logging.info('Training loop:')
        try:
            if self.num_gpus is not None and self.num_gpus > 1:
                self.training_loader.sampler.set_epoch(epoch)
            for i, (input_batch, target_batch) in enumerate(self.training_loader):
                if self.gpu_id == 0 or self.gpu_id is None:
                    logging.info(
                        f'Epoch {epoch + 1}/{self.epochs}, train loop: Batch {i + 1} / {len(self.training_loader)}'
                    )
                if i <= 3 or i % 30 == 0:
                    if self.gpu_id == 0:
                        show_memory(self.device.type, self.num_gpus)
                    elif self.gpu_id is None:
                        show_memory(self.device, self.num_gpus)

                targets, outputs, loss = self.get_targets_outputs_and_loss(input_batch, target_batch)

                self.scaler.scale(loss).backward()
                self.scaler.step(self.optimizer)
                self.scaler.update()
                # Setting gradient to None has a slightly different numerical behavior than setting it to zero
                # see: https://pytorch.org/docs/master/optim.html#torch.optim.Optimizer.zero_grad
                # This will in general have lower memory footprint, and can modestly improve performance. However, it
                # changes certain behaviors. For example: 1. When the user tries to access a gradient and perform manual
                # ops on it, a None attribute or a Tensor full of 0s will behave differently. 2. If the user requests
                # zero_grad(set_to_none=True) followed by a backward pass, .grads are guaranteed to be None for params
                # that did not receive a gradient. 3. torch.optim optimizers have a different behavior if the gradient
                # is 0 or None (in one case it does the step with a gradient of 0 and in the other it skips the step
                # altogether).
                self.optimizer.zero_grad(set_to_none=True)

                # if last batch has fewer elements, last batch must be weighted different. Therefore: loss x batch_size
                train_loss += loss.item() * targets.size(0)
                train_acc += self.compute_accuracy(outputs, targets)

        except (RuntimeError, ValueError) as e:
            non_intuitive_vram_errors = [
                'CUDA out of memory',
                'CUDA error: out of memory',
                'Unable to find a valid cuDNN algorithm to run convolution',
                'cuDNN error: CUDNN_STATUS_NOT_SUPPORTED',
            ]
            for non_intuitive_error in non_intuitive_vram_errors:
                if non_intuitive_error in e.args[0]:
                    raise RuntimeError(f'\n!!! Not enough memory to compute !!!\nError: {e}') from e
            raise RuntimeError(f'Got an error during training:\n{e}') from e

        if self.lr_scheduler is not None:
            self.lr_scheduler.step()
        return self.reduce_loss_and_acc(train_loss, train_acc, len(self.training_loader.dataset))

    def valid_loop(self, epoch):
        valid_loss = 0.0
        valid_acc = 0.0

        if self.gpu_id == 0 or self.gpu_id is None:
            logging.info(f'Validation loop:')
        self.model.eval()
        with torch.no_grad():
            for i, (input_batch, target_batch) in enumerate(self.validation_loader):
                if self.gpu_id is None or self.gpu_id == 0:
                    logging.info(
                        f'Epoch {epoch + 1}/{self.epochs}, valid loop: Batch {i + 1} / {len(self.validation_loader)}'
                    )
                if i <= 3 or i % 30 == 0:
                    if self.gpu_id == 0:
                        show_memory(self.device.type, self.num_gpus)
                    elif self.gpu_id is None:
                        show_memory(self.device, self.num_gpus)

                targets, outputs, loss = self.get_targets_outputs_and_loss(input_batch, target_batch)

                valid_loss += loss.item() * targets.size(0)
                valid_acc += self.compute_accuracy(outputs, targets)

        return self.reduce_loss_and_acc(valid_loss, valid_acc, len(self.validation_loader.dataset))

    def show_stats(self, history, start_time):
        epoch, train_loss, valid_loss, train_acc, valid_acc = history[-1][:5]

        human_time_spent = get_time_spent_string(start_time)
        human_time_remaining = get_time_remaining_string(
            start_time,
            total_count=self.epochs - self.start_epoch,
            current_idx=epoch - 1 - self.start_epoch,
        )
        percent_processed = int(round(epoch / self.epochs * 100))

        logging.info(
            f'Processed: {percent_processed}%, time spent: {human_time_spent}, time remaining: {human_time_remaining}'
        )

        logging.info(f'Training: Loss: {train_loss:.6f}, Accuracy: {train_acc:.6f}%')
        logging.info(f'Validation: Loss: {valid_loss:.6f}, Accuracy: {valid_acc:.6f}%')

    def get_ckp_model_name_on_os(self, history, is_best_loss, is_best_acc):
        file_number = str(history[-1][0]).zfill(len(str(self.epochs)))
        model_name_on_os = get_os_model_name(
            self.train_preparer.dataset_name,
            self.train_preparer.architecture,
            self.train_preparer.backbone
        )

        if is_best_loss and is_best_acc:
            model_name_on_os += '_best_loss_and_acc'
        elif is_best_loss:
            model_name_on_os += '_best_loss'
        elif is_best_acc:
            model_name_on_os += '_best_acc'
        else:
            model_name_on_os += f'_saved_every_{self.save_every}'
        checkpoint_file_name = f'{model_name_on_os}_epoch_{str(file_number)}.pt'
        file_path = os.path.join(self.train_preparer.model_folder, checkpoint_file_name)

        # python path has \\ in it on Windows. However, it is counted correctly as 1 not 2 using len on a str with \\
        if len(os.path.normpath(file_path)) > 255:
            # Windows: https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=registry
            # Ubuntu: https://en.wikipedia.org/wiki/Ext4
            logging.info(f'File path too long, using abbreviation in file path: loss -> l, acc -> a, saved_every -> se')
            file_path = file_path.replace('_loss_', '_l_').replace('_acc_', '_a_').replace('_saved_every_', '_se_')
        return file_path

    def rename_stored_ckp(self, checkpoint_path, history, is_best_loss, is_best_acc):
        """

        Args:
            checkpoint_path: path of a previous checkpoint
            history:
            is_best_loss: status of current checkpoint
            is_best_acc: status of current checkpoint

        Returns:

        """
        basename = os.path.basename(checkpoint_path)
        # for correct naming, the history needs to be cut when using self.get_ckp_model_name_on_os
        epoch = int(os.path.basename(checkpoint_path).split('_epoch_')[1].split('.pt')[0])
        history = history[:epoch]  # epoch is not index -> starts at 1
        if is_best_loss and is_best_acc:
            # is_best_loss and is_best_acc refers to the newest checkpoint, but we want to rename previous checkpoints
            new_path = self.get_ckp_model_name_on_os(history, False, False)
            os.rename(checkpoint_path, new_path)
        elif is_best_acc and not is_best_loss:
            # new ckp has the best acc --> remove "best acc" from previous name
            if '_best_loss_and_acc' in basename or '_best_l_and_a' in basename:
                # rename from "best loss and acc" to only best loss
                new_path = self.get_ckp_model_name_on_os(history, True, False)
                os.rename(checkpoint_path, new_path)
            elif '_best_acc' in basename or '_best_a' in basename:
                # rename from "best acc" to save every
                new_path = self.get_ckp_model_name_on_os(history, False, False)
                os.rename(checkpoint_path, new_path)
        elif is_best_loss and not is_best_acc:
            # new ckp has the best loss --> remove "best loss" from previous name
            if '_best_loss_and_acc' in basename or '_best_l_and_a' in basename:
                # rename from "best loss and acc" to only best loss
                new_path = self.get_ckp_model_name_on_os(history, False, True)
                os.rename(checkpoint_path, new_path)
            elif '_best_loss' in basename or '_best_l' in basename:
                # rename from "best loss" to save every
                new_path = self.get_ckp_model_name_on_os(history, False, False)
                os.rename(checkpoint_path, new_path)

    def save_checkpoint(self, history, is_best_loss, is_best_acc):
        if not isinstance(history, list):
            raise ValueError('history must be a list!')

        if isinstance(self.model, DDP):
            model = self.model.module
        else:
            model = self.model

        python_state = random.getstate()
        numpy_state = np.random.get_state()
        torch_cpu_state = torch.get_rng_state()
        if self.device.type == 'cuda' and not isinstance(self.model, DDP):
            torch_cuda_states = [torch.cuda.get_rng_state(device=self.device)]
        elif isinstance(self.model, DDP):
            torch_cuda_states = []
            for i in range(self.num_gpus):
                torch_cuda_states.append(torch.cuda.get_rng_state(device=torch.device(f'cuda:{i}')))
        else:
            torch_cuda_states = None

        saving_dict = {
            'architecture': self.train_preparer.architecture,
            'total_epochs': self.epochs,
            'current_epoch': history[-1][0],
            'image_channels': self.train_preparer.channels,
            'classes': self.classes,
            'image_input_size': self.train_preparer.input_size,
            'history': history,
            'model_state_dict': model.state_dict(),
            'optimizer_name': self.train_preparer.optimizer_name,
            'optimizer_state_dict': self.optimizer.state_dict(),
            'scaler_state_dict': self.scaler.state_dict(),
            'lr_scheduler': self.lr_scheduler_name,
            'lr_scheduler_state': self.lr_scheduler.state_dict(),
            'lr_adjust_step_size': self.train_preparer.lr_adjust_step_size,
            'lr_warm_up_period': self.train_preparer.lr_warm_up_period,
            'lr_constant_period': self.train_preparer.lr_constant_period,
            'lr_decay_period': self.train_preparer.lr_decay_period,
            'fine_tuning_epoch': self.train_preparer.fine_tuning_epoch,
            'normalization': self.train_preparer.norm,
            'dataset_mean': self.train_preparer.dataset_mean,
            'dataset_std': self.train_preparer.dataset_std,
            'online_aug': self.train_preparer.online_aug,
            'aug_strength': self.train_preparer.aug_strength,
            'aug_start_epoch': self.train_preparer.aug_start_epoch,
            'dataset_name': self.train_preparer.dataset_name,
            'batch_size': self.train_preparer.batch_size,
            'python_state': python_state,
            'numpy_state': numpy_state,
            'torch_cpu_state': torch_cpu_state,
            'torch_cuda_state': torch_cuda_states,
            'training_started_time': timezone.localtime(self.model_analysis.created_datetime),
            'auto_weight': self.train_preparer.auto_weight,
            'weight_per_class': self.train_preparer.weight_per_class,
        }

        if constants.ARCHITECTURES_AND_BACKBONES[self.train_preparer.architecture]:
            saving_dict['backbone'] = self.train_preparer.backbone

        if self.train_preparer.architecture in constants.THREE_D_NETWORKS:
            saving_dict['input_depth'] = self.train_preparer.input_depth

        if self.train_preparer.online_aug:
            saving_dict['online_aug'] = True
            saving_dict['aug_start_epoch'] = self.train_preparer.aug_start_epoch
            saving_dict['online_aug_kwargs'] = self.train_preparer.online_aug_kwargs

        file_path = self.get_ckp_model_name_on_os(history, is_best_loss, is_best_acc)
        best_epoch_data_val_acc = sorted(history, key=lambda x: x[4], reverse=True)[0]
        best_epoch_data_val_loss = sorted(history, key=lambda x: x[2])[0]

        best_val_acc_epoch = best_epoch_data_val_acc[0]
        best_val_loss_epoch = best_epoch_data_val_loss[0]

        # removing previous files to reduce space except for last checkpoint and best checkpoints
        # only remove others if we save a new best model.
        create_folder(self.train_preparer.model_folder)
        checkpoints = get_files(self.train_preparer.model_folder, extension='.pt')
        for c in checkpoints:
            checkpoint_epoch = int(os.path.basename(c).split('_epoch_')[1].split('.pt')[0])
            if checkpoint_epoch % self.save_every == 0:
                # do not remove "save_every" models but rename
                self.rename_stored_ckp(c, history, is_best_loss, is_best_acc)
                continue

            # keep the best validation accuracy and best validation loss epochs
            if checkpoint_epoch == best_val_acc_epoch:
                continue
            if checkpoint_epoch == best_val_loss_epoch:
                continue

            logging.info(f'Removing checkpoint: {c}')
            remove_file(c)

        logging.info(f'Saved checkpoint {file_path}')
        torch.save(saving_dict, file_path)

        self.model_analysis.storage_size = Decimal(round(os.stat(file_path).st_size / 1000 / 1000, 2))
        self.model_analysis.training_loss = Decimal(best_epoch_data_val_loss[1])
        self.model_analysis.validation_loss = Decimal(best_epoch_data_val_loss[2])
        self.model_analysis.training_accuracy = Decimal(best_epoch_data_val_loss[3])
        self.model_analysis.validation_accuracy = Decimal(best_epoch_data_val_loss[4])
        self.model_analysis.save()

    def start_training(self):
        history, header = self.profiler.get_history()

        epoch = 0
        best_loss = 10
        best_val_acc = 0
        start_time = time.time()
        analysis_10_epochs_start_time = 0

        if self.start_epoch >= self.epochs:
            logging.info(f'Already trained the model for {self.epochs} epochs.')
            return

        if self.gpu_id is not None:
            logging.info(f'Rank {self.gpu_id}: Start time: {get_human_time_string_from_seconds(start_time, True)}')
        else:
            logging.info(f'Start time: {get_human_time_string_from_seconds(start_time, True)}')

        for epoch in range(self.start_epoch, self.epochs):
            # start epoch is index based - starts at 0
            # human epoch starts at 1 - readability
            human_epoch = epoch + 1
            if self.gpu_id is not None:
                logging.info(f'Rank {self.gpu_id}: Epoch: {human_epoch}/{self.epochs}')
            else:
                logging.info(f'Epoch: {human_epoch}/{self.epochs}')

            if human_epoch == self.train_preparer.aug_start_epoch:
                self.training_loader.dataset.online_aug_active = True

            if 0 < self.fine_tuning_epoch == epoch and self.training_loader.dataset.aug_pipeline is not None:
                self.training_loader.dataset.online_aug_active = False
                if self.gpu_id == 0 or self.gpu_id is None:
                    logging.info('Reached fine-tuning period. Deactivating online augmentation.')

            if self.gpu_id == 0 or self.gpu_id is None:
                if epoch - self.start_epoch == 2:
                    # first epoch may not be perfectly stable for timing - use stable condition during later epochs
                    # for timing
                    analysis_10_epochs_start_time = time.time()
                if epoch - self.start_epoch == 12:
                    self.model_analysis.training_duration_10_epochs = time.time() - analysis_10_epochs_start_time
                    self.model_analysis.save()

            if self.gpu_id is not None:
                dist.barrier()

            train_loss, train_acc = self.train_loop(epoch)
            valid_loss, valid_acc = self.valid_loop(epoch)

            if self.gpu_id == 0 or self.gpu_id is None:

                self.profiler.update(human_epoch, train_loss, valid_loss, 100 * train_acc, 100 * valid_acc)

                history, header = self.profiler.get_history()

                self.model_analysis.refresh_from_db()
                self.model_analysis.total_epochs = human_epoch
                self.model_analysis.save()
                self.model_analysis.save_history(history, header)

                if (human_epoch % self.save_every == 0 or human_epoch == self.epochs or valid_loss < best_loss or
                        valid_acc > best_val_acc):
                    is_best_acc = False
                    is_best_loss = False
                    if valid_loss < best_loss:
                        logging.info('New best model (validation loss).')
                        best_loss = valid_loss
                        is_best_loss = True

                    if valid_acc > best_val_acc:
                        logging.info('New best model (validation accuracy).')
                        best_val_acc = valid_acc
                        is_best_acc = True

                    self.save_checkpoint(history, is_best_loss, is_best_acc)

                self.show_stats(history, start_time)

            if self.gpu_id is not None:
                dist.barrier()

        end_time = time.time()
        if self.gpu_id is not None and self.gpu_id == 0:
            logging.info(f'Rank {self.gpu_id}: End time: {get_human_time_string_from_seconds(end_time, True)}')
            logging.info(f'Rank {self.gpu_id}: Seconds spent: {end_time - start_time}')
        elif self.gpu_id is None:
            logging.info(f'End time: {get_human_time_string_from_seconds(end_time, True)}')
            logging.info(f'Seconds spent: {end_time - start_time}')

        # store real used time, not just approximation.
        if self.gpu_id == 0 or self.gpu_id is None:
            self.model_analysis.training_duration_10_epochs = (time.time() - start_time) / \
                                                              (self.epochs - self.start_epoch) * 10
            self.model_analysis.save()
            self.model_analysis.save_history(history, header)

        if epoch >= 1 and (self.gpu_id == 0 or self.gpu_id is None):
            save_history(history, self.train_preparer.model_folder, header=header)
            show_best_five_stats(history)
