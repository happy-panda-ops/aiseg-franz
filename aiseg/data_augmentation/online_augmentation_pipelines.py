import math

import numpy as np
import random

# image
from .geometric_transformations import elastic_distortion
from .geometric_transformations import execute_random_flip
from .geometric_transformations import execute_random_tilt
from .geometric_transformations import grid_distortion
from .geometric_transformations import optical_distortion
from .geometric_transformations import random_crop
from .geometric_transformations import random_grid_shuffle
from .geometric_transformations import random_squeeze
from .geometric_transformations import resize
from .geometric_transformations import rotate_by_angle
from .pixel_manipulation import brightness
from .pixel_manipulation import contrast
from .pixel_manipulation import gaussian_filter
from .pixel_manipulation import gaussian_noise
from .pixel_manipulation import random_erasing
from .rgb_manipulation import channel_shuffle
from .rgb_manipulation import color_to_hsv
from .rgb_manipulation import iso_noise
from .rgb_manipulation import random_fog
from .rgb_manipulation import random_rain
from .rgb_manipulation import random_shadow
from .rgb_manipulation import random_snow
from .rgb_manipulation import random_sun_flair
from .rgb_manipulation import rgb_shift
from .rgb_manipulation import solarize
from .rgb_manipulation import to_gray
from .rgb_manipulation import to_sepia

# volume
from .geometric_volume_transformations import random_flip as random_flip_3d
from .geometric_volume_transformations import random_crop as random_crop_3d
from .geometric_volume_transformations import random_resize as random_resize_3d
from .geometric_volume_transformations import rotate_z as rotate_z_3d
from .geometric_volume_transformations import random_rotate as random_rotate_3d
from .geometric_volume_transformations import random_squeeze as random_squeeze_3d
from .geometric_volume_transformations import execute_random_tilt as execute_random_tilt_3d
from .pixel_volume_transformations import add_blur_3d
from .pixel_volume_transformations import add_noise as add_noise_3d
from .pixel_volume_transformations import brightness as brightness_3d
from .pixel_volume_transformations import contrast as contrast_3d
from .pixel_volume_transformations import histogram_normalization_3d
from .pixel_volume_transformations import random_erasing as random_erasing_3d
from .pixel_volume_transformations import random_shadow_3d
from .pixel_volume_transformations import sharpen_3d_with_blur as sharpen_3d_with_blur


class BaseAugmentFunc:
    def __init__(self, func, channels, probability, *args, **kwargs):
        if 0 > probability < 1:
            raise ValueError('Probability must be in range: 0 to 1')
        self.func = func
        self.channels = channels if isinstance(channels, list) else [channels]
        self.probability = probability

        self.expected_input_size = None
        for key, value in kwargs.items():
            self.__setattr__(key, value)

    def __str__(self):
        return f'AugmentFunc_{self.func.__name__}'

    def __call__(self, inputs, targets, global_probability=1.0, *args, **kwargs):
        raise NotImplementedError


class ImageAugmentFunc(BaseAugmentFunc):
    def __init__(self, func, channels, probability, *args, **kwargs):
        super(ImageAugmentFunc, self).__init__(func, channels, probability, *args, **kwargs)

    def _adjust_resized(self, inputs, targets):
        """
        Helper if func is a resizing.
        Needed during augmentation --> then the pipeline will resize the result back to the original if not caught here
        """
        if self.expected_input_size is None:
            raise ValueError(
                'The function ImageAugmentFunc._adjust_resized is only callable if self has the following attribute:\n'
                'expected_input_size'
            )

        in_size = self.expected_input_size  # just for programming/reading --> needs less space

        # crop or fill directly to model_input_size so that the resizing at the end of the
        # pipeline does not undo the resizing operation
        h, w = inputs.shape[:2]
        # if created img is too small: fill with mirrored input.
        padding_h = (0, 0)
        padding_w = (0, 0)
        if h < in_size:
            padding_h = [(in_size - h) // 2, math.ceil((in_size - h) / 2)]
            random.shuffle(padding_h)
            padding_h = tuple(padding_h)

        if w < in_size:
            padding_w = [(in_size - w) // 2, math.ceil((in_size - w) / 2)]
            random.shuffle(padding_w)
            padding_w = tuple(padding_w)

        if len(inputs.shape) == 3:
            inputs = np.pad(inputs, (padding_h, padding_w, (0, 0)), mode='reflect')
        else:
            inputs = np.pad(inputs, (padding_h, padding_w), mode='reflect')
        targets = np.pad(targets, (padding_h, padding_w), mode='reflect')

        h, w = inputs.shape[:2]
        if h > in_size or w > in_size:
            h_start = (h - in_size) // 2
            w_start = (w - in_size) // 2
            inputs = inputs[h_start:in_size + h_start, w_start:in_size + w_start]
            targets = targets[h_start:in_size + h_start, w_start:in_size + w_start]

        return inputs, targets

    def __call__(self, inputs, targets, *args, **kwargs):
        """
        Executes function to inputs and targets with a given probability

        If the function accepts further arguments, you can pass those in the keyword arguments.
        See at if elif loop for options

        Returns: augmented inputs, augmented targets, augmentation_was_executed
        """

        if random.randint(1, 100) / 100 > self.probability:
            return inputs, targets, False

        arguments = []
        # RESIZING
        if 'resize' in str(self) and 'resizing_scale' in kwargs.keys():
            factor = 10 ** 2  # percent exact
            scale_factor = random.randint(
                # min max to prevent wrong usage
                int(factor * min(kwargs['resizing_scale'])), int(factor * max(kwargs['resizing_scale']))
            )
            scale_factor = scale_factor / factor
            arguments.append(scale_factor)
        # ROTATION
        elif 'rotate' in str(self) and 'max_rot_angle' in kwargs.keys():
            angle = random.randint(0, kwargs['max_rot_angle'])
            angle = -1 * angle if random.randint(0, 1) else angle
            arguments.append(angle)
        # SQUEEZING
        elif 'squeeze' in str(self) and 'squeeze_scale' in kwargs.keys():
            factor = 10 ** 2
            scale_factor = random.randint(
                # min max to prevent wrong usage
                int(factor * min(kwargs['squeeze_scale'])), int(factor * max(kwargs['squeeze_scale']))
            )
            scale_factor = scale_factor / factor
            arguments.append(scale_factor)
        # TILTING
        elif 'tilt' in str(self) and 'tilting_start_end_factor' in kwargs.keys():
            # min max to prevent wrong usage
            arguments.append(min(kwargs['tilting_start_end_factor']))
            arguments.append(max(kwargs['tilting_start_end_factor']))

        # Flipping
        elif 'flip' in str(self) and 'vertical_flip_allowed' in kwargs.keys():
            # min max to prevent wrong usage
            arguments.append(kwargs['vertical_flip_allowed'])

        # AUGMENT EXECUTION
        inputs, targets = self.func(inputs, targets, *arguments)

        # In case of resizing: adjust to expected input size
        if self.func == resize:
            inputs, targets = self._adjust_resized(inputs, targets)

        return inputs, targets, True


class VolumeAugmentFunc(BaseAugmentFunc):
    def __init__(self, func, channels, probability, *args, **kwargs):
        self.expected_input_depth = None
        super(VolumeAugmentFunc, self).__init__(func, channels, probability, *args, **kwargs)

    def _adjust_resized(self, inputs, targets):
        """Helper if func is a resizing -> chaining may otherwise result to unexpected dimension errors"""
        if self.expected_input_size is None or self.expected_input_depth is None:
            raise ValueError(
                'The function VolumeAugmentFunc._adjust_resized is only callable if self has the following attribute:\n'
                'expected_input_size and expected_input_depth.'
            )

        in_size = self.expected_input_size  # just for programming/reading --> needs less space
        in_depth = self.expected_input_depth

        # crop directly to model input size so that the resizing at the end of the
        # pipeline does not undo the resizing operation
        d, h, w = inputs.shape[:]
        # if created volume is too small: fill with mirrored input.
        padding_d = (0, 0)
        padding_h = (0, 0)
        padding_w = (0, 0)
        if h < in_size:
            padding_h = [(in_size - h) // 2, math.ceil((in_size - h) // 2)]
            random.shuffle(padding_h)
            padding_h = tuple(padding_h)

        if w < in_size:
            padding_w = [(in_size - w) // 2, math.ceil((in_size - w) // 2)]
            random.shuffle(padding_w)
            padding_w = tuple(padding_w)

        if d < in_depth:
            padding_d = [(in_depth - d) // 2, math.ceil((in_depth - d) // 2)]
            random.shuffle(padding_d)
            padding_d = tuple(padding_d)

        inputs = np.pad(inputs, (padding_d, padding_h, padding_w), mode='reflect')
        targets = np.pad(targets, (padding_d, padding_h, padding_w), mode='reflect')

        d, h, w = inputs.shape[:]
        if h > in_size or w > in_size:
            h_start = random.randint(0, h - in_size)
            w_start = random.randint(0, w - in_size)
            inputs = inputs[:, h_start:in_size + h_start, w_start:in_size + w_start]
            targets = targets[:, h_start:in_size + h_start, w_start:in_size + w_start]

        if d > in_depth:
            d_start = random.randint(0, d - in_depth)
            inputs = inputs[d_start:in_depth + d_start, :, :]
            targets = targets[d_start:in_depth + d_start, :, :]
        return inputs, targets

    def __call__(self, inputs, targets, *args, **kwargs):
        """
        Executes function to inputs and targets with a given probability

        If the function accepts further arguments, you can pass those in the keyword arguments.
        See at if elif loop for options

        kwargs are set using the AugPipelines options_dict. See BasePipe class
        """
        if random.randint(1, 100) / 100 > self.probability:
            return inputs, targets, False

        arguments = []
        # RESIZING
        if 'resize' in str(self) and 'resizing_scale' in kwargs.keys():
            factor = 10 ** 2  # percent exact
            scale_factor = random.randint(
                # min max to prevent wrong usage
                int(factor * min(kwargs['resizing_scale'])), int(factor * max(kwargs['resizing_scale']))
            )
            scale_factor = scale_factor / factor
            arguments.append(scale_factor)
        # ROTATION
        elif 'rotate' in str(self) and 'max_rot_angle' in kwargs.keys():
            if 'rotate_z' in str(self):
                # randomization has to be done here to prevent unexpected behaviour during offline augmentation
                angle = random.randint(0, kwargs['max_rot_angle'])
                angle = -1 * angle if random.randint(0, 1) else angle
            else:
                # randomization will be done in random rotate function to allow for more randomization during the
                # chaining
                angle = kwargs['max_rot_angle']
            arguments.append(angle)
            arguments.append(kwargs['rot_chaining_allowed'])
            arguments.append(kwargs['fixed_rotation_angle'])
        # SQUEEZING
        elif 'squeeze' in str(self) and 'squeeze_scale' in kwargs.keys():
            factor = 10 ** 2
            scale_factor = random.randint(
                # min max to prevent wrong usage
                int(factor * min(kwargs['squeeze_scale'])), int(factor * max(kwargs['squeeze_scale']))
            )
            scale_factor = scale_factor / factor
            arguments.append(scale_factor)
        # TILTING
        elif 'tilt' in str(self) and 'tilting_start_end_factor' in kwargs.keys():
            # min max to prevent wrong usage
            arguments.append(min(kwargs['tilting_start_end_factor']))
            arguments.append(max(kwargs['tilting_start_end_factor']))
        # Flipping
        elif 'flip' in str(self) and 'depth_flip_allowed' in kwargs.keys():
            # min max to prevent wrong usage
            arguments.append(kwargs['depth_flip_allowed'])

        # AUGMENT EXECUTION
        inputs, targets = self.func(inputs, targets, *arguments)

        # In case of resizing: adjust to expected input size
        if self.func == random_resize_3d:
            inputs, targets = self._adjust_resized(inputs, targets)

        return inputs, targets, True


class BasePipe:
    """ships some functions, nothing else"""
    def __int__(self):
        self.global_strength = 0
        self.expected_input_size = 0
        self.options_dict = {}

        # it is beneficial, to execute those first for some geo first policy
        self.first_geo_functions = []
        self.geometric_functions = []
        self.radiometric_functions = []
        self.max_augmentations = 0
        self.policies = {
            'random': self.policy_random,
            'geo_first': self.policy_geo_first,
            'pix_first': self.policy_pix_first,
            'geo_only': self.policy_geo_only,
            'pix_only': self.policy_pix_only,
        }
        self.policy = self.policies['geo_first']

        self.aug_type = 'base'

    def execute_aug_func(self, inputs, targets, aug_func):
        """

        Args:
            inputs:
            targets:
            aug_func:

        Returns: augmented inputs, targets, function_was_executed, error_if_happened

        """
        if self.aug_type == 'image':
            rgb_shape_check = 3
        elif self.aug_type == 'volume':
            rgb_shape_check = 4
        else:
            raise RuntimeError('Not supported aug_type. Must be: volume or image')

        if len(inputs.shape) == rgb_shape_check:
            channels = 3
        else:
            channels = 1

        executed = False
        if channels not in aug_func.channels:
            return inputs, targets, executed, None
        try:
            inputs, targets, executed = aug_func(inputs, targets, **self.options_dict)
        except RuntimeError as e:
            return inputs, targets, executed, e
        return inputs, targets, executed, None

    @staticmethod
    def __check_error(error, executed_functions, current_function):
        if error is not None:
            executed_funcs = ', '.join(executed_functions)
            error_func = current_function.func.__name__
            raise RuntimeError(
                f'Executed following funcs: {executed_funcs}, got Error at current func: {error_func}\n'
                f'Error: {error}'
            )

    def execute_functions(self, inputs, targets, functions, executed_funcs, current_max_augmentations=None):
        if current_max_augmentations is None:
            current_max_augmentations = self.max_augmentations

        called_function_count = 0
        for augmentation_func in functions:
            if called_function_count == current_max_augmentations:
                break

            inputs, targets, executed, error = self.execute_aug_func(inputs, targets, augmentation_func)
            self.__check_error(error, executed_funcs, augmentation_func)

            if executed:
                executed_funcs.append(augmentation_func.func.__name__)
            called_function_count += 1

        return inputs, targets, executed_funcs

    def get_current_operation_distribution(self):
        """
        Calculates distribution of geometric and pixel operations

        Returns: geo operations, pix operations
        """
        if self.max_augmentations == 1:
            if random.randint(0, 1):
                return 1, 0
            return 0, 1

        if random.randint(0, 1):
            pix_augmentations = self.max_augmentations // 2
        else:
            pix_augmentations = math.ceil(self.max_augmentations / 2)
        geo_augmentations = self.max_augmentations - pix_augmentations
        return geo_augmentations, pix_augmentations

    def policy_random(self, inputs, targets):
        functions = self.first_geo_functions + self.geometric_functions + self.radiometric_functions
        random.shuffle(functions)

        executed_funcs = []
        inputs, targets, executed_funcs = self.execute_functions(
            inputs, targets, functions, executed_funcs
        )

        return inputs, targets

    def policy_geo_first(self, inputs, targets):
        """
        If using geo first, the intention is to get the best possible geometric augmentations. Therefore, I split the
        geometric augmentations into the most beneficial: resizing and rotation and the rest.
        One or both of resizing and rotation will be executed almost anytime, if possible.
        """

        geo_augmentations, pix_augmentations = self.get_current_operation_distribution()

        if geo_augmentations == 1:
            if random.randint(0, 1):
                first_geo_count = 1
                other_geo_count = 0
            else:
                first_geo_count = 0
                other_geo_count = 1
        else:
            first_geo_to_all_distribution = len(self.first_geo_functions) / (
                    len(self.first_geo_functions) + len(self.geometric_functions))

            first_geo_float = first_geo_to_all_distribution * geo_augmentations
            if first_geo_float > 2:
                first_geo_count = 2
                other_geo_count = geo_augmentations - first_geo_count
            else:
                first_geo_count = math.ceil(first_geo_float)
                other_geo_count = geo_augmentations - first_geo_count

        executed_funcs = []  # keep track of executed functions during policy
        # rotate or resize
        first_geo_functions = self.first_geo_functions
        if first_geo_count == 1:
            random.shuffle(first_geo_functions)
        inputs, targets, executed_funcs = self.execute_functions(
            inputs, targets, first_geo_functions, executed_funcs, first_geo_count
        )

        # other geometric manipulations
        geometric_functions = self.geometric_functions
        random.shuffle(geometric_functions)

        inputs, targets, executed_funcs = self.execute_functions(
            inputs, targets, geometric_functions, executed_funcs, other_geo_count
        )

        # pixel manipulations
        pixel_functions = self.radiometric_functions
        random.shuffle(pixel_functions)

        inputs, targets, executed_funcs = self.execute_functions(
            inputs, targets, pixel_functions, executed_funcs, pix_augmentations
        )

        return inputs, targets

    def policy_pix_first(self, inputs, targets):
        executed_funcs = []  # keep track of executed functions during policy

        # pixel manipulations
        pixel_functions = self.radiometric_functions
        random.shuffle(pixel_functions)

        inputs, targets, executed_funcs = self.execute_functions(inputs, targets, pixel_functions, executed_funcs)

        # geometric manipulations
        geometric_functions = self.geometric_functions + self.first_geo_functions
        random.shuffle(geometric_functions)

        inputs, targets, executed_funcs = self.execute_functions(inputs, targets, geometric_functions, executed_funcs)

        return inputs, targets

    def policy_geo_only(self, inputs, targets):
        executed_funcs = []  # keep track of executed functions during policy
        # rotate or resize
        inputs, targets, executed_funcs = self.execute_functions(
            inputs, targets, self.first_geo_functions, executed_funcs
        )

        # other geometric manipulations
        geometric_functions = self.geometric_functions
        random.shuffle(geometric_functions)

        inputs, targets, executed_funcs = self.execute_functions(inputs, targets, geometric_functions, executed_funcs)
        return inputs, targets

    def policy_pix_only(self, inputs, targets):
        executed_funcs = []  # keep track of executed functions during policy

        # pixel manipulations
        pixel_functions = self.radiometric_functions
        random.shuffle(pixel_functions)

        inputs, targets, executed_funcs = self.execute_functions(inputs, targets, pixel_functions, executed_funcs)
        return inputs, targets


class VolumeAugmentationPipeline(BasePipe):
    """
    Attention: to get the best results, ROTATE BEFORE: rotate initial data by 90, 180 and 270 degrees around x and y
        You can use the VolumeAugmenter or start start_volume_augmentation.py and skip all other augmentations.

    Explanation:
        This pipeline only rotates around z since the depth of a network might be totally different from height and
        width. h and w are most likely a constraint by a networks architecture (e.g. UNet3D--> h and w must be divisible
        by two**n (n = depth of UNet3D -> 64/(2**4) for depth = 4)) and the depth isn't.

    """
    def __init__(self,
                 global_strength=0.0,
                 max_augmentations=None,
                 policy='geo_first',

                 expected_input_size=256,
                 expected_input_depth=20,

                 # voxel manipulation probabilities
                 add_blur_3d_prob=0,
                 add_noise_3d_prob=0,
                 brightness_3d_prob=0,
                 contrast_3d_prob=0,
                 # histogram_normalization_3d_prob=0,
                 random_erasing_3d_prob=0,
                 sharpen_3d_with_blur_prob=0,
                 random_shadow_3d_prob=0,

                 # geometric probabilities
                 flip_prob=0,
                 random_crop_3d_prob=0,
                 random_resize_3d_prob=0,
                 rotate_3d_prob=0,
                 squeeze_prob=0,
                 tilt_prob=0,

                 # further options
                 max_rot_angle=180,
                 resizing_scale=(0.8, 1.2),
                 squeeze_scale=(0.8, 1.2),
                 depth_flip_allowed=True,
                 tilting_start_end_factor=(1 / 5, 4 / 5),
                 xy_rot_allowed=False,
                 fixed_rotation_angle=False,
                 ):
        """

        Args:
            global_strength: global augmentation strength. Calculate using the training profiler for adaptive change.
            max_augmentations:
            policy: one of [random, geo_first, pix_first, geo_only, pix_only]
                random: executes all functions randomly until max_augmentations is reached
                geo_first: executes random geometric operations (based on their probabilities of course) like:
                        1. rotation, 2. resizing, then other geometric operations and
                        afterwards random pixel manipulations are executed - depending on
                        max_augmentations
                pix_first: executes random pixel manipulations (based on their probabilities of course), then
                        random geometric operations - depending on max_augmentations
                geo_only: executes random geometric operations (based on their probabilities of course) like:
                        1. rotation, 2. resizing, then other geometric operations
                pix_only: executes only random pixel manipulations (based on their probabilities of course)

                Help:
                    When to use what?
                        random: if you have a high number of max augmentations per volume and are willing to train long
                        geo_first: volumes during testing/validation/inference are captured with different cameras,
                            angles, resolutions, ... Not useful if you have a static camera e.g. in a car driving around
                        pix_first: useful if you have a static camera e.g. in a car driving around, night day shift, ..
                        geo_only: your dataset consists of many volumes covering many lighting situations but not much
                            variance in their geometry
                        pix_only: your dataset consists of many volumes covering a large variance in their geometry but
                            not different lighting
            expected_input_size: to ensure the volume will be resized at the end to the network expected size
            expected_input_depth: to ensure the volume will be resized at the end to the network expected depth

            ### voxel manipulation probabilities ###
            add_blur_3d_prob:
            add_noise_3d_prob:
            brightness_3d_prob:
            contrast_3d_prob:
            # histogram_normalization_3d_prob:
            random_erasing_3d_prob:
            sharpen_3d_with_blur_prob:
            random_shadow_3d_prob:

            ### geometric probabilities ###
            flip_prob:
            random_crop_3d_prob:
            random_resize_3d_prob:
            rotate_3d_prob:
            squeeze_prob:
            tilt_prob:

            ### further options ###
            max_rot_angle:
            resizing_scale: (min, max)
            squeeze_scale: (min, max)
            depth_flip_allowed:
            tilting_start_end_factor: (start multiplier, end multiplier), see tilt functions for more explanation
        """
        super(VolumeAugmentationPipeline, self).__init__()

        if global_strength < 0 or global_strength > 1:
            raise ValueError('global aug_strength must be between 0 and 1')
        if max_augmentations is not None and max_augmentations < 1:
            raise ValueError(f'max_augmentations_per_image must be 1 or more, not {max_augmentations}')

        self.global_strength = global_strength
        self.expected_input_size = expected_input_size
        self.expected_input_depth = expected_input_depth

        rot_chaining_allowed = True if xy_rot_allowed and fixed_rotation_angle else False
        self.options_dict = {
            'max_rot_angle': max_rot_angle,
            'resizing_scale': resizing_scale,
            'squeeze_scale': squeeze_scale,
            'tilting_start_end_factor': tilting_start_end_factor,
            'depth_flip_allowed': depth_flip_allowed,
            'rot_chaining_allowed': rot_chaining_allowed,
            'fixed_rotation_angle': fixed_rotation_angle,
        }
        rot_func = random_rotate_3d if xy_rot_allowed else rotate_z_3d

        self.first_geo_functions = [
            VolumeAugmentFunc(
                func=random_resize_3d,
                channels=1,
                probability=random_resize_3d_prob,
                # cropping at end of operation to ensure pipeline does not undo this operation if it was the only
                # executed one
                **{'expected_input_size': expected_input_size, 'expected_input_depth': expected_input_depth},
            ),
            VolumeAugmentFunc(func=rot_func, channels=1, probability=rotate_3d_prob),
        ]
        idx_to_pop = []
        for i, func in enumerate(self.first_geo_functions):
            if func.probability == 0:
                idx_to_pop.append(i)
        for idx in sorted(idx_to_pop, reverse=True):
            self.first_geo_functions.pop(idx)

        self.geometric_functions = [
            VolumeAugmentFunc(func=random_flip_3d, channels=1, probability=flip_prob),
            VolumeAugmentFunc(func=random_crop_3d, channels=1, probability=random_crop_3d_prob),
            VolumeAugmentFunc(func=random_squeeze_3d, channels=1, probability=squeeze_prob),
            VolumeAugmentFunc(func=execute_random_tilt_3d, channels=1, probability=tilt_prob),
        ]
        idx_to_pop = []
        for i, func in enumerate(self.geometric_functions):
            if func.probability == 0:
                idx_to_pop.append(i)
        for idx in sorted(idx_to_pop, reverse=True):
            self.geometric_functions.pop(idx)

        self.radiometric_functions = [
            VolumeAugmentFunc(func=add_blur_3d, channels=1, probability=add_blur_3d_prob),
            VolumeAugmentFunc(func=add_noise_3d, channels=1, probability=add_noise_3d_prob),
            VolumeAugmentFunc(func=brightness_3d, channels=1, probability=brightness_3d_prob),
            VolumeAugmentFunc(func=contrast_3d, channels=1, probability=contrast_3d_prob),
            # VolumeAugmentFunc(
            # func=histogram_normalization_3d, channels=1, probability=histogram_normalization_3d_prob
            # ),  # This function is not beneficial
            VolumeAugmentFunc(func=random_erasing_3d, channels=1, probability=random_erasing_3d_prob),
            VolumeAugmentFunc(func=sharpen_3d_with_blur, channels=1, probability=sharpen_3d_with_blur_prob),
            VolumeAugmentFunc(func=random_shadow_3d, channels=1, probability=random_shadow_3d_prob)
        ]
        idx_to_pop = []
        for i, func in enumerate(self.radiometric_functions):
            if func.probability == 0:
                idx_to_pop.append(i)
        for idx in sorted(idx_to_pop, reverse=True):
            self.radiometric_functions.pop(idx)

        if max_augmentations is None:
            self.max_augmentations = len(self.radiometric_functions) + len(self.geometric_functions) \
                                               + len(self.first_geo_functions)
        else:
            self.max_augmentations = max_augmentations

        self.policies = {
            'random': self.policy_random,
            'geo_first': self.policy_geo_first,
            'pix_first': self.policy_pix_first,
            'geo_only': self.policy_geo_only,
            'pix_only': self.policy_pix_only,
        }
        self.policy = self.policies[policy]

        self.aug_type = 'volume'

    def __call__(self, inputs, targets, *args, **kwargs):
        if random.randint(0, 100) / 100 > self.global_strength:
            return inputs, targets

        inputs, targets = self.policy(inputs, targets)

        d, h, w = inputs.shape[:3]
        if h != self.expected_input_size or w != self.expected_input_size or d != self.expected_input_depth:
            # if created volume is too small: fill with mirrored input.
            padding_d = (0, 0)
            padding_h = (0, 0)
            padding_w = (0, 0)
            if h < self.expected_input_size:
                padding_h = [(self.expected_input_size - h) // 2, math.ceil((self.expected_input_size - h) / 2)]
                random.shuffle(padding_h)
                padding_h = tuple(padding_h)

            if w < self.expected_input_size:
                padding_w = [(self.expected_input_size - w) // 2, math.ceil((self.expected_input_size - w) / 2)]
                random.shuffle(padding_w)
                padding_w = tuple(padding_w)

            if d < self.expected_input_depth:
                padding_d = [(self.expected_input_depth - d) // 2, math.ceil((self.expected_input_depth - d) / 2)]
                random.shuffle(padding_d)
                padding_d = tuple(padding_d)

            inputs = np.pad(inputs, (padding_d, padding_h, padding_w), mode='reflect')
            targets = np.pad(targets, (padding_d, padding_h, padding_w), mode='reflect')

            # eventual cropping if volume dimensions changed --> using center of volume
            d, h, w = inputs.shape[:3]
            if any([d < self.expected_input_depth, h < self.expected_input_size, h < self.expected_input_size]):
                raise RuntimeError(f'Dimension missmatch after online augmentation. Got shape: {d}x{h}x{w}.')
            start_h = (h - self.expected_input_size) // 2
            end_h = start_h + self.expected_input_size
            start_w = (w - self.expected_input_size) // 2
            end_w = start_w + self.expected_input_size
            start_d = (d - self.expected_input_depth) // 2
            end_d = start_d + self.expected_input_depth

            inputs = inputs[start_d:end_d, start_h:end_h, start_w:end_w]
            targets = targets[start_d:end_d, start_h:end_h, start_w:end_w]
        return inputs, targets


class ImageAugmentationPipeline(BasePipe):
    def __init__(self,
                 global_strength=0.0,
                 max_augmentations=None,
                 policy='geo_first',

                 expected_input_size=256,

                 # pixel manipulation probabilities
                 brightness_prob=0,
                 contrast_prob=0,
                 gaussian_filter_prob=0,
                 gaussian_noise_prob=0,
                 random_erasing_prob=0,
                 channel_shuffle_prob=0,
                 color_to_hsv_prob=0,
                 iso_noise_prob=0,
                 random_fog_prob=0,
                 random_rain_prob=0,
                 random_shadow_prob=0,
                 random_snow_prob=0,
                 random_sun_flair_prob=0,
                 rgb_shift_prob=0,
                 solarize_prob=0,
                 to_gray_prob=0,
                 to_sepia_prob=0,

                 # geometric probabilities
                 elastic_distortion_prob=0,
                 flip_prob=0,
                 grid_distortion_prob=0,
                 optical_distortion_prob=0,
                 random_crop_prob=0,
                 resize_prob=0,
                 rotate_by_angle_prob=0,
                 squeeze_prob=0,
                 tilt_prob=0,
                 grid_shuffle_prob=0,

                 # further properties:
                 max_rot_angle=30,
                 resizing_scale=(0.8, 1.2),
                 squeeze_scale=(0.8, 1.2),
                 vertical_flip_allowed=False,
                 tilting_start_end_factor=(1/5, 4/5),
                 ):
        """

        Args:
            global_strength: global augmentation strength. Calculate using the training profiler for adaptive change.
            max_augmentations: if a target was augmented this many times, the pipeline stops
            policy: one of [random, geo_first, pix_first, geo_only, pix_only]
                random: executes all functions randomly until max_augmentations_per_image is reached
                geo_first: executes random geometric operations (based on their probabilities of course) like:
                        1. rotation, 2. resizing, then other geometric operations and
                        afterwards random pixel manipulations are executed - depending on
                        max_augmentations_per_image
                pix_first: executes random pixel manipulations (based on their probabilities of course), then
                        random geometric operations - depending on max_augmentations_per_image
                geo_only: executes random geometric operations (based on their probabilities of course) like:
                        1. rotation, 2. resizing, then other geometric operations
                pix_only: executes only random pixel manipulations (based on their probabilities of course)

                Help:
                    When to use what?
                        random: if you have a high number of max augmentations per image and are willing to train long
                        geo_first: images during testing/validation/inference are captured with different cameras,
                            angles, resolutions, ... Not useful if you have a static camera e.g. in a car driving around
                        pix_first: useful if you have a static camera e.g. in a car driving around, night day shift, ..
                        geo_only: your dataset consists of many images covering many lighting situations but not much
                            variance in their geometry
                        pix_only: your dataset consists of many images covering a large variance in their geometry but
                            not different lighting
            expected_input_size: to ensure the image will be resized at the end to the network expected size

            ### pixel manipulation probabilities ###
            brightness_prob:
            contrast_prob:
            gaussian_filter_prob:
            gaussian_noise_prob:
            random_erasing_prob:
            channel_shuffle_prob:
            color_to_hsv_prob:
            iso_noise_prob:
            random_fog_prob:
            random_rain_prob:
            random_shadow_prob:
            random_snow_prob:
            random_sun_flair_prob:
            rgb_shift_prob:
            solarize_prob:
            to_gray_prob:
            to_sepia_prob:

            ### geometric probabilities ###
            elastic_distortion_prob:
            flip_prob:
            grid_distortion_prob:
            optical_distortion_prob:
            random_crop_prob:
            resize_prob:
            rotate_by_angle_prob:
            tilt_prob:
            grid_shuffle_prob:

            ### MORE OPTIONS for some arguments ###
            max_rot_angle: degrees
            resizing_scale: (min, max)
            squeeze_scale: (min, max)
            vertical_flip_allowed:
            tilting_start_end_factor: (start multiplier, end multiplier), see tilt functions for more explanation
        """
        super(ImageAugmentationPipeline, self).__init__()

        if global_strength < 0 or global_strength > 1:
            raise ValueError('global aug_strength must be between 0 and 1')
        if max_augmentations is not None and max_augmentations < 1:
            raise ValueError(f'max_augmentations_per_image must be 1 or more, not {max_augmentations}')

        self.global_strength = global_strength
        self.expected_input_size = expected_input_size
        self.options_dict = {
            'max_rot_angle': max_rot_angle,
            'resizing_scale': resizing_scale,
            'squeeze_scale': squeeze_scale,
            'tilting_start_end_factor': tilting_start_end_factor,
            'vertical_flip_allowed': vertical_flip_allowed,
        }

        # it is beneficial, to execute those first for some geo first policy
        self.first_geo_functions = [
            ImageAugmentFunc(func=rotate_by_angle, channels=[1, 3], probability=rotate_by_angle_prob),
            ImageAugmentFunc(
                func=resize,
                channels=[1, 3],
                probability=resize_prob,
                # cropping at end of operation to ensure pipeline does not undo this operation if it was the only
                # executed one
                **{'expected_input_size': expected_input_size},
            ),
        ]
        idx_to_pop = []
        for i, func in enumerate(self.first_geo_functions):
            if func.probability == 0:
                idx_to_pop.append(i)
        for idx in sorted(idx_to_pop, reverse=True):
            self.first_geo_functions.pop(idx)

        self.geometric_functions = [
            ImageAugmentFunc(func=elastic_distortion, channels=[1, 3], probability=elastic_distortion_prob),
            ImageAugmentFunc(func=execute_random_flip, channels=[1, 3], probability=flip_prob),
            ImageAugmentFunc(func=grid_distortion, channels=[1, 3], probability=grid_distortion_prob),
            ImageAugmentFunc(func=optical_distortion, channels=[1, 3], probability=optical_distortion_prob),
            ImageAugmentFunc(func=random_crop, channels=[1, 3], probability=random_crop_prob),
            ImageAugmentFunc(func=execute_random_tilt, channels=[1, 3], probability=tilt_prob),
            ImageAugmentFunc(func=random_squeeze, channels=[1, 3], probability=squeeze_prob),
            ImageAugmentFunc(func=random_grid_shuffle, channels=[1, 3], probability=grid_shuffle_prob),
        ]
        idx_to_pop = []
        for i, func in enumerate(self.geometric_functions):
            if func.probability == 0:
                idx_to_pop.append(i)
        for idx in sorted(idx_to_pop, reverse=True):
            self.geometric_functions.pop(idx)

        self.radiometric_functions = [
            ImageAugmentFunc(func=brightness, channels=[1, 3], probability=brightness_prob),
            ImageAugmentFunc(func=contrast, channels=[1, 3], probability=contrast_prob),
            ImageAugmentFunc(func=gaussian_filter, channels=[1, 3], probability=gaussian_filter_prob),
            ImageAugmentFunc(func=gaussian_noise, channels=[1, 3], probability=gaussian_noise_prob),
            ImageAugmentFunc(func=random_erasing, channels=[1, 3], probability=random_erasing_prob),
            ImageAugmentFunc(func=channel_shuffle, channels=[3], probability=channel_shuffle_prob),
            ImageAugmentFunc(func=color_to_hsv, channels=[3], probability=color_to_hsv_prob),
            ImageAugmentFunc(func=iso_noise, channels=[3], probability=iso_noise_prob),
            ImageAugmentFunc(func=random_fog, channels=[3], probability=random_fog_prob),
            ImageAugmentFunc(func=random_rain, channels=[3], probability=random_rain_prob),
            ImageAugmentFunc(func=random_shadow, channels=[3], probability=random_shadow_prob),
            ImageAugmentFunc(func=random_snow, channels=[3], probability=random_snow_prob),
            ImageAugmentFunc(func=random_sun_flair, channels=[3], probability=random_sun_flair_prob),
            ImageAugmentFunc(func=rgb_shift, channels=[3], probability=rgb_shift_prob),
            ImageAugmentFunc(func=solarize, channels=[3], probability=solarize_prob),
            ImageAugmentFunc(func=to_gray, channels=[3], probability=to_gray_prob),
            ImageAugmentFunc(func=to_sepia, channels=[3], probability=to_sepia_prob),
        ]
        idx_to_pop = []
        for i, func in enumerate(self.radiometric_functions):
            if func.probability == 0:
                idx_to_pop.append(i)
        for idx in sorted(idx_to_pop, reverse=True):
            self.radiometric_functions.pop(idx)

        if max_augmentations is None:
            self.max_augmentations = len(self.radiometric_functions) + len(self.geometric_functions) \
                                               + len(self.first_geo_functions)
        else:
            self.max_augmentations = max_augmentations

        self.policies = {
            'random': self.policy_random,
            'geo_first': self.policy_geo_first,
            'pix_first': self.policy_pix_first,
            'geo_only': self.policy_geo_only,
            'pix_only': self.policy_pix_only,
        }
        self.policy = self.policies[policy]
        self.aug_type = 'image'

    def __call__(self, inputs, targets):
        """inputs and targets must be one! image and the related mask"""
        if random.randint(0, 100) / 100 > self.global_strength:
            return inputs, targets

        inputs, targets = self.policy(inputs, targets)

        h, w = inputs.shape[:2]
        if h != self.expected_input_size or w != self.expected_input_size:
            # invert true --> max_width_or_height = min_width_or_height
            # if created volume is too small: fill with mirrored input.
            padding_h = (0, 0)
            padding_w = (0, 0)
            if h < self.expected_input_size:
                padding_h = [(self.expected_input_size - h) // 2, math.ceil((self.expected_input_size - h) / 2)]
                random.shuffle(padding_h)
                padding_h = tuple(padding_h)

            if w < self.expected_input_size:
                padding_w = [(self.expected_input_size - w) // 2, math.ceil((self.expected_input_size - w) / 2)]
                random.shuffle(padding_w)
                padding_w = tuple(padding_w)

            if len(inputs.shape) == 3:
                inputs = np.pad(inputs, (padding_h, padding_w, (0, 0)), mode='reflect')
            else:
                inputs = np.pad(inputs, (padding_h, padding_w), mode='reflect')
            targets = np.pad(targets, (padding_h, padding_w), mode='reflect')

            # eventual cropping if image dimensions changed --> using center of image
            h, w = inputs.shape[:2]
            start_h = (h - self.expected_input_size) // 2
            end_h = start_h + self.expected_input_size
            start_w = (w - self.expected_input_size) // 2
            end_w = start_w + self.expected_input_size

            inputs = inputs[start_h:end_h, start_w:end_w]
            targets = targets[start_h:end_h, start_w:end_w]

        return inputs, targets
