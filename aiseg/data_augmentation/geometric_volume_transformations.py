import numpy as np
import random

from scipy import ndimage
from scipy.ndimage import zoom

from .geometric_transformations import get_rotated_rectangle_with_max_area
from .geometric_transformations import tilt_backwards
from .geometric_transformations import tilt_forward
from .geometric_transformations import tilt_left
from .geometric_transformations import tilt_right


def apply_2d_function_to_volume(volume, masked_volume, function, **kwargs):
    """
    Iterates through the volume in z direction. Manipulate your volume if you want a different axis
    """
    d, h, w = volume.shape

    img = volume[0, :, :]
    mask = masked_volume[0, :, :]

    first_manipulated_image, first_manipulated_mask = function(img, mask, **kwargs)

    new_volume = np.zeros((d, first_manipulated_image.shape[0], first_manipulated_image.shape[1]), dtype=np.uint8)
    new_mask = np.zeros((d, first_manipulated_image.shape[0], first_manipulated_image.shape[1]), dtype=np.uint8)

    new_volume[0, :, :] = first_manipulated_image[:, :]
    new_mask[0, :, :] = first_manipulated_mask[:, :]

    for z in range(1, d):
        img = volume[z, :, :]
        mask = masked_volume[z, :, :]
        img, mask = function(img, mask, **kwargs)
        new_volume[z, :, :] = img[:, :]
        new_mask[z, :, :] = mask[:, :]
    return new_volume, new_mask


def random_resize(volume, mask, scale_factor=None):
    if scale_factor is None:
        percentage = random.randint(80, 120)
        scale_factor = percentage / 100

    new_volume = zoom(volume, (scale_factor, scale_factor, scale_factor))
    new_mask = zoom(mask, (scale_factor, scale_factor, scale_factor), mode='nearest', order=0)
    return new_volume, new_mask


def rotate_by_angle(volume, angle, axes, crop_volume=True, allow_interpolation=True):
    """

    Args:
        volume:
        angle: in degrees
        axes: The two axes that define the plane of rotation.
            e.g.:   (1, 2) -> h,w -> rotate around z (or depth axis)
                    (0, 1) -> d,h -> around x (or width)
                    (0, 2) -> d,w -> around y (or height)
        crop_volume: if true, no black boundaries are given
        allow_interpolation: if False, mode is set to nearest neighbour interpolation and order is set to 0

    Returns: rotated volume

    """
    # Error: some of the strides of a given numpy array are negative
    #        This means that the numpy array has undergone such operation:
    #        image = image[..., ::-1] or np.rot90()
    #        I guess this has something to do with how numpy array are stored in memory, and unfortunately PyTorch
    #        doesn’t currently support numpy array that has been reversed using negative stride.
    if angle == 0:
        pass
    elif angle == 90:
        volume = np.rot90(volume, 1, axes=axes)
    elif angle == -90:
        volume = np.rot90(volume, 1, axes=(axes[1], axes[0]))
    elif angle == 180 or angle == -180:
        volume = np.rot90(volume, 2, axes=axes)
    else:
        if 0 not in axes:
            img_before_rot = volume[0, :, :]
        elif 1 not in axes:
            img_before_rot = volume[:, 0, :]
        else:
            img_before_rot = volume[:, :, 0]

        if allow_interpolation:
            volume = ndimage.rotate(volume, angle, axes=axes)
        else:
            volume = ndimage.rotate(volume, angle, axes=axes, mode='nearest', order=0)

        if 0 not in axes:
            img_after_rot = volume[0, :, :]
        elif 1 not in axes:
            img_after_rot = volume[:, 0, :]
        else:
            img_after_rot = volume[:, :, 0]

        if crop_volume:

            h, w = img_before_rot.shape[:2]
            width, height = get_rotated_rectangle_with_max_area(w, h, angle)

            image_center = (int(img_after_rot.shape[0] * 0.5), int(img_after_rot.shape[1] * 0.5))

            x1 = int(image_center[1] - width * 0.5)
            x2 = int(image_center[1] + width * 0.5)
            y1 = int(image_center[0] - height * 0.5)
            y2 = int(image_center[0] + height * 0.5)

            if 0 not in axes:
                volume = volume[:, y1:y2, x1:x2]
            elif 1 not in axes:
                volume = volume[y1:y2, :, x1:x2]
            else:
                volume = volume[y1:y2, x1:x2, :]

    return volume


def rotate_x(volume, mask, angle, *args, **kwargs):
    """
    Rotate around x by a given angle. I recommend using multiprocessing and stacking the images later.
    """
    volume = rotate_by_angle(volume, angle, (0, 1), True, allow_interpolation=True)
    mask = rotate_by_angle(mask, angle, (0, 1), True, allow_interpolation=False)
    return volume, mask


def rotate_y(volume, mask, angle, *args, **kwargs):
    """
    Rotate around y by a given angle. I recommend using multiprocessing and stacking the images later.
    """
    volume = rotate_by_angle(volume, angle, (0, 2), True, allow_interpolation=True)
    mask = rotate_by_angle(mask, angle, (0, 2), True, allow_interpolation=False)
    return volume, mask


def rotate_z(volume, mask, angle, *args, **kwargs):
    """
    Rotate around z by a given angle. I recommend using multiprocessing and stacking the images later.
    """
    volume = rotate_by_angle(volume, angle, (1, 2), True, allow_interpolation=True)
    mask = rotate_by_angle(mask, angle, (1, 2), True, allow_interpolation=False)
    return volume, mask


def random_rotate(volume, mask, angle, chaining_allowed, fixed_rotation_angle):
    """

    Args:
        volume:
        mask:
        angle: the maximum rotation angle. e.g. if 180 and no chaining_allowed, then the angle will be set randomly to
            a value between 0 and +- angle
        chaining_allowed:
        fixed_rotation_angle:

    Returns:

    """
    if angle == 0:
        return volume, mask
    if chaining_allowed:
        chains = [
            [rotate_x, rotate_y, rotate_z],
            [rotate_y, rotate_z],
            [rotate_x, rotate_z],
            [rotate_x, rotate_y],
            [rotate_x],
            [rotate_y],
            [rotate_z],
        ]
        chain = random.choice(chains)
        random.shuffle(chain)
        base_angle = angle
        for operation in chain:
            # as described in online aug frontend: when using 180 as angle and a set it to fixed, this
            # allows also 90 degrees as fixed rotation choice
            if fixed_rotation_angle and (base_angle == 180 or base_angle == -180):
                angle = random.choice([90, 180])

            else:
                angle = random.randint(0, base_angle)
            angle = -1 * angle if random.randint(0, 1) else angle

            volume, mask = operation(volume, mask, angle)
    else:
        angle = random.randint(0, angle)
        angle = -1 * angle if random.randint(0, 1) else angle
        volume, mask = random.choice([rotate_x, rotate_y, rotate_z])(volume, mask, angle)
    return volume, mask


def execute_random_tilt(volume, mask, *args, **kwargs):
    start_factor = 1 / 4
    end_factor = 3 / 4

    if len(args) == 2 and ('start_factor' in kwargs.keys() or 'end_factor' in kwargs.keys()):
        raise ValueError('execute_random_tilt takes either arguments or keyword arguments. Not both')

    if args and len(args) == 2:
        start_factor = min(args)
        end_factor = max(args)
        assert start_factor >= 0
        assert end_factor > start_factor
        assert end_factor <= 1

    if 'start_factor' in kwargs.keys():
        start_factor = kwargs['start_factor']
    if 'end_factor' in kwargs.keys():
        end_factor = kwargs['end_factor']

    if 'func' in kwargs.keys():
        function = kwargs['func']
    else:
        functions = [tilt_forward, tilt_backwards, tilt_left, tilt_right]
        function = random.choice(functions)

    f_kwargs = {}

    if random.randint(0, 1):
        start_factor = start_factor
        end_factor = end_factor
    else:
        if random.randint(0, 1):
            start_factor = 0
            end_factor = end_factor
        else:
            start_factor = start_factor
            end_factor = 1

    f_kwargs['start_factor'] = start_factor
    f_kwargs['end_factor'] = end_factor

    return apply_2d_function_to_volume(volume, mask, function, **f_kwargs)


def tilt_volume_backwards(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_backwards, **kwargs)


def tilt_volume_forwards(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_forward, **kwargs)


def tilt_volume_left(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_left, **kwargs)


def tilt_volume_right(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_right, **kwargs)


def random_squeeze_execution(volume, mask, axis, scale_factor=None):
    if scale_factor is None:
        scale_factor = random.randint(20, 80) / 100

    if axis == 0:
        zoom_factors = (scale_factor, 1, 1)
    elif axis == 1:
        zoom_factors = (1, scale_factor, 1)
    else:
        zoom_factors = (1, 1, scale_factor)

    new_volume = zoom(volume, zoom_factors)
    new_mask = zoom(mask, zoom_factors, mode='nearest', order=0)
    return new_volume, new_mask


def squeeze_x(volume, mask, scale_factor=None):
    return random_squeeze_execution(volume, mask, 2, scale_factor)


def squeeze_y(volume, mask, scale_factor=None):
    return random_squeeze_execution(volume, mask, 1, scale_factor)


def squeeze_z(volume, mask, scale_factor=None):
    return random_squeeze_execution(volume, mask, 0, scale_factor)


def squeeze_xy(volume, mask, scale_factor=None):
    volume, mask = squeeze_x(volume, mask, scale_factor)
    volume, mask = squeeze_y(volume, mask, scale_factor)
    return volume, mask


def squeeze_xz(volume, mask, scale_factor=None):
    volume, mask = squeeze_x(volume, mask, scale_factor)
    volume, mask = squeeze_z(volume, mask, scale_factor)
    return volume, mask


def squeeze_yz(volume, mask, scale_factor=None):
    volume, mask = squeeze_y(volume, mask, scale_factor)
    volume, mask = squeeze_z(volume, mask, scale_factor)
    return volume, mask


def squeeze_xyz(volume, mask, scale_factor=None):
    volume, mask = squeeze_x(volume, mask, scale_factor)
    volume, mask = squeeze_y(volume, mask, scale_factor)
    volume, mask = squeeze_z(volume, mask, scale_factor)
    return volume, mask


def random_squeeze(volume, mask, scale_factor=None):
    functions = [squeeze_x, squeeze_y, squeeze_z, squeeze_xy, squeeze_xz, squeeze_yz, squeeze_xyz]
    return random.choice(functions)(volume, mask, scale_factor)


def random_crop(volume, mask, min_size=16, cropped_size=None):
    """
    Crops random height and width but not depth
    Args:
        volume:
        mask:
        min_size: min dim of the new volume
        cropped_size: none or: (depth, width, height)

    Returns:

    """
    d, h, w = volume.shape

    if cropped_size is None:
        try:
            h_start = random.randint(0, h // 4 - min_size // 2)
            w_start = random.randint(0, w // 4 - min_size // 2)

            h_end = random.randint(h // 4 + min_size // 2, h)
            w_end = random.randint(w // 4 + min_size // 2, w)
        except ValueError:
            # raised for example when: d // 2 - min_size // 2 <= 0. randint(0, 0) fails
            return None, None

    else:
        if isinstance(cropped_size, int):
            cropped_size = (cropped_size, cropped_size, cropped_size)

        try:
            max_start_height = h - cropped_size[1]
            max_start_width = w - cropped_size[2]

            h_start = random.randint(0, max_start_height)
            w_start = random.randint(0, max_start_width)

            h_end = h_start + cropped_size[1]
            w_end = w_start + cropped_size[2]
        except ValueError:
            return None, None

    return volume[:, h_start:h_end, w_start:w_end], mask[:, h_start:h_end, w_start:w_end]


def flip_x(volume, mask):
    d, h, w = volume.shape

    new_volume = np.zeros_like(volume)
    new_mask = np.zeros_like(mask)

    new_x = 0
    for x in range(w - 1, -1, -1):
        new_volume[:, :, new_x] = volume[:, :, x]
        new_mask[:, :, new_x] = mask[:, :, x]
        new_x += 1
    return new_volume, new_mask


def flip_y(volume, mask):
    d, h, w = volume.shape

    new_volume = np.zeros_like(volume)
    new_mask = np.zeros_like(mask)

    new_y = 0
    for y in range(h - 1, -1, -1):
        new_volume[:, new_y, :] = volume[:, y, :]
        new_mask[:, new_y, :] = mask[:, y, :]
        new_y += 1
    return new_volume, new_mask


def flip_z(volume, mask):
    d, h, w = volume.shape

    new_volume = np.zeros_like(volume)
    new_mask = np.zeros_like(mask)

    new_z = 0
    for z in range(d - 1, -1, -1):
        new_volume[new_z, :, :] = volume[z, :, :]
        new_mask[new_z, :, :] = mask[z, :, :]
        new_z += 1
    return new_volume, new_mask


def flip_xy(volume, mask):
    volume, mask = flip_x(volume, mask)
    volume, mask = flip_y(volume, mask)
    return volume, mask


def flip_xz(volume, mask):
    volume, mask = flip_x(volume, mask)
    volume, mask = flip_z(volume, mask)
    return volume, mask


def flip_yz(volume, mask):
    volume, mask = flip_y(volume, mask)
    volume, mask = flip_z(volume, mask)
    return volume, mask


def flip_xyz(volume, mask):
    volume, mask = flip_x(volume, mask)
    volume, mask = flip_y(volume, mask)
    volume, mask = flip_z(volume, mask)
    return volume, mask


def random_flip(volume, mask, depth_flip_allowed=True):
    functions = [flip_x, flip_y, flip_z, flip_xy, flip_xz, flip_yz, flip_xyz]
    if not depth_flip_allowed:
        functions = [flip_x, flip_y, flip_xy]
    return random.choice(functions)(volume, mask)


def random_tilt(volume, mask):
    functions = [tilt_volume_forwards, tilt_volume_backwards, tilt_volume_left, tilt_volume_right]
    return random.choice(functions)(volume, mask)
