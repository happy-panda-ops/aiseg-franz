import warnings

import albumentations as A
import math
import cv2
import numpy as np
import random

from scipy import ndimage


def get_rotated_rectangle_with_max_area(w, h, angle):
    """
    https://stackoverflow.com/questions/16702966/rotate-image-and-crop-out-black-borders

    Given a rectangle of size wxh that has been rotated by 'angle' (in
    degrees), computes the width and height of the largest possible
    axis-aligned rectangle (maximal area) within the rotated rectangle.

    Args:
        w: width before image was rotated
        h: height before image was rotated
        angle: in degrees

    Returns:

    """
    angle = math.radians(angle)

    if w <= 0 or h <= 0:
        return 0, 0

    width_is_longer = w >= h
    side_long, side_short = (w, h) if width_is_longer else (h, w)

    # since the solutions for angle, -angle and 180-angle are all the same,
    # if suffices to look at the first quadrant and the absolute values of sin,cos:
    sin_a, cos_a = abs(math.sin(angle)), abs(math.cos(angle))
    if side_short <= 2. * sin_a * cos_a * side_long or abs(sin_a - cos_a) < 1e-10:
        # half constrained case: two crop corners touch the longer side,
        #   the other two corners are on the mid-line parallel to the longer line
        x = 0.5 * side_short
        wr, hr = (x / sin_a, x / cos_a) if width_is_longer else (x / cos_a, x / sin_a)
    else:
        # fully constrained case: crop touches all 4 sides
        cos_2a = cos_a * cos_a - sin_a * sin_a
        wr, hr = (w * cos_a - h * sin_a) / cos_2a, (h * cos_a - w * sin_a) / cos_2a

    return int(round(wr, 2)), int(round(hr, 2))


def rotate_by_angle(img, mask, angle=None, crop_img=True):
    """

    Args:
        img:
        mask:
        angle: in degrees
        crop_img: if true, no black boundaries are given

    Returns:

    """
    if angle is None:
        angle = random.randint(15, 30)
        angle = -1 * angle if random.randint(0, 1) else angle

    h, w = img.shape[:2]

    img = ndimage.rotate(img, angle, reshape=True)
    mask = ndimage.rotate(mask, angle, reshape=True, mode='nearest', order=0)

    if crop_img:
        width, height = get_rotated_rectangle_with_max_area(w, h, angle)

        image_size = (img.shape[1], img.shape[0])
        image_center = (int(image_size[0] * 0.5), int(image_size[1] * 0.5))

        if width > image_size[0]:
            width = image_size[0]

        if height > image_size[1]:
            height = image_size[1]

        x1 = int(image_center[0] - width * 0.5)
        x2 = int(image_center[0] + width * 0.5)
        y1 = int(image_center[1] - height * 0.5)
        y2 = int(image_center[1] + height * 0.5)

        img = img[y1:y2, x1:x2]
        mask = mask[y1:y2, x1:x2]

    return img, mask


def squeeze_height(img, mask, scale_factor=None):
    if scale_factor is None:
        scale_factor = random.randint(50, 80) / 100
    h, w = img.shape[:2]
    h = int(round(h * scale_factor))
    img = cv2.resize(img, (w, h))
    mask = cv2.resize(mask, (w, h), interpolation=cv2.INTER_NEAREST)
    return img, mask


def squeeze_width(img, mask, scale_factor=None):
    if scale_factor is None:
        scale_factor = random.randint(50, 80) / 100
    h, w = img.shape[:2]
    w = int(round(w * scale_factor))
    img = cv2.resize(img, (w, h))
    mask = cv2.resize(mask, (w, h), interpolation=cv2.INTER_NEAREST)
    return img, mask


def random_squeeze(img, mask, scale_factor=None):
    if random.randint(0, 1):
        return squeeze_height(img, mask, scale_factor)
    return squeeze_width(img, mask, scale_factor)


def resize(img, mask, scale_factor=None):
    if scale_factor is None:
        scale_factor = random.randint(5, 15) / 10
    h, w = img.shape[:2]
    h = int(round(h * scale_factor))
    w = int(round(w * scale_factor))
    img = cv2.resize(img, (w, h))
    mask = cv2.resize(mask, (w, h), interpolation=cv2.INTER_NEAREST)
    return img, mask


def random_crop(img, mask, cropped_size=None):
    """

    Args:
        img:
        mask:
        cropped_size: int or tuple of (height, width)
    """
    if cropped_size is None:
        try:
            h, w = img.shape[:2]
            if h > w:
                cropped_size = random.randint(w // 2, w - 10)
            else:
                cropped_size = random.randint(h // 2, h - 10)
        except ValueError:
            return img, mask

    if isinstance(cropped_size, int):
        cropped_size = (cropped_size, cropped_size)

    h, w = img.shape[:2]
    max_start_width_index = w - cropped_size[1] - 1
    max_start_height_index = h - cropped_size[0] - 1

    if max_start_width_index < 0:
        max_start_width_index = 0
    if max_start_height_index < 0:
        max_start_height_index = 0

    w_start = random.randint(0, max_start_width_index)
    h_start = random.randint(0, max_start_height_index)
    w_end = w_start + cropped_size[1]
    h_end = h_start + cropped_size[0]

    img = img[h_start:h_end, w_start:w_end]
    mask = mask[h_start:h_end, w_start:w_end]
    return img, mask


def get_perspective_mat(h, w, dst):
    """
    src:
        bottom_left = (0, h)
        bottom_right = (w, h)
        top_left = (0, 0)
        top_right = (w, 0)
    dst are the same coordinates but distorted.

    Args:
        h: height of original img
        w: width of original img
        dst: distorted corner coordinates

    Returns:

    """
    bottom_left = np.array([0, h], dtype=np.float32)
    bottom_right = np.array([w, h], dtype=np.float32)
    top_left = np.array([0, 0], dtype=np.float32)
    top_right = np.array([w, 0], dtype=np.float32)

    src = np.array([
        bottom_left,
        bottom_right,
        top_left,
        top_right
    ])

    warp_mat = cv2.getPerspectiveTransform(src, dst)
    return warp_mat


def tilt_backwards(img, mask, start_factor=None, end_factor=None):
    h, w = img.shape[:2]
    if start_factor is None:
        start_factor = 1 / 4
    if end_factor is None:
        end_factor = 3 / 4

    w_start = int(math.ceil(start_factor * w))
    w_end = int((end_factor * w))

    dst_bottom_left = np.array([0, h], dtype=np.float32)
    dst_bottom_right = np.array([w, h], dtype=np.float32)
    dst_top_left = np.array([w_start, 0], dtype=np.float32)
    dst_top_right = np.array([w_end, 0], dtype=np.float32)

    dst = np.array([
        dst_bottom_left,
        dst_bottom_right,
        dst_top_left,
        dst_top_right,
    ])

    warp_mat = get_perspective_mat(h, w, dst)

    img = cv2.warpPerspective(img, warp_mat, (w, h))
    mask = cv2.warpPerspective(mask, warp_mat, (w, h), flags=cv2.INTER_NEAREST)

    # due to inter nearest, backward tilting gets a black bar at the bottom.
    img = img[:h - 1, w_start:w_end]
    mask = mask[:h - 1, w_start:w_end]

    return img, mask


def tilt_forward(img, mask, start_factor=None, end_factor=None):
    h, w = img.shape[:2]
    if start_factor is None:
        start_factor = 1 / 4
    if end_factor is None:
        end_factor = 3 / 4

    w_start = int(math.ceil(start_factor * w))
    w_end = int((end_factor * w))

    dst_top_left = np.array([0, 0], dtype=np.float32)
    dst_top_right = np.array([w, 0], dtype=np.float32)
    dst_bottom_left = np.array([w_start, h], dtype=np.float32)
    dst_bottom_right = np.array([w_end, h], dtype=np.float32)

    dst = np.array([
        dst_bottom_left,
        dst_bottom_right,
        dst_top_left,
        dst_top_right,
    ])

    warp_mat = get_perspective_mat(h, w, dst)

    img = cv2.warpPerspective(img, warp_mat, (w, h))
    mask = cv2.warpPerspective(mask, warp_mat, (w, h), flags=cv2.INTER_NEAREST)

    img = img[:, w_start:w_end]
    mask = mask[:, w_start:w_end]

    return img, mask


def tilt_left(img, mask, start_factor=None, end_factor=None):
    h, w = img.shape[:2]
    if start_factor is None:
        start_factor = 1 / 4
    if end_factor is None:
        end_factor = 3 / 4

    h_start = int(math.ceil(start_factor * h))
    h_end = int((end_factor * h))

    dst_top_left = np.array([0, h_start], dtype=np.float32)
    dst_top_right = np.array([w, 0], dtype=np.float32)
    dst_bottom_left = np.array([0, h_end], dtype=np.float32)
    dst_bottom_right = np.array([w, h], dtype=np.float32)

    dst = np.array([
        dst_bottom_left,
        dst_bottom_right,
        dst_top_left,
        dst_top_right,
    ])

    warp_mat = get_perspective_mat(h, w, dst)

    img = cv2.warpPerspective(img, warp_mat, (w, h))
    mask = cv2.warpPerspective(mask, warp_mat, (w, h), flags=cv2.INTER_NEAREST)

    img = img[h_start:h_end, :]
    mask = mask[h_start:h_end, :]

    return img, mask


def tilt_right(img, mask, start_factor=None, end_factor=None):
    h, w = img.shape[:2]
    if start_factor is None:
        start_factor = 1 / 4
    if end_factor is None:
        end_factor = 3 / 4

    h_start = int(math.ceil(start_factor * h))
    h_end = int((end_factor * h))

    dst_top_left = np.array([0, 0], dtype=np.float32)
    dst_top_right = np.array([w, h_start], dtype=np.float32)
    dst_bottom_left = np.array([0, h], dtype=np.float32)
    dst_bottom_right = np.array([w, h_end], dtype=np.float32)

    dst = np.array([
        dst_bottom_left,
        dst_bottom_right,
        dst_top_left,
        dst_top_right,
    ])

    warp_mat = get_perspective_mat(h, w, dst)

    img = cv2.warpPerspective(img, warp_mat, (w, h))
    mask = cv2.warpPerspective(mask, warp_mat, (w, h), flags=cv2.INTER_NEAREST)

    img = img[h_start:h_end, :]
    mask = mask[h_start:h_end, :]

    return img, mask


def execute_random_tilt(img, mask, *args, **kwargs):
    start_factor = 1 / 4
    end_factor = 3 / 4

    functions = [tilt_forward, tilt_backwards, tilt_left, tilt_right]
    function = functions[random.randint(0, len(functions) - 1)]

    if len(args) == 2 and ('start_factor' in kwargs.keys() or 'end_factor' in kwargs.keys()):
        raise ValueError('execute_random_tilt takes either arguments or keyword arguments for start factors. Not both')

    if args and len(args) == 2:
        start_factor = min(args)
        end_factor = max(args)
        assert start_factor >= 0
        assert end_factor > start_factor
        assert end_factor <= 1

    if 'start_factor' in kwargs.keys():
        start_factor = kwargs['start_factor']
    if 'end_factor' in kwargs.keys():
        end_factor = kwargs['end_factor']

    return function(img, mask, start_factor, end_factor)


def elastic_distortion(img, mask):
    transform = A.Compose([
        A.ElasticTransform(always_apply=True),
    ])
    transformed = transform(image=img, mask=mask)
    img = transformed['image']
    mask = transformed['mask']
    return img, mask


def grid_distortion(img, mask):
    transform = A.Compose([
        A.GridDistortion(always_apply=True, distort_limit=0.5),
    ])
    transformed = transform(image=img, mask=mask)
    img = transformed['image']
    mask = transformed['mask']
    return img, mask


def optical_distortion(img, mask):
    transform = A.Compose([
        A.OpticalDistortion(always_apply=True, distort_limit=0.7, shift_limit=0.7),
    ])
    transformed = transform(image=img, mask=mask)
    img = transformed['image']
    mask = transformed['mask']
    return img, mask


def random_grid_shuffle(img, mask):
    h, w = img.shape[:2]
    min_dim = min(h, w)
    if min_dim >= 1024:
        grid_selection = [(2, 2), (3, 3), (4, 4), (5, 5)]
    elif min_dim >= 768:
        grid_selection = [(2, 2), (3, 3), (4, 4)]
    elif min_dim >= 512:
        grid_selection = [(2, 2), (3, 3)]
    elif min_dim >= 256:
        grid_selection = [(2, 2)]
    else:
        return img, mask
    grid = random.choice(grid_selection)
    transform = A.Compose([
        A.RandomGridShuffle(grid=grid, p=1),
    ])
    with warnings.catch_warnings(record=True):
        # function RandomGridShuffle uses deprecated np.int so i suppress it:
        # DeprecationWarning: `np.int` is a deprecated alias for the builtin `int`. To silence this warning, use `int`
        # by itself. Doing this will not modify any behavior and is safe. When replacing `np.int`, you may wish to use
        # e.g. `np.int64` or `np.int32` to specify the precision. If you wish to review your current use, check the
        # release note link for additional information.
        # Deprecated in NumPy 1.20; for more details and guidance:
        # https://numpy.org/devdocs/release/1.20.0-notes.html#deprecations
        #   height_split = np.linspace(0, height, n + 1, dtype=np.int)
        transformed = transform(image=img, mask=mask)

    img = transformed['image']
    mask = transformed['mask']
    return img, mask


def flip_x(img, mask):
    """horizontal flip"""
    img = cv2.flip(img, 1)
    mask = cv2.flip(mask, 1)
    return img, mask


def flip_y(img, mask):
    """vertical flip"""
    img = cv2.flip(img, 0)
    mask = cv2.flip(mask, 0)
    return img, mask


def flip_xy(img, mask):
    """horizontal and vertical flip"""
    img = cv2.flip(img, -1)
    mask = cv2.flip(mask, -1)
    return img, mask


def execute_random_flip(img, mask, allow_vertical_flip=True):
    if allow_vertical_flip:
        functions = [flip_x, flip_y, flip_xy]
        return random.choice(functions)(img, mask)
    else:
        return flip_x(img, mask)
