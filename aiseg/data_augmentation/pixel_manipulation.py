import albumentations as A
import cv2
import logging
import numpy as np
import random


def gaussian_filter(img, mask, kernel_size=(5, 5), sigma=None):
    """
    Smooths the image but also edges
    Args:
        img:
        mask: no need to change the mask
        kernel_size: tuple, size in x and y
        sigma: int

    Returns: blurred img, mask

    """
    if not isinstance(kernel_size, tuple):
        kernel_size = (kernel_size, kernel_size)

    not_odd = False
    if kernel_size[0] % 2 == 0:
        kernel_size[0] += 1
        not_odd = True
    if kernel_size[1] % 2 == 0:
        kernel_size[1] += 1
        not_odd = True

    if not_odd:
        logging.info(
            f'Kernel size for gaussian filter must be odd. Setting kernel size to: ({kernel_size[0]}, {kernel_size[1]})'
        )

    # kernel size should be 3*sigma to use whole gauss bell and leave edges at 0
    # sigma is standard deviation, look at gauss formula if more info is needed
    if sigma is None:
        sigma = int(kernel_size[0] / 3)
    return cv2.GaussianBlur(img, kernel_size, sigmaX=sigma), mask


def sharpen(img, mask):
    """
    simply calculate a blurred image and then subtract from original but with adjusted weight
    """
    # giving more weight to weaker kernel
    kernels = [
        np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]]),
        np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]]),
        np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]]),
        np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]]),
    ]
    img = cv2.filter2D(img, -1, random.choice(kernels))
    return img, mask


def to_gray_and_non_local_means_filter(img, mask, strength=9, kernel=7, search_kernel=21):
    """
    converts to gray!

    Best way to smooth images and preserve edges but heaviest

    Idea:
    Imagine you have 10 images of the same scene and every image is noised. If you compute the average over
    all images, you will heavily reduce the noise.
    Now we only have one Image:

    The filter looks at the current region of the image and then takes all almost equal regions in
    the image search kernel and calculates a weighted average of these regions.

    Args:
        img: gray image
        mask: is simply returned
        strength: Parameter regulating filter strength. Big value perfectly removes noise but also removes image
            details, smaller h value preserves details but also preserves some noise
        kernel: Template patch that is used to compute weights, Recommended: 7
        search_kernel: Window that is used to compute weighted average for given pixel

    Returns: denoised image, mask

    """
    is_gray = True
    if len(img.shape) == 3:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        is_gray = False
    else:
        gray = img

    img = cv2.fastNlMeansDenoising(gray, None, h=strength, templateWindowSize=kernel, searchWindowSize=search_kernel)
    if is_gray:
        return img, mask

    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img, mask


def contrast(img, mask, contrast_factor=None):
    """
    calculates the mean, subtracts the mean, multiplies with the contrast_factor
    -> negative values will get smaller, positive larger if > 1
    adds the mean again

    Args:
        img:
        mask:
        contrast_factor:

    Returns:

    """
    if contrast_factor is None:
        contrast_factor = random.randint(50, 150) / 100

    if len(img.shape) == 2:
        mean = np.mean(img)
        new_img = img.astype(np.float16)
        new_img -= mean
        new_img = new_img * contrast_factor
        new_img += mean
        new_img = np.around(new_img)
        new_img[new_img < 0] = 0
        new_img[new_img > 255] = 255
        new_img = new_img.astype(np.uint8)
    else:
        mean_b = np.mean(img[:, :, 0])
        mean_g = np.mean(img[:, :, 1])
        mean_r = np.mean(img[:, :, 2])

        new_img = img.astype(np.float16)
        new_img[:, :, 0] -= mean_b
        new_img[:, :, 1] -= mean_g
        new_img[:, :, 2] -= mean_r

        new_img = new_img * contrast_factor

        new_img[:, :, 0] += mean_b
        new_img[:, :, 1] += mean_g
        new_img[:, :, 2] += mean_r
        new_img = np.around(new_img)
        new_img[new_img > 255] = 255
        new_img[new_img < 0] = 0
        new_img = new_img.astype(np.uint8)

    return new_img, mask


def brightness(img, mask, brightness_offset=None):
    if len(img.shape) == 2:
        if brightness_offset is None:
            brightness_offset = random.randint(10, 30)
            if random.randint(0, 1):
                brightness_offset *= -1

        img = img.astype(np.float16) + brightness_offset
        img[img < 0] = 0
        img[img > 255] = 255
        img = img.astype(np.uint8)
    else:
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        h, s, v = cv2.split(hsv)

        if brightness_offset is None:
            brightness_offset = random.randint(10, 30)
            if random.randint(0, 1):
                brightness_offset *= -1

        v = cv2.add(v, brightness_offset)
        v[v > 255] = 255
        v[v < 0] = 0
        final_hsv = cv2.merge((h, s, v))
        img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img, mask


def to_gray_and_histogram_normalization(img, mask):
    was_color = False
    if len(img.shape) == 3:
        was_color = True
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.equalizeHist(img)
    if was_color:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img, mask


def to_gray_and_adaptive_histogram(img, mask):
    """CLAHE (Contrast Limited Adaptive Histogram Equalization)
    """
    was_color = False
    if len(img.shape) == 3:
        was_color = True
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    img = clahe.apply(img)
    if was_color:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img, mask


def random_erasing(img, mask, scale_factor=None, max_erasing=None):
    """
    adds a random patch somewhere into the image.
    The patch is either black, white or random noise
    color and gray is supported
    Args:
        img:
        mask:
        scale_factor: 0 > factor < 1 this will be multiplied with the image dimensions to get the maximum possible
            size of the patch (will be randomized)
        max_erasing: maximum number of erased areas
    """
    if scale_factor is None:
        scale_factor = 1 / 10
    if max_erasing is None:
        max_erasing = random.randint(1, 8)

    patch_max_w = int(scale_factor * img.shape[1])
    patch_max_h = int(scale_factor * img.shape[0])

    if patch_max_w < 4 or patch_max_h < 4:
        return img, mask

    out_img = np.copy(img)

    for i in range(max_erasing):
        patch_type = random.randint(1, 3)

        patch_w = random.randint(3, patch_max_w)
        patch_h = random.randint(3, patch_max_h)

        if patch_type == 1:  # black
            if len(img.shape) == 3:
                patch = np.zeros((patch_h, patch_w, 3), dtype=np.uint8)
            else:
                patch = np.zeros((patch_h, patch_w), dtype=np.uint8)

        elif patch_type == 2:  # white
            if len(img.shape) == 3:
                patch = np.full((patch_h, patch_w, 3), 255, dtype=np.uint8)
            else:
                patch = np.full((patch_h, patch_w), 255, dtype=np.uint8)

        else:  # noise
            if len(img.shape) == 3:
                patch = np.random.randint(low=0, high=255, size=(patch_h, patch_w, 3), dtype=np.uint8)
            else:
                patch = np.random.randint(low=0, high=255, size=(patch_h, patch_w), dtype=np.uint8)

        h, w = img.shape[:2]
        end_w = w - patch_w
        end_h = h - patch_h

        start_w = random.randint(0, end_w)
        start_h = random.randint(0, end_h)

        out_img[start_h:start_h + patch_h, start_w:start_w + patch_w] = patch[:, :]

    return out_img, mask


def gaussian_noise(img, mask):
    transform = A.Compose([
        A.GaussNoise(var_limit=(10, 50), always_apply=True),
    ])
    transformed = transform(image=img, mask=mask)
    img = transformed['image']
    mask = transformed['mask']
    return img, mask
