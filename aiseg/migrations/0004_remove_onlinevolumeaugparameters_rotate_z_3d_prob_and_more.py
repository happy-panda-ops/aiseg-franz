# Generated by Django 4.1 on 2023-08-23 09:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aiseg', '0003_onlinevolumeaugparameters_rotate_3d_prob_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='onlinevolumeaugparameters',
            name='rotate_z_3d_prob',
        ),
        migrations.AddField(
            model_name='onlinevolumeaugparameters',
            name='fixed_rotation_angle',
            field=models.BooleanField(default=False, help_text='Converts the maximum rotation angle to a fixed angle. If "Rotation around X and Y allowed" and this are activated, the random rotation rotates the volume randomly around all axes, not only one. In this case: If you set the angle to 180, the rotation will be 90 or 180 for each axis to boost randomness.', verbose_name='Use fixed rotation angle'),
        ),
        migrations.AlterField(
            model_name='onlinevolumeaugparameters',
            name='max_rot_angle',
            field=models.PositiveSmallIntegerField(default=180, help_text='Maximum angle, the volume gets rotated (+- this angle). If "Rotation around X and Y allowed" is activated, the volume is rotated only around one of the three axes, as this can cause unwanted changes to the information of the input and target. Use a fixed rotation angle (such as 90, 180, ..) to allow for the rotation around multiple axes simultaneously.', verbose_name='Maximum rotation angle'),
        ),
    ]
