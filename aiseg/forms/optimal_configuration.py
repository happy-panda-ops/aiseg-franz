from django import forms

from aiseg import constants
from aiseg.models import TrainingConfiguration, InferenceConfiguration


class InferenceConfigurationForm(forms.ModelForm):

    class Meta:
        model = InferenceConfiguration
        fields = [
            'input_height',
            'input_depth',
            'batch_size',
            'approximated_ram_usage',
        ]
        widgets = {
            'approximated_ram_usage': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_height': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_depth': forms.NumberInput(attrs={'class': 'form-control'}),
            'batch_size': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance and self.instance.pk is not None:
            if self.instance.architecture in constants.TWO_D_NETWORKS:
                self.fields['input_depth'].disabled = True
                self.fields['input_depth'].required = False

    def save(self, commit=True):
        optimal_configuration = super().save(commit=False)

        if 'input_height' in self.cleaned_data and self.cleaned_data['input_height']:
            optimal_configuration.input_width = self.cleaned_data['input_height']

        if commit:
            optimal_configuration.save()

        return optimal_configuration


class OptimalConfigurationForm(forms.ModelForm):

    class Meta:
        model = TrainingConfiguration
        fields = [
            'input_height',
            'input_depth',
            'batch_size',
            'approximated_ram_usage',
        ]
        widgets = {
            'approximated_ram_usage': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_height': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_depth': forms.NumberInput(attrs={'class': 'form-control'}),
            'batch_size': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance and self.instance.pk is not None:
            if self.instance.architecture in constants.TWO_D_NETWORKS or self.instance.fixed_depth:
                self.fields['input_depth'].disabled = True
                self.fields['input_depth'].required = False

    def save(self, commit=True):
        optimal_configuration = super().save(commit=False)

        if 'input_height' in self.cleaned_data and self.cleaned_data['input_height']:
            optimal_configuration.input_width = self.cleaned_data['input_height']

        if commit:
            optimal_configuration.save()

        return optimal_configuration
