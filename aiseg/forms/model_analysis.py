import os

import torch.cuda
from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.safestring import mark_safe

from aiseg import constants
from aiseg.models import ModelAnalysis
from aiseg.utils import get_gpu_memory


class AnalysisCustomNameForm(forms.ModelForm):
    class Meta:
        model = ModelAnalysis
        fields = [
            'custom_name',
        ]
        widgets = {
            'custom_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Set a custom name. Empty for reset.',
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial['custom_name'] = ''
        self.fields['custom_name'].required = False
        
    def has_changed(self):
        has_changed = super(AnalysisCustomNameForm, self).has_changed()
        self.instance.refresh_from_db()
        if 'custom_name' in self.cleaned_data and self.instance.custom_name:
            has_changed = True
        return has_changed
    
    def save(self, commit=True):
        analysis = super(AnalysisCustomNameForm, self).save(commit=False)
        fcd = self.cleaned_data

        if self.has_changed() and 'custom_name' in fcd:
            analysis.custom_name = fcd['custom_name']

        if commit:
            analysis.save()
        return analysis


class AnalysesCleanUpForm(forms.Form):
    remove_with_less_epochs_than = forms.IntegerField(
        validators=[MinValueValidator(0)],
        initial=0,
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
        help_text='Remove all analyses with histories containing less epochs than this value.'
    )

    def __init__(self, *args, **kwargs):
        super(AnalysesCleanUpForm, self).__init__(*args, **kwargs)


class RunTestForm(forms.Form):
    checkpoint_path = forms.CharField(
        max_length=255,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Path to related checkpoint',
            },
        ),
    )
    device = forms.ChoiceField(
        label='Device',
        choices=([]),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    overlap = forms.IntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(90)],
        label='Overlap',
        initial=50,
        required=True,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Overlap', 'unit': '%'}),
    )

    def __init__(self, *args, **kwargs):
        if 'dataset_folder_name' in kwargs:
            self.dataset_folder_name = kwargs['dataset_folder_name']
            kwargs.pop('dataset_folder_name')
        else:
            self.dataset_folder_name = None

        if 'architecture' in kwargs:
            self.architecture = kwargs['architecture']
            kwargs.pop('architecture')
        else:
            self.architecture = None

        if 'backbone' in kwargs:
            self.backbone = kwargs['backbone']
            kwargs.pop('backbone')
        else:
            self.backbone = None

        if 'disable' in kwargs:
            self.disable = kwargs['disable']
            kwargs.pop('disable')
        else:
            self.disable = False

        if 'disable_testing' in kwargs:
            self.disable_testing = kwargs['disable_testing']
            kwargs.pop('disable_testing')
        else:
            self.disable_testing = False

        if args:
            if 'run_inference' in args[0]:
                self.is_test_calculation = False
            elif 'run_test' in args[0]:
                self.is_test_calculation = True
        else:
            self.is_test_calculation = None

        super().__init__(*args, **kwargs)

        device_choices = [('cpu', 'use CPU')]
        if torch.cuda.is_available():
            device_count = torch.cuda.device_count()
            if device_count == 1:
                device_choices.append(('cuda:0', 'use GPU'))
            else:
                for i in range(device_count):
                    device_choices.append((f'cuda:{i}', f'use GPU:{i}'))

        default_device = 'cpu'
        if torch.cuda.is_available():
            device_count = torch.cuda.device_count()
            if device_count == 1:
                default_device = 'cuda:0'
            else:
                memory_used_values, memory_total_values = get_gpu_memory()
                default_device = f'cuda:{memory_used_values.index(min(memory_used_values))}'

        self.fields['device'].choices = device_choices
        self.fields['device'].initial = default_device

        if self.disable:
            for field in self.fields:
                self.fields[field].widget.attrs['disabled'] = True
                if field == 'checkpoint_path':
                    self.fields[field].widget.attrs['placeholder'] = 'There are processes running, wait for them to ' \
                                                                     'finish.'
        if self.disable_testing:
            self.fields['checkpoint_path'].widget.attrs[
                'placeholder'] += ' (only timing test possible)'

    def __load_model_information(self):
        """
        Returns: architecture name, backbone
        """
        fcd = self.cleaned_data
        checkpoint = torch.load(fcd['checkpoint_path'], map_location=torch.device('cpu'))
        checkpoint_keys = checkpoint.keys()

        backbone = ''
        if 'backbone' in checkpoint_keys:
            backbone = checkpoint['backbone']

        return checkpoint['architecture'], backbone

    def clean(self):
        fcd = super().clean()

        if not os.path.isfile(fcd['checkpoint_path']):
            self.add_error('checkpoint_path', 'The path does not exist!')
            return fcd

        architecture, backbone = self.__load_model_information()

        if architecture != self.architecture:
            self.add_error(
                'checkpoint_path',
                mark_safe(
                    f'The architecture of the checkpoint does not relate to this analysis!<br>'
                    f'The loaded architecture is: {architecture}; Backbone: {backbone}!'
                ),
            )
        elif backbone != self.backbone:
            self.add_error(
                'checkpoint_path',
                mark_safe(
                    f'The architectures backbone of the checkpoint does not relate to this analysis!<br>'
                    f'The loaded architecture is: {architecture}; Backbone: {backbone}!'
                ),
            )
        elif self.is_test_calculation:

            if self.dataset_folder_name is not None:
                dataset_dir = os.path.join(constants.DATASET_FOLDER, self.dataset_folder_name)
                test_data_dir = os.path.join(dataset_dir, 'test')
            else:
                dataset_dir = None
                test_data_dir = None
                self.add_error('checkpoint_path', 'Error: the dataset to test on was not found.')

            if dataset_dir is not None:
                if not os.path.isdir(dataset_dir):
                    self.add_error('checkpoint_path', 'Error: The dataset does not exist.')
                elif not os.path.isdir(test_data_dir):
                    self.add_error('checkpoint_path', 'Error: The dataset does not contain a testing folder.')
        return fcd
