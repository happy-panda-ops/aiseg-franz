import os.path

import torch
from django import forms
from django.contrib import messages
from django.core.validators import MinValueValidator
from django.utils.safestring import mark_safe

from aiseg import constants
from aiseg.file_handling.read import read_csv_to_list
from aiseg.file_handling.utils import get_sub_folders
from aiseg.models import TrainingParameters
from aiseg.models import TrainingContinuationParameters


class TrainingInputForm(forms.ModelForm):
    dataset_choice = forms.ChoiceField(
        label='Dataset',
        choices=([]),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
        help_text='Select the folder name of the training dataset located inside segmentation_datasets/.',
    )

    calculate_with_fixed_depth = forms.BooleanField(
        label='Calculate with fixed depth',
        initial=False,
        required=False,
        help_text='Select, if you want to calculate using a fixed depth (for 3D models only, set an input depth).',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'})
    )
    min_dataset_size = min(
        min([choice[0] for choice in constants.IN_SIZE_CHOICES_3D]),
        min([choice[0] for choice in constants.IN_SIZE_CHOICES_2D]),
    )
    max_dataset_size = max(
        max([choice[0] for choice in constants.IN_SIZE_CHOICES_3D]),
        max([choice[0] for choice in constants.IN_SIZE_CHOICES_2D]),
    )
    max_in_size = forms.IntegerField(
        label='Max dataset input size',
        validators=[
            MinValueValidator(min_dataset_size),
        ],
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
        help_text=f'Optional. Specify the maximum input size of your dataset images. '
                  f'If empty: {max_dataset_size} is set as a maximum (likely not reachable anyways). '
                  f'Speeds up calculation when an upper boundary < {max_dataset_size} is given.'
    )

    class Meta:
        model = TrainingParameters
        fields = [
            'dataset_choice',  # replaces dataset -> set at save method to allow dynamic choices if user adds a folder
            'architecture',
            'backbone',
            'normalization',

            'pretrained',
            'auto_weight',
            'online_aug',
            'reproduce',

            'channels',
            'classes',
            'input_size',
            'input_depth',

            'batch_size',
            'epochs',
            'fine_tuning_epoch',
            'save_every',

            'device',
            'num_gpus',
            'optimizer_name',

            'learning_rate',
            'lr_scheduler',

            'lr_adjust_step_size',
            'lr_warm_up_period',
            'lr_constant_period',
            'lr_decay_period',

            'calculate_with_fixed_depth',
            'max_in_size',
        ]
        widgets = {
            'pretrained': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'reproduce': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'online_aug': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'auto_weight': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'architecture': forms.Select(attrs={'class': 'form-control'}),
            'backbone': forms.Select(attrs={'class': 'form-control'}),
            'channels': forms.Select(attrs={'class': 'form-control'}),
            'device': forms.Select(attrs={'class': 'form-control'}),
            'normalization': forms.Select(attrs={'class': 'form-control'}),
            'optimizer_name': forms.Select(attrs={'class': 'form-control'}),
            'lr_scheduler': forms.Select(attrs={'class': 'form-control'}),

            'classes': forms.NumberInput(attrs={'class': 'form-control'}),
            'epochs': forms.NumberInput(attrs={'class': 'form-control'}),
            'save_every': forms.NumberInput(attrs={'class': 'form-control'}),
            'batch_size': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_size': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_depth': forms.NumberInput(attrs={'class': 'form-control'}),
            'online_aug_start_epoch': forms.NumberInput(attrs={'class': 'form-control'}),
            'learning_rate': forms.NumberInput(attrs={'class': 'form-control'}),
            'online_aug_strength': forms.NumberInput(attrs={'class': 'form-control'}),
            'num_gpus': forms.NumberInput(attrs={'class': 'form-control'}),
            'lr_adjust_step_size': forms.NumberInput(attrs={'class': 'form-control'}),
            'lr_warm_up_period': forms.NumberInput(attrs={'class': 'form-control'}),
            'lr_constant_period': forms.NumberInput(attrs={'class': 'form-control'}),
            'lr_decay_period': forms.NumberInput(attrs={'class': 'form-control', 'disabled': ''}),
            'fine_tuning_epoch': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # will be set/checked during clean
        not_required_fields = [
            'lr_decay_period',
            'lr_adjust_step_size',
            'lr_warm_up_period',
            'lr_constant_period',
        ]

        dataset_choices = []
        datasets = get_sub_folders(constants.DATASET_FOLDER, depth=1)
        for folder in datasets:
            sub_folders = get_sub_folders(folder, depth=1)
            sub_folders = [os.path.basename(sub_folder) for sub_folder in sub_folders]
            if 'images' in sub_folders and 'masks' in sub_folders:
                dataset_choices.append((os.path.basename(folder), os.path.basename(folder)))

        dataset_choice_keys = [dataset_choice[0] for dataset_choice in dataset_choices]
        if self.instance is not None and self.instance.pk is not None:
            if self.instance.dataset and self.instance.dataset not in dataset_choice_keys:
                dataset_choices.append(('__deleted__', f'{self.instance.dataset} (deleted!)'))

            if self.instance.device != 'cuda':
                # multi gpu leave empty or set to previous value
                self.fields['num_gpus'].initial = 2

        dataset_choices = sorted(dataset_choices, key=lambda x: x[1])

        for field in self.fields:
            if field in not_required_fields:
                self.fields[field].required = False

            if field == 'dataset_choice':
                if not dataset_choices or (len(dataset_choices) == 1 and dataset_choices[0][0] == '__deleted__'):
                    messages.add_message(
                        request,
                        messages.INFO,
                        mark_safe(
                            'There are no datasets to choose from. Read '
                            '<a class="form-a" '
                            'href="https://gitlab.com/fra-wa/aiseg/-/blob/master/README.md">Readme</a> '
                            'how to setup your data correctly!'
                        ),
                    )
                self.fields[field].choices = dataset_choices
                if self.instance and self.instance.pk is not None:
                    if self.instance.dataset not in dataset_choice_keys:
                        self.fields[field].initial = '__deleted__'
                    else:
                        self.fields[field].initial = self.instance.dataset

    def clean(self):
        fcd = super().clean()
        self.data = self.data.copy()
        is_required_error = 'This field is required.'
        if 'dataset_choice' in fcd and fcd['dataset_choice'] == '__deleted__':
            self.add_error(
                'dataset_choice',
                'The dataset does not exist on this PC!'
            )
        else:
            # pre-check if classes are correct. If no such file exists, the dataset will be preprocessed anyway
            folder_is_valid_file = os.path.join(constants.DATASET_FOLDER, fcd['dataset_choice'], 'masks_are_valid.txt')
            if os.path.isfile(folder_is_valid_file):
                valid_content = read_csv_to_list(folder_is_valid_file)
                expected_classes = None
                for line in valid_content:
                    if 'classes' in line:
                        expected_classes = line[-1]

                if expected_classes is not None and expected_classes != fcd['classes']:
                    self.add_error(
                        'classes',
                        f'The dataset has {int(expected_classes)} classes, but you set {fcd["classes"]}.'
                    )

        if 'num_gpus' in fcd:
            if fcd['num_gpus']:
                if fcd['num_gpus'] > torch.cuda.device_count():
                    self.add_error(
                        'num_gpus',
                        f'Only {torch.cuda.device_count()} GPUs are available.'
                    )
            elif 'device' in fcd and fcd['device'] == 'cuda' and not fcd['num_gpus']:
                self.add_error(
                    'num_gpus',
                    f'Set the number of gpus to use.'
                )

        if 'pretrained' in fcd and fcd['pretrained']:
            if fcd['normalization'] == constants.GROUP_NORM:
                self.add_error(
                    'pretrained',
                    f'You are using Group Normalization (GN). If pretrained=True, the weights are '
                    f'loaded using Batch Normalization (BN) and therefore unusable with GN. Train from scratch then. '
                    f'Rule of thumb: train from scratch with GN (saves VRAM). If you want a pretrained net, use BN and '
                    f'a batch size >= 16 for optimal performance. Deactivate pretrained then.'
                )
            if fcd['channels'] == 1:
                self.add_error(
                    'pretrained',
                    f'You are using grayscale. If pretrained=True, the weights are loaded using RGB (ImageNet). '
                    f'Exchanging the first layer (channels 3 -> 1) will make the pretrained weights useless. '
                    f'Deactivate pretrained then.'
                )

        if fcd['architecture'] in constants.THREE_D_NETWORKS and fcd['channels'] == 3:
            self.add_error(
                'channels',
                f'3D networks only support grayscale.'
            )

        if 'lr_scheduler' in fcd and fcd['lr_scheduler'] == constants.LR_SCHEDULER_LINEAR:
            user_passed_period_values = True
            if 'lr_warm_up_period' not in fcd:
                self.add_error('lr_warm_up_period', is_required_error)
                user_passed_period_values = False
            if 'lr_constant_period' not in fcd:
                self.add_error('lr_constant_period', is_required_error)
                user_passed_period_values = False

            if user_passed_period_values:
                # validate/set actually required values
                if 'lr_decay_period' not in fcd or fcd['lr_decay_period'] is None:
                    self.cleaned_data['lr_decay_period'] = self.cleaned_data['epochs'] - \
                        self.cleaned_data['lr_constant_period'] + self.cleaned_data['lr_warm_up_period']
                    self.data['lr_decay_period'] = str(self.cleaned_data['lr_decay_period'])

                if self.cleaned_data['lr_decay_period'] < 0:
                    self.add_error(
                        'lr_decay_period',
                        'The decay period cannot be negative. Check your epochs and the lr constant period!'
                    )

                if 'lr_adjust_step_size' not in fcd or fcd['lr_adjust_step_size'] is None:
                    self.data['lr_adjust_step_size'] = "0"
                    self.cleaned_data['lr_adjust_step_size'] = 0

        else:
            user_passed_period_values = True
            if 'lr_adjust_step_size' not in fcd:
                self.add_error('lr_adjust_step_size', is_required_error)
                user_passed_period_values = False
            elif fcd['lr_adjust_step_size'] == 0:
                self.add_error('lr_adjust_step_size', 'This must be > 0.')

            if 'lr_adjust_step_size' in fcd and 'lr_scheduler' in fcd and \
                    fcd['lr_scheduler'] == constants.LR_SCHEDULER_STEP:
                step_size = fcd['lr_adjust_step_size']
                epochs = fcd['epochs']
                lr = fcd['learning_rate']
                reductions = epochs // step_size
                for i in range(reductions):
                    lr = lr/2
                    if lr < constants.MINIMUM_LR:
                        self.add_error(
                            'lr_adjust_step_size',
                            f'At that rate, your learning rate will drop below the minimum reasonable learning rate of '
                            f'{constants.MINIMUM_LR} (at epoch {(i+1) * step_size}, your lr will be {lr}). '
                            f'Increase the lr or the step size. {constants.LR_SCHEDULER_STEP} reduces the lr by 2 '
                            f'every step_size iterations.'
                        )

            if user_passed_period_values:
                self.data['lr_decay_period'] = 0
                self.cleaned_data['lr_decay_period'] = 0
                self.data['lr_warm_up_period'] = 0
                self.cleaned_data['lr_warm_up_period'] = 0
                self.data['lr_constant_period'] = self.cleaned_data['epochs']
                self.cleaned_data['lr_constant_period'] = self.cleaned_data['epochs']

        if 'fine_tuning_epoch' in fcd and 'epochs' in fcd:
            if fcd['fine_tuning_epoch'] > fcd['epochs']:
                self.data['fine_tuning_epoch'] = fcd['epochs']
                self.cleaned_data['fine_tuning_epoch'] = fcd['epochs']

    def save(self, commit=True):
        parameters = super().save(commit)

        parameters.dataset = self.cleaned_data['dataset_choice']
        if parameters.device != 'cuda':
            parameters.num_gpus = None

        if commit:
            parameters.save()

        return parameters


class TrainingContinuationForm(forms.ModelForm):
    dataset_choice = forms.ChoiceField(
        label='Dataset',
        choices=([]),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = TrainingContinuationParameters
        fields = [
            'model_path',

            'dataset_choice',  # replaces dataset -> set at save method to allow dynamic choices if user adds a folder
            'reproduce',

            'epochs',
            'batch_size',

            'device',
            'num_gpus',

            'save_every',
        ]
        widgets = {
            'model_path': forms.TextInput(attrs={'class': 'form-control'}),

            'reproduce': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'device': forms.Select(attrs={'class': 'form-control'}),

            'epochs': forms.NumberInput(attrs={'class': 'form-control'}),
            'save_every': forms.NumberInput(attrs={'class': 'form-control'}),
            'batch_size': forms.NumberInput(attrs={'class': 'form-control'}),
            'num_gpus': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
            'model_path',
        ]
        self.initial['batch_size'] = None

        dataset_choices = []
        datasets = get_sub_folders(constants.DATASET_FOLDER, depth=1)
        for folder in datasets:
            sub_folders = get_sub_folders(folder, depth=1)
            sub_folders = [os.path.basename(sub_folder) for sub_folder in sub_folders]
            if 'images' in sub_folders and 'masks' in sub_folders:
                dataset_choices.append((os.path.basename(folder), os.path.basename(folder)))

        dataset_choice_keys = [dataset_choice[0] for dataset_choice in dataset_choices]
        if self.instance is not None and self.instance.pk is not None:
            if self.instance.dataset and self.instance.dataset not in dataset_choice_keys:
                dataset_choices.append(('__deleted__', f'{self.instance.dataset} (deleted!)'))

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True

            if field == 'dataset_choice':
                if not dataset_choices or (len(dataset_choices) == 1 and dataset_choices[0][0] == '__deleted__'):
                    messages.add_message(
                        request,
                        messages.INFO,
                        mark_safe(
                            'There are no datasets to choose from. Read '
                            '<a class="form-a" '
                            'href="https://gitlab.com/fra-wa/aiseg/-/blob/master/README.md">Readme</a> '
                            'how to setup your data correctly!'
                        ),
                    )
                self.fields[field].choices = dataset_choices
                if self.instance and self.instance.pk is not None:
                    if self.instance.dataset not in dataset_choice_keys:
                        self.fields[field].initial = '__deleted__'
                    else:
                        self.fields[field].initial = self.instance.dataset

    def clean(self):
        fcd = super().clean()
        if 'dataset_choice' in fcd and fcd['dataset_choice'] == '__deleted__':
            self.add_error(
                'dataset_choice',
                'The dataset does no more exist!'
            )

        if 'model_path' in fcd and fcd['model_path']:
            if not os.path.isfile(fcd['model_path']):
                self.add_error(
                    'model_path',
                    'This file does not exist.'
                )
            else:
                checkpoint = torch.load(fcd['model_path'], map_location=torch.device('cpu'))
                checkpoint_keys = checkpoint.keys()
                if fcd['reproduce']:
                    if 'batch_size' in checkpoint_keys:
                        if fcd['batch_size']:
                            self.add_error(
                                'batch_size',
                                'You selected reproduce. The batch size will be loaded from the checkpoint. '
                                'Leave this field empty!'
                            )
                        else:
                            fcd['batch_size'] = checkpoint['batch_size']

                    elif not fcd['batch_size']:
                        self.add_error(
                            'batch_size',
                            'Sorry, the loaded checkpoint does not have batch size information (old checkpoint).'
                            ' You can find it in the logs.'
                        )
                elif not fcd['batch_size'] and 'batch_size' not in checkpoint_keys:
                    self.add_error(
                        'batch_size',
                        'Sorry, the loaded checkpoint does not have batch size information (old checkpoint).'
                        ' You can find it in the logs or set a different batch size.'
                    )
                else:
                    fcd['batch_size'] = checkpoint['batch_size']

        if 'num_gpus' in fcd and fcd['num_gpus']:
            if fcd['num_gpus'] > torch.cuda.device_count():
                self.add_error(
                    'num_gpus',
                    f'Only {torch.cuda.device_count()} GPUs are available.'
                )

    def save(self, commit=True):
        parameters = super().save(commit)

        parameters.dataset = self.cleaned_data['dataset_choice']

        if commit:
            parameters.save()

        return parameters

