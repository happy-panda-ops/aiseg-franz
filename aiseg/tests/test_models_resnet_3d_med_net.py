from aiseg.dl_models import ResNetMed3D
from aiseg.tests.model_base_tests import ModelBaseTests3D


class ResNetMed3DTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('ResNetMed3D', ResNetMed3D)
