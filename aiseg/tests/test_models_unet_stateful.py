import cv2
import os
import torch

from unittest import TestCase

from aiseg import constants
from aiseg.file_handling.utils import get_files
from aiseg.dl_models import SFUNet, model_constants
from aiseg.dl_models.image.unet.blocks.rnn_cells import ConvGRUCell


def get_model_class(architecture):
    if architecture == constants.SFUNet_NAME:
        model_class = SFUNet
    else:
        raise NotImplementedError()
    return model_class


def get_expected_recurrent_cell_count(architecture, recurrent_depth):
    if architecture == constants.SFUNet_NAME:
        count = recurrent_depth * 5
    else:
        raise NotImplementedError()
    return count


class StatefulUnetTests(TestCase):

    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

    def test_no_backbone(self):
        with torch.no_grad():
            for architecture in constants.STATEFUL_RECURRENT_UNETS:
                model_class = get_model_class(architecture)
                recurrent_depth = 2
                unet = model_class(
                    image_channels=1,
                    num_classes=6,
                    use_gru=True,
                    backbone=None,
                    pretrained=True,
                    use_residual_decoder=False,
                    recurrent_depth=recurrent_depth
                )

                count = 0
                for name, module in unet.named_modules():
                    if isinstance(module, ConvGRUCell):
                        count += 1

                self.assertEqual(count, get_expected_recurrent_cell_count(architecture, recurrent_depth))

                image_paths = get_files(self.train_images)
                images = []
                for i in range(6):
                    images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

                image_batch = torch.stack(images)
                image_batch = torch.unsqueeze(image_batch, dim=1)
                image_batch = image_batch.view(2, 3, 1, 256, 256)  # [B, F, C, H, W]

                image_batch = image_batch.to(self.device)
                unet.to(self.device)

                out = unet(image_batch.float(), reset_state=True)
                self.assertEqual(out.shape[0], 2)
                self.assertEqual(out.shape[1], 3)
                self.assertEqual(out.shape[2], 6)
                self.assertEqual(out.shape[3], 256)
                self.assertEqual(out.shape[4], 256)

    def test_backbones(self):
        with torch.no_grad():
            for architecture in constants.STATEFUL_RECURRENT_UNETS:
                model_class = get_model_class(architecture)
                image_paths = get_files(self.train_images)
                images = []
                for i in range(4):
                    images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

                image_batch = torch.stack(images)
                image_batch = torch.unsqueeze(image_batch, dim=1)
                image_batch = image_batch.view(2, 2, 1, 256, 256)  # [B, F, C, H, W]
                image_batch = image_batch.to(self.device)

                backbones = [b for b in constants.ARCHITECTURES_AND_BACKBONES[SFUNet.__name__] if b]
                error_count = 0
                for backbone in backbones:
                    try:
                        unet = model_class(
                            image_channels=1,
                            num_classes=6,
                            use_gru=True,
                            backbone=backbone,
                            pretrained=False,
                            use_residual_decoder=False,
                            recurrent_depth=1,
                        )
                        unet.to(self.device)

                        out = unet(image_batch.float(), reset_state=True)
                        self.assertEqual(out.shape[0], 2)
                        self.assertEqual(out.shape[1], 2)
                        self.assertEqual(out.shape[2], 6)
                        self.assertEqual(out.shape[3], 256)
                        self.assertEqual(out.shape[4], 256)
                    except RuntimeError:
                        error_count += 1
                        print(f'\nNot enough VRam for {model_class.__name__} with {backbone} backbone!\n')

                if error_count == len(backbones):
                    print('All backbones failed! This is most likely no VRam issue!')
                self.assertNotEqual(error_count, len(backbones))

    def test_lstm(self):
        with torch.no_grad():
            for architecture in constants.STATEFUL_RECURRENT_UNETS:
                model_class = get_model_class(architecture)
                unet = model_class(
                    image_channels=1,
                    num_classes=6,
                    use_gru=False,
                    backbone=None,
                    pretrained=True,
                    use_residual_decoder=False,
                )

                image_paths = get_files(self.train_images)
                images = []
                for i in range(6):
                    images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

                image_batch = torch.stack(images)
                image_batch = torch.unsqueeze(image_batch, dim=1)
                image_batch = image_batch.view(2, 3, 1, 256, 256)  # [B, F, C, H, W]

                image_batch = image_batch.to(self.device)
                unet.to(self.device)

                out = unet(image_batch.float(), reset_state=True)
                self.assertEqual(out.shape[0], 2)
                self.assertEqual(out.shape[1], 3)
                self.assertEqual(out.shape[2], 6)
                self.assertEqual(out.shape[3], 256)
                self.assertEqual(out.shape[4], 256)

    def test_normalizations(self):
        for architecture in constants.STATEFUL_RECURRENT_UNETS:
            model_class = get_model_class(architecture)
            for norm in model_constants.NORMALIZATIONS:
                if norm == 'gn':
                    excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
                elif norm == 'bn':
                    excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm3d]
                else:
                    raise NotImplementedError()

                unet = model_class(
                    image_channels=1,
                    num_classes=6,
                    use_gru=True,
                    backbone=None,
                    pretrained=True,
                    use_residual_decoder=False,
                    normalization=norm,
                )
                for name, module in unet.named_modules():
                    for excluded_norm in excluded_norms:
                        self.assertFalse(isinstance(module, excluded_norm))

    def test_device_is_cpu(self):
        with torch.no_grad():
            for architecture in constants.STATEFUL_RECURRENT_UNETS:
                model_class = get_model_class(architecture)
                device = 'cpu'
                unet = model_class(
                    image_channels=1,
                    num_classes=6,
                    use_gru=False,
                    backbone=None,
                    pretrained=True,
                    use_residual_decoder=False,
                    device=device,
                )

                image_paths = get_files(self.train_images)
                images = []
                for i in range(6):
                    images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

                image_batch = torch.stack(images)
                image_batch = torch.unsqueeze(image_batch, dim=1)
                image_batch = image_batch.view(2, 3, 1, 256, 256)  # [B, F, C, H, W]

                image_batch = image_batch.to(device)
                unet.to(device)

                out = unet(image_batch.float(), reset_state=True)
                self.assertEqual(out.shape[0], 2)
                self.assertEqual(out.shape[1], 3)
                self.assertEqual(out.shape[2], 6)
                self.assertEqual(out.shape[3], 256)
                self.assertEqual(out.shape[4], 256)

                unet = model_class(
                    image_channels=1,
                    num_classes=6,
                    use_gru=True,
                    backbone=None,
                    pretrained=True,
                    use_residual_decoder=False,
                    device=device,
                )

                image_paths = get_files(self.train_images)
                images = []
                for i in range(6):
                    images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

                image_batch = torch.stack(images)
                image_batch = torch.unsqueeze(image_batch, dim=1)
                image_batch = image_batch.view(2, 3, 1, 256, 256)  # [B, F, C, H, W]

                image_batch = image_batch.to(device)
                unet.to(device)

                out = unet(image_batch.float(), reset_state=True)
                self.assertEqual(out.shape[0], 2)
                self.assertEqual(out.shape[1], 3)
                self.assertEqual(out.shape[2], 6)
                self.assertEqual(out.shape[3], 256)
                self.assertEqual(out.shape[4], 256)
