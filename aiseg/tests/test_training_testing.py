import numpy as np

from unittest import TestCase

from aiseg.training.metrics import MetricsCalculator


class MetricsCalculatorTest(TestCase):
    def setUp(self) -> None:
        pass

    def test_calculation_binary_2d(self):
        classes = 2
        # Class of interest: foreground

        gt_binary_2d = np.zeros((10, 10))
        gt_binary_2d[0, :] = 1
        ones = np.sum(gt_binary_2d)
        self.assertEqual(ones, 10)

        net_binary_2d = np.zeros((10, 10))
        net_binary_2d[0:2, :] = 1

        expected_tp = 10
        expected_fp = 10
        expected_tn = 80
        expected_fn = 0

        gt_false_count = 90
        gt_true_count = 10

        expected_tp_ratio = expected_tp / gt_true_count
        expected_tn_ratio = expected_tn / gt_false_count
        expected_fp_ratio = 1 - expected_tn_ratio
        expected_fn_ratio = 1 - expected_tp_ratio

        expected_dice = 2 * expected_tp / (2*expected_tp + expected_fp + expected_fn)
        expected_iou = expected_dice / (2 - expected_dice)

        calculator = MetricsCalculator(net_binary_2d, gt_binary_2d, classes)
        calculator.calculate()

        self.assertEqual(calculator.overall_accuracy, 0.9)
        self.assertEqual(calculator.tp[1], expected_tp)
        self.assertEqual(calculator.tp_ratio[1], expected_tp_ratio)
        self.assertEqual(calculator.fp[1], expected_fp)
        self.assertEqual(calculator.fp_ratio[1], expected_fp_ratio)
        self.assertEqual(calculator.fn[1], expected_fn)
        self.assertEqual(calculator.fn_ratio[1], expected_fn_ratio)
        self.assertEqual(calculator.fp[1], expected_fp)
        self.assertEqual(calculator.fp_ratio[1], expected_fp_ratio)
        self.assertEqual(calculator.dice_coefficient[1], expected_dice)
        self.assertEqual(calculator.mean_iou[1], expected_iou)
        self.assertEqual(calculator.pixels_per_class[0], gt_false_count)
        self.assertEqual(calculator.pixels_per_class[1], gt_true_count)

    def test_calculation_binary_3d(self):
        classes = 2

        gt_binary_3d = np.zeros((10, 10, 10))
        gt_binary_3d[0, :, :] = 1
        ones = np.sum(gt_binary_3d)
        self.assertEqual(ones, 100)

        net_binary_3d = np.zeros((10, 10, 10))
        net_binary_3d[0, 1:, :] = 1  # 90
        net_binary_3d[1, :, :] = 1  # 100

        expected_tp = np.array([800, 90])
        expected_fp = np.array([10, 100])
        expected_tn = np.array([90, 800])
        expected_fn = np.array([100, 10])

        gt_false_count = np.array([100, 900])
        gt_true_count = np.array([900, 100])

        expected_tp_ratio = expected_tp / gt_true_count
        expected_tn_ratio = expected_tn / gt_false_count
        expected_fp_ratio = 1 - expected_tn_ratio
        expected_fn_ratio = 1 - expected_tp_ratio

        expected_dice = 2 * expected_tp / (2 * expected_tp + expected_fp + expected_fn)
        expected_iou = expected_dice / (2 - expected_dice)

        calculator = MetricsCalculator(net_binary_3d, gt_binary_3d, classes)
        calculator.calculate()

        self.assertEqual(calculator.overall_accuracy, 0.89)

        for idx in range(classes):
            self.assertEqual(calculator.tp[idx], expected_tp[idx])
            self.assertEqual(calculator.tp_ratio[idx], expected_tp_ratio[idx])
            self.assertEqual(calculator.fp[idx], expected_fp[idx])
            self.assertEqual(calculator.fp_ratio[idx], expected_fp_ratio[idx])
            self.assertEqual(calculator.fn[idx], expected_fn[idx])
            self.assertEqual(calculator.fn_ratio[idx], expected_fn_ratio[idx])
            self.assertEqual(calculator.fp[idx], expected_fp[idx])
            self.assertEqual(calculator.fp_ratio[idx], expected_fp_ratio[idx])

        for idx in range(classes):
            self.assertEqual(calculator.dice_coefficient[idx], expected_dice[idx])
            self.assertEqual(calculator.mean_iou[idx], expected_iou[idx])

        self.assertEqual(calculator.pixels_per_class[0], gt_false_count[1])
        self.assertEqual(calculator.pixels_per_class[1], gt_true_count[1])

    def test_calculation_multiclass_2d(self):
        classes = 3
        # Class of interest: foreground

        gt_2d = np.zeros((10, 10))
        gt_2d[0, :] = 1
        gt_2d[1, :] = 2

        net_2d = np.zeros((10, 10))
        net_2d[0, :] = 1
        net_2d[1:3, :] = 2

        expected_tp = np.array([70, 10, 10])
        expected_fp = np.array([0, 0, 10])
        expected_tn = np.array([20, 90, 80])
        expected_fn = np.array([10, 0, 0])
        expected_acc = np.array([0.9, 1, 0.9])

        gt_false_count = np.array([20, 90, 90])
        gt_true_count = 100 - gt_false_count

        expected_tp_ratio = expected_tp / gt_true_count
        expected_tn_ratio = expected_tn / gt_false_count
        expected_fp_ratio = 1 - expected_tn_ratio
        expected_fn_ratio = 1 - expected_tp_ratio

        expected_dice = 2 * expected_tp / (2*expected_tp + expected_fp + expected_fn)
        expected_iou = expected_dice / (2 - expected_dice)

        calculator = MetricsCalculator(net_2d, gt_2d, classes)
        calculator.calculate()

        self.assertEqual(calculator.overall_accuracy, 0.9)
        for idx in range(classes):
            try:
                self.assertEqual(calculator.tp[idx], expected_tp[idx])
                self.assertEqual(calculator.tp_ratio[idx], expected_tp_ratio[idx])
                self.assertEqual(calculator.fp[idx], expected_fp[idx])
                self.assertEqual(calculator.fp_ratio[idx], expected_fp_ratio[idx])
                self.assertEqual(calculator.fn[idx], expected_fn[idx])
                self.assertEqual(calculator.fn_ratio[idx], expected_fn_ratio[idx])
                self.assertEqual(calculator.fp[idx], expected_fp[idx])
                self.assertEqual(calculator.fp_ratio[idx], expected_fp_ratio[idx])
                self.assertEqual(calculator.dice_coefficient[idx], expected_dice[idx])
                self.assertEqual(calculator.mean_iou[idx], expected_iou[idx])
                self.assertEqual(calculator.pixels_per_class[idx], gt_true_count[idx])
                self.assertEqual(calculator.accuracy_per_class[idx], expected_acc[idx])
            except AssertionError as e:
                raise AssertionError(f'Failed at class {idx}!') from e

    def test_calculation_multiclass_3d(self):
        classes = 3

        gt_3d = np.zeros((10, 10, 10))
        gt_3d[0, :, :] = 1
        gt_3d[1, :, :] = 2

        net_3d = np.zeros((10, 10, 10))
        net_3d[0, :, :] = 1
        net_3d[1:3, :, :] = 2

        expected_tp = np.array([700, 100, 100])
        expected_fp = np.array([0, 0, 100])
        expected_tn = np.array([200, 900, 800])
        expected_fn = np.array([100, 0, 0])
        expected_acc = np.array([0.9, 1, 0.9])

        gt_false_count = np.array([200, 900, 900])
        gt_true_count = 1000 - gt_false_count

        expected_tp_ratio = expected_tp / gt_true_count
        expected_tn_ratio = expected_tn / gt_false_count
        expected_fp_ratio = 1 - expected_tn_ratio
        expected_fn_ratio = 1 - expected_tp_ratio

        expected_dice = 2 * expected_tp / (2 * expected_tp + expected_fp + expected_fn)
        expected_iou = expected_dice / (2 - expected_dice)

        calculator = MetricsCalculator(net_3d, gt_3d, classes)
        calculator.calculate()

        self.assertEqual(calculator.overall_accuracy, 0.9)
        for idx in range(classes):
            try:
                self.assertEqual(calculator.tp[idx], expected_tp[idx])
                self.assertEqual(calculator.tp_ratio[idx], expected_tp_ratio[idx])
                self.assertEqual(calculator.fp[idx], expected_fp[idx])
                self.assertEqual(calculator.fp_ratio[idx], expected_fp_ratio[idx])
                self.assertEqual(calculator.fn[idx], expected_fn[idx])
                self.assertEqual(calculator.fn_ratio[idx], expected_fn_ratio[idx])
                self.assertEqual(calculator.fp[idx], expected_fp[idx])
                self.assertEqual(calculator.fp_ratio[idx], expected_fp_ratio[idx])
                self.assertEqual(calculator.dice_coefficient[idx], expected_dice[idx])
                self.assertEqual(calculator.mean_iou[idx], expected_iou[idx])
                self.assertEqual(calculator.pixels_per_class[idx], gt_true_count[idx])
                self.assertEqual(calculator.accuracy_per_class[idx], expected_acc[idx])
            except AssertionError as e:
                raise AssertionError(f'Failed at class {idx}!') from e
