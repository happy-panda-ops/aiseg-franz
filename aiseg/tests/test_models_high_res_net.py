from aiseg.dl_models import HighResNet3D
from aiseg.tests.model_base_tests import ModelBaseTests3D


class HighResNet3DTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('HighResNet3D', HighResNet3D)
