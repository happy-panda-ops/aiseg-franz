import cv2
import os
import torch
import warnings

from unittest import TestCase

from aiseg import constants
from aiseg.file_handling.utils import get_files
from aiseg.dl_models import model_constants


def get_class_inheritor_names(some_class):
    class_names = [base.__name__ for base in some_class.__bases__]
    for base_class in some_class.__bases__:
        class_names += get_class_inheritor_names(base_class)
    return class_names


class ModelBaseTests2D(TestCase):

    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

        image_paths = get_files(self.train_images)
        images = []
        for i in range(5):
            images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

        image_batch = torch.stack(images)
        self.image_batch = torch.unsqueeze(image_batch, dim=1)

        self.group_norm_not_supported_models = ['PSPDenseNet']

    def start_test(self, model_name, model_class, **kwargs):
        with torch.no_grad():

            # test standard and check correct inheritance and first/last layer info
            try:
                model = model_class(image_channels=1, num_classes=6, **kwargs)

                name, conv_layer = model.get_first_layer_information()
                self.assertEqual(conv_layer.in_channels, 1)

                model_bases = get_class_inheritor_names(model_class)
                self.assertIn('BaseModel', model_bases)

                model.to(self.device)
                self.image_batch = self.image_batch.to(self.device)

                out_batch = model(self.image_batch.float())
                self.assertEqual(out_batch.shape[0], 5)
                self.assertEqual(out_batch.shape[1], 6)
                self.assertEqual(out_batch.shape[2], 256)
                self.assertEqual(out_batch.shape[3], 256)
            except BaseException as error:
                if isinstance(error, KeyboardInterrupt):
                    raise KeyboardInterrupt('User stopped test.')
                raise RuntimeError(f'Failed at model: {model_name}.')

            # test binary
            try:
                model = model_class(image_channels=1, num_classes=2, **kwargs)
                model.to(self.device)
                self.image_batch = self.image_batch.to(self.device)

                out_batch = model(self.image_batch.float())
                self.assertEqual(out_batch.shape[0], 5)
                self.assertEqual(out_batch.shape[1], 1)
                self.assertEqual(out_batch.shape[2], 256)
                self.assertEqual(out_batch.shape[3], 256)
            except BaseException as error:
                if isinstance(error, KeyboardInterrupt):
                    raise KeyboardInterrupt('User stopped test.')
                raise RuntimeError(f'Failed at model: {model_name}. (Binary classification)')

            # test normalization
            with warnings.catch_warnings():
                # ignoring warnings: dl_models.utils.group_norm() will definitely warn you
                # -> don't want that during tests
                warnings.simplefilter('ignore')
                for norm in model_constants.NORMALIZATIONS:
                    if norm == 'gn' and model_name in self.group_norm_not_supported_models:
                        continue
                    if norm == 'gn':
                        excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
                    elif norm == 'bn':
                        excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm3d]
                    else:
                        raise NotImplementedError()
                    model = model_class(image_channels=1, num_classes=2, normalization=norm, **kwargs)

                    try:
                        for name, module in model.named_modules():
                            for excluded_norm in excluded_norms:
                                self.assertFalse(isinstance(module, excluded_norm))

                    except AssertionError:
                        raise AssertionError(
                            f'Failed at model: {model_name}. '
                            f'(Found norm layer: {str(module)}, but {str(excluded_norm)} was excluded)')

            # test all supported backbones
            if len(constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__]) > 1:
                backbones = [b for b in constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__] if b]
                backbone_to_skip = None
                if 'backbone' in kwargs.keys():
                    backbone_to_skip = kwargs['backbone']

                for backbone in backbones:
                    if backbone == backbone_to_skip:
                        # no need to test given backbone again
                        continue
                    try:
                        kwargs['backbone'] = backbone
                        model = model_class(image_channels=1, num_classes=2, **kwargs)
                        model.to(self.device)
                        self.image_batch = self.image_batch.to(self.device)

                        out_batch = model(self.image_batch.float())
                        self.assertEqual(out_batch.shape[0], 5)
                        self.assertEqual(out_batch.shape[1], 1)
                        self.assertEqual(out_batch.shape[2], 256)
                        self.assertEqual(out_batch.shape[3], 256)
                    except BaseException as error:
                        if isinstance(error, KeyboardInterrupt):
                            raise KeyboardInterrupt('User stopped test.')
                        raise RuntimeError(
                            f'Failed at model: {model_name}, backbone: {backbone}. (Backbone verification)'
                        )

        torch.cuda.empty_cache()


class ModelBaseTests3D(TestCase):

    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

        image_paths = get_files(self.train_images)
        images = []
        for i in range(16):
            images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

        image_batch = torch.stack(images)
        self.image_batch = torch.unsqueeze(torch.unsqueeze(image_batch, dim=0), dim=0)

    def start_test(self, model_name, model_class, **kwargs):
        with torch.no_grad():

            # test standard and check correct inheritance
            try:
                model = model_class(image_channels=1, num_classes=6, **kwargs)

                name, conv_layer = model.get_first_layer_information()
                self.assertEqual(conv_layer.in_channels, 1)

                model_bases = get_class_inheritor_names(model_class)
                self.assertIn('BaseModel', model_bases)

                model.to(self.device)
                self.image_batch = self.image_batch.to(self.device)

                out_batch = model(self.image_batch.float())
                self.assertEqual(out_batch.shape[0], 1)
                self.assertEqual(out_batch.shape[1], 6)
                self.assertEqual(out_batch.shape[2], 16)
                self.assertEqual(out_batch.shape[3], 256)
                self.assertEqual(out_batch.shape[4], 256)

            except BaseException as error:
                if isinstance(error, KeyboardInterrupt):
                    raise KeyboardInterrupt('User stopped test.')
                raise RuntimeError(f'Failed at model: {model_name}.')

            # test binary
            try:
                model = model_class(image_channels=1, num_classes=2, **kwargs)
                model.to(self.device)
                self.image_batch = self.image_batch.to(self.device)

                out_batch = model(self.image_batch.float())
                self.assertEqual(out_batch.shape[0], 1)
                self.assertEqual(out_batch.shape[1], 1)
                self.assertEqual(out_batch.shape[2], 16)
                self.assertEqual(out_batch.shape[3], 256)
                self.assertEqual(out_batch.shape[4], 256)
            except BaseException as error:
                if isinstance(error, KeyboardInterrupt):
                    raise KeyboardInterrupt('User stopped test.')
                raise RuntimeError(f'Failed at model: {model_name}. (Binary classification)')

            # test normalization
            with warnings.catch_warnings():
                # ignoring warnings: dl_models.utils.group_norm() will definitely warn you
                # -> don't want that during tests
                warnings.simplefilter('ignore')
                for norm in model_constants.NORMALIZATIONS:
                    if norm == 'gn':
                        excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
                    elif norm == 'bn':
                        excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm2d]
                    else:
                        raise NotImplementedError()
                    model = model_class(image_channels=1, num_classes=2, normalization=norm, **kwargs)

                    try:
                        for name, module in model.named_modules():
                            for excluded_norm in excluded_norms:
                                self.assertFalse(isinstance(module, excluded_norm))

                    except AssertionError:
                        raise AssertionError(
                            f'Failed at model: {model_name}. '
                            f'(Found norm layer: {norm}, but {excluded_norm} was excluded)'
                        )

            # test all supported backbones
            if len(constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__]) > 1:
                backbones = [b for b in constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__] if b]
                backbone_to_skip = None
                if 'backbone' in kwargs.keys():
                    backbone_to_skip = kwargs['backbone']

                for backbone in backbones:
                    if backbone == backbone_to_skip:
                        # no need to test given backbone again
                        continue
                    try:
                        kwargs['backbone'] = backbone
                        model = model_class(image_channels=1, num_classes=2, **kwargs)
                        model.to(self.device)
                        self.image_batch = self.image_batch.to(self.device)

                        out_batch = model(self.image_batch.float())
                        self.assertEqual(out_batch.shape[0], 1)
                        self.assertEqual(out_batch.shape[1], 1)
                        self.assertEqual(out_batch.shape[2], 16)
                        self.assertEqual(out_batch.shape[3], 256)
                        self.assertEqual(out_batch.shape[4], 256)
                    except BaseException as error:
                        if isinstance(error, KeyboardInterrupt):
                            raise KeyboardInterrupt('User stopped test.')
                        raise RuntimeError(
                            f'Failed at model: {model_name}, backbone: {backbone}. (Backbone verification)'
                        )

        torch.cuda.empty_cache()
