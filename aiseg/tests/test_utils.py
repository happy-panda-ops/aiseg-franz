import cv2
import numpy as np

from unittest import TestCase

from aiseg.utils import resize_cv2_or_pil_image


class UtilsTests(TestCase):

    def test_resize_cv2_or_pil_image(self):
        sizes_to_check = [100, 200, 128, 127, 256, 257, 255, 374]
        for size in sizes_to_check:
            img = np.zeros((size, size), dtype=np.uint8)
            resized = resize_cv2_or_pil_image(img, 512, interpolation=cv2.INTER_NEAREST, invert=True)
            h, w = resized.shape[:2]
            self.assertEqual(h, w)
            self.assertTrue(h == 512)

        sizes_to_check = [(100, 99), (200, 199), (128, 127), (127, 126), (256, 255), (257, 256), (255, 254), (374, 373)]
        for size in sizes_to_check:
            img = np.zeros(size, dtype=np.uint8)
            resized = resize_cv2_or_pil_image(img, 512, interpolation=cv2.INTER_NEAREST)
            h, w = resized.shape[:2]
            self.assertTrue(h == 512)
            self.assertNotEqual(h, w)

        img = np.zeros((510, 511), dtype=np.uint8)
        resized = resize_cv2_or_pil_image(img, 512, interpolation=cv2.INTER_NEAREST)
        h, w = resized.shape[:2]
        self.assertEqual(h, 511)
        self.assertEqual(w, 512)

        img = np.zeros((341, 342), dtype=np.uint8)
        resized = resize_cv2_or_pil_image(img, 512, interpolation=cv2.INTER_NEAREST)
        h, w = resized.shape[:2]
        self.assertEqual(h, 510)
        self.assertEqual(w, 512)
