import cv2
import io
import math
import numpy as np
import os
import sys

from unittest import TestCase
from unittest import mock

from aiseg import constants
from aiseg.data_augmentation.geometric_transformations import tilt_backwards
from aiseg.data_augmentation.geometric_volume_transformations import apply_2d_function_to_volume
from aiseg.data_augmentation.geometric_volume_transformations import execute_random_tilt
from aiseg.data_augmentation.geometric_volume_transformations import flip_x
from aiseg.data_augmentation.geometric_volume_transformations import flip_y
from aiseg.data_augmentation.geometric_volume_transformations import flip_z
from aiseg.data_augmentation.geometric_volume_transformations import random_crop
from aiseg.data_augmentation.geometric_volume_transformations import random_resize
from aiseg.data_augmentation.geometric_volume_transformations import random_squeeze_execution
from aiseg.data_augmentation.geometric_volume_transformations import rotate_x
from aiseg.data_augmentation.geometric_volume_transformations import rotate_y
from aiseg.data_augmentation.geometric_volume_transformations import rotate_z
from aiseg.file_handling.utils import get_files


def custom_func(img_2d, mask_2d):
    img = np.zeros_like(img_2d)
    return img, img


class MockedRandomFirstVal:
    @staticmethod
    def randint(val1, val2):
        return val1


class MockedRandomSecondVal:
    @staticmethod
    def randint(val1, val2):
        return val2


class GeometricVolumeManipulationTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'data_3d', 'images', 'volume_1')
        self.masks_folder = os.path.join(files_path, 'data_3d', 'masks', 'volume_1')

        self.volume_paths = get_files(self.volume_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)[:10]
        self.mask_paths = get_files(self.masks_folder, extension=constants.SUPPORTED_MASK_TYPES)[:10]

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

    @staticmethod
    def create_volume_from_files(files):
        h, w = cv2.imread(files[0]).shape[:2]
        volume = np.zeros((len(files), h, w), dtype=np.uint8)

        for z, img in enumerate(files):
            volume[z] = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
        smaller_volume = np.zeros((10, 100, 100), dtype=np.uint8)
        smaller_volume[:, :, :] = volume[:, :100, :100]

        return smaller_volume

    def test_apply_2d_function_to_volume(self):
        volume = self.create_volume_from_files(self.volume_paths)

        self.assertNotEqual(len(np.unique(volume[0, :, :])), 1)
        new_vol, new_mask = apply_2d_function_to_volume(volume, volume, custom_func)
        self.assertEqual(len(np.unique(new_vol[0, :, :])), 1)

    @mock.patch('aiseg.data_augmentation.geometric_volume_transformations.random', MockedRandomFirstVal)
    def test_random_resize(self):
        volume = self.create_volume_from_files(self.volume_paths)
        d, h, w = volume.shape

        self.assertEqual(d, 10)
        self.assertEqual(h, 100)
        self.assertEqual(w, 100)
        new_vol, new_mask = random_resize(volume, volume)
        d, h, w = new_vol.shape
        self.assertEqual(d, 8)
        self.assertEqual(h, 80)
        self.assertEqual(w, 80)
        d, h, w = new_mask.shape
        self.assertEqual(d, 8)
        self.assertEqual(h, 80)
        self.assertEqual(w, 80)

    def test_rotate_x(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)
        d, h, w = volume.shape

        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)
        new_vol, new_mask = rotate_x(volume, volume, 90)
        d, h, w = new_vol.shape
        self.assertEqual(d, 20)
        self.assertEqual(h, 10)
        self.assertEqual(w, 30)
        d, h, w = new_mask.shape
        self.assertEqual(d, 20)
        self.assertEqual(h, 10)
        self.assertEqual(w, 30)

    def test_rotate_y(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)
        d, h, w = volume.shape

        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)
        new_vol, new_mask = rotate_y(volume, volume, 90)
        d, h, w = new_vol.shape
        self.assertEqual(d, 30)
        self.assertEqual(h, 20)
        self.assertEqual(w, 10)
        d, h, w = new_mask.shape
        self.assertEqual(d, 30)
        self.assertEqual(h, 20)
        self.assertEqual(w, 10)

    def test_rotate_z(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)
        d, h, w = volume.shape

        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)
        new_vol, new_mask = rotate_z(volume, volume, 90)
        d, h, w = new_vol.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 30)
        self.assertEqual(w, 20)
        d, h, w = new_mask.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 30)
        self.assertEqual(w, 20)

    # def test_rotate_3d(self):
    #     volume = np.zeros((10, 20, 30), dtype=np.uint8)
    #     d, h, w = volume.shape
    #
    #     self.assertEqual(d, 10)
    #     self.assertEqual(h, 20)
    #     self.assertEqual(w, 30)
    #     new_vol = rotate_3d(volume, rot_x=0, rot_y=0, rot_z=0)
    #     d, h, w = new_vol.shape
    #     self.assertEqual(d, 10)
    #     self.assertEqual(h, 20)
    #     self.assertEqual(w, 30)
    #
    #     new_vol = rotate_3d(volume, rot_x=90, rot_y=0, rot_z=0)
    #     d, h, w = new_vol.shape
    #     self.assertEqual(d, 30)
    #     self.assertEqual(h, 10)
    #     self.assertEqual(w, 20)
    #
    #     new_vol = rotate_3d(volume, rot_x=0, rot_y=90, rot_z=0)
    #     d, h, w = new_vol.shape
    #     self.assertEqual(d, 30)
    #     self.assertEqual(h, 20)
    #     self.assertEqual(w, 10)
    #
    #     new_vol = rotate_3d(volume, rot_x=0, rot_y=0, rot_z=90)
    #     d, h, w = new_vol.shape
    #     self.assertEqual(d, 10)
    #     self.assertEqual(h, 30)
    #     self.assertEqual(w, 20)

    def test_execute_random_tilt(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)
        d, h, w = volume.shape

        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)
        with mock.patch(
                'aiseg.data_augmentation.geometric_volume_transformations.random', MockedRandomFirstVal):
            new_vol, new_mask = execute_random_tilt(volume, volume, func=tilt_backwards)
        start = int(math.ceil(1 / 4 * w))
        end = 30
        expected_width = end - start
        d, h, w = new_vol.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 19)  # one pixel is removed during tilting to avoid black edge
        self.assertEqual(w, expected_width)
        d, h, w = new_mask.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 19)
        self.assertEqual(w, expected_width)

        d, h, w = volume.shape
        with mock.patch(
                'aiseg.data_augmentation.geometric_volume_transformations.random',
                MockedRandomSecondVal):
            new_vol, new_mask = execute_random_tilt(volume, volume, func=tilt_backwards)
        start = int(math.ceil(1 / 4 * w))
        end = int(3 / 4 * w)
        expected_width = end - start
        d, h, w = new_vol.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 19)  # one pixel is removed during tilting to avoid black edge
        self.assertEqual(w, expected_width)
        d, h, w = new_mask.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 19)
        self.assertEqual(w, expected_width)

    def test_random_squeeze(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)
        d, h, w = volume.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)

        new_vol, new_mask = random_squeeze_execution(volume, volume, 0, scale_factor=0.5)
        d, h, w = new_vol.shape
        self.assertEqual(d, 5)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)
        d, h, w = new_mask.shape
        self.assertEqual(d, 5)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)

        new_vol, new_mask = random_squeeze_execution(volume, volume, 1, scale_factor=0.5)
        d, h, w = new_vol.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 10)
        self.assertEqual(w, 30)
        d, h, w = new_mask.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 10)
        self.assertEqual(w, 30)

        new_vol, new_mask = random_squeeze_execution(volume, volume, 2, scale_factor=0.5)
        d, h, w = new_vol.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 15)
        d, h, w = new_mask.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 15)

    def test_random_crop(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)
        d, h, w = volume.shape
        self.assertEqual(d, 10)
        self.assertEqual(h, 20)
        self.assertEqual(w, 30)

        with mock.patch(
                'aiseg.data_augmentation.geometric_volume_transformations.random', MockedRandomFirstVal):
            new_vol, new_mask = random_crop(volume, volume, min_size=2)
        d, h, w = new_vol.shape
        d_target, h_target, w_target = 10, 6, 8
        self.assertEqual(d, d_target)
        self.assertEqual(h, h_target)
        self.assertEqual(w, w_target)
        d, h, w = new_mask.shape
        self.assertEqual(d, d_target)
        self.assertEqual(h, h_target)
        self.assertEqual(w, w_target)

    def test_flips(self):

        base_volume = np.zeros((10, 5, 3), dtype=np.uint8)
        base_volume[0, 0, 0] = 1

        flipped_x, _ = flip_x(base_volume, base_volume)
        self.assertTrue(flipped_x[0, 0, 2] == 1)

        flipped_y, _ = flip_y(base_volume, base_volume)
        self.assertTrue(flipped_y[0, 4, 0] == 1)

        flipped_z, _ = flip_z(base_volume, base_volume)
        self.assertTrue(flipped_z[9, 0, 0] == 1)
