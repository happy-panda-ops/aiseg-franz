import cv2
import os

from unittest import TestCase

import numpy as np

from aiseg.data_augmentation.pixel_manipulation import to_gray_and_adaptive_histogram
from aiseg.data_augmentation.pixel_manipulation import brightness
from aiseg.data_augmentation.pixel_manipulation import contrast
from aiseg.data_augmentation.pixel_manipulation import gaussian_filter
from aiseg.data_augmentation.pixel_manipulation import gaussian_noise
from aiseg.data_augmentation.pixel_manipulation import to_gray_and_histogram_normalization
from aiseg.data_augmentation.pixel_manipulation import to_gray_and_non_local_means_filter
from aiseg.data_augmentation.pixel_manipulation import sharpen
from aiseg.data_augmentation.pixel_manipulation import random_erasing
from aiseg.file_handling.utils import create_folder


class GeometricManipulationTests(TestCase):
    def setUp(self) -> None:
        # Testing with BatchedSequenceLoader to get the base implementations
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        augmentation_folder = os.path.join(files_path, 'augmentation')
        self.result_folder = os.path.join(augmentation_folder, 'results')

        self.file_path = os.path.join(augmentation_folder, 'small.png')
        self.color_mask = os.path.join(augmentation_folder, 'color_mask.png')
        self.pattern = cv2.imread(self.file_path)
        self.pattern_gray = cv2.imread(self.file_path, cv2.IMREAD_GRAYSCALE)
        self.mask = cv2.imread(self.color_mask)
        self.mask_gray = cv2.imread(self.file_path, cv2.IMREAD_GRAYSCALE)
        self.mask_gray[self.mask_gray >= 200] = 255
        self.mask_gray[self.mask_gray < 200] = 10

    def test_adaptive_histogram(self):
        folder = os.path.join(self.result_folder, 'adaptive_histogram')
        if self.save_files:
            create_folder(folder)

        img, mask = to_gray_and_adaptive_histogram(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'adaptive_histogram.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        # only execute to see behaviour with color
        img, mask = to_gray_and_adaptive_histogram(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = to_gray_and_adaptive_histogram(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_contrast_and_brightness(self):
        folder = os.path.join(self.result_folder, 'contrast_and_brightness')
        if self.save_files:
            create_folder(folder)

        img, mask = contrast(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        if self.save_files:
            file_name = 'contrast.png'
            cv2.imwrite(os.path.join(folder, file_name), img)
        # only execute to see behaviour with color
        img, mask = contrast(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = contrast(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        img, mask = brightness(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        if self.save_files:
            file_name = 'brightness.png'
            cv2.imwrite(os.path.join(folder, file_name), img)
        # only execute to see behaviour with color
        img, mask = brightness(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = brightness(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_gaussian_filter(self):
        folder = os.path.join(self.result_folder, 'gaussian_filter')
        if self.save_files:
            create_folder(folder)

        img, mask = gaussian_filter(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'gaussian_filter.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        # only execute to see behaviour with color
        img, mask = gaussian_filter(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = gaussian_filter(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_gaussian_noise(self):
        folder = os.path.join(self.result_folder, 'gaussian_noise')
        if self.save_files:
            create_folder(folder)

        img, mask = gaussian_noise(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'gaussian_noise.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        # only execute to see behaviour with color
        img, mask = gaussian_noise(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = gaussian_noise(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_histogram_normalization(self):
        folder = os.path.join(self.result_folder, 'histogram_normalization')
        if self.save_files:
            create_folder(folder)

        img, mask = to_gray_and_histogram_normalization(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'histogram_normalization.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        # only execute to see behaviour with color
        img, mask = to_gray_and_histogram_normalization(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = to_gray_and_histogram_normalization(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_non_local_means_filter(self):
        folder = os.path.join(self.result_folder, 'non_local_means_filter')
        if self.save_files:
            create_folder(folder)

        img, mask = to_gray_and_non_local_means_filter(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'non_local_means_filter.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        # only execute to see behaviour with color
        img, mask = to_gray_and_non_local_means_filter(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = to_gray_and_non_local_means_filter(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_sharpen(self):
        folder = os.path.join(self.result_folder, 'sharpen')
        if self.save_files:
            create_folder(folder)

        img, mask = sharpen(self.pattern_gray, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'sharpen.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        # only execute to see behaviour with color
        img, mask = sharpen(self.pattern, self.mask_gray)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = sharpen(self.pattern_gray, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_random_erasing(self):
        folder = os.path.join(self.result_folder, 'random_erasing')
        if self.save_files:
            create_folder(folder)

        for i in range(10):

            img, mask = random_erasing(self.pattern_gray, self.mask_gray)
            self.assertEqual(np.unique(mask).shape[0], 2)

            if self.save_files:
                file_name = f'random_erasing_gray_{str(i)}.png'
                cv2.imwrite(os.path.join(folder, file_name), img)

            # only execute to see behaviour with color
            img, mask = random_erasing(self.pattern, self.mask_gray)
            if self.save_files:
                file_name = f'random_erasing_color_{str(i)}.png'
                cv2.imwrite(os.path.join(folder, file_name), img)

            unique_mask_colors = np.unique(self.mask).shape[0]
            img, mask = random_erasing(self.pattern_gray, self.mask)
            self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)
