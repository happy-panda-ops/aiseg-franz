import cv2
import os
import torch

from unittest import TestCase

from aiseg import constants
from aiseg.file_handling.utils import get_files
from aiseg.dl_models import UNetResNet, model_constants


class ResidualUnetTests(TestCase):

    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')

    def test_resnet_backbones(self):
        with torch.no_grad():
            image_paths = get_files(self.train_images)
            images = []
            for i in range(5):
                images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

            image_batch = torch.stack(images)
            image_batch = torch.unsqueeze(image_batch, dim=1)
            image_batch.to('cpu')

            backbones = [b for b in constants.ARCHITECTURES_AND_BACKBONES[UNetResNet.__name__] if b]

            for backbone in backbones:
                unet = UNetResNet(
                    image_channels=1,
                    num_classes=6,
                    backbone=backbone,
                    use_residual_decoder=False,
                )
                try:
                    out = unet(image_batch.float())
                except RuntimeError as e:
                    print(f'UNet Resnet backbone failed! Backbone: {backbone}')
                    raise RuntimeError(e)
                self.assertEqual(out.shape[0], 5)
                self.assertEqual(out.shape[1], 6)
                self.assertEqual(out.shape[2], 256)
                self.assertEqual(out.shape[3], 256)

    def test_correct_normalizations(self):
        for norm in model_constants.NORMALIZATIONS:
            if norm == 'gn':
                excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
            elif norm == 'bn':
                excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm3d]
            else:
                raise NotImplementedError()

            unet = UNetResNet(
                image_channels=1,
                num_classes=6,
                backbone=constants.ARCHITECTURES_AND_BACKBONES[UNetResNet.__name__][0],
                use_residual_decoder=False,
                normalization=norm,
            )
            for name, module in unet.named_modules():
                for excluded_norm in excluded_norms:
                    self.assertFalse(isinstance(module, excluded_norm))
