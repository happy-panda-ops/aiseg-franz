from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import DANet
from aiseg.dl_models import Dran


class DANetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DANet', DANet, backbone='resnet50')


class DranTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('Dran', Dran, backbone='resnet50')
