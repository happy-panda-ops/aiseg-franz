import time

import cv2
import logging
import numpy as np
import os

from torch import multiprocessing

try:
    multiprocessing.set_start_method('spawn')
except RuntimeError:
    pass

from aiseg.inference.utils import decode_logits, get_logits_weight
from aiseg.inference.utils import decode_segmentation_map
from aiseg.file_handling.read import read_binary_to_numpy_array
from aiseg.file_handling.utils import get_files, remove_files, remove_file
from aiseg.inference.datasets.inference_dataset_2d import InferenceDataset2D
from aiseg.inference.predictor_base import PredictorBase


def combine_sub_logits_to_mask(input_size,
                               model_out_channels,
                               first_layer_padding,
                               logits_data,
                               decode_to_color,
                               logit_threshold=0.5,
                               weight_mode='gauss',
                               ):
    height_list = []
    width_list = []

    for binary, h_start, w_start in logits_data:
        height_list.append(h_start)
        width_list.append(w_start)

    heights = sorted(list(set(height_list)))
    widths = sorted(list(set(width_list)))

    rows = len(heights)
    columns = len(widths)

    # create a list holding the logits for every row and column. Example with 9 images:
    # aligned_logits = [
    #       [bin_top_left,      bin_top_middle,     bin_top_right],
    #       [bin_mid_left,      bin_middle,         bin_mid_right],
    #       [bin_bottom_left,   bin_bottom_middle,  bin_bottom_right],
    #   ]
    # initialize here since loaded images are not sorted in correct order due to naming
    aligned_logits = [columns * [np.array(1)] for _ in range(rows)]
    for binary, h_start, w_start in logits_data:
        row = heights.index(h_start)
        column = widths.index(int(w_start))
        logits = read_binary_to_numpy_array(binary)
        aligned_logits[row][column] = logits

    # prepare out mask
    out_height = max(heights) + input_size
    out_width = max(widths) + input_size

    sliding_window = SlidingImageCalculator(model_out_channels, out_height, out_width)

    # now combine the aligned logits
    for row_idx, row in enumerate(aligned_logits):
        h_start = heights[row_idx]
        for col_idx, column in enumerate(row):
            w_start = widths[col_idx]

            sliding_window.add_current_logits(
                aligned_logits[row_idx][col_idx],
                h_start,
                w_start,
                first_layer_padding,
                weight_mode=weight_mode,
            )

    out_logits = sliding_window.get_avg_logits_volume()
    out_logits = np.expand_dims(out_logits, 0)  # add batch information to decode properly
    segmentation_map, classes = decode_logits(out_logits, model_out_channels, binary_logit_threshold=logit_threshold)
    out_mask = decode_segmentation_map(segmentation_map, classes, decode_to_color)[0]  # batch size is 1 in this case

    return out_mask


def images_recreation_process(bin_folder,
                              out_folder,
                              input_size,
                              model_out_channels,
                              first_layer_padding,
                              decode_to_color,
                              logit_threshold,
                              segmentation_finished_file,
                              weight_mode):
    while True:
        try:
            files_on_device = get_files(bin_folder, extension='npy')
            if not files_on_device and os.path.isfile(segmentation_finished_file):
                remove_file(segmentation_finished_file)
                break
            elif not files_on_device:
                time.sleep(1)
                continue
        except OSError:
            # folder deleted --> likely by process or user
            break

        h_w_count_ext = [file.split('_')[-3:] for file in files_on_device]
        names_and_counts = {}
        names = []
        for path, h_w_count_and_extension in zip(files_on_device, h_w_count_ext):
            h, w, count_ext = h_w_count_and_extension[:]
            name = os.path.basename(path).split(f'_{h}_{w}_{count_ext}')[0]
            if name in names_and_counts.keys():
                names_and_counts[name]['logits_data'].append((path, int(h), int(w)))
            else:
                count = int(count_ext.split('.')[0])
                names_and_counts[name] = {
                    'count': count,
                    'logits_data': [(path, int(h), int(w))],
                }
            names.append(name)

        for name, count_and_logits in names_and_counts.items():
            if names.count(name) == count_and_logits['count']:
                out_mask = combine_sub_logits_to_mask(
                    input_size=input_size,
                    model_out_channels=model_out_channels,
                    first_layer_padding=first_layer_padding,
                    logits_data=count_and_logits['logits_data'],
                    decode_to_color=decode_to_color,
                    logit_threshold=logit_threshold,
                    weight_mode=weight_mode,
                )
                file_name = name + '_mask.png'
                file_name = os.path.join(out_folder, file_name)
                cv2.imwrite(file_name, out_mask)
                remove_files([data[0] for data in count_and_logits['logits_data']])


class SlidingImageCalculator:

    def __init__(self, classes, height, width):
        self.logits_image = np.zeros((classes, height, width), dtype=np.float32)
        self.weighting_image = np.zeros((classes, height, width), dtype=np.uint16)

    def add_current_logits(self, logits, h_start, w_start, padding, weight=None, weight_mode='gauss'):
        """

        Args:
            logits: current logits representing a sub image
            h_start: start coordinate in height
            w_start: start coordinate in width
            padding: first layer padding -> will be removed from calculation
            weight: pass a custom weighting image or use the default.
            weight_mode: 'sum', 'linear' or 'gauss'; where:
                sum: simply adds the logits together and divides by n
                linear: creates a pyramid like weighting
                gauss: uses a gaussian bell with 3 sigma and my is the center

        Returns:

        """
        weight_modes = ['sum', 'linear', 'gauss']
        if weight_mode not in weight_modes:
            raise ValueError(f'Weight mode must be one of: {", ".join(weight_modes)}')

        n_logits, h_logits, w_logits = logits.shape

        if weight is None:
            weight = get_logits_weight(weight_mode, h_logits, w_logits)

        weight = weight.astype(self.weighting_image.dtype)

        if n_logits == 1:
            weight = np.expand_dims(weight, 0)
        else:
            weight = np.repeat(weight[np.newaxis, :, :], n_logits, axis=0)

        h_end = h_start + h_logits
        w_end = w_start + w_logits

        # zero out padding values
        if h_start != 0:
            weight[:, 0:padding, :] = 0
        if h_end != self.logits_image.shape[1]:
            weight[:, h_logits - padding:, :] = 0

        if w_start != 0:
            weight[:, :, 0:padding] = 0
        if w_end != self.logits_image.shape[2]:
            weight[:, :, w_logits - padding:] = 0

        padding_removed_logits = weight * logits

        self.logits_image[:, h_start:h_end, w_start:w_end] += padding_removed_logits
        self.weighting_image[:, h_start:h_end, w_start:w_end] += weight

    def get_avg_logits_volume(self):
        return np.divide(self.logits_image, self.weighting_image)


class SlidingImagePredictor(PredictorBase):

    def __init__(self,
                 folder,
                 input_size,
                 model,
                 batch_size,
                 channels=1,
                 decode_to_color=True,
                 overlap=0,
                 binary_logit_threshold=0.5,
                 weight_mode='gauss',
                 logger=None,
                 ):
        """
        Predicts small sub volumes of shape: input_size x input_size x depth

        Args:
            folder: image folder
            input_size: input size of the network
            model: the model
            batch_size: in this case: how many images will be predicted simultaneously
            channels: 1 for gray, 3 for rgb, ..
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            binary_logit_threshold: Threshold value for the logits of a binary classification.
                Above is foreground, beneath background.
            overlap: overlap in percent
            weight_mode: gauss sum or linear
            logger:
        """
        super(SlidingImagePredictor, self).__init__(
            folder=folder,
            input_size=input_size,
            model=model,
            channels=channels,
            decode_to_color=decode_to_color,
            binary_logit_threshold=binary_logit_threshold,
            overlap=overlap,
            logger=logger,
            batch_size=batch_size,
            weight_mode=weight_mode,
        )
        assert self.model.first_layer_padding[0] == self.model.first_layer_padding[1]

        self.inference_dataset = InferenceDataset2D(
            self.input_images,
            self.temp_out_folder,
            self.channels,
            self.input_size,
            self.first_layer_padding,
            overlap_w=self.overlap,
            overlap_h=self.overlap,
            dataset_mean=self.model.dataset_mean,
            dataset_std=self.model.dataset_std,
        )

        if self.batch_size > len(self.inference_dataset):
            self.batch_size = len(self.inference_dataset)

        logging.info('Prediction parameters:')
        logging.info(f'Folder: {folder}')
        logging.info(f'Network input size:')
        logging.info(f'Height and width: {input_size}')
        logging.info(f'Batch size: {batch_size}')
        logging.info(f'Image channels: {channels}')
        logging.info(f'Overlap: {overlap} %')
        logging.info(f'Overlap weighting strategy: {weight_mode}')
        logging.info(f'Logit threshold if binary classification: {binary_logit_threshold}')

    def start_recreation_process(self):
        recreation_arguments = (
            self.temp_out_folder,
            self.out_folder,
            self.input_size,
            self.model.out_channels,
            self.first_layer_padding,
            self.decode_to_color,
            self.binary_logit_threshold,
            self.prediction_done_file,
            self.weight_mode,
        )
        p = multiprocessing.Process(target=images_recreation_process, args=recreation_arguments)
        p.start()
        self.process = p
