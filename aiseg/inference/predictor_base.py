import cv2
import logging
import numpy as np
import os
import time
import torch

from torch import multiprocessing
from torch.utils.data import DataLoader

try:
    multiprocessing.set_start_method('spawn')
except RuntimeError:
    pass

from aiseg import constants
from aiseg.file_handling.utils import create_folder, remove_dir
from aiseg.file_handling.utils import get_files
from aiseg.file_handling.write import save_numpy_array_to_binary, save_csv
from aiseg.inference import inference_constants
from aiseg.inference.utils import map_to_color, convert_mask_to_color
from aiseg.multicore_utils import slice_multicore_parts, slice_list_equally, multiprocess_function
from aiseg.training.data_loading import get_num_workers_and_prefetch
from aiseg.training.transformations import get_gray_image_transformation
from aiseg.training.transformations import get_rgb_image_transformation
from aiseg.utils import get_max_threads, show_memory, get_human_time_string_from_seconds


class PredictorBase:
    def __init__(self,
                 folder,
                 input_size,
                 model,
                 batch_size,
                 channels=1,
                 decode_to_color=True,
                 binary_logit_threshold=0.5,
                 overlap=0,
                 weight_mode='gauss',
                 input_depth=None,
                 logger=None,
                 ):
        """

        Args:
            folder: folder containing images of the volume or 2D images
            input_size: input size of images to pass in (the model should take any squared input size)
            model:
            channels: image channels of the data to predict
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            binary_logit_threshold: Threshold value for the logits of a binary classification.
                Above is foreground, beneath background.
            overlap: overlap in percent
            input_depth:
            logger:
        """
        self.decode_to_color = decode_to_color
        self.use_nice_color = True  # do not change or ensure correct decoding at: self.multi_combining
        self.binary_logit_threshold = binary_logit_threshold
        self.logger = logger

        self.first_layer_padding = model.first_layer_padding[0]
        self.overlap = overlap  # the padding will be cut away in every case. You can additionally add an overlap here.
        self.weight_mode = weight_mode

        if not os.path.isdir(folder):
            raise FileNotFoundError(f'Could not find folder: "{folder}"')

        self.input_images = get_files(folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        if not self.input_images:
            raise FileNotFoundError(f'Could not find a file at: {folder}')

        if 0 >= overlap >= 100:
            raise ValueError(f'Overlap percentage must be 0 or greater and smaller than 100!')

        self.channels = channels
        if channels == 1:
            self.img_transform = get_gray_image_transformation(mean=model.dataset_mean, std=model.dataset_std)
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation(mean=model.dataset_mean, std=model.dataset_std)
        else:
            raise NotImplementedError(f'{channels} channels are not supported.')

        self.model = model
        self.model.eval()
        self.input_size = input_size
        self.input_depth = input_depth

        self.folder = folder

        self.temp_out_folder = os.path.join(folder, 'temp')
        create_folder(self.temp_out_folder)

        self.out_folder = os.path.join(folder, 'segmented')
        self.combined_folder = os.path.join(folder, 'combined')

        self.already_segmented = False
        if os.path.isdir(self.out_folder):
            # check if segmentation already exists
            segmented_files = get_files(self.out_folder)
            if len(segmented_files) == len(self.input_images):
                self.already_segmented = True

        if self.already_segmented:
            logging.info(f'The folder {self.folder} is already segmented! Remove the segmentation to segment again.')
            try:
                self.remove_temp_folder()
            except PermissionError:
                logging.info('Could not remove temporary data due to permission error.')
        else:
            create_folder(self.temp_out_folder)
            create_folder(self.out_folder)
            create_folder(self.combined_folder)

        self.prediction_done_file = os.path.join(self.temp_out_folder, 'prediction_done.txt')
        self.batch_size = batch_size

        self.process = None
        self.inference_dataset = None

    @staticmethod
    def multi_combining(images, masks, out_folder, process_nr, num_classes):
        opacity = 0.4
        masks_range_0_255 = False
        for i, (img_path, mask_path) in enumerate(zip(images, masks)):
            if process_nr == 0:
                logging.info(f'Process {process_nr}: {i + 1}/{len(images)}')
            elif process_nr == -1:
                logging.info(f'Combining: {i + 1}/{len(images)}')
            mask = cv2.imread(mask_path, -1)
            image = cv2.imread(img_path, cv2.IMREAD_COLOR)

            if len(image.shape) == 2:
                image = cv2.imread(img_path, cv2.IMREAD_COLOR)
            if len(mask.shape) == 2:
                if masks_range_0_255 or np.max(mask) == 255:
                    masks_range_0_255 = True
                    mask = convert_mask_to_color(mask, num_classes)
                else:
                    mask = map_to_color(mask, num_classes)

            background_color = inference_constants.BEAUTIFUL_COLORS_RGB[0][::-1].astype(np.uint8)
            mask[np.all(mask == background_color, axis=-1)] = np.array([0, 0, 0], dtype=np.uint8)

            img_with_mask = np.copy(image)
            # np.all(mask != np.array([0, 0, 0]), axis=-1) does not work?
            indexes = np.invert(np.all(mask == np.array([0, 0, 0]), axis=-1))
            img_with_mask[indexes] = mask[indexes]

            combined = cv2.addWeighted(image, opacity, img_with_mask, 1 - opacity, 0)
            img_name = os.path.basename(img_path).split('.')[0]
            out_name = os.path.join(out_folder, f'{img_name}_segmented.png')
            cv2.imwrite(out_name, combined)

    def combine_images_and_masks(self):
        logging.info('Combining images and segmentations for visualization.')
        images = get_files(self.folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_files(self.out_folder, extension=constants.SUPPORTED_MASK_TYPES)
        if len(images) != len(masks):
            logging.info(f'Error: found {len(images)} images and {len(masks)} segmentations! Cannot combine.')
            return

        num_classes = 2 if self.model.out_channels == 1 else self.model.out_channels
        if len(images) > 100:
            threads = get_max_threads(logical=True)  # this is an easy task, threading is fine
            images_map = slice_multicore_parts(images, logical=True, total_maps=threads)
            masks_map = slice_list_equally(masks, len(images_map))
            out_folder_map = len(images_map) * [self.combined_folder]
            process_nr_map = list(range(len(images_map)))
            num_classes_map = len(images_map) * [num_classes]

            multiprocess_function(
                self.multi_combining,
                [images_map, masks_map, out_folder_map, process_nr_map, num_classes_map],
                self.logger
            )
        else:
            self.multi_combining(images, masks, self.combined_folder, -1, num_classes)

        logging.info('Finished combining')

    def remove_temp_folder(self):
        if os.path.isdir(self.temp_out_folder):
            remove_dir(self.temp_out_folder)

    def get_data_loader(self):
        if self.inference_dataset is None:
            raise ValueError('Inference dataset was not created')

        _, inf_workers, _, inf_prefetch = get_num_workers_and_prefetch(
            online_aug=False,
            architecture=self.model.__class__.__name__,
            batch_size=self.batch_size,
            channels=self.channels,
            input_size=self.input_size,
            input_depth=self.input_depth,
            train_dataset=self.inference_dataset,
            valid_dataset=self.inference_dataset,
            is_inference=True,
        )

        data_loader = DataLoader(
            dataset=self.inference_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            drop_last=False,
            num_workers=inf_workers,
            prefetch_factor=inf_prefetch,
        )
        return data_loader

    @staticmethod
    def save_out_batch(out_logits, out_paths):
        for i, path in enumerate(out_paths):
            save_numpy_array_to_binary(out_logits[i], path)

    def prediction_process(self, device, data_loader):
        device = torch.device(device)
        # use_amp = True if device.type == 'cuda' else False
        self.model.to(device)
        self.model.eval()
        total_iterations = len(data_loader)

        logging.info('Starting prediction of preprocessed data')

        with torch.no_grad():
            with torch.cuda.amp.autocast(enabled=constants.ENABLE_AMP):
                for i, (in_batch, out_batch_paths) in enumerate(data_loader):
                    logging.info(
                        f'Batch: {(i + 1)}/{total_iterations} ({(i + 1) / total_iterations * 100:.1f}%)'
                    )
                    # Todo: add time remaining, attention: first batches will be loaded much slower
                    if os.path.isfile(out_batch_paths[0]):
                        continue
                    if i <= 2 or i % 30 == 0:
                        show_memory(device)
                    in_batch = in_batch.to(device)
                    out_logits = self.model(in_batch)
                    out_logits = out_logits.float().cpu().numpy()
                    self.save_out_batch(out_logits, out_batch_paths)

        # this file is used by the recreation process. The recreation process checks if this file exists. If so,
        # it will shut down when the remaining files are processed or tmp out folder is empty.
        save_csv(self.prediction_done_file, [], 'Tmp file, states, that prediction is done.')

    def start_recreation_process(self):
        raise NotImplementedError('set up at your class')

    def __start_waiting_loop(self, out_files, total_files):
        missing_images = total_files - out_files
        logging.info(f'Missing segmentations: {missing_images}')
        is_2d = True if self.model.__class__.__name__ in constants.TWO_D_NETWORKS else False
        time_to_wait = 30 if is_2d else 12 * 60
        log_time_after_seconds = 5
        if not is_2d:
            log_time_after_seconds = 15
            logging.info('Depending on the overlap, the volume recreation may take a while.')
            logging.info(
                f'If no new segmentations are created for '
                f'{get_human_time_string_from_seconds(time_to_wait, localize=False)}, '
                f'the recreation process will be stopped.'
            )
        time_waited = 0
        files_count = out_files
        while missing_images and time_waited <= time_to_wait:
            time.sleep(1)
            out_files = len(
                get_files(self.out_folder, extension=constants.SUPPORTED_MASK_TYPES)
            )
            missing_images = total_files - out_files
            logging.info(f'Missing segmentations: {missing_images}')
            if out_files == files_count:
                if time_waited > 1 and time_waited % log_time_after_seconds == 0:
                    logging.info(f'Missing segmentations: {missing_images}.')
                    remaining_recreation_files = get_files(self.temp_out_folder, extension='npy')
                    logging.info(
                        f'Remaining files for recreation: {len(remaining_recreation_files)}. Processing... Please wait'
                    )
                    if is_2d:
                        logging.info(
                            f'No new segmentations created.'
                            f' Waiting for {time_to_wait - time_waited} seconds before stopping.'
                        )
                    else:
                        human_time = get_human_time_string_from_seconds(time_to_wait - time_waited, localize=False)
                        logging.info(
                            f'No new segmentations created.'
                            f' Waiting for {human_time} before stopping.'
                        )

                time_waited += 1
            else:
                files_count = out_files
                time_waited = 0
            if time_waited > time_to_wait:
                raise RuntimeError('Error in recreation process! Check console.')
            if not self.process.is_alive():
                logging.info('Done.')
                break
            elif not missing_images and os.path.isfile(self.prediction_done_file):
                logging.info('Done.')
                if self.process.is_alive():
                    self.process.terminate()
                    time.sleep(1)
                break

    def wait_for_and_stop_recreation_process(self):
        if isinstance(self.process, multiprocessing.Process):
            if self.process.is_alive():
                # hack for error handling: files should get reduced! if not: kill child --> got error
                total_files = len(
                    get_files(self.folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
                )
                out_files = len(get_files(self.out_folder, extension=constants.SUPPORTED_MASK_TYPES))
                missing_images = total_files - out_files

                proces_done_file_exists = os.path.isfile(self.prediction_done_file)
                if proces_done_file_exists and missing_images:
                    self.__start_waiting_loop(out_files, total_files)

                elif proces_done_file_exists and not missing_images:
                    logging.info('Recreation already finished')
                    if self.process.is_alive():
                        self.process.terminate()
                        time.sleep(1)
                else:
                    logging.info(
                        f'Unexpected error during recreation. Process done file found: {proces_done_file_exists}, '
                        f'Missing images: {missing_images}.'
                    )
                    self.process.terminate()
                    time.sleep(1)
            else:
                logging.info('Unexpected error during recreation, recreation process already terminated')
        elif self.process is not None:
            raise RuntimeError(f'self.process is not none: {type(self.process)}')

    def predict(self, device='cuda'):
        if self.already_segmented:
            return
        data_loader = self.get_data_loader()
        self.start_recreation_process()
        self.prediction_process(device, data_loader)
        logging.info(f'Network finished prediction.')
        logging.info(f'Waiting for mask recreation process to complete.')
        self.wait_for_and_stop_recreation_process()
        try:
            self.remove_temp_folder()
            logging.info('Finished prediction!')
        except PermissionError:
            logging.info('Finished prediction! Could not remove temporary data due to permission error.')
