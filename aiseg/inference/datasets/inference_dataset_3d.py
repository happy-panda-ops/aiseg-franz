import cv2
import logging
import math
import numpy as np
import os
import torch

from torch.utils.data.dataset import Dataset

from aiseg.multicore_utils import multiprocess_function, slice_multicore_parts
from aiseg.training.transformations import get_gray_image_transformation

from aiseg import constants


class Sample:
    def __init__(self, image_paths, d_start, h_start, w_start, input_size):

        self.image_paths = image_paths
        self.h_start = h_start
        self.w_start = w_start
        self.input_size = input_size
        self.channels = 1
        self.out_name = f'{d_start}_{h_start}_{w_start}.npy'

    def load(self):
        # d, h, w, c
        if self.channels == 1:
            input_volume = np.zeros((len(self.image_paths), self.input_size, self.input_size), dtype=np.uint8)
            flag = cv2.IMREAD_GRAYSCALE
        else:
            input_volume = np.zeros((len(self.image_paths), self.input_size, self.input_size, 3), dtype=np.uint8)
            flag = cv2.IMREAD_COLOR

        for i, image in enumerate(self.image_paths):
            img = cv2.imread(image, flag)
            img = img[self.h_start:self.h_start + self.input_size, self.w_start:self.w_start + self.input_size]
            input_volume[i] = img

        if self.channels == 3:
            # bgr to rgb
            input_volume = input_volume[..., ::-1]

        return input_volume, self.out_name


class Section:
    def __init__(self, image_paths, d_start, input_size, overlap_d, overlap_h, overlap_w, total_images, padding):
        """

        Args:
            image_paths:
            d_start:
            input_size: height and width
            overlap_d: in pixel, must be at least 2 * padding
            overlap_h: in pixel, must be at least 2 * padding
            overlap_w: in pixel, must be at least 2 * padding
            total_images:
            padding:
        """
        # Sections are needed for recreation. If a section has sections in ends_sections, the related section which it
        # ends can be recreated to images
        if overlap_d < 2 * padding or overlap_h < 2 * padding or overlap_w < 2 * padding:
            raise ValueError('overlap must be larger or equal than 2 * padding!')
        self.image_paths = image_paths
        self.input_depth = len(image_paths)  # equals input depth of neural network
        img = cv2.imread(image_paths[0], cv2.IMREAD_GRAYSCALE)
        self.width = img.shape[1]
        self.height = img.shape[0]
        self.d_start = d_start
        self.d_end = self.d_start + self.input_depth
        self.__sample_list = []
        self.input_size = input_size
        self.overlap_w = overlap_w
        self.overlap_h = overlap_h
        self.section_id = None

        section_depths = []
        for d in range(0, total_images - self.input_depth, self.input_depth - overlap_d):
            section_depths.append((d, d + self.input_depth))
        section_depths.append((total_images - self.input_depth, total_images - self.input_depth + self.input_depth))

        # following code calculates which section can be recreated to a masked volume when current section is predicted.
        overlaps = []
        for i, (current_start, current_end) in enumerate(section_depths):
            section_overlaps_with = []
            if self.d_start == d_start and current_end == self.d_start + self.input_depth:
                self.section_id = i

            if current_start != 0:
                current_start += padding
            if self.d_end != total_images:
                current_end -= padding
            for j, (start, end) in enumerate(section_depths):
                if start != 0:
                    start += padding
                if end != total_images:
                    end -= padding
                if start <= current_end - 1 and end - 1 >= current_start:
                    section_overlaps_with.append(j)
            if i + 1 != len(section_depths):
                overlaps.append([section_overlaps_with[0]])
            else:
                overlaps.append(section_overlaps_with)

        reversed_overlaps = list(reversed(overlaps))
        reversed_end_sections = []
        seen_sections = []
        for overlap_list in reversed_overlaps:
            end_sections = []
            for section_id in overlap_list:
                if section_id not in seen_sections:
                    end_sections.append(section_id)
                    seen_sections.append(section_id)
            reversed_end_sections.append(end_sections)

        end_sections = list(reversed(reversed_end_sections))  # all sections with overlaps
        # after this is created, the end_sections[self.section_id] can be recreated and removed from disk
        self.ends_sections = end_sections[self.section_id]

    @property
    def samples(self):
        if not self.__sample_list:
            self.create_samples()
        return self.__sample_list

    def create_samples(self):
        img = cv2.imread(self.image_paths[0], cv2.IMREAD_GRAYSCALE)
        img_w = img.shape[1]
        img_h = img.shape[0]

        increment_w = self.input_size - self.overlap_w
        increment_h = self.input_size - self.overlap_h

        if increment_w <= 0:
            raise RuntimeError(
                f'Can not width-overlap {self.overlap_w} pixels with an input size of {self.input_size}'
            )
        if increment_h <= 0:
            raise RuntimeError(
                f'Can not height-overlap {self.overlap_h} pixels with an input size of {self.input_size}'
            )

        w_sub_indexes = [w for w in range(0, img_w - self.input_size, increment_w)]
        w_sub_indexes.append(img_w - self.input_size)
        h_sub_indexes = [h for h in range(0, img_h - self.input_size, increment_h)]
        h_sub_indexes.append(img_h - self.input_size)

        for h_index in h_sub_indexes:
            for w_index in w_sub_indexes:
                sample = Sample(self.image_paths, self.d_start, h_index, w_index, self.input_size)
                self.__sample_list.append(sample)


class InferenceDataset3D(Dataset):
    """
    Dataset for volumes or time sequences
    """

    def __init__(self,
                 img_paths,
                 out_folder,
                 input_size,
                 input_depth,
                 first_layer_padding,
                 overlap_w=0,
                 overlap_h=0,
                 overlap_d=0,
                 dataset_mean=None,
                 dataset_std=None,
                 logger=None,):
        """
        A volume gets sliced into sections, where each section contains input_depth images.
        Multiple sections can overlap.
        A section is divided into multiple samples
        A sample represents a sub volume of the section of size h = w = input_size, depth = section depth
        Samples can again overlap

        This Dataset first initializes sections, then the sections are initializing the samples

        The samples are in the end stored and the items which are loaded.

        The sections need to be passed to the recreation process:
            - it checks if sections are completely predicted
            - it checks if a section can already be recreated (in case of overlapping sections)

        Args:
            img_paths: images of the volume
            out_folder: where to store
            input_size: height and width
            input_depth: depth
            first_layer_padding: neural networks first layer padding
            overlap_w: in percent
            overlap_h: in percent
            overlap_d: in percent
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
            logger: processes logger
        """

        if not isinstance(img_paths, list):
            raise ValueError('Image paths need to be lists')

        if dataset_mean is not None and not isinstance(dataset_mean, list):
            raise ValueError('mean must be a list')

        if dataset_std is not None and not isinstance(dataset_std, list):
            raise ValueError('std must be a list')

        self.channels = 1
        self.logger = logger
        self.out_folder = out_folder

        self.image_paths = img_paths
        self.volume_d = len(img_paths)
        self.volume_h, self.volume_w = cv2.imread(img_paths[0], cv2.IMREAD_GRAYSCALE).shape[:2]

        self.img_transform = get_gray_image_transformation()
        self.mean = constants.DEFAULT_GRAY_MEAN if dataset_mean is None else dataset_mean
        self.std = constants.DEFAULT_GRAY_STD if dataset_std is None else dataset_std

        self.input_size = input_size
        self.input_depth = input_depth
        self.padding = first_layer_padding
        self.overlap_w = math.ceil(
            (self.input_size - 2 * first_layer_padding) * overlap_w / 100 + first_layer_padding * 2
        )
        self.overlap_h = math.ceil(
            (self.input_size - 2 * first_layer_padding) * overlap_h / 100 + first_layer_padding * 2
        )
        self.overlap_d = math.ceil(
            (self.input_depth - 2 * first_layer_padding) * overlap_d / 100 + first_layer_padding * 2
        )

        self.input_samples = []  # sample class instances -> created using sections
        self.sections = []  # section class instances --> pass this to the recreation process

        increment_w = self.input_size - self.overlap_w
        increment_h = self.input_size - self.overlap_h

        if increment_w <= 0:
            raise RuntimeError(f'Width-overlap ({self.overlap_w}) too large for input size of {self.input_size}.')
        if increment_h <= 0:
            raise RuntimeError(f'Height-overlap ({self.overlap_h}) too large for input size of {self.input_size}.')

        self.setup_items()

    def __len__(self):
        return len(self.input_samples)

    @staticmethod
    def multi_setup_sections(sections, process_nr):
        for i, section in enumerate(sections):
            if not isinstance(section, Section):
                raise RuntimeError('section is not a Section instance')
            if process_nr == 0:
                logging.info(f'Progress: {(i+1)/len(sections) * 100:.1f}%')
            section.create_samples()
        return sections, process_nr

    def setup_items(self):
        total_images = len(self.image_paths)
        section_depths = []
        for d in range(0, total_images - self.input_depth, self.input_depth - self.overlap_d):
            section_depths.append((d, d + self.input_depth))
        section_depths.append((total_images - self.input_depth, total_images - self.input_depth + self.input_depth))

        sections = []
        for d_start, d_end in section_depths:
            sections.append(Section(
                image_paths=self.image_paths[d_start:d_end],
                d_start=d_start,
                input_size=self.input_size,
                overlap_d=self.overlap_d,
                overlap_h=self.overlap_h,
                overlap_w=self.overlap_w,
                total_images=total_images,
                padding=self.padding,
            ))

        increment_w = self.input_size - self.overlap_w
        increment_h = self.input_size - self.overlap_h

        w_sub_indexes = [w for w in range(0, self.volume_w - self.input_size, increment_w)]
        w_sub_indexes.append(self.volume_w - self.input_size)
        h_sub_indexes = [h for h in range(0, self.volume_h - self.input_size, increment_h)]
        h_sub_indexes.append(self.volume_h - self.input_size)

        total_samples = len(sections) * len(h_sub_indexes) * len(w_sub_indexes)

        logging.info(f'Preparing input volumes.')
        if total_samples > 100:
            section_map = slice_multicore_parts(sections)
            process_nr = list(range(len(section_map)))
            results = multiprocess_function(self.multi_setup_sections, [section_map, process_nr], logger=self.logger)
            results = sorted(results, key=lambda x: x[1])
            for result, process_nr in results:
                self.sections += result
        else:
            self.sections = self.multi_setup_sections(sections, 0)[0]

        for section in self.sections:
            self.input_samples += section.samples

        if total_samples != len(self.input_samples):
            raise RuntimeError(f'Created samples ({len(self.input_samples)}) is not as expected ({total_samples})')

    def normalize_and_to_tensor(self, volume):
        """
        converts sequence or volume from bgr to rgb
        normalizes the volume against global values (important!)
        converts to tensor --> shape: [D, H, W, C] it's up to you to change that!
        """
        if self.channels == 3:
            # normalize and to rgb format
            normalized = np.zeros_like(volume, dtype=np.float32)
            normalized[:, :, :, 0] = (volume[:, :, :, 0] - self.mean[0]) / self.std[0]
            normalized[:, :, :, 1] = (volume[:, :, :, 1] - self.mean[1]) / self.std[1]
            normalized[:, :, :, 2] = (volume[:, :, :, 2] - self.mean[2]) / self.std[2]

        else:
            normalized = ((volume - self.mean[0]) / self.std[0]).astype(np.float32)
            normalized = np.expand_dims(normalized, 3)

        normalized = torch.from_numpy(normalized)
        return normalized

    def __getitem__(self, i):
        """Loads the volume to tensor and returns the related temp out file path"""
        sample = self.input_samples[i]
        if not isinstance(sample, Sample):
            raise RuntimeError('sample is not a Sample instance')
        input_volume, out_name = sample.load()
        input_volume = self.normalize_and_to_tensor(input_volume)
        input_volume = input_volume.permute(3, 0, 1, 2)
        return input_volume, os.path.join(self.out_folder, out_name)
