import numpy as np
import torch

from aiseg.inference import inference_constants


def decode_logits(out_logits, classes, binary_logit_threshold=0.5):
    """Assumption: Only one class is true not multiple"""
    if isinstance(out_logits, np.ndarray):
        out_logits = out_logits.astype(np.float32)
        out_logits = torch.from_numpy(out_logits)

    if classes == 1:
        # binary classification image: out_logits of shape: B, C, H, W
        # binary classification volume: out_logits of shape: B, 1, D, H, W
        out = torch.sigmoid(out_logits)
        segmentation_map = (out.squeeze(dim=1) > binary_logit_threshold).float().cpu().numpy()
        classes = 2

    else:
        # img: out_logits of shape: B, C, H, W
        # vol: out_logits of shape: B, C, D, H, W
        values, predictions = torch.max(out_logits, dim=1)
        # img: predictions of shape: B, H, W
        # vol: predictions of shape: predictions of shape: B, D, H, W
        segmentation_map = predictions.cpu().numpy()

    return segmentation_map, classes


def create_decoding_array(classes):
    """Important to have here: also needed during testing!"""
    # mask contains values from 0 to 255 like:
    decoding_array = np.arange(0, classes)
    decoding_array = (decoding_array / (classes - 1)) * 255
    decoding_array = np.round(decoding_array)
    decoding_array[decoding_array > 255] = 255
    decoding_array = decoding_array.astype(np.uint8)
    return decoding_array


def map_to_gray(class_map, class_count):
    decoding_array = create_decoding_array(class_count)
    gray_map = np.zeros_like(class_map).astype(np.uint8)
    for class_number in range(class_count):
        idx = class_map == class_number
        gray_map[idx] = decoding_array[class_number]

    return gray_map


def map_to_color(class_map, class_count):
    """Helper for decode_segmentation_map"""
    decoding_map = inference_constants.BEAUTIFUL_COLORS_RGB

    r = np.zeros_like(class_map).astype(np.uint8)
    g = np.zeros_like(class_map).astype(np.uint8)
    b = np.zeros_like(class_map).astype(np.uint8)

    for class_number in range(class_count):
        idx = class_map == class_number
        r[idx] = decoding_map[class_number, 0]
        g[idx] = decoding_map[class_number, 1]
        b[idx] = decoding_map[class_number, 2]

    if len(class_map.shape) == 4:
        colored_map = np.stack([b, g, r], axis=4)
    elif len(class_map.shape) == 3:
        colored_map = np.stack([b, g, r], axis=3)
    elif len(class_map.shape) == 2:
        colored_map = np.stack([b, g, r], axis=2)
    else:
        raise RuntimeError(f'Invalid shape of class_map: {class_map.shape}')

    return colored_map


def convert_mask_to_color(mask, class_count):
    """Converts binary or multi binary map to color map"""
    decoding_array = create_decoding_array(class_count)
    # reversed decoding:
    # encode 0-255 into 0-(num_classes - 1)
    for i, c in enumerate(decoding_array):
        mask[mask == c] = i

    # decode to rgb
    mask = map_to_color(mask, class_count=class_count)
    return mask


def decode_segmentation_map(segmentation_map, class_count, decode_to_color):
    """
    0 in segmentation map relates to background! If not, you need to change.

    Args:
        segmentation_map: either a batch of volumes or a single volume or a stack of 2D images
            shape for volumes:
                (b, d, h, w) or (d, h, w)
            shape for images:
                (b, h, w) or (h, w)
        class_count: max classes, which the model can predict (at least 2 because true or false)
        decode_to_color: clear.

    Returns: decoded image(s) or volume(s)
        shapes if not decoded to color: mapped between 0 and 255
        shapes if decoded to color:
            shape for volumes:
                (b, d, h, w, 3) or (d, h, w, 3)
            shape for images:
                (b, h, w, 3) or (h, w, 3)

    """
    if not isinstance(segmentation_map, np.ndarray):
        raise ValueError('Segmentation map must be a numpy array')

    if decode_to_color:
        if class_count > inference_constants.BEAUTIFUL_COLORS_RGB.shape[0]:
            raise RuntimeError('Cant detect that many classes, increase decoding labels')

        decoded_stack = map_to_color(segmentation_map, class_count)
    else:
        decoded_stack = map_to_gray(segmentation_map, class_count)

    return decoded_stack


def expand_1d_weight_to_3d(weight_array, height, width):
    """
    What it does:
    Step 1:
        assert a volume of depth = 5 --> linear weight would be: [1,2,3,2,1]
        this will be reshaped to:
        [[1],
         [2],
         [3],
         [2],
         [1]]
    Step 2:
        then expanded to the height of for example 3:
        [[1, 1, 1],
         [2, 2, 2],
         [3, 3, 3],
         [2, 2, 2],
         [1, 1, 1]]
        and finally repeated "into" the monitor width times to get a 3d weight out of the input array.
    """
    depth = weight_array.shape[0]
    weight_slice = weight_array.reshape(depth, 1).repeat(height, axis=1)  # Step 2: 2D: depth, height
    weight_volume = weight_slice.reshape(depth, height, 1).repeat(width, axis=2)  # --> shape: depth, height, width
    return weight_volume


def calculate_gauss_weight(ax_h, ax_w, ax_d=None, weight=1/8):
    """
    Input size is even!

    Args:
        ax_h: np.linspace(-(height - 1) / 2., (height - 1) / 2., height)
        ax_w: np.linspace(-(width - 1) / 2., (width - 1) / 2., width)
        ax_d: np.linspace(-(depth - 1) / 2., (depth - 1) / 2., depth)
        weight: defaults to 1/3 at corner, 1 at center

    Returns: weighted 2D or 3D array for multiplication

    """
    if ax_d is None:
        numerator = -0.5 * (ax_h[0] ** 2 + ax_w[0] ** 2)
    else:
        numerator = -0.5 * (ax_h[0] ** 2 + ax_w[0] ** 2 + ax_d[0] ** 2)
    denominator = np.log(weight)
    sigma = np.sqrt(numerator / denominator)
    gauss_h = np.exp(-0.5 * np.square(ax_h) / np.square(sigma))
    gauss_w = np.exp(-0.5 * np.square(ax_w) / np.square(sigma))
    weight = np.outer(gauss_h, gauss_w)
    if ax_d is not None:
        gauss_d = np.exp(-0.5 * np.square(ax_d) / np.square(sigma))
        tmp_weight_volume = expand_1d_weight_to_3d(gauss_d, ax_h.shape[0], ax_w.shape[0])
        weight = np.array([weight] * ax_d.shape[0])
        weight = weight * tmp_weight_volume
    return weight


def get_logits_weight(mode, height, width, depth=None, gauss_weight=1/8):
    """

    Args:
        mode: 'sum', 'linear', 'gauss'
        height:
        width:
        depth:
        gauss_weight: defaults to 33% at corner, 100% at center
    Examples:
        gauss weighting: 1/3 weighting at corner, 1 at center

    Returns:

    """
    weight_modes = ['sum', 'linear', 'gauss']
    if mode not in weight_modes:
        raise ValueError(f'Weight mode must be one of: {", ".join(weight_modes)}')
    if height != width and mode != 'sum':
        raise ValueError('logits height != width. Weighting can only use sum. Implement for different shapes.')

    if mode == 'sum':
        if depth is not None:
            shape = (depth, height, width)
        else:
            shape = (height, width)
        weight = np.ones(shape)
    elif mode == 'linear':
        assert height == width
        ranged = np.arange(1, width + 1)
        weight_array = np.minimum(ranged, ranged[::-1])
        weight = np.minimum.outer(weight_array, weight_array)
        if depth is not None:
            ranged = np.arange(1, depth + 1)
            weight_array = np.minimum(ranged, ranged[::-1])
            depth_weight = expand_1d_weight_to_3d(weight_array, height, width)
            weight = np.array([weight] * depth)
            weight = weight * depth_weight

    elif mode == 'gauss':
        assert height == width
        # weight outer values 1/3 of inner
        ax_h = np.linspace(-(height - 1) / 2., (height - 1) / 2., height)
        ax_w = np.linspace(-(width - 1) / 2., (width - 1) / 2., width)
        ax_d = None
        if depth is not None:
            ax_d = np.linspace(-(depth - 1) / 2., (depth - 1) / 2., depth)
        weight = calculate_gauss_weight(ax_h, ax_w, ax_d, weight=gauss_weight)
        weight = np.round(weight * 100, 0)

    else:
        raise RuntimeError('Weight mode not supported')

    return weight
