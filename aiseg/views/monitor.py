import datetime
import json

import numpy as np
import os
import sys

import psutil

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone

from .utils import get_running_logs, kill_process_and_children
from .utils import aiseg_render as render
from .. import constants
from ..models import LogFile


def monitor(request, log_pk=None):
    if log_pk is None:
        return redirect('aiseg:logs')

    if log_pk == '-1':
        log_file = LogFile.objects.last()

        if log_file is None:
            messages.add_message(request, messages.INFO, 'There is no log to monitor.')
            return redirect('aiseg:logs')
        else:
            local_log_time = timezone.localtime(log_file.process_start_datetime)
            messages.add_message(
                request,
                messages.INFO,
                f"Last log ({local_log_time.strftime('%d.%m.%Y, %H:%M:%S')}). If you want to see a specific log, go to"
                f" Logs."
            )
            return redirect('aiseg:monitor', log_pk=log_file.pk)

    try:
        log_file = LogFile.objects.get(pk=log_pk)
    except (ObjectDoesNotExist, ValueError):
        messages.add_message(request, messages.INFO, 'The requested log does not exist.')
        return redirect('aiseg:logs')

    p = None
    if psutil.pid_exists(log_file.process_id):
        p = psutil.Process(log_file.process_id)
        start_time = round(p.create_time())
        log_start_time = round(datetime.datetime.timestamp(log_file.process_start_datetime))
        if start_time != log_start_time:
            p = None
        if p is not None and sys.platform == 'linux' and p.status() == 'zombie':
            p = None

    if os.path.isfile(log_file.log_file_path):
        with open(log_file.log_file_path, 'r') as f:
            log_file_data = f.readlines()

        log_text = ''.join(log_file_data)
        log_file.log_text = log_text
        log_file.save()

    else:
        if log_file.log_text:
            log_text = log_file.log_text
        else:
            log_file_data = [
                f'No log text was stored.',
                f'The log file: "',
                f'{log_file.log_file_path}" ',
                f'does not exist. ',
                f"It was started on {log_file.process_start_datetime.strftime('%d.%m.%Y, %H:%M:%S')}"
                f" ({log_file.process_start_datetime.tzname()}).",
            ]
            log_text = '\n'.join(log_file_data)

    if request.method == 'POST':
        if p is not None and p.is_running() and 'kill_process' in request.POST.keys():
            if sys.platform == 'linux':
                kill_process_and_children(p)
                print('INFO: Terminated process. The process is still listed in the process table as zombie.')
                print('INFO: This is not an issue.')
                print('INFO: When you stop the application (close this terminal), the zombies are removed.')
            else:
                kill_process_and_children(p)
            p = None

    log_type = None
    for choice in constants.LOG_EXECUTION_CHOICES:
        if choice[0] == log_file.execution_process:
            log_type = choice[1]
            break

    running_logs = get_running_logs()
    if log_file in running_logs:
        running_logs.pop(running_logs.index(log_file))

    if p is not None:
        max_lines_to_show = 100
        log_text = ''.join(log_file_data[-max_lines_to_show:])
    else:
        max_lines_to_show = None

    train_loss = None
    valid_loss = None
    train_acc = None
    valid_acc = None
    log_further_info = None
    if log_file.model_analysis:
        header, history = log_file.model_analysis.get_history()
        if header is not None:
            epochs = list(history[:, 0])
            train_loss = list(zip(epochs, list(np.around(history[:, 1].astype(float), decimals=4))))
            valid_loss = list(zip(epochs, list(np.around(history[:, 2].astype(float), decimals=4))))
            train_acc = list(zip(epochs, list(np.around(history[:, 3].astype(float), decimals=2))))
            valid_acc = list(zip(epochs, list(np.around(history[:, 4].astype(float), decimals=2))))
            train_loss = json.dumps(train_loss)
            valid_loss = json.dumps(valid_loss)
            train_acc = json.dumps(train_acc)
            valid_acc = json.dumps(valid_acc)
        architecture = log_file.model_analysis.architecture
        backbone = log_file.model_analysis.backbone
        dataset = log_file.model_analysis.dataset
        if log_file.execution_process == constants.DL_EXECUTION_TRAIN:
            log_further_info = f'Training of {architecture}'
        elif log_file.execution_process == constants.DL_EXECUTION_TEST:
            log_further_info = f'Testing of {architecture}'
        elif log_file.execution_process == constants.DL_EXECUTION_TEST_INF:
            log_further_info = f'Testing inference of {architecture}'
        else:
            log_further_info = f'{architecture}'
        if backbone:
            log_further_info += f' ({backbone})'
        log_further_info += f' on {dataset.name}.'

    context = {
        'log_text': log_text,
        'max_log_lines': max_lines_to_show,
        'log_type': log_type,
        'log_further_info': log_further_info,
        'process_start_datetime': log_file.process_start_datetime,
        'process_is_active': False if p is None else True,
        'running_logs': running_logs,
        'post_kill_url': reverse('aiseg:monitor', kwargs={'log_pk': log_pk}),
        'train_loss': train_loss,
        'valid_loss': valid_loss,
        'train_acc': train_acc,
        'valid_acc': valid_acc,
        'navbar_view': 'monitor',
    }

    return render(request,
                  'aiseg/monitor.html',
                  context
                  )


