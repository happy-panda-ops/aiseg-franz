import torch

from torch.utils.data.dataset import Dataset


class Dataset2DSimulator(Dataset):
    def __init__(self, num_samples, input_size, channels):
        if channels not in [1, 3]:
            raise ValueError(f'Channels must be 1 or 3, not {channels}!')
        self.channels = channels

        self.images_count = num_samples
        self.in_size = input_size

    def __getitem__(self, i):
        img = torch.ones((self.channels, self.in_size, self.in_size), dtype=torch.float)
        mask = torch.zeros((self.in_size, self.in_size), dtype=torch.float)

        return img, mask

    def __len__(self):
        return self.images_count


class Dataset3DSimulator(Dataset):
    """
    Dataset for volumes or time sequences
    """

    def __init__(self,
                 num_samples,
                 input_size,
                 input_depth,
                 channels,
                 load_c_d_h_w=True,  # true for volumes, false for sequences
                 ):
        if channels not in [1, 3]:
            raise ValueError(f'Channels must be 1 or 3, not {channels}!')
        self.channels = channels
        self.volumes_count = num_samples
        self.in_size = input_size
        self.in_depth = input_depth
        self.channels = channels
        self.load_C_D_H_W = load_c_d_h_w

    def __getitem__(self, item):
        if self.load_C_D_H_W:
            input_volume = torch.ones((self.channels, self.in_depth, self.in_size, self.in_size), dtype=torch.float)
        else:
            # [D, H, W, C] --> [F, C, H, W]
            input_volume = torch.ones((self.in_depth, self.channels, self.in_size, self.in_size), dtype=torch.float)
        target_volume = torch.zeros((self.in_depth, self.in_size, self.in_size), dtype=torch.float)

        return input_volume, target_volume

    def __len__(self):
        return self.volumes_count
