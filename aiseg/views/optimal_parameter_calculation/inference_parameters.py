import logging
import torch
from django.db.models import Q, F

from aiseg import constants
from aiseg.utils import get_memory, create_3d_iteration_map
from aiseg.multicore_utils import multiprocess_function
from .utils import get_data_loader, ModelLoader
from ...models import InferenceConfiguration


def try_inference_loop_threaded(model_loader,
                                batch_size,
                                input_size,
                                input_depth=None,
                                ):
    if model_loader.architecture == constants.PSANet_NAME:
        model_loader.in_size = input_size
    model, optimizer, loss_func = model_loader.setup_model_optimizer_loss()

    device = model_loader.device
    channels = model.in_channels

    model.eval()
    # ram usage with workers is stable after 3 iterations
    iterations = 3
    num_samples = iterations * batch_size
    data_loader = get_data_loader(model, num_samples, batch_size, channels, input_size, input_depth)
    with torch.no_grad():
        with torch.cuda.amp.autocast(enabled=constants.ENABLE_AMP):
            try:
                for i, (input_batch, target_batch) in enumerate(data_loader):
                    input_batch = input_batch.to(device)
                    out_logits = model(input_batch)
                    out_logits = out_logits.float().cpu().numpy()
                    ram_used, ram_total, _ = get_memory(device)
            except (RuntimeError, ValueError) as e:
                for non_intuitive_error in constants.NON_INTUITIVE_RAM_ERRORS:
                    if non_intuitive_error in e.args[0]:
                        model_loader.wipe_memory()
                        return False, None, None
                return False, None, e.args[0]

    model_loader.wipe_memory()
    return True, ram_used, None


def try_inference_loop(model_loader,
                       batch_size,
                       input_size,
                       input_depth=None,
                       run_subprocess=True
                       ):
    logging.info(f'Testing inference run with:')
    logging.info(f'batch size: {batch_size}')
    logging.info(f'input size: {input_size}')
    if input_depth is not None:
        logging.info(f'input depth: {input_depth}')

    if run_subprocess:
        calculation_possible, ram_used, error_message = multiprocess_function(
            try_inference_loop_threaded,
            [[model_loader], [batch_size], [input_size], [input_depth]]
        )[0]
    else:
        calculation_possible, ram_used, error_message = try_inference_loop_threaded(
            model_loader, batch_size, input_size, input_depth
        )
    if error_message is not None:
        raise RuntimeError(f'Got an error during calculation:\n{error_message}')
    if calculation_possible:
        logging.info('Success.')
    else:
        logging.info('Failed.')
    return calculation_possible, ram_used


def calculate_2d_inference_parameters(model_loader, max_in_size):
    reversed_input_sizes = list(reversed(sorted(
        [size_choice[0] for size_choice in constants.IN_SIZE_CHOICES_2D]
    )))

    input_size = None
    ram_used = 0

    for in_size in reversed_input_sizes:
        if in_size > max_in_size:
            continue
        calculation_possible, ram_used = try_inference_loop(model_loader, batch_size=1, input_size=in_size)
        if calculation_possible:
            input_size = in_size
            break

    if input_size is None:
        return None, None, None

    # on the cpu, calculation would take forever --> reduce max batch size
    if model_loader.device == 'cpu' or model_loader.device == torch.device('cpu'):
        reversed_batch_sizes = list(reversed(sorted(
            [batch_choice[0] for batch_choice in constants.CPU_INFERENCE_BATCH_SIZE_CHOICES_2D]
        )))
    else:
        reversed_batch_sizes = list(reversed(sorted(
            [batch_choice[0] for batch_choice in constants.INFERENCE_BATCH_SIZE_CHOICES_2D]
        )))

    batch_size = 1

    for batch_size in reversed_batch_sizes:
        calculation_possible, ram_used = try_inference_loop(model_loader, batch_size, input_size)
        if calculation_possible:
            batch_size = batch_size
            break

    return input_size, batch_size, ram_used


def calculate_3d_inference_parameters(model_loader, max_in_size, max_in_depth):
    logging.info(
        f'Calculation procedure:'
        f'\n'
        f'1. use maximum possible input size and depth\n'
        f'2. increase batch size until max is reached'
    )

    used, total, unit = get_memory(model_loader.device)
    if total < 4097:
        max_ram_dependent_depth = 64
    elif total < 6145:
        max_ram_dependent_depth = 128
    elif total < 8193:
        max_ram_dependent_depth = 128
    elif total < 10241:
        max_ram_dependent_depth = 196
    elif total < 12289:
        max_ram_dependent_depth = 196
    else:
        max_ram_dependent_depth = 256  # max if VRAM > 12GB

    in_size_in_depth_map = []
    for in_size, in_depth in create_3d_iteration_map():
        if in_depth <= max_ram_dependent_depth:
            in_size_in_depth_map.append((in_size, in_depth))

    input_size = None
    input_depth = None
    ram_used = 0

    for in_size, in_depth in in_size_in_depth_map:
        if in_size > max_in_size or in_depth > max_in_depth:
            continue
        calculation_possible, ram_used = try_inference_loop(model_loader, 1, in_size, in_depth)
        if calculation_possible:
            input_size = in_size
            input_depth = in_depth
            break

    if input_size is None:
        return None, None, None, None

    if model_loader.device == 'cpu' or model_loader.device == torch.device('cpu'):
        batch_sizes = list(sorted(
            [batch_choice[0] for batch_choice in constants.CPU_INFERENCE_BATCH_SIZE_CHOICES_3D]
        ))
    else:
        batch_sizes = list(sorted(
            [batch_choice[0] for batch_choice in constants.INFERENCE_BATCH_SIZE_CHOICES_3D]
        ))

    batch_size = 1

    for current_batch_size in batch_sizes:
        calculation_possible, current_ram_used = try_inference_loop(model_loader, current_batch_size, input_size, input_depth)
        if calculation_possible:
            batch_size = current_batch_size
            ram_used = current_ram_used
        else:
            break

    return input_size, input_depth, batch_size, ram_used


def log_and_return_stored_values(config):
    logging.info(f'Found optimal parameters.')
    input_size = config.input_height
    input_depth = config.input_depth
    batch_size = config.batch_size
    logging.info(f'Approximated RAM usage: {config.approximated_ram_usage} MiB')
    if input_depth is not None:
        logging.info(
            f'Calculated parameters: input_size={input_size}, input_depth={input_depth}, batch_size={batch_size}')
    else:
        logging.info(
            f'Calculated parameters: input_size={input_size}, batch_size={batch_size}')
    return input_size, input_depth, batch_size


def get_optimal_inference_parameters(model_path, device, max_in_size, max_in_depth=None):
    """
    Run in extra process to release cuda memory completely. Main thread caches due to CUDA drivers
    https://discuss.pytorch.org/t/cuda-memory-is-not-freed-properly/48395/4
    """
    device = torch.device(device)
    checkpoint = torch.load(model_path, map_location=device)
    checkpoint_keys = checkpoint.keys()
    backbone = None
    normalization = None

    if 'backbone' in checkpoint_keys:
        backbone = checkpoint['backbone']
    if 'normalization' in checkpoint_keys:
        normalization = checkpoint['normalization']

    _, total_ram, _ = get_memory(device)

    config_q = InferenceConfiguration.objects.filter(
        architecture=checkpoint['architecture'],
        backbone=backbone,
        normalization=normalization,
        channels=checkpoint['image_channels'],
        classes=checkpoint['classes'],
        device=device,
        device_ram=total_ram,
    )
    current_qs = config_q.filter(
        max_input_size=max_in_size,
        max_input_depth=max_in_depth,
    )
    # check if we had exact settings before
    if current_qs.count() > 0:
        logging.info('Found stored configuration.')
        return log_and_return_stored_values(current_qs[0])
    else:
        logging.info('Calculating inference using this model on this device for the first time can take a while.')
        logging.info('Optimal configuration will be stored for future use.')

    if checkpoint['architecture'] in constants.TWO_D_NETWORKS:
        # check if calculated height of any others is smaller than max and not equal to max
        # input size cannot be > max input size -> if those are unequal, the max was found previously
        current_qs = config_q.filter(
            ~Q(max_input_size=F('input_height'))
        )
    else:
        # 3D here: depth might be the same but max input size must be smaller!
        if max_in_depth is not None:
            current_qs = config_q.filter(max_input_depth__lte=max_in_depth).filter(
                ~Q(max_input_size=F('input_height'))
            )
        else:
            current_qs = config_q.filter(
                ~Q(max_input_size=F('input_height'))
            )

    if current_qs.count() > 0:
        return log_and_return_stored_values(current_qs[0])

    logging.info(f'Calculating optimal inference parameters')
    logging.info('This might take some time.')
    if device == 'cpu' or device == torch.device('cpu'):
        if checkpoint['architecture'] in constants.TWO_D_NETWORKS:
            max_inference_bs_size = max(constants.CPU_INFERENCE_BATCH_SIZE_CHOICES_2D)[0]
        else:
            max_inference_bs_size = max(constants.CPU_INFERENCE_BATCH_SIZE_CHOICES_3D)[0]

        logging.info(
            f'Device is CPU: Batch size limit reduced to {max_inference_bs_size}! Otherwise '
            f'computation will take too long. Check task manager to see ram usage during calculation.'
        )

    model_loader = ModelLoader(
        architecture=checkpoint['architecture'],
        input_channels=checkpoint['image_channels'],
        classes=checkpoint['classes'],
        device=device,
        backbone=backbone,
        pretrained=False,
        normalization=normalization,
    )

    if checkpoint['architecture'] in constants.TWO_D_NETWORKS:
        input_depth = None
        input_size, batch_size, ram_used = calculate_2d_inference_parameters(model_loader, max_in_size)
    else:
        if max_in_depth is None:
            raise ValueError('Pass the value for the maximum depth as well!')
        input_size, input_depth, batch_size, ram_used = calculate_3d_inference_parameters(
            model_loader, max_in_size, max_in_depth
        )
        if input_depth is None:
            raise RuntimeError('Calculation failed.')
    if input_size is None or batch_size is None:
        raise RuntimeError('Calculation failed.')
    logging.info(f'Finished. Approximated RAM usage: {ram_used} MiB')
    InferenceConfiguration.objects.create(
        architecture=checkpoint['architecture'],
        backbone=backbone,
        normalization=normalization,
        channels=checkpoint['image_channels'],
        classes=checkpoint['classes'],
        device=device,
        device_ram=total_ram,
        max_input_size=max_in_size,
        max_input_depth=max_in_depth,
        input_height=input_size,
        input_width=input_size,
        input_depth=input_depth,
        batch_size=batch_size,
        approximated_ram_usage=ram_used,
    )

    if input_depth is not None:
        logging.info(
            f'Calculated parameters: input_size={input_size}, input_depth={input_depth}, batch_size={batch_size}')
    else:
        logging.info(
            f'Calculated parameters: input_size={input_size}, batch_size={batch_size}')

    return input_size, input_depth, batch_size
