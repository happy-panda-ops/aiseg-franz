import logging
import torch

from django.db.models import Q, F

from .utils import get_data_loader, ModelLoader

from aiseg import constants
from aiseg.models import TrainingConfiguration
from aiseg.utils import get_memory, create_3d_iteration_map
from aiseg.multicore_utils import multiprocess_function


def get_targets_outputs_and_loss(input_batch, target_batch, model, loss_func, device, output_channels):
    """
    Used by train and validation loop
    Returns output batch with channels always at out_batch.shape[1]
    """
    input_batch = input_batch.to(device, dtype=torch.float)

    # long for multiclass else we could get too large numbers resulting in nan
    masks_type = torch.float32 if output_channels == 1 else torch.long
    target_batch = target_batch.to(device, dtype=masks_type)

    output_batch = model(input_batch)

    if output_channels == 1:
        loss = loss_func(output_batch.squeeze(dim=1), target_batch)
    else:
        loss = loss_func(output_batch, target_batch)

    return target_batch, output_batch, loss


def try_train_loop(model_loader, batch_size, input_size, input_depth=None, run_subprocess=True):
    # Run in extra process if you are in the main thread to release cuda memory completely.
    # Main thread caches memory due to CUDA drivers and thus
    # correct info about model and optimizer ram usage cannot be displayed. If you not believe, see:
    # https://discuss.pytorch.org/t/cuda-memory-is-not-freed-properly/48395/4
    # or remove the multiprocess function and check the task manager

    logging.info(f'Testing training run with parameters:')
    logging.info(f'batch size: {batch_size}')
    logging.info(f'input size: {input_size}')
    if input_depth is not None:
        logging.info(f'input depth: {input_depth}')

    if run_subprocess:
        calculation_possible, ram_used, error_message = multiprocess_function(
            try_train_loop_threaded,
            [[model_loader], [batch_size], [input_size], [input_depth]]
        )[0]
    else:
        calculation_possible, ram_used, error_message = try_train_loop_threaded(
            model_loader, batch_size, input_size, input_depth
        )

    if calculation_possible:
        logging.info(f'Test worked!')
    else:
        logging.info(f'Calculation not possible')
    return calculation_possible, ram_used, error_message


def try_train_loop_threaded(model_loader, batch_size, input_size, input_depth=None, custom_num_workers=None):
    try:
        if model_loader.architecture == constants.PSANet_NAME:
            model_loader.in_size = input_size
        model, optimizer, loss_func = model_loader.setup_model_optimizer_loss()

        device = model_loader.device

        channels = model.in_channels

        output_channels = model.out_channels

        iterations = 5  # ram usage with workers is stable after 3 to 4 iterations. 5 for safety
        num_samples = iterations * batch_size
        data_loader = get_data_loader(
            model, num_samples, batch_size, channels, input_size, input_depth, custom_num_workers
        )

        ram_used = constants.MiB_VRAM_OFFSET
        model.train()
        train_loss = 0.0
        scaler = torch.cuda.amp.GradScaler()
        with torch.cuda.amp.autocast(enabled=constants.ENABLE_AMP):
            for i, (input_batch, target_batch) in enumerate(data_loader):
                targets, out_logits, loss = get_targets_outputs_and_loss(
                    input_batch, target_batch, model, loss_func, device, output_channels
                )
                scaler.scale(loss).backward()
                scaler.step(optimizer)
                scaler.update()
                optimizer.zero_grad(set_to_none=True)
                train_loss += loss.item() * targets.size(0)

                ram_used, ram_total, _ = get_memory(device)

                # some initializations are missing, no user interface (logging) and user does stuff --> add offset
                # observation: offset is ~600 MB
                ram_used += constants.MiB_VRAM_OFFSET
                if ram_used > ram_total:
                    return False, None, None

    except (RuntimeError, ValueError) as e:
        for non_intuitive_error in constants.NON_INTUITIVE_RAM_ERRORS:
            if non_intuitive_error in e.args[0]:
                # os.environ['PYTORCH_NO_CUDA_MEMORY_CACHING'] = '0'
                model_loader.wipe_memory()
                return False, None, None
        return False, None, e.args[0]

    model_loader.wipe_memory()
    return True, ram_used, None


def calculate_2d_training_parameters(model_loader, max_input_size=None):
    """
    Follows this (there is no "correct" rule) procedure:

    1. try to use a batch size >= 4 for batch norm, 16 max or increase input size as bs to allow batch normalization
        using batch norm is enabled, which can speed up training by deactivating convolutional bias followed by a batch
        norm layer. This prepares for a future todo (issue is created)
        https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm

    2. use largest possible input size
        using multiples of 8 at batch and input size! for mixed precision and amp:  - currently not active, skipped 8
        https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#use-mixed-precision-and-amp

    3. increase batch size until max is reached

    Returns: input_size, batch size, ram_predicted, error_message

    """
    logging.info(f'Calculation follows this (there is no "correct" rule) procedure:')
    input_size = constants.IN_SIZE_CHOICES_2D[0][0]
    logging.info(
        f'\n'
        f'1. try to use the largest batch size, until 4 is reached for effective group norm.\n'
        f'    Starts at batch size == 4, using an input size of {input_size}.\n'
        f'2. use largest possible input size\n'
        f'    Decrease input size from max to smallest until the calculation is possible with current batch size.\n'
        f'3. increase batch size until max is reached.'
    )
    input_size = constants.IN_SIZE_CHOICES_2D[0][0]
    reversed_batch_sizes = list(reversed(sorted(
        [batch_choice[0] for batch_choice in constants.BATCH_SIZE_CHOICES_2D]
    )))
    batch_size = None
    used_ram = None
    error_msg = None

    if max_input_size is not None and input_size > max_input_size:
        error_msg = f'Maximum input size {max_input_size} is larger than minimum size ({input_size}).'
        return None, None, None, error_msg

    # 1.  # start with bs = 4, decrease input size then bs until calculation is possible
    for batch_choice in reversed_batch_sizes:
        if batch_choice > 4:
            continue

        calculation_possible, current_ram, error_msg = try_train_loop(model_loader, batch_choice, input_size)
        if calculation_possible:
            batch_size = batch_choice
            used_ram = current_ram
            break
        elif error_msg is not None:
            break

    if batch_size is None or error_msg is not None:
        return None, None, None, error_msg

    minimum_target_bs = 4
    # batch size is now 4, if we reached this, go from largest to smallest input size
    reversed_input_sizes = list(reversed(sorted(
        [size_choice[0] for size_choice in constants.IN_SIZE_CHOICES_2D]
    )))

    if max_input_size is not None:
        reversed_input_sizes = [size for size in reversed_input_sizes if size <= max_input_size]

    # 2. try to get max input size
    calculation_possible = False
    for input_choice in reversed_input_sizes:
        if input_choice == input_size:
            if len(reversed_input_sizes) == 1:
                calculation_possible = True
            break

        calculation_possible, current_ram, error_msg = try_train_loop(model_loader, batch_size, input_choice)
        if calculation_possible:
            input_size = input_choice
            used_ram = current_ram
            break
        elif error_msg is not None:
            break

    if not calculation_possible or error_msg is not None:
        return input_size, batch_size, used_ram, error_msg

    # 3. check larger batch sizes with current input size. Do not go beneath 4, do not go above 4 * 4
    # (covered by larger input size)
    for batch_choice in reversed_batch_sizes:
        if batch_choice < minimum_target_bs:
            break
        if batch_choice >= 4 * minimum_target_bs:
            if max_input_size is not None and input_size == max_input_size:
                pass
            else:
                # this is covered by the input size estimation if max is not reached
                continue

        if batch_choice == batch_size:
            break

        calculation_possible, current_ram, error_msg = try_train_loop(model_loader, batch_choice, input_size)
        if calculation_possible:
            batch_size = batch_choice
            used_ram = current_ram
            break
        elif error_msg is not None:
            break

    return input_size, batch_size, used_ram, error_msg


def calculate_3d_training_parameters(model_loader, fixed_depth=None, max_input_size=None, max_input_depth=None):
    """
    1. increase batch size until 4 for effective group norm

    if depth is not fixed:
        2. increase input depth until depth == input size (cube)
    else:
        skip this step

    3. increase batch size until 8

    4. minimum requirements are valid! now increase volume size until max is reached, afterwards the batch size

    """
    logging.info(f'Calculation follows this (there is no "correct" rule) procedure:')
    logging.info(
        f'\n'
        f'1. increase batch size until 4 for effective group norm\n'
        f'if depth is not fixed:\n'
        f'    2. increase input depth until depth == input size (cube)\n'
        f'else:\n'
        f'    skip 2.\n'
        f'3. minimum requirements are valid! now increase volume size until max is reached, afterwards the batch size'
    )

    in_size_in_depth_map = create_3d_iteration_map()
    input_size, input_depth = in_size_in_depth_map[-1][:]
    error_msg = None

    if fixed_depth is not None:
        input_depth = fixed_depth
    if max_input_size is not None and input_size > max_input_size:
        error_msg = f'Maximum input size {max_input_size} is larger than minimum size ({input_size}).'
        return None, None, None, None, error_msg

    if max_input_size is not None:
        in_size_in_depth_map = [choice for choice in in_size_in_depth_map if choice[0] <= max_input_size]
    if max_input_depth is not None:
        in_size_in_depth_map = [choice for choice in in_size_in_depth_map if choice[1] <= max_input_depth]

    batch_size = None
    used_ram = 0

    reversed_batch_sizes = list(reversed(sorted(
        [batch_choice[0] for batch_choice in constants.BATCH_SIZE_CHOICES_3D]
    )))

    # 1.
    calculation_possible = False
    for batch_choice in reversed_batch_sizes:
        if batch_choice > 4:
            continue

        calculation_possible, used_ram, error_msg = try_train_loop(model_loader, batch_choice, input_size, input_depth)
        if calculation_possible:
            batch_size = batch_choice
            break
        elif error_msg is not None:
            break

    if not calculation_possible or error_msg is not None:
        return None, None, None, None, error_msg

    # 2.
    calculation_possible = False
    if fixed_depth is None:
        for in_size, in_depth in in_size_in_depth_map:
            if in_size > input_size:
                continue
            if in_size == input_size and in_depth == input_depth:
                if len(in_size_in_depth_map) == 1:
                    calculation_possible = True
                break

            calculation_possible, used_ram, error_msg = try_train_loop(model_loader, batch_size, in_size, in_depth)
            if calculation_possible:
                input_size = in_size
                input_depth = in_depth
                break
            elif error_msg is not None:
                break
    else:
        calculation_possible = True

    if not calculation_possible or error_msg is not None:
        # could not increase input depth or input size or got error, stop
        return input_size, input_depth, batch_size, used_ram, error_msg

    # # 3. increasing batch size to 8 --> useless. skipped
    # calculation_possible = False
    # for batch_choice in reversed_batch_sizes:
    #     if batch_choice > 8:
    #         continue
    #     if batch_choice == batch_size:
    #         break
    #
    #     calculation_possible, current_used_ram, error_msg = try_train_loop(
    #         model_loader, batch_choice, input_size, input_depth
    #     )
    #     if calculation_possible:
    #         batch_size = batch_choice
    #         used_ram = current_used_ram
    #         break
    #     elif error_msg is not None:
    #         break
    #
    # if not calculation_possible or error_msg is not None:
    #     # could not increase batch size anymore
    #     return input_size, input_depth, batch_size, used_ram, error_msg

    # 4.
    # this if else significantly speeds up the calculation process
    # if we cannot even use a bs of 8, then we already found an upper border. Increasing the input size/depth is likely
    # to stop after first few increments -> no need to go from top to bottom. Otherwise, use top to bottom loop
    calculation_possible = False
    if batch_size < 4:
        sorted_in_size_in_depth_map = list(reversed(in_size_in_depth_map))
        current_input_size = input_size
        current_input_depth = input_depth
        if fixed_depth is None:
            for in_size, in_depth in sorted_in_size_in_depth_map:
                if in_size <= current_input_size and in_depth <= current_input_depth:
                    continue

                calculation_possible, current_used_ram, error_msg = try_train_loop(
                    model_loader, batch_size, in_size, in_depth
                )
                if calculation_possible:
                    input_size = in_size
                    input_depth = in_depth
                    used_ram = current_used_ram
                elif error_msg is not None:
                    break
                else:
                    # found best parameters
                    break
        else:
            for in_size, in_depth in sorted_in_size_in_depth_map:
                if in_size <= current_input_size:
                    continue

                calculation_possible, current_used_ram, error_msg = try_train_loop(
                    model_loader, batch_size, in_size, input_depth
                )
                if calculation_possible:
                    input_size = in_size
                    used_ram = current_used_ram
                elif error_msg is not None:
                    break
                else:
                    # found best parameters
                    break
    else:
        if fixed_depth is None:
            for in_size, in_depth in in_size_in_depth_map:
                if in_size == input_size and in_depth == input_depth:
                    break

                calculation_possible, used_ram, error_msg = try_train_loop(model_loader, batch_size, in_size, in_depth)
                if calculation_possible:
                    input_size = in_size
                    input_depth = in_depth
                    break
                elif error_msg is not None:
                    break
        else:
            for in_size, in_depth in in_size_in_depth_map:
                if in_size <= input_size:
                    break

                calculation_possible, used_ram, error_msg = try_train_loop(
                    model_loader, batch_size, in_size, input_depth
                )
                if calculation_possible:
                    input_size = in_size
                    break
                elif error_msg is not None:
                    break
        # we can still increase the batch size (currently 4) even if we could not increase the input size or depth
        calculation_possible = True

    if not calculation_possible or error_msg is not None:
        # already found optimal parameters
        return input_size, input_depth, batch_size, used_ram, error_msg

    # finally, batch size until max
    # this if else significantly speeds up the calculation process
    # if we cannot use the max input size, then we already found an upper border. Increasing the batch size is likely to
    # stop after first few increments -> no need to go from top to bottom. Otherwise, use top to bottom loop
    if (input_size, input_depth) != max(constants.IN_SIZE_CHOICES_3D):
        batch_choices = sorted(reversed_batch_sizes)
        current_batch_size = batch_size
        for batch_choice in batch_choices:
            if batch_choice <= current_batch_size:
                continue

            calculation_possible, current_used_ram, error_msg = try_train_loop(
                model_loader, batch_choice, input_size, input_depth
            )
            if calculation_possible:
                batch_size = batch_choice
                used_ram = current_used_ram
            elif error_msg is not None:
                break
            else:
                # found optimal parameters
                break
    else:
        for batch_choice in reversed_batch_sizes:
            if batch_choice == batch_size:
                break

            calculation_possible, used_ram, error_msg = try_train_loop(
                model_loader, batch_choice, input_size, input_depth
            )
            if calculation_possible:
                batch_size = batch_choice
                break
            elif error_msg is not None:
                break

    return input_size, input_depth, batch_size, used_ram, error_msg


def get_optimal_training_parameters(architecture,
                                    backbone,
                                    normalization,
                                    pretrained,
                                    input_channels,
                                    classes,
                                    device,
                                    fixed_depth=None,
                                    max_in_size=None,
                                    max_in_depth=None,
                                    ):
    """

    Args:
        architecture:
        backbone:
        normalization:
        pretrained:
        input_channels:
        classes:
        device:
        fixed_depth: calculate with a given, fixed depth
        max_in_size: maximum input size in width and height of the dataset
        max_in_depth: maximum depth of the dataset (3D networks)

    Returns: input_size, input_depth, batch_size, predicted_ram

    """
    _, total_ram, _ = get_memory(device)

    optimal_config_q = TrainingConfiguration.objects.filter(
        architecture=architecture,
        backbone='' if backbone is None else backbone,
        device=device,
        total_ram_in_mib=total_ram,
        normalization=normalization,
        pretrained=pretrained,
        channels=input_channels,
        classes=classes,
        fixed_depth=True if fixed_depth is not None else False,
        input_depth=fixed_depth if fixed_depth is not None else None,
    )
    if optimal_config_q.count() > 0:
        # first: try with exact settings:
        current_qs = optimal_config_q.filter(max_input_size=max_in_size)
        if current_qs.count() > 0:
            config = current_qs[0]
            logging.info(f'Found stored optimal parameters.')
            return config.input_height, config.input_depth, config.batch_size, config.approximated_ram_usage, None

        # now: try None (if not already done) and check if the calculated input size is smaller than the current max
        if max_in_size is not None:
            current_qs = optimal_config_q.filter(max_input_size=None, input_height__lte=max_in_size)
            if current_qs.count() > 0:
                config = current_qs[0]
                logging.info(f'Found stored optimal parameters.')
                return config.input_height, config.input_depth, config.batch_size, config.approximated_ram_usage, None

        # last but not least:
        # if a max in size was given for older trainings but the calculated input size is even
        # smaller, the maximum size was calculated already
        current_qs = optimal_config_q.exclude(max_input_size=None).filter(
            ~Q(max_input_size=F('input_height'))
        )
        if current_qs.count() > 0:
            config = current_qs[0]
            logging.info(f'Found stored optimal parameters.')
            return config.input_height, config.input_depth, config.batch_size, config.approximated_ram_usage, None

    logging.info(f'Calculating optimal parameters')
    model_loader = ModelLoader(
        architecture, input_channels, classes, device, backbone, pretrained, normalization
    )
    if architecture in constants.TWO_D_NETWORKS:
        input_depth = None
        input_size, batch_size, used_ram, error_msg = calculate_2d_training_parameters(model_loader, max_in_size)
    else:
        input_size, input_depth, batch_size, used_ram, error_msg = calculate_3d_training_parameters(
            model_loader, fixed_depth, max_in_size, max_in_depth
        )

    if used_ram is None or error_msg is not None:
        logging.info(f'Could not calculate optimal parameters.')
        if error_msg:
            logging.info(f'Error: {error_msg}')
        return input_size, input_depth, batch_size, used_ram, error_msg

    used_ram = int(round(used_ram))

    TrainingConfiguration.objects.create(
        architecture=architecture,
        backbone=backbone if backbone is not None else '',
        device=device,
        total_ram_in_mib=total_ram,
        approximated_ram_usage=used_ram,
        normalization=normalization,
        pretrained=pretrained,
        channels=input_channels,
        classes=classes,
        input_height=input_size,
        input_width=input_size,
        input_depth=input_depth,
        batch_size=batch_size,
        fixed_depth=True if fixed_depth is not None else False,
        max_input_size=max_in_size,
    )
    logging.info(f'Saved optimal parameters.')

    return input_size, input_depth, batch_size, used_ram, None
