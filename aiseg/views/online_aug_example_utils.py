import cv2
import numpy as np
import os
import random


from .. import constants
from ..file_handling.utils import get_files, get_sub_folders, create_folder
from ..file_handling.write import save_volume
from ..inference.utils import map_to_color

from ..models import TrainingParameters
from ..models.online_aug_example_images import OnlineImageAugExamples
from ..train_data_preprocessing.image_manipulation import get_sub_images
from ..training.training_utils import create_online_aug_kwargs_from_db, setup_online_aug_pipe
from ..utils import resize_cv2_or_pil_image, create_file_field_upload_image_from_memory_image, get_human_datetime, \
    open_file


def preprocess_image_and_mask(image_path, mask_path, channels, input_size):
    if channels == 1:
        original_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    else:
        original_img = cv2.imread(image_path, cv2.IMREAD_COLOR)
    original_mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
    h, w, = original_img.shape[:2]
    h_mask, w_mask = original_mask.shape
    if h != h_mask or w != w_mask:
        raise ValueError('Masks and images are not of same shape. Make sure each image has a related mask.')

    if h < input_size or w < input_size:
        # enlarge image with min edge size = input_size
        original_img = resize_cv2_or_pil_image(
            original_img, input_size, interpolation=cv2.INTER_NEAREST, invert=True
        )
        original_mask = resize_cv2_or_pil_image(
            original_mask, input_size, interpolation=cv2.INTER_NEAREST, invert=True
        )
        h, w = original_mask.shape[:2]

    if h > input_size or w > input_size:

        sub_images, sub_coordinates = get_sub_images(original_img, input_size)
        sub_masks, sub_coordinates = get_sub_images(original_mask, input_size)
        sub_images_and_coords = list(zip(sub_images, sub_masks, sub_coordinates))
        sub_image, sub_mask, sub_coordinates = random.choice(sub_images_and_coords)
        sub_image = np.copy(sub_image)
        sub_mask = np.copy(sub_mask)

    else:
        assert h == w
        sub_image = np.copy(original_img)
        sub_mask = np.copy(original_mask)
        sub_coordinates = (0, 0)

    sub_end_coords = (
        sub_coordinates[0] + input_size,
        sub_coordinates[1] + input_size
    )
    sub_coordinates = tuple(reversed(sub_coordinates))  # (y, x) to opencv (x, y)
    sub_end_coords = tuple(reversed(sub_end_coords))  # (y, x) to opencv (x, y)

    if len(original_img.shape) == 3:
        color = (255, 255, 255)
    else:
        color = 255
    original_img = cv2.rectangle(
        original_img, sub_coordinates, sub_end_coords, color, thickness=3, lineType=cv2.LINE_4
    )
    original_mask = cv2.rectangle(
        original_mask, sub_coordinates, sub_end_coords, 255, thickness=3, lineType=cv2.LINE_4
    )

    return original_img, original_mask, sub_image, sub_mask


def create_2d_examples(training_parameters, images_folder, masks_folder, total_examples, random_select):
    images = get_files(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    masks = get_files(masks_folder, extension=constants.SUPPORTED_MASK_TYPES)
    if len(images) != len(masks):
        return f'Unequal amount of images ({len(images)}) and masks ({len(masks)})!'

    images_and_masks = list(zip(images, masks))

    online_aug_kwargs = create_online_aug_kwargs_from_db(training_parameters.architecture)
    augmentation_pipeline = setup_online_aug_pipe(
        online_aug_kwargs,
        training_parameters.architecture,
        training_parameters.input_size,
        training_parameters.input_depth,
        log_info=False,
    )
    multiplier = 0
    for i in range(total_examples):
        if random_select:
            image_path, mask_path = random.choice(images_and_masks)
        else:
            if i > 0 and i % len(images_and_masks) == 0:
                multiplier += 1
            image_path, mask_path = images_and_masks[i - multiplier * len(images_and_masks)]

        if not os.path.isfile(image_path):
            return f'Missing image file: {image_path}'
        if not os.path.isfile(mask_path):
            return f'Missing mask file: {mask_path}'

        original_img, original_mask, sub_img, sub_mask = preprocess_image_and_mask(
            image_path, mask_path, training_parameters.channels, training_parameters.input_size
        )

        out_img, out_mask = augmentation_pipeline(sub_img, sub_mask)

        original_img = resize_cv2_or_pil_image(original_img, 512, invert=True)
        original_mask = resize_cv2_or_pil_image(original_mask, 512, invert=True, interpolation=cv2.INTER_NEAREST)
        sub_img = resize_cv2_or_pil_image(sub_img, 512, invert=True)
        sub_mask = resize_cv2_or_pil_image(sub_mask, 512, invert=True, interpolation=cv2.INTER_NEAREST)
        out_img = resize_cv2_or_pil_image(out_img, 512, invert=True)
        out_mask = resize_cv2_or_pil_image(out_mask, 512, invert=True, interpolation=cv2.INTER_NEAREST)

        original_mask = map_to_color(original_mask, training_parameters.classes)
        sub_mask = map_to_color(sub_mask, training_parameters.classes)
        out_mask = map_to_color(out_mask, training_parameters.classes)

        example = OnlineImageAugExamples.objects.create()
        example.image = create_file_field_upload_image_from_memory_image(original_img)
        example.mask = create_file_field_upload_image_from_memory_image(original_mask)
        example.sub_image = create_file_field_upload_image_from_memory_image(sub_img)
        example.sub_mask = create_file_field_upload_image_from_memory_image(sub_mask)
        example.out_image = create_file_field_upload_image_from_memory_image(out_img)
        example.out_mask = create_file_field_upload_image_from_memory_image(out_mask)
        example.save()
    return None


def get_original_volumes(training_parameters, images, masks):
    if not isinstance(training_parameters, TrainingParameters):
        raise ValueError('training_parameters is no TrainingParameters instance!')

    if len(images) - training_parameters.input_depth == 0:
        start_depth_index = 0
    else:
        start_depth_index = random.randint(0, (len(images) - 1) - training_parameters.input_depth)
    images = images[start_depth_index:start_depth_index + training_parameters.input_depth]
    masks = masks[start_depth_index:start_depth_index + training_parameters.input_depth]

    h, w = cv2.imread(images[0], cv2.IMREAD_GRAYSCALE).shape[:2]
    error_msg = None

    original_volume = np.zeros(
        (training_parameters.input_depth, training_parameters.input_size, training_parameters.input_size),
        dtype=np.uint8
    )
    original_mask = np.zeros(
        (training_parameters.input_depth, training_parameters.input_size, training_parameters.input_size),
        dtype=np.uint8
    )

    img = cv2.imread(images[0], cv2.IMREAD_GRAYSCALE)
    mask = cv2.imread(masks[0], cv2.IMREAD_GRAYSCALE)

    if (h >= training_parameters.input_size and w > training_parameters.input_size) or \
            (h > training_parameters.input_size and w >= training_parameters.input_size):

        sub_images, sub_coordinates = get_sub_images(img, training_parameters.input_size)
        sub_masks, sub_coordinates = get_sub_images(mask, training_parameters.input_size)
        sub_images_and_coords = list(zip(sub_images, sub_masks, sub_coordinates))
        sub_image, sub_mask, sub_coordinates = random.choice(sub_images_and_coords)
        sub_image = np.copy(sub_image)
        sub_mask = np.copy(sub_mask)

    elif h == w and h == training_parameters.input_size:
        sub_image = np.copy(img)
        sub_mask = np.copy(mask)
        sub_coordinates = (0, 0)
    else:
        error_msg = f'The images of the volume: "{os.path.dirname(images[0])}" are too small (h={h}, w={w}, ' \
                    f'min size={training_parameters.input_size})'
        return None, None, error_msg

    original_volume[0] = sub_image
    original_mask[0] = sub_mask

    y_start = sub_coordinates[0]
    x_start = sub_coordinates[1]
    y_end = y_start + training_parameters.input_size
    x_end = x_start + training_parameters.input_size

    for i, (image, mask) in enumerate(zip(images, masks)):
        if i == 0:
            continue
        img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
        mask = cv2.imread(mask, cv2.IMREAD_GRAYSCALE)
        img = img[y_start:y_end, x_start:x_end]
        mask = mask[y_start:y_end, x_start:x_end]
        original_volume[i] = img
        original_mask[i] = mask

    return original_volume, original_mask, error_msg


def convert_volume_masks_to_rgb(mask, class_count):
    out_volume = np.zeros((mask.shape[0], mask.shape[1], mask.shape[2], 3), dtype=np.uint8)
    for z in range(mask.shape[0]):
        mask_slice = mask[z]
        rgb_mask = map_to_color(mask_slice, class_count)
        out_volume[z] = rgb_mask
    return out_volume


def create_3d_examples(training_parameters, volumes_folder, masks_folder, total_examples, random_select, out_folder):
    if not isinstance(training_parameters, TrainingParameters):
        raise ValueError('training_parameters is no TrainingParameters instance!')
    create_folder(out_folder)
    volume_folders = get_sub_folders(volumes_folder, depth=1)
    mask_folders = get_sub_folders(masks_folder, depth=1)

    online_aug_kwargs = create_online_aug_kwargs_from_db(training_parameters.architecture)
    augmentation_pipeline = setup_online_aug_pipe(
        online_aug_kwargs,
        training_parameters.architecture,
        training_parameters.input_size,
        training_parameters.input_depth,
        log_info=False,
    )

    time = get_human_datetime(datetime_format='%Y%m%d_%H%M%S')

    for i in range(total_examples):
        if random_select:
            volume_folder, mask_folder = random.choice(list(zip(volume_folders, mask_folders)))
        else:
            volume_folder = volume_folders[i]
            mask_folder = mask_folders[i]

        images = get_files(volume_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_files(mask_folder, extension=constants.SUPPORTED_MASK_TYPES)
        depth = len(images)
        if depth != len(masks):
            return f'Unequal amount of images ({len(images)}) and masks ({len(masks)}) of volume: "{volume_folder}"'

        if training_parameters.input_depth > depth:
            return f'Volume depth of: "{volume_folder}" is smaller ({depth}) than the input depth ' \
                   f'{training_parameters.input_depth}'

        original_volume, original_mask, error_msg = get_original_volumes(training_parameters, images, masks)
        if error_msg is not None:
            return error_msg

        augmented_volume, augmented_mask = augmentation_pipeline(original_volume, original_mask)

        original_mask = convert_volume_masks_to_rgb(original_mask, training_parameters.classes)
        augmented_mask = convert_volume_masks_to_rgb(augmented_mask, training_parameters.classes)

        current_out_folder = os.path.join(out_folder, f'{time}_example_{i}')
        create_folder(current_out_folder)
        save_volume(original_volume, os.path.join(current_out_folder, 'original_volume'), multiprocessing=False)
        save_volume(original_mask, os.path.join(current_out_folder, 'original_mask'), multiprocessing=False)
        save_volume(augmented_volume, os.path.join(current_out_folder, 'augmented_volume'), multiprocessing=False)
        save_volume(augmented_mask, os.path.join(current_out_folder, 'augmented_mask'), multiprocessing=False)

    open_file(out_folder)

    return None


def create_online_aug_examples(total_examples, random_select):
    training_parameters = TrainingParameters.objects.last()

    dataset_folder = os.path.join(constants.DATASET_FOLDER, training_parameters.dataset)

    if not os.path.isdir(dataset_folder):
        return f'The dataset: "{dataset_folder}" does not exist!'

    images_folder = os.path.join(dataset_folder, 'images')
    masks_folder = os.path.join(dataset_folder, 'masks')

    if training_parameters.architecture in constants.TWO_D_NETWORKS:
        error_message = create_2d_examples(
            training_parameters, images_folder, masks_folder, total_examples, random_select
        )
    else:
        out_folder = os.path.join(dataset_folder, 'online_aug_examples')
        error_message = create_3d_examples(
            training_parameters, images_folder, masks_folder, total_examples, random_select, out_folder
        )
        if error_message is None:
            error_message = f'Can not visualize 3D volumes in browser. The created examples are here: "{out_folder}".'

    return error_message
