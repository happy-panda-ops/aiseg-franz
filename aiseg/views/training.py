import json
import logging
import os
import time
import traceback
from concurrent.futures.process import BrokenProcessPool

from django.contrib import messages
from django.shortcuts import redirect
from django.utils.safestring import mark_safe
from torch import multiprocessing

from .optimal_parameter_calculation.training_parameters import get_optimal_training_parameters
from .utils import aiseg_render as render
from .utils import get_and_check_running_logs_and_warn
from .utils import kill_running_process_if_needed
from .utils import start_process_and_create_log_file
from .. import constants
from ..forms.training import TrainingInputForm, TrainingContinuationForm
from ..models import TrainingParameters
from ..models import TrainingContinuationParameters
from ..temporary_parameters.parameters import FixedParameters
from ..training.training_init import start_training
from ..utils import get_memory, get_time_spent_string, get_human_datetime, kill_children_and_log_help_info


def execute_function(fixed_parameters, function, process_logger, method='', **kwargs):
    if not isinstance(fixed_parameters, FixedParameters):
        raise ValueError('fixed_parameters must be a FixedParameters instance!')

    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting {method} at: {get_human_datetime()}')
        function(fixed_parameters, logger=process_logger, **kwargs)

        time_string = get_time_spent_string(start_time)
        logging.info(f'Finished {method} at: {get_human_datetime()} (time spent: {time_string})')
    except Exception:
        logging.error(traceback.format_exc())
        kill_children_and_log_help_info()
    finally:
        process_logger.remove_logger_handlers()  # To free resources


def init_training(parameters, process_logger):
    if not isinstance(parameters, TrainingParameters):
        raise ValueError('params must be a TrainingParameters instance!')

    params = FixedParameters()
    params.architecture = parameters.architecture
    params.backbone = parameters.backbone
    params.pretrained = parameters.pretrained
    params.dataset_name = parameters.dataset
    params.batch_size = parameters.batch_size
    params.channels = parameters.channels
    params.classes = parameters.classes
    params.epochs = parameters.epochs
    params.save_every = parameters.save_every
    params.input_size = parameters.input_size
    params.input_depth = parameters.input_depth
    params.reproduce = parameters.reproduce
    params.norm = parameters.normalization
    params.lr = float(parameters.learning_rate)
    params.optimizer_name = parameters.optimizer_name
    params.device = parameters.device
    params.online_aug = parameters.online_aug
    params.auto_weight = parameters.auto_weight
    params.multi_gpu_training = True if parameters.num_gpus else False
    params.num_gpus = parameters.num_gpus
    params.lr_scheduler = parameters.lr_scheduler
    params.lr_adjust_step_size = parameters.lr_adjust_step_size
    params.lr_warm_up_period = parameters.lr_warm_up_period
    params.lr_constant_period = parameters.lr_constant_period
    params.lr_decay_period = parameters.lr_decay_period
    params.fine_tuning_epoch = parameters.fine_tuning_epoch

    execute_function(params, start_training, process_logger, method='training')


def init_continuation(parameters, process_logger):
    if not isinstance(parameters, TrainingContinuationParameters):
        raise ValueError('params must be a TrainingParameters instance!')

    params = FixedParameters()
    params.model_path = parameters.model_path
    params.dataset_name = parameters.dataset
    params.batch_size = parameters.batch_size
    params.device = parameters.device
    params.multi_gpu_training = True if parameters.num_gpus and parameters.num_gpus > 1 else False
    params.num_gpus = parameters.num_gpus
    params.epochs = parameters.epochs
    params.save_every = parameters.save_every
    params.reproduce = parameters.reproduce

    execute_function(
        params,
        start_training,
        process_logger,
        method='training continuation',
    )


def overview(request):
    context = {
        'navbar_view': 'training',
    }
    return render(request, 'aiseg/training/training_overview.html', context)


def get_optimal_parameter_calculation_worker(architecture,
                                             backbone,
                                             normalization,
                                             pretrained,
                                             input_channels,
                                             classes,
                                             device,
                                             fixed_depth,
                                             max_in_size,
                                             return_dict,  # for multiprocessing return
                                             process_logger,
                                             ):
    process_logger.set_up_root_logger()
    input_size = None
    input_depth = None
    batch_size = None
    used_ram = None
    error_msg = None
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting optimal parameter calculation at: {get_human_datetime()}')
        logging.info('Using the following parameters:')
        logging.info(f'architecture: {architecture}')
        logging.info(f'backbone: {backbone}')
        logging.info(f'normalization: {normalization}')
        logging.info(f'pretrained: {pretrained}')
        logging.info(f'input_channels: {input_channels}')
        logging.info(f'classes: {classes}')
        logging.info(f'device: {device}')
        logging.info(f'fixed_depth: {fixed_depth}')
        logging.info(f'max_in_size: {max_in_size}')
        logging.info('')
        try:
            input_size, input_depth, batch_size, used_ram, error_msg = get_optimal_training_parameters(
                architecture=architecture,
                backbone=backbone,
                normalization=normalization,
                pretrained=pretrained,
                input_channels=input_channels,
                classes=classes,
                device=device,
                fixed_depth=fixed_depth,
                max_in_size=max_in_size,
            )
            time_string = get_time_spent_string(start_time)
            logging.info(
                f'Finished optimal parameter calculation at: {get_human_datetime()} (time spent: {time_string})')
        except BrokenProcessPool:
            logging.info('Process stopped by user!')

    except Exception:
        logging.error(traceback.format_exc())
        kill_children_and_log_help_info()
    finally:
        process_logger.remove_logger_handlers()  # To free resources

    return_dict[f'input_size'] = input_size
    return_dict[f'input_depth'] = input_depth
    return_dict[f'batch_size'] = batch_size
    return_dict[f'used_ram'] = used_ram
    return_dict[f'error_msg'] = error_msg


def start_process_with_log_and_return(execution_type, device, process_func, process_func_args):
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    process_func_args.append(return_dict)
    p = start_process_and_create_log_file(
        execution_type,
        device,
        process_func,
        parameters=None,
        process_func_args=process_func_args,
        return_process=True,
    )
    p.join()

    try:
        input_size = return_dict['input_size']
        input_depth = return_dict['input_depth']
        batch_size = return_dict['batch_size']
        used_ram = return_dict['used_ram']
        error_msg = return_dict['error_msg']
    except KeyError:
        input_size = None
        input_depth = None
        batch_size = None
        used_ram = None
        error_msg = mark_safe(f'Process killed by user.')
    return input_size, input_depth, batch_size, used_ram, error_msg


def new_training(request):
    running_logs = get_and_check_running_logs_and_warn(request)
    parameters = TrainingParameters.objects.last()
    if parameters is None:
        parameters = TrainingParameters.objects.create()

    used_ram = None
    ram_unit = None
    input_size = None
    input_depth = None
    batch_size = None

    form = TrainingInputForm(request, instance=parameters)
    if request.method == 'POST':
        form = TrainingInputForm(request, request.POST, instance=parameters)
        if 'calculate_optimal_parameters' in request.POST:
            # suppress not necessary form errors, this is only for calculation!
            error_keys_to_delete = []
            keys_errors_to_keep = [
                'architecture', 'backbone', 'pretrained', 'normalization', 'device', 'channels', 'classes'
            ]
            got_errors = False

            for key, value in form.errors.items():
                if key not in keys_errors_to_keep:
                    error_keys_to_delete.append(key)

            for key in error_keys_to_delete:
                del form.errors[key]

            # validate calculation parameters
            if 'calculate_with_fixed_depth' in request.POST:
                keys_errors_to_keep.append('input_depth')

                if int(request.POST['input_depth']) == 0:
                    got_errors = True
                    form.add_error(
                        'input_depth',
                        f'Depth must be larger than 0 when calculating with a fixed depth.'
                    )

            if request.POST['architecture'] == constants.PSANet_NAME:
                form.add_error(
                    'input_size', f'{constants.PSANet_NAME} needs a fixed input size! Auto calculation is not possible.'
                )
                got_errors = True

            if request.POST['architecture'] in constants.THREE_D_NETWORKS and int(request.POST['channels']) == 3:
                form.add_error(
                    'channels',
                    f'{request.POST["architecture"]} is three dimensional. Currently only grayscale (channels = 1) is '
                    f'supported as input.'
                )
                got_errors = True

            if not got_errors:
                fixed_depth = int(request.POST['input_depth']) if 'calculate_with_fixed_depth' in request.POST else None
                if 'max_in_size' in request.POST.keys() and request.POST['max_in_size']:
                    max_in_size = int(request.POST['max_in_size'])
                else:
                    max_in_size = None

                if 'backbone' in request.POST.keys() and request.POST['backbone']:
                    backbone = request.POST['backbone']
                else:
                    backbone = None

                args = [
                    request.POST['architecture'],  # architecture
                    backbone,  # backbone
                    request.POST['normalization'],  # normalization
                    True if 'pretrained' in request.POST else False,  # pretrained
                    int(request.POST['channels']),  # input_channels
                    int(request.POST['classes']),  # classes
                    request.POST['device'],  # device
                    fixed_depth,  # fixed_depth
                    max_in_size,  # max_in_size
                ]

                kill_running_process_if_needed(running_logs, request.POST['device'], request)
                input_size, input_depth, batch_size, used_ram, error_msg = start_process_with_log_and_return(
                    execution_type=constants.DL_EXECUTION_PARAMETER_CALC,
                    device=request.POST['device'],
                    process_func=get_optimal_parameter_calculation_worker,
                    process_func_args=args,
                )

                if input_size is None and error_msg is None:
                    messages.add_message(
                        request,
                        messages.WARNING,
                        "Can't calculate optimal parameters. The network is too complex or any other error occurred. "
                        "Try to close all applications and check the log file."
                    )
                elif error_msg is not None:
                    messages.add_message(
                        request,
                        messages.WARNING,
                        mark_safe(
                            f"Got an error during calculation: <br>{error_msg}<br>"
                        )
                    )
                else:
                    ram_unit = get_memory(request.POST['device'])[2]

                    post = request.POST.copy()
                    post['input_size'] = str(input_size)
                    post['batch_size'] = str(batch_size)
                    if batch_size < 16:
                        post['normalization'] = constants.GROUP_NORM

                    if input_depth is not None:
                        post['input_depth'] = str(input_depth)

                    request.POST = post
                    form = TrainingInputForm(request, request.POST, instance=parameters)

        elif 'calculate_optimal_parameters' not in request.POST and form.is_valid():
            fcd = form.cleaned_data
            if form.has_changed():
                parameters = form.save()

            kill_running_process_if_needed(running_logs, fcd['device'], request)

            if fcd['online_aug']:
                if fcd['architecture'] in constants.TWO_D_NETWORKS:
                    return redirect('aiseg:online_image_aug')
                else:
                    return redirect('aiseg:online_volume_aug')

            log_file = start_process_and_create_log_file(
                execution_type=constants.DL_EXECUTION_TRAIN,
                device=fcd['device'],
                process_func=init_training,
                parameters=parameters,
            )

            return redirect('aiseg:monitor', log_pk=log_file.pk)

    blocked_devices = [log.device for log in running_logs]
    if 'cuda' in blocked_devices:
        blocked_devices = [choice[0] for choice in form.fields['device'].choices]
        blocked_text = 'Load or calculate optimal parameters (kills multi gpu process)'
    else:
        blocked_text = 'Load or calculate optimal parameters (kills process on this device)'

    context = {
        'form': form,
        'navbar_view': 'training',
        'process_is_running': True if running_logs else False,
        'backbone_choices_dict': json.dumps(constants.BACKBONE_CHOICES_DICT),

        'predicted_ram': used_ram,
        'ram_unit': ram_unit,
        'blocked_devices': blocked_devices,
        'calc_button_text': 'Load or calculate optimal parameters',
        'calc_button_blocked_text': blocked_text,

        'input_size': input_size,
        'input_depth': input_depth,
        'batch_size': batch_size,
    }

    return render(request, 'aiseg/training/training.html', context)


def continuing(request):
    running_logs = get_and_check_running_logs_and_warn(request)
    parameters = TrainingContinuationParameters.objects.last()
    if parameters is None:
        parameters = TrainingContinuationParameters.objects.create()

    form = TrainingContinuationForm(request, instance=parameters)
    if request.method == 'POST':
        form = TrainingContinuationForm(request, request.POST, instance=parameters)
        if form.is_valid():
            fcd = form.cleaned_data

            if form.has_changed():
                parameters = form.save()

            kill_running_process_if_needed(running_logs, fcd['device'], request)

            log_file = start_process_and_create_log_file(
                execution_type=constants.DL_EXECUTION_TRAIN,
                device=fcd['device'],
                process_func=init_continuation,
                parameters=parameters,
            )

            return redirect('aiseg:monitor', log_pk=log_file.pk)

    context = {
        'form': form,
        'navbar_view': 'training',
        'process_is_running': True if running_logs else False,
    }

    return render(request, 'aiseg/training/training_continuing.html', context)
