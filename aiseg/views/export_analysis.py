import os

from django.contrib import messages
from django.shortcuts import redirect
from django.utils import timezone

from .. import constants
from ..file_handling.utils import create_folder, convert_binary_to_np_array
from ..file_handling.write import save_csv
from ..forms import ExportForm
from ..models import TrainingDataset, ModelAnalysis
from ..training.saving import save_history
from ..utils import open_file
from .utils import aiseg_render as render


def export_analysis(analysis, out_folder):
    count = 0
    while os.path.isdir(out_folder):
        out_folder = os.path.join(os.path.dirname(out_folder), os.path.basename(out_folder) + f'_{count}')
        count += 1

    create_folder(out_folder)

    header, history = analysis.get_history()
    if header is None:
        return None, None, None, None, None
    save_history(history, out_folder, header)

    # general information:
    info_filename = 'model_information.csv'
    if analysis.training_duration_10_epochs:
        training_duration_10_epochs = f'{analysis.training_duration_10_epochs} s'
    else:
        training_duration_10_epochs = 'not measured'
    normalization = analysis.normalization if analysis.normalization else 'unknown see logs'
    pretrained = analysis.pretrained if analysis.pretrained is not None else 'False'
    backbone = analysis.backbone if analysis.backbone else '/'
    input_depth = analysis.input_depth if analysis.input_depth else '/'
    online_aug = 'Yes' if analysis.online_aug_kwargs_string and analysis.online_aug_kwargs_string != '{}' else 'No'
    custom_name = analysis.custom_name if analysis.custom_name else '/'

    info_data = [
        ('architecture', analysis.architecture),
        ('backbone', backbone),
        ('custom name', custom_name),
        ('normalization', normalization),
        ('pretrained', pretrained),
        ('created at', timezone.localtime(analysis.created_datetime).strftime('%d.%m.%Y %H:%M:%S')),
        ('total epochs', analysis.total_epochs),
        ('batch size', f'{analysis.batch_size}'),  # do not trigger number formatting
        ('channels', f'{analysis.channels}'),
        ('classes', f'{analysis.classes}'),
        ('input size', f'h and w: {analysis.input_height} x {analysis.input_width}'),
        ('input depth', f'{input_depth}'),
        ('storage size', f'{float(analysis.storage_size)} MB'),
        ('time spent for 10 epochs', f'{training_duration_10_epochs}'),
        ('online aug active', f'{online_aug}'),
        ('optimizer name', f'{analysis.optimizer_name}'),
        ('initial learning rate', f'{analysis.init_learning_rate}'),
    ]
    info_header = ', '.join([entry[0] for entry in info_data])
    info_line = [[entry[1] for entry in info_data]]
    save_csv(os.path.join(out_folder, info_filename), info_line, info_header)

    # online aug kwargs if needed
    if analysis.online_aug_kwargs_string and analysis.online_aug_kwargs_string != '{}':
        aug_filename = 'online_aug_parameters.csv'
        aug_kwargs_header = 'Online augmentation arguments'
        aug_data = analysis.get_online_aug_parameter_list(improve_readability=True)
        for i, element in enumerate(aug_data):
            if isinstance(element[1], str) and '&#177;' in element[1]:
                element = list(element)
                element[1] = element[1].replace('&#177;', '+-')
                element = tuple(element)
                aug_data[i] = element
        save_csv(os.path.join(out_folder, aug_filename), aug_data, aug_kwargs_header)

    # class testing results:
    accuracy_data = [
        ('training accuracy %', analysis.training_accuracy),
        ('validation accuracy %', analysis.validation_accuracy),
        ('training loss', analysis.training_loss),
        ('validation loss', analysis.validation_loss),
    ]
    if analysis.test_accuracy:
        accuracy_data.append(('test accuracy %', analysis.test_accuracy))
        accuracy_data.append(('testing all pixels', analysis.testing_all_pixels))
        accuracy_data.append(('testing true positives ratio', analysis.testing_true_positives_ratio))
        accuracy_data.append(('testing true negatives ratio', analysis.testing_true_negatives_ratio))
        accuracy_data.append(('testing false positives ratio', analysis.testing_false_positives_ratio))
        accuracy_data.append(('testing false negatives ratio', analysis.testing_false_negatives_ratio))
        accuracy_data.append(('testing precision', analysis.testing_precision))
        accuracy_data.append(('testing mean iou', analysis.testing_mean_iou))
        accuracy_data.append(('testing dice coefficient', analysis.testing_dice_coefficient))
    else:
        accuracy_data.append(('test accuracy %', '/'))
        accuracy_data.append(('testing all pixels', '/'))
        accuracy_data.append(('testing true positives ratio', '/'))
        accuracy_data.append(('testing true negatives ratio', '/'))
        accuracy_data.append(('testing false positives ratio', '/'))
        accuracy_data.append(('testing false negatives ratio', '/'))
        accuracy_data.append(('testing precision', '/'))
        accuracy_data.append(('testing mean iou', '/'))
        accuracy_data.append(('testing dice coefficient', '/'))

    accuracy_header = ', '.join([entry[0] for entry in accuracy_data])
    accuracy_line = [[f'{entry[1]}' for entry in accuracy_data]]
    accuracy_filename = 'overall_accuracy_data.csv'
    save_csv(os.path.join(out_folder, accuracy_filename), accuracy_line, accuracy_header, number_format='.4f')

    # Single class performance
    single_class_performance_filename = 'single_class_performance.csv'

    single_class_performance_data = None
    single_class_performance_header = 'class, class occurred in gt, accuracy %, precision, mean iou, ' \
                                      'dice coefficient, true positives ratio, true negatives ratio, ' \
                                      'false positives ratio, false negatives ratio, pixels, % of all pixels'
    if analysis.test_accuracy:
        single_class_performance_data = []
        class_suited_for_calculation = convert_binary_to_np_array(
            analysis.testing_class_suited_for_calculation
        )
        true_positives_ratio = convert_binary_to_np_array(analysis.testing_true_positives_ratio_per_class)
        true_negatives_ratio = convert_binary_to_np_array(analysis.testing_true_negatives_ratio_per_class)
        false_positives_ratio = convert_binary_to_np_array(analysis.testing_false_positives_ratio_per_class)
        false_negatives_ratio = convert_binary_to_np_array(analysis.testing_false_negatives_ratio_per_class)
        precision = convert_binary_to_np_array(analysis.testing_precision_per_class)
        mean_iou = convert_binary_to_np_array(analysis.testing_mean_iou_per_class)
        dice_coefficient = convert_binary_to_np_array(analysis.testing_dice_coefficient_per_class)
        pixels = convert_binary_to_np_array(analysis.testing_pixels_per_class)
        pixel_ratio_per_class = pixels / analysis.testing_all_pixels
        accuracy = convert_binary_to_np_array(analysis.testing_accuracy_per_class)

        for class_id in range(class_suited_for_calculation.shape[0]):
            occurred = 'Yes' if class_suited_for_calculation[class_id] else 'No'
            single_class_performance_data.append([
                f'{class_id}',
                f'{occurred}',
                f'{accuracy[class_id] * 100:.3f}',
                f'{precision[class_id]:.5f}',
                f'{mean_iou[class_id]:.5f}',

                f'{dice_coefficient[class_id]:.5f}',
                f'{true_positives_ratio[class_id]:.5f}',
                f'{true_negatives_ratio[class_id]:.5f}',

                f'{false_positives_ratio[class_id]:.5f}',
                f'{false_negatives_ratio[class_id]:.5f}',
                f'{pixels[class_id]}',
                f'{pixel_ratio_per_class[class_id] * 100:.3f}',
            ])

        save_csv(
            os.path.join(out_folder, single_class_performance_filename),
            single_class_performance_data,
            single_class_performance_header,
        )

    return info_data, accuracy_data, single_class_performance_data, single_class_performance_header, out_folder


def export_analyses_with_dataset(dataset_pks):
    create_folder(constants.EXPORT_DIR)
    dataset_and_exports = []
    for dataset in TrainingDataset.objects.filter(pk__in=dataset_pks).order_by('name'):
        analyses_qs = ModelAnalysis.objects.filter(dataset=dataset)
        if analyses_qs.count() == 0:
            continue
        dataset_out_folder = os.path.join(constants.EXPORT_DIR, dataset.folder_name_on_device)
        create_folder(dataset_out_folder)

        comparison_info_data = []
        comparison_info_header = ''
        comparison_info_filename = 'all_models_info.csv'
        comparison_accuracy_data = []
        comparison_accuracy_header = ''
        comparison_accuracy_filename = 'all_models_accuracies.csv'
        dataset_and_exports.append([dataset.name, 0])
        for analysis in analyses_qs:
            arch = analysis.architecture
            backbone = analysis.backbone
            created_datetime = analysis.created_datetime
            created_datetime = timezone.localtime(created_datetime)

            folder_time_string = created_datetime.strftime('y%Y_m%m_d%d_h%H_m%M_s%S')
            if backbone:
                folder_name = f'{arch}_{backbone}_{folder_time_string}'
            else:
                folder_name = f'{arch}_{folder_time_string}'

            out_sub_folder = os.path.join(dataset_out_folder, folder_name)
            info_data, accuracy_data, class_performance_data, class_performance_header, out_folder = export_analysis(
                analysis, out_sub_folder
            )
            if info_data is None:
                continue
            comparison_info_header = ', '.join([entry[0] for entry in info_data])
            comparison_accuracy_header = ', '.join([entry[0] for entry in accuracy_data])
            comparison_info_data.append([entry[1] for entry in info_data])
            comparison_accuracy_data.append([entry[1] for entry in accuracy_data])
            dataset_and_exports[0][1] += 1

        save_csv(
            os.path.join(dataset_out_folder, comparison_info_filename),
            comparison_info_data,
            comparison_info_header,
        )
        save_csv(
            os.path.join(dataset_out_folder, comparison_accuracy_filename),
            comparison_accuracy_data,
            comparison_accuracy_header,
            number_format='.4f'
        )

    open_file(constants.EXPORT_DIR)
    return dataset_and_exports


def export(request):

    if TrainingDataset.objects.all().count() == 0:
        messages.add_message(request, messages.ERROR, f'There is no nothing to export.')
        return redirect('aiseg:trained_model_overview')

    form = ExportForm()
    if request.method == 'POST':
        form = ExportForm(request.POST)
        if form.is_valid():
            fcd = form.cleaned_data
            dataset_qs = TrainingDataset.objects.all()

            dataset_pks_to_export = []
            for dataset in dataset_qs:
                if f'dataset_{dataset.pk}' in fcd and fcd[f'dataset_{dataset.pk}']:
                    dataset_pks_to_export.append(dataset.pk)

            dataset_and_exports = export_analyses_with_dataset(dataset_pks_to_export)
            all_exports = sum([entry[1] for entry in dataset_and_exports])

            messages.add_message(
                request, messages.SUCCESS, f'Successfully exported {all_exports} analyses to {constants.EXPORT_DIR}'
            )
            return redirect('aiseg:trained_model_overview')

    context = {
        'form': form,
        'navbar_view': 'trained_models',
    }
    return render(request, 'aiseg/analysis/export.html', context)
