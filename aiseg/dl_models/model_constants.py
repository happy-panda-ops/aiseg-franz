from aiseg import constants
# possible normalization techniques: batch norm, group norm
NORMALIZATIONS = [constants.BATCH_NORM, constants.GROUP_NORM]

# group norm paper stated, that 16 channels per group are best
# group norm paper stated, that 8 or 32 groups are best
# channels per group = num_features / num_group
# channels per group have less influence when being >= 8 so here i try to set optimal values.
# Paper values:
# groups (G)
# 64   32   16   8    4    2    1 (=LN)
# 24.6 24.1 24.6 24.4 24.6 24.7 25.3  # validation error in %
# 0.5  -    0.5  0.3  0.5  0.6  1.2  # error against 32 groups
# channels per group
# 64   32   16   8    4    2    1 (=IN)
# 24.4 24.5 24.2 24.3 24.8 25.6 28.4  # validation error in %
# 0.2  0.3  -    0.1  0.6  1.4  4.2  # error against 16 channels
GROUP_NORM_LOOKUP = {
    16: 2,  # -> channels per group: 8
    32: 4,  # -> channels per group: 8
    64: 8,  # -> channels per group: 8
    256: 16,  # -> channels per group: 16
    512: 32,  # -> channels per group: 16
    4096: 64,   # -> channels per group: 64
    8192: 128,   # -> channels per group: 64
}
