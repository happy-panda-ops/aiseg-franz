import torch
import torch.nn as nn

# original: https://github.com/sacmehta/ESPNet/blob/master/test/Model.py
# refactored,
# fixed bug for less than 5 classes,
# added possibility to pass gray channel images
# however, it seems to not learn currently. (0% valid accuracy after 3 epochs?! TODO: confirmation needed)

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import group_norm


class CBR(nn.Module):
    """
    This class defines the convolution layer with batch normalization and PReLU activation
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, norm_layer=nn.BatchNorm2d):
        """
        :param in_channels: number of input channels
        :param out_channels: number of output channels
        :param kernel_size: kernel size
        :param stride: stride rate for down-sampling. Default is 1
        """
        super().__init__()
        padding = int((kernel_size - 1) / 2)
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride, padding=padding, bias=False)
        self.bn = norm_layer(out_channels, eps=1e-03)
        self.act = nn.PReLU(out_channels)

    def forward(self, x):
        output = self.conv(x)
        output = self.bn(output)
        output = self.act(output)
        return output


class BR(nn.Module):
    """
    This class groups the batch normalization and PReLU activation
    """

    def __init__(self, out_channels, norm_layer=nn.BatchNorm2d):
        """
        :param out_channels: output feature maps
        """
        super().__init__()
        self.bn = norm_layer(out_channels, eps=1e-03)
        self.act = nn.PReLU(out_channels)

    def forward(self, x):
        output = self.bn(x)
        output = self.act(output)
        return output


class CB(nn.Module):
    """
       This class groups the convolution and batch normalization
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, norm_layer=nn.BatchNorm2d):
        """
        :param in_channels: number of input channels
        :param out_channels: number of output channels
        :param kernel_size: kernel size
        :param stride: optinal stide for down-sampling
        """
        super().__init__()
        padding = int((kernel_size - 1) / 2)
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride, padding=padding, bias=False)
        self.bn = norm_layer(out_channels, eps=1e-03)

    def forward(self, x):
        output = self.conv(x)
        output = self.bn(output)
        return output


class ConvLayer(nn.Module):
    """
    This class is for a convolutional layer.
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1):
        """
        :param in_channels: number of input channels
        :param out_channels: number of output channels
        :param kernel_size: kernel size
        :param stride: optional stride rate for down-sampling
        """
        super().__init__()
        padding = int((kernel_size - 1) / 2)
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride, padding=padding, bias=False)

    def forward(self, x):
        output = self.conv(x)
        return output


class CDilated(nn.Module):
    """
    This class defines the dilated convolution.
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, d=1):
        """
        :param in_channels: number of input channels
        :param out_channels: number of output channels
        :param kernel_size: kernel size
        :param stride: optional stride rate for down-sampling
        :param d: optional dilation rate
        """
        super().__init__()
        padding = int((kernel_size - 1) / 2) * d
        self.conv = nn.Conv2d(
            in_channels, out_channels, kernel_size, stride=stride, padding=padding, bias=False, dilation=d
        )

    def forward(self, x):
        output = self.conv(x)
        return output


class DownSamplerB(nn.Module):
    def __init__(self, in_channels, out_channels, norm_layer=nn.BatchNorm2d):
        super().__init__()
        n = int(out_channels / 5)
        n1 = out_channels - 4 * n
        self.c1 = ConvLayer(in_channels, n, 3, 2)
        self.d1 = CDilated(n, n1, 3, 1, 1)
        self.d2 = CDilated(n, n, 3, 1, 2)
        self.d4 = CDilated(n, n, 3, 1, 4)
        self.d8 = CDilated(n, n, 3, 1, 8)
        self.d16 = CDilated(n, n, 3, 1, 16)
        self.bn = norm_layer(out_channels, eps=1e-3)
        self.act = nn.PReLU(out_channels)

    def forward(self, x):
        output1 = self.c1(x)
        d1 = self.d1(output1)
        d2 = self.d2(output1)
        d4 = self.d4(output1)
        d8 = self.d8(output1)
        d16 = self.d16(output1)

        add1 = d2
        add2 = add1 + d4
        add3 = add2 + d8
        add4 = add3 + d16

        combine = torch.cat([d1, add1, add2, add3, add4], 1)
        output = self.bn(combine)
        output = self.act(output)
        return output


class DilatedParllelResidualBlockB(nn.Module):
    """
    This class defines the ESP block, which is based on the following principle
        Reduce ---> Split ---> Transform --> Merge
    """

    def __init__(self, in_channels, out_channels, add=True, norm_layer=nn.BatchNorm2d):
        """
        :param in_channels: number of input channels
        :param out_channels: number of output channels
        :param add: if true, add a residual connection through identity operation. You can use projection too as
                in ResNet paper, but we avoid to use it if the dimensions are not the same because we do not want to
                increase the module complexity
        """
        super().__init__()
        # bug fix for less than 5 classes
        # original: https://github.com/sacmehta/ESPNet/blob/master/test/Model.py
        n = out_channels // 5
        if n == 0:
            n = 1
            n1 = 1
            self.less_than_5_classes_conv = nn.Conv2d(5, out_channels, 3, 1, 1)
        else:
            self.less_than_5_classes_conv = None
            n1 = out_channels - 4 * n
        # bug fix end for less than 5 classes

        self.c1 = ConvLayer(in_channels, n, 1, 1)
        self.d1 = CDilated(n, n1, 3, 1, 1)  # dilation rate of 2^0
        self.d2 = CDilated(n, n, 3, 1, 2)  # dilation rate of 2^1
        self.d4 = CDilated(n, n, 3, 1, 4)  # dilation rate of 2^2
        self.d8 = CDilated(n, n, 3, 1, 8)  # dilation rate of 2^3
        self.d16 = CDilated(n, n, 3, 1, 16)  # dilation rate of 2^4
        self.bn = BR(out_channels, norm_layer=norm_layer)
        self.add = add

    def forward(self, x):
        # reduce
        output1 = self.c1(x)
        # split and transform
        d1 = self.d1(output1)
        d2 = self.d2(output1)
        d4 = self.d4(output1)
        d8 = self.d8(output1)
        d16 = self.d16(output1)

        # heirarchical fusion for de-gridding
        add1 = d2
        add2 = add1 + d4
        add3 = add2 + d8
        add4 = add3 + d16

        # merge
        combine = torch.cat([d1, add1, add2, add3, add4], 1)

        if self.less_than_5_classes_conv is not None:
            combine = self.less_than_5_classes_conv(combine)

        # if residual version
        if self.add:
            combine = x + combine

        output = self.bn(combine)
        return output


class InputProjectionA(nn.Module):
    """
    This class projects the input image to the same spatial dimensions as the feature map.
    For example, if the input image is 512 x512 x3 and spatial dimensions of feature map size are 56x56xF, then
    this class will generate an output of 56x56x3
    """

    def __init__(self, number_down_samplings):
        """
        :param number_down_samplings: The rate at which you want to down-sample the image
        """
        super().__init__()
        self.pool = nn.ModuleList()
        for i in range(0, number_down_samplings):
            # pyramid-based approach for down-sampling
            self.pool.append(nn.AvgPool2d(3, stride=2, padding=1))

    def forward(self, x):
        for pool in self.pool:
            x = pool(x)
        return x


class ESPNetEncoder(nn.Module):
    """
    This class defines the ESPNet-C network in the paper
    """

    def __init__(self, image_channels, classes, p=5, q=3, norm_layer=nn.BatchNorm2d):
        """
        :param classes: number of classes in the dataset.
        :param p: depth multiplier
        :param q: depth multiplier
        """
        super().__init__()
        self.level1 = CBR(image_channels, 16, 3, 2, norm_layer=norm_layer)
        self.sample1 = InputProjectionA(1)
        self.sample2 = InputProjectionA(2)

        self.b1 = BR(16 + image_channels, norm_layer=norm_layer)
        self.level2_0 = DownSamplerB(16 + image_channels, 64, norm_layer=norm_layer)

        self.level2 = nn.ModuleList()
        for i in range(0, p):
            self.level2.append(DilatedParllelResidualBlockB(64, 64, norm_layer=norm_layer))
        self.b2 = BR(128 + image_channels, norm_layer=norm_layer)

        self.level3_0 = DownSamplerB(128 + image_channels, 128, norm_layer=norm_layer)
        self.level3 = nn.ModuleList()
        for i in range(0, q):
            self.level3.append(DilatedParllelResidualBlockB(128, 128, norm_layer=norm_layer))
        self.b3 = BR(256, norm_layer=norm_layer)

        self.classifier = ConvLayer(256, classes, 1, 1)

    def forward(self, x):
        output0 = self.level1(x)
        inp1 = self.sample1(x)
        inp2 = self.sample2(x)

        output0_cat = self.b1(torch.cat([output0, inp1], 1))
        output1_0 = self.level2_0(output0_cat)  # down-sampled

        output1 = self.level2[0](output1_0)
        for i, layer in enumerate(self.level2, start=1):
            output1 = layer(output1)

        output1_cat = self.b2(torch.cat([output1, output1_0, inp2], 1))

        output2_0 = self.level3_0(output1_cat)  # down-sampled
        output2 = self.level3[0](output2_0)

        for i, layer in enumerate(self.level3, start=1):
            output2 = layer(output2)

        output2_cat = self.b3(torch.cat([output2_0, output2], 1))

        classifier = self.classifier(output2_cat)

        return classifier


class ESPNet(BaseModel):
    """
    This class defines the ESPNet network
    """

    def __init__(self, image_channels, num_classes, p=2, q=3, normalization='bn', **kwargs):
        """
        :param num_classes: number of classes in the dataset. Default is 20 for the cityscapes
        :param p: depth multiplier
        :param q: depth multiplier
        """
        super().__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1
        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.encoder = ESPNetEncoder(image_channels, num_classes, p, q, norm_layer=norm_layer)

        # load the encoder modules
        self.modules = []
        for i, m in enumerate(self.encoder.children()):
            self.modules.append(m)

        # light-weight decoder
        self.level3_C = ConvLayer(128 + image_channels, num_classes, 1, 1)
        self.br = norm_layer(num_classes)
        self.conv = CBR(16 + image_channels + num_classes, num_classes, 3, 1, norm_layer=norm_layer)

        self.up_l3 = nn.Sequential(
            nn.ConvTranspose2d(num_classes, num_classes, 2, stride=2, padding=0, output_padding=0, bias=False)
        )
        self.combine_l2_l3 = nn.Sequential(
            BR(2 * num_classes, norm_layer=norm_layer),
            DilatedParllelResidualBlockB(2 * num_classes, num_classes, add=False, norm_layer=norm_layer)
        )
        self.up_l2 = nn.Sequential(
            nn.ConvTranspose2d(num_classes, num_classes, 2, stride=2, padding=0, output_padding=0, bias=False),
            BR(num_classes, norm_layer=norm_layer)
        )

        self.classifier = nn.ConvTranspose2d(
            num_classes, num_classes, 2, stride=2, padding=0, output_padding=0, bias=False
        )

    def forward(self, x):

        output0 = self.modules[0](x)
        inp1 = self.modules[1](x)
        inp2 = self.modules[2](x)

        output0_cat = self.modules[3](torch.cat([output0, inp1], 1))
        output1_0 = self.modules[4](output0_cat)  # down-sampled
        output1 = self.modules[5][0](output1_0)

        for i, layer in enumerate(self.modules[5]):
            output1 = layer(output1)

        output1_cat = self.modules[6](torch.cat([output1, output1_0, inp2], 1))

        output2_0 = self.modules[7](output1_cat)  # down-sampled
        output2 = self.modules[8][0](output2_0)

        for i, layer in enumerate(self.modules[8]):
            output2 = layer(output2)

        output2_cat = self.modules[9](torch.cat([output2_0, output2], 1))  # concatenate for feature map width expansion

        output2_c = self.up_l3(self.br(self.modules[10](output2_cat)))  # RUM

        output1_C = self.level3_C(output1_cat)  # project to C-dimensional space

        down_combine_l2_l3 = self.combine_l2_l3(torch.cat([output1_C, output2_c], 1))
        comb_l2_l3 = self.up_l2(down_combine_l2_l3)  # RUM

        concat_features = self.conv(torch.cat([comb_l2_l3, output0_cat], 1))

        classifier = self.classifier(concat_features)
        return classifier
