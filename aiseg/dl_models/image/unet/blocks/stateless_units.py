"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from .rnn_cells import ConvGRUCell
from .rnn_cells import ConvLSTMCell


class RnnUnitBase(torch.nn.Module):
    """
    The unit processes sequences of data.
    It resets the hidden states before every new sequence.

    The unit is able to set up multiple lstm or gru cells to go even deeper.
    Rule of thumb: use 2 cells.
    """
    def __init__(self,
                 rnn_cell,
                 in_channels,
                 hidden_channels,
                 kernel_size,
                 bias=True):
        """

        Args:
            rnn_cell: cell to be used
            in_channels: Number of channels in input
            hidden_channels: List of channels for each hidden layer. Length of this sets the number of lstm cells used.
                Rule of thumb: 2 layers. More can be better, must be tested.
                Most likely, more layers will not be better but harder to train.
                Output will have shape of last hidden
            kernel_size (tuple/list of tuples): Overall kernel size of the filter or list of kernel sizes like channels
            bias: CNN Layer bias -> adds learnable bias to the output.
                See also:
                    https://stackoverflow.com/questions/51959507/does-bias-in-the-convolutional-layer-really-make-a-difference-to-the-test-accura
        """
        super(RnnUnitBase, self).__init__()

        if not isinstance(kernel_size, tuple) and not isinstance(kernel_size, list):
            raise ValueError('`kernel_size` must be tuple or list of tuples')

        if isinstance(kernel_size, list):
            # all kernels should be tuples
            if not all([isinstance(elem, tuple) for elem in kernel_size]):
                raise ValueError('`kernel_size` must be tuple or list of tuples')
        else:
            # make list of tuples
            kernel_size = self.extend_for_multilayer(kernel_size, len(hidden_channels))

        self.input_dim = in_channels
        self.hidden_dim = hidden_channels
        self.kernel_size = kernel_size
        self.num_layers = len(hidden_channels)
        self.bias = bias

        cell_list = []
        for i in range(self.num_layers):
            cur_input_dim = self.input_dim if i == 0 else self.hidden_dim[i - 1]

            cell_list.append(rnn_cell(
                in_channels=cur_input_dim,
                hidden_channels=self.hidden_dim[i],
                kernel_size=self.kernel_size[i],
                bias=self.bias,
            ))

        self.cell_list = torch.nn.ModuleList(cell_list)

    @staticmethod
    def extend_for_multilayer(param, num_layers):
        if not isinstance(param, list):
            param = [param] * num_layers
        return param

    def init_cells_hidden(self, height, width):
        init_states = []
        for i in range(self.num_layers):
            init_states.append(self.cell_list[i].init_hidden(height, width))
        return init_states

    def forward(self, input_tensor):
        raise NotImplementedError('implement at your class')


class ConvLSTM(RnnUnitBase):

    def __init__(self,
                 in_channels,
                 hidden_channels,
                 kernel_size,
                 bias=True):
        """

        Args:
            in_channels: Number of channels in input
            hidden_channels: List of channels for each hidden layer. Length of this sets the number of lstm cells used.
                Rule of thumb: 2 layers. More can be better, must be tested.
                Most likely, more layers will not be better but harder to train.
                Output will have shape of last hidden
            kernel_size (tuple/list of tuples): Overall kernel size of the filter or list of kernel sizes like channels
            bias: CNN Layer bias -> adds learnable bias to the output.
                See also:
                    https://stackoverflow.com/questions/51959507/does-bias-in-the-convolutional-layer-really-make-a-difference-to-the-test-accura
            return_all_layers: Return the list of computations for all layers for debugging or whatever you need
        """
        super(ConvLSTM, self).__init__(
            ConvLSTMCell,
            in_channels,
            hidden_channels,
            kernel_size,
            bias=bias,
        )

    def forward(self, input_tensor):
        """
        Args:
            input_tensor: [f, c, h, w)
                f ... tensor of stacked images e.g. 5: four latest images and current image to segment
                c ... in channels
                h ... height
                w ... width

        Returns: last hidden state of shape [1, C, H, W]

        """
        sequence_length, _, height, width = input_tensor.shape
        hidden_state = self.init_cells_hidden(height, width)

        # if instantiated with 0 cells, return frame to process which is the last one in each sequence of each batch
        h = input_tensor[-1, :, :, :].unsqueeze(0)

        for layer_idx in range(self.num_layers):

            h, c = hidden_state[layer_idx]
            output_inner = []
            for t in range(sequence_length):
                h, c = self.cell_list[layer_idx](input_tensor[t, :, :, :].unsqueeze(0), [h, c])
                output_inner.append(h)

            layer_output = torch.stack(output_inner).squeeze()
            input_tensor = layer_output

        # ht of each batch
        return h


class ConvGRU(RnnUnitBase):

    def __init__(self,
                 in_channels,
                 hidden_channels,
                 kernel_size,
                 bias=True):
        """

        Args:
            in_channels: Number of channels in input
            hidden_channels: List of channels for each hidden layer. Length of this sets the number of lstm cells used.
                Rule of thumb: 2 layers. More can be better, must be tested.
                Most likely, more layers will not be better but harder to train.
                Output will have shape of last hidden
            kernel_size (tuple/list of tuples): Overall kernel size of the filter or list of kernel sizes like channels
            bias: CNN Layer bias -> adds learnable bias to the output.
                See also:
                    https://stackoverflow.com/questions/51959507/does-bias-in-the-convolutional-layer-really-make-a-difference-to-the-test-accura
            return_all_layers: Return the list of computations for all layers for debugging or whatever you need
        """
        super(ConvGRU, self).__init__(
            ConvGRUCell,
            in_channels,
            hidden_channels,
            kernel_size,
            bias=bias,
        )

    def forward(self, input_tensor):
        """
        Args:
            input_tensor: [f, c, h, w]
                Pass related batch_first parameter for ConvLSTM!
                f ... tensor of stacked images e.g. 5: four latest images and current image to segment
                c ... in channels
                h ... height
                w ... width

        Returns: last hidden state of shape [1, C, H, W]

        """
        sequence_length, _, height, width = input_tensor.shape
        hidden_state = self.init_cells_hidden(height, width)

        # if instantiated with 0 cells, return frame to process which is the last one in each sequence of each batch
        h = input_tensor[-1, :, :, :].unsqueeze(0)

        for layer_idx in range(self.num_layers):

            h = hidden_state[layer_idx]
            output_inner = []
            for t in range(sequence_length):
                h = self.cell_list[layer_idx](input_tensor[t, :, :, :].unsqueeze(0), h)
                output_inner.append(h)

            layer_output = torch.stack(output_inner).squeeze()
            input_tensor = layer_output

        return h
