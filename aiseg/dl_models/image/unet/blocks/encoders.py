import torch

from .conv_relu_blocks import ConvConvReluBlock
from .nn_blocks import DoubleConvReluBlock


class BaseEncoder(torch.nn.Module):
    def __init__(self, input_channels, output_channels, is_3d, conv_block, normalization='gn'):
        super(BaseEncoder, self).__init__()

        self.conv_block = conv_block(input_channels, output_channels, is_3d=is_3d, normalization=normalization)
        pool = torch.nn.MaxPool3d if is_3d else torch.nn.MaxPool2d
        self.pool = pool(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.conv_block(x)  # skip connection
        x = self.pool(x)  # next depth
        return x


class DoubleCREncoder(BaseEncoder):
    """
    Conv -> Norm -> ReLU -> Conv -> Norm -> ReLU -> MaxPool(2x2)
    """
    def __init__(self, input_channels, output_channels, is_3d=False, normalization='gn'):
        super(DoubleCREncoder, self).__init__(
            input_channels, output_channels, is_3d, DoubleConvReluBlock, normalization
        )


class CCREncoder(BaseEncoder):
    """
    Conv -> Conv -> Norm -> ReLU -> MaxPool(2x2)
    """
    def __init__(self, input_channels, output_channels, is_3d=False, normalization='gn'):
        super(CCREncoder, self).__init__(
            input_channels, output_channels, is_3d, ConvConvReluBlock, normalization
        )
