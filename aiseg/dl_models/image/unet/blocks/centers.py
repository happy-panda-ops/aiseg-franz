import torch

from .conv_relu_blocks import ConvReluBlock
from .interpolation import Interpolate
from .stateful_units import StatefulConvGRU
from .stateful_units import StatefulConvLSTM
from aiseg.dl_models.utils import get_group_norm_groups


class CenterInterpolate(torch.nn.Module):

    def __init__(self, in_channels, mid_channels, out_channels, normalization='gn'):
        super(CenterInterpolate, self).__init__()

        self.center = torch.nn.Sequential(
            Interpolate(scale_factor=2, mode='bilinear'),
            ConvReluBlock(in_channels, mid_channels, normalization=normalization),
            ConvReluBlock(mid_channels, out_channels, normalization=normalization),
        )
        self.pool = torch.nn.MaxPool2d(kernel_size=(2, 2))

    def forward(self, x):
        x = self.pool(x)
        x = self.center(x)
        return x


class StatefulCenter(torch.nn.Module):

    def __init__(self,
                 in_channels,
                 mid_channels,
                 out_channels,
                 normalization='gn',
                 use_gru=True,
                 device='cuda',
                 recurrent_depth=1):
        super(StatefulCenter, self).__init__()

        rnn_class = StatefulConvGRU if use_gru else StatefulConvLSTM
        self.rnn_layer = rnn_class(
            mid_channels,
            hidden_channels=[out_channels for i in range(recurrent_depth)],
            kernel_size=(3, 3),
            device=device,
        )

        if normalization == 'bn':
            self.norm = torch.nn.BatchNorm2d(out_channels)
        else:
            self.norm = torch.nn.GroupNorm(get_group_norm_groups(out_channels), out_channels)

        self.center = torch.nn.Sequential(
            Interpolate(scale_factor=2, mode='bilinear'),
            ConvReluBlock(in_channels, mid_channels, normalization=normalization),
        )
        self.pool = torch.nn.MaxPool2d(kernel_size=(2, 2))

    def forward(self, x, reset_state):
        x = self.pool(x)
        x = self.center(x)
        x = self.rnn_layer(x, reset_state)
        x = self.norm(x)
        return x
