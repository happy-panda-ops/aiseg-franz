import torch
import torch.nn as nn
import torch.nn.functional as F

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import group_norm


def channel_shuffle(x, groups):
    batchsize, num_channels, height, width = x.data.size()

    channels_per_group = num_channels // groups

    # reshape
    x = x.view(batchsize, groups,
               channels_per_group, height, width)

    # transpose
    # - contiguous() required if transpose() is used before view().
    #   See https://github.com/pytorch/pytorch/issues/764
    x = torch.transpose(x, 1, 2).contiguous()

    # flatten
    x = x.view(batchsize, -1, height, width)

    return x


class CBR(nn.Module):
    """
    This class defines the convolution layer with batch normalization and PReLU activation
    """

    def __init__(self, nIn, nOut, kSize, stride=1, norm_layer=nn.BatchNorm2d):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        :param kSize: kernel size
        :param stride: stride rate for down-sampling. Default is 1
        """
        super().__init__()
        padding = int((kSize - 1) / 2)

        self.conv = nn.Conv2d(nIn, nOut, (kSize, kSize), stride=stride, padding=(padding, padding), bias=False)
        self.bn = norm_layer(nOut)
        self.act = nn.PReLU(nOut)

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        output = self.conv(x)
        output = self.bn(output)
        output = self.act(output)
        return output


class SeparableCBR(nn.Module):
    """
    This class defines the convolution layer with batch normalization and PReLU activation
    """

    def __init__(self, nIn, nOut, kSize, stride=1, norm_layer=nn.BatchNorm2d):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        :param kSize: kernel size
        :param stride: stride rate for down-sampling. Default is 1
        """
        super().__init__()
        padding = int((kSize - 1) / 2)

        self.conv = nn.Sequential(
            nn.Conv2d(nIn, nIn, (kSize, kSize), stride=stride, padding=(padding, padding), groups=nIn, bias=False),
            nn.Conv2d(nIn, nOut, kernel_size=1, stride=1, bias=False),
        )
        self.bn = norm_layer(nOut)
        self.act = nn.PReLU(nOut)

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        output = self.conv(x)
        output = self.bn(output)
        output = self.act(output)
        return output


class SqueezeBlock(nn.Module):
    def __init__(self, exp_size, divide=4.0):
        super(SqueezeBlock, self).__init__()

        if divide > 1:
            self.dense = nn.Sequential(
                nn.Linear(exp_size, int(exp_size / divide)),
                nn.PReLU(int(exp_size / divide)),
                nn.Linear(int(exp_size / divide), exp_size),
                nn.PReLU(exp_size),
            )
        else:
            self.dense = nn.Sequential(
                nn.Linear(exp_size, exp_size),
                nn.PReLU(exp_size)
            )

    def forward(self, x):
        batch, channels, height, width = x.size()
        out = torch.nn.functional.avg_pool2d(x, kernel_size=[height, width]).view(batch, -1)
        out = self.dense(out)
        out = out.view(batch, channels, 1, 1)
        # out = hard_sigmoid(out)

        return out * x


class SEseparableCBR(nn.Module):
    """
    This class defines the convolution layer with batch normalization and PReLU activation
    """

    def __init__(self, nIn, nOut, kSize, stride=1, divide=2.0, norm_layer=nn.BatchNorm2d):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        :param kSize: kernel size
        :param stride: stride rate for down-sampling. Default is 1
        """
        super().__init__()
        padding = int((kSize - 1) / 2)

        self.conv = nn.Sequential(
            nn.Conv2d(nIn, nIn, (kSize, kSize), stride=stride, padding=(padding, padding), groups=nIn, bias=False),
            SqueezeBlock(nIn, divide=divide),
            nn.Conv2d(nIn, nOut, kernel_size=1, stride=1, bias=False),
        )

        self.bn = norm_layer(nOut)
        self.act = nn.PReLU(nOut)

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        output = self.conv(x)

        output = self.bn(output)
        output = self.act(output)
        return output


class BR(nn.Module):
    """
        This class groups the batch normalization and PReLU activation
    """

    def __init__(self, nOut, norm_layer=nn.BatchNorm2d):
        """
        :param nOut: output feature maps
        """
        super().__init__()
        self.bn = norm_layer(nOut)
        self.act = nn.PReLU(nOut)

    def forward(self, x):
        """
        :param x: x feature map
        :return: normalized and thresholded feature map
        """
        output = self.bn(x)
        output = self.act(output)
        return output


class CB(nn.Module):
    """
       This class groups the convolution and batch normalization
    """

    def __init__(self, nIn, nOut, kSize, stride=1, norm_layer=nn.BatchNorm2d):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        :param kSize: kernel size
        :param stride: optinal stide for down-sampling
        """
        super().__init__()
        padding = int((kSize - 1) / 2)
        self.conv = nn.Conv2d(nIn, nOut, (kSize, kSize), stride=stride, padding=(padding, padding), bias=False)
        self.bn = norm_layer(nOut)

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        output = self.conv(x)
        output = self.bn(output)
        return output


class C(nn.Module):
    """
    This class is for a convolutional layer.
    """

    def __init__(self, nIn, nOut, kSize, stride=1, group=1):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        :param kSize: kernel size
        :param stride: optional stride rate for down-sampling
        """
        super().__init__()
        padding = int((kSize - 1) / 2)
        self.conv = nn.Conv2d(nIn, nOut, (kSize, kSize), stride=stride,
                              padding=(padding, padding), bias=False, groups=group)

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        output = self.conv(x)
        return output


class S2block(nn.Module):
    """
    This class defines the dilated convolution.
    """

    def __init__(self, nIn, nOut, config, norm_layer=nn.BatchNorm2d):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        """
        super().__init__()
        kSize = config[0]
        avgsize = config[1]

        self.resolution_down = False
        if avgsize > 1:
            self.resolution_down = True
            self.down_res = nn.AvgPool2d(avgsize, avgsize)
            self.up_res = nn.UpsamplingBilinear2d(scale_factor=avgsize)
            self.avgsize = avgsize

        padding = int((kSize - 1) / 2)
        self.conv = nn.Sequential(
            nn.Conv2d(nIn, nIn, kernel_size=(kSize, kSize), stride=1,
                      padding=(padding, padding), groups=nIn, bias=False),
            norm_layer(nIn))

        self.act_conv1x1 = nn.Sequential(
            nn.PReLU(nIn),
            nn.Conv2d(nIn, nOut, kernel_size=1, stride=1, bias=False),
        )

        self.bn = norm_layer(nOut)

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        if self.resolution_down:
            x = self.down_res(x)
        output = self.conv(x)

        output = self.act_conv1x1(output)
        if self.resolution_down:
            output = self.up_res(output)
        return self.bn(output)


class S2module(nn.Module):
    """
    This class defines the ESP block, which is based on the following principle
        Reduce ---> Split ---> Transform --> Merge
    """

    def __init__(self, nIn, nOut, add=True, config=None, norm_layer=nn.BatchNorm2d):
        """
        :param nIn: number of x channels
        :param nOut: number of output channels
        :param add: if true, add a residual connection through identity operation. You can use projection too as
                in ResNet paper, but we avoid to use it if the dimensions are not the same because we do not want to
                increase the module complexity
        """
        super().__init__()
        if config is None:
            config = [[3, 1], [5, 1]]

        group_n = len(config)
        n = int(nOut / group_n)
        n1 = nOut - group_n * n

        self.c1 = C(nIn, n, 1, 1, group=group_n)

        for i in range(group_n):
            var_name = 'd{}'.format(i + 1)
            if i == 0:
                self.__dict__["_modules"][var_name] = S2block(n, n + n1, config[i], norm_layer=norm_layer)
            else:
                self.__dict__["_modules"][var_name] = S2block(n, n, config[i], norm_layer=norm_layer)

        self.BR = BR(nOut, norm_layer=norm_layer)
        self.add = add
        self.group_n = group_n

    def forward(self, x):
        """
        :param x: x feature map
        :return: transformed feature map
        """
        # reduce
        output1 = self.c1(x)
        output1 = channel_shuffle(output1, self.group_n)

        for i in range(self.group_n):
            var_name = 'd{}'.format(i + 1)
            result_d = self.__dict__["_modules"][var_name](output1)
            if i == 0:
                combine = result_d
            else:
                combine = torch.cat([combine, result_d], 1)

        # if residual version
        if self.add:
            combine = x + combine
        output = self.BR(combine)
        return output


class XProjectionA(nn.Module):
    """
    This class projects the x image to the same spatial dimensions as the feature map.
    For example, if the x image is 512 x512 x3 and spatial dimensions of feature map size are 56x56xF, then
    this class will generate an output of 56x56x3
    """

    def __init__(self, down_sampling_count):
        """
        :param down_sampling_count: The rate at which you want to down-sample the image
        """
        super().__init__()
        self.pool = nn.ModuleList()
        for i in range(0, down_sampling_count):
            # pyramid-based approach for down-sampling
            self.pool.append(nn.AvgPool2d(2, stride=2))

    def forward(self, x):
        """
        :param x: x RGB Image
        :return: down-sampled image (pyramid-based approach)
        """
        for pool in self.pool:
            x = pool(x)
        return x


class SINetEncoder(nn.Module):

    def __init__(self, config, image_channels, classes, p=5, q=3, chnn=1.0, norm_layer=nn.BatchNorm2d):
        """
        :param classes: number of classes in the dataset. Default is 20 for the cityscapes
        :param p: depth multiplier
        :param q: depth multiplier
        """
        super().__init__()
        dim1 = 16
        dim2 = 48 + 4 * (chnn - 1)
        dim3 = 96 + 4 * (chnn - 1)

        self.level1 = CBR(image_channels, 12, 3, 2, norm_layer=norm_layer)

        self.level2_0 = SEseparableCBR(12, dim1, 3, 2, divide=1, norm_layer=norm_layer)

        self.level2 = nn.ModuleList()
        for i in range(0, p):
            if i == 0:
                self.level2.append(S2module(dim1, dim2, config=config[i], add=False, norm_layer=norm_layer))
            else:
                self.level2.append(S2module(dim2, dim2, config=config[i], norm_layer=norm_layer))
        self.BR2 = BR(dim2 + dim1, norm_layer=norm_layer)

        self.level3_0 = SEseparableCBR(dim2 + dim1, dim2, 3, 2, divide=2, norm_layer=norm_layer)
        self.level3 = nn.ModuleList()
        for i in range(0, q):
            if i == 0:
                self.level3.append(S2module(dim2, dim3, config=config[2 + i], add=False, norm_layer=norm_layer))
            else:
                self.level3.append(S2module(dim3, dim3, config=config[2 + i], norm_layer=norm_layer))
        self.BR3 = BR(dim3 + dim2, norm_layer=norm_layer)

        self.classifier = C(dim3 + dim2, classes, 1, 1)

    def forward(self, x):
        """
        :param x: Receives the x RGB image
        :return: the transformed feature map with spatial dimensions 1/8th of the x image
        """
        output1 = self.level1(x)  # 8h 8w

        output2_0 = self.level2_0(output1)  # 4h 4w

        for i, layer in enumerate(self.level2):
            if i == 0:
                output2 = layer(output2_0)
            else:
                output2 = layer(output2)  # 2h 2w

        output3_0 = self.level3_0(self.BR2(torch.cat([output2_0, output2], 1)))  # h w

        for i, layer in enumerate(self.level3):
            if i == 0:
                output3 = layer(output3_0)
            else:
                output3 = layer(output3)

        output3_cat = self.BR3(torch.cat([output3_0, output3], 1))

        classifier = self.classifier(output3_cat)

        return classifier


class SINet(BaseModel):

    def __init__(self, image_channels, num_classes, p=2, q=8, chnn=1, normalization='bn', **kwargs):
        """
        :param num_classes: number of classes in the dataset. Default is 20 for the cityscapes
        :param p: depth multiplier
        :param q: depth multiplier
        """
        super().__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1
        config = [[[3, 1], [5, 1]], [[3, 1], [3, 1]],
                  [[3, 1], [5, 1]], [[3, 1], [3, 1]], [[5, 1], [3, 2]], [[5, 2], [3, 4]],
                  [[3, 1], [3, 1]], [[5, 1], [5, 1]], [[3, 2], [3, 4]], [[3, 1], [5, 2]]]
        dim2 = 48 + 4 * (chnn - 1)

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.encoder = SINetEncoder(config, image_channels, num_classes, p, q, chnn, norm_layer=norm_layer)

        self.up = nn.UpsamplingBilinear2d(scale_factor=2)  # (scale_factor=2, mode='bilinear')
        self.bn_3 = norm_layer(num_classes, eps=1e-03)

        self.level2_C = CBR(dim2, num_classes, 1, 1, norm_layer=norm_layer)

        self.bn_2 = norm_layer(num_classes, eps=1e-03)

        # self.classifier = nn.ConvTranspose2d(classes, classes, 2, stride=2, padding=0, output_padding=0, bias=False)

        self.classifier = nn.Sequential(
            nn.UpsamplingBilinear2d(scale_factor=2),
            nn.Conv2d(num_classes, num_classes, 3, 1, 1, bias=False))

    def forward(self, x):
        """
        :param x: RGB image
        :return: transformed feature map
        """
        output1 = self.encoder.level1(x)  # 8h 8w
        output2_0 = self.encoder.level2_0(output1)  # 4h 4w

        for i, layer in enumerate(self.encoder.level2):
            if i == 0:
                output2 = layer(output2_0)
            else:
                output2 = layer(output2)  # 2h 2w

        output3_0 = self.encoder.level3_0(self.encoder.BR2(torch.cat([output2_0, output2], 1)))  # h w

        for i, layer in enumerate(self.encoder.level3):
            if i == 0:
                output3 = layer(output3_0)
            else:
                output3 = layer(output3)

        output3_cat = self.encoder.BR3(torch.cat([output3_0, output3], 1))
        enc_final = self.encoder.classifier(output3_cat)  # 1/8

        dnc_stage1 = self.bn_3(self.up(enc_final))  # 1/4
        stage1_confidence = torch.max(nn.Softmax2d()(dnc_stage1), dim=1)[0]
        b, c, h, w = dnc_stage1.size()
        # TH = torch.mean(torch.median(stage1_confidence.view(b,-1),dim=1)[0])

        stage1_gate = (1 - stage1_confidence).unsqueeze(1).expand(b, c, h, w)

        dnc_stage2_0 = self.level2_C(output2)  # 2h 2w
        dnc_stage2 = self.bn_2(self.up(dnc_stage2_0 * stage1_gate + dnc_stage1))  # 4h 4w

        classifier = self.classifier(dnc_stage2)

        return classifier
