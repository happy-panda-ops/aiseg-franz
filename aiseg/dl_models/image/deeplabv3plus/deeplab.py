import torch.nn as nn
import torch.nn.functional as F

from aiseg.dl_models.base_model import BaseModel
from .backbone import drn
from .backbone import resnet
from .backbone import xception
from .sync_batchnorm.batchnorm import SynchronizedBatchNorm2d
from .aspp import build_aspp
from .decoder import build_decoder
from aiseg.dl_models.utils import group_norm


def build_backbone(backbone, output_stride, norm_layer, image_channels):
    if backbone == 'resnet101':
        model = resnet.ResNet101(output_stride, norm_layer)
    elif backbone == 'xception':
        model = xception.AlignedXception(output_stride, norm_layer)
    elif backbone == 'drn':
        model = drn.drn_d_54(norm_layer)
    else:
        raise NotImplementedError(f'Backbone {backbone} is not supported')

    if image_channels != 3:
        if backbone == 'resnet101':
            model.conv1 = nn.Conv2d(image_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)
        elif backbone == 'xception':
            model.conv1 = nn.Conv2d(image_channels, 32, 3, stride=2, padding=1, bias=False)
        elif backbone == 'drn':
            model.conv1 = nn.Conv2d(image_channels, 16, kernel_size=7, stride=1, padding=3, bias=False)
        else:
            raise NotImplementedError

    return model


class DeepLabV3Plus(BaseModel):
    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone='resnet101',
                 output_stride=16,
                 sync_bn=False,
                 freeze_bn=False,
                 normalization='bn',
                 **kwargs):
        super(DeepLabV3Plus, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1

        if backbone == 'drn':
            output_stride = 8

        if sync_bn and normalization != 'gn':
            norm_layer = SynchronizedBatchNorm2d
        else:
            norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.backbone = build_backbone(backbone, output_stride, norm_layer, image_channels)

        if backbone == 'drn' and image_channels != 3:
            in_planes = self.backbone.layer0[0].out_channels
            self.backbone.layer0 = nn.Sequential(
                nn.Conv2d(image_channels, in_planes, kernel_size=7, stride=1, padding=3, bias=False),
                norm_layer(in_planes),
                nn.ReLU(inplace=True)
            )

        self.aspp = build_aspp(backbone, output_stride, norm_layer)
        self.decoder = build_decoder(num_classes, backbone, norm_layer)

        self.freeze_bn = freeze_bn

    def forward(self, in_tensor):
        x, low_level_feat = self.backbone(in_tensor)
        x = self.aspp(x)
        x = self.decoder(x, low_level_feat)
        x = F.interpolate(x, size=in_tensor.size()[2:], mode='bilinear', align_corners=True)

        return x

    def freeze_bn(self):
        for m in self.modules():
            if isinstance(m, SynchronizedBatchNorm2d):
                m.eval()
            elif isinstance(m, nn.BatchNorm2d):
                m.eval()

    def get_1x_lr_params(self):
        modules = [self.backbone]
        for i in range(len(modules)):
            for m in modules[i].named_modules():
                if self.freeze_bn:
                    if isinstance(m[1], nn.Conv2d):
                        for p in m[1].parameters():
                            if p.requires_grad:
                                yield p
                else:
                    if isinstance(m[1], nn.Conv2d) or isinstance(m[1], SynchronizedBatchNorm2d) \
                            or isinstance(m[1], nn.BatchNorm2d):
                        for p in m[1].parameters():
                            if p.requires_grad:
                                yield p

    def get_10x_lr_params(self):
        modules = [self.aspp, self.decoder]
        for i in range(len(modules)):
            for m in modules[i].named_modules():
                if self.freeze_bn:
                    if isinstance(m[1], nn.Conv2d):
                        for p in m[1].parameters():
                            if p.requires_grad:
                                yield p
                else:
                    if isinstance(m[1], nn.Conv2d) or isinstance(m[1], SynchronizedBatchNorm2d) \
                            or isinstance(m[1], nn.BatchNorm2d):
                        for p in m[1].parameters():
                            if p.requires_grad:
                                yield p
