import torch
import torch.nn.functional as F

from torch import nn
from collections import OrderedDict
from torch.nn import BatchNorm2d

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import group_norm


def get_parameters(version):
    bn_size = 4
    drop_rate = 0

    if version == 121:
        growth_rate = 32
        num_init_features = 64
        block_config = (6, 12, 24, 16)
        d_feature0 = 128
        d_feature1 = 64
    elif version == 161:
        growth_rate = 48
        num_init_features = 96
        block_config = (6, 12, 36, 24)
        d_feature0 = 512
        d_feature1 = 128
    elif version == 169:
        growth_rate = 32
        num_init_features = 64
        block_config = (6, 12, 32, 32)
        d_feature0 = 336
        d_feature1 = 168
    elif version == 201:
        growth_rate = 32
        num_init_features = 64
        block_config = (6, 12, 48, 32)
        d_feature0 = 480
        d_feature1 = 240
    else:
        raise NotImplementedError()

    return bn_size, drop_rate, growth_rate, num_init_features, block_config, d_feature0, d_feature1


class DenseASPP(BaseModel):
    """
    * output_scale can only set as 8 or 16
    Versions: 121, 161, 169, 201
    """

    def __init__(self, image_channels, num_classes, output_stride=8, version=161, normalization='bn'):
        super(DenseASPP, self).__init__(image_channels, num_classes)

        if num_classes == 2:
            num_classes = 1

        bn_size, drop_rate, growth_rate, num_init_features, block_config, d_feature0, d_feature1 = get_parameters(
            version
        )

        dropout0 = 0.1
        dropout1 = 0.1

        feature_size = int(output_stride / 8)

        # First convolution
        self.features = nn.Sequential(OrderedDict([
            ('conv0', nn.Conv2d(image_channels, num_init_features, kernel_size=7, stride=2, padding=3, bias=False)),
            ('norm0', BatchNorm2d(num_init_features) if normalization == 'bn' else group_norm(num_init_features)),
            ('relu0', nn.ReLU(inplace=True)),
            ('pool0', nn.MaxPool2d(kernel_size=3, stride=2, padding=1)),
        ]))

        # Each denseblock
        num_features = num_init_features
        # block1*****************************************************************************************************
        block = _DenseBlock(num_layers=block_config[0], num_input_features=num_features,
                            bn_size=bn_size, growth_rate=growth_rate, drop_rate=drop_rate, normalization=normalization)
        self.features.add_module('denseblock%d' % 1, block)
        num_features = num_features + block_config[0] * growth_rate

        trans = _Transition(num_input_features=num_features, num_output_features=num_features // 2,
                            normalization=normalization)
        self.features.add_module('transition%d' % 1, trans)
        num_features = num_features // 2

        # block2*****************************************************************************************************
        block = _DenseBlock(num_layers=block_config[1], num_input_features=num_features,
                            bn_size=bn_size, growth_rate=growth_rate, drop_rate=drop_rate, normalization=normalization)
        self.features.add_module('denseblock%d' % 2, block)
        num_features = num_features + block_config[1] * growth_rate

        trans = _Transition(num_input_features=num_features, num_output_features=num_features // 2, stride=feature_size,
                            normalization=normalization)
        self.features.add_module('transition%d' % 2, trans)
        num_features = num_features // 2

        # block3*****************************************************************************************************
        block = _DenseBlock(num_layers=block_config[2], num_input_features=num_features, bn_size=bn_size,
                            growth_rate=growth_rate, drop_rate=drop_rate, dilation_rate=int(2 / feature_size),
                            normalization=normalization)
        self.features.add_module('denseblock%d' % 3, block)
        num_features = num_features + block_config[2] * growth_rate

        trans = _Transition(num_input_features=num_features, num_output_features=num_features // 2, stride=1,
                            normalization=normalization)
        self.features.add_module('transition%d' % 3, trans)
        num_features = num_features // 2

        # block4*****************************************************************************************************
        block = _DenseBlock(num_layers=block_config[3], num_input_features=num_features, bn_size=bn_size,
                            growth_rate=growth_rate, drop_rate=drop_rate, dilation_rate=int(4 / feature_size),
                            normalization=normalization)
        self.features.add_module('denseblock%d' % 4, block)
        num_features = num_features + block_config[3] * growth_rate

        trans = _Transition(num_input_features=num_features, num_output_features=num_features // 2, stride=1,
                            normalization=normalization)
        self.features.add_module('transition%d' % 4, trans)
        num_features = num_features // 2

        # Final batch norm
        self.features.add_module(
            'norm5', BatchNorm2d(num_features) if normalization == 'bn' else group_norm(num_features)
        )
        if feature_size > 1:
            self.features.add_module('upsample', nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True))

        self.ASPP_3 = _DenseAsppBlock(input_num=num_features, num1=d_feature0, num2=d_feature1,
                                      dilation_rate=3, drop_out=dropout0, bn_start=False, normalization=normalization)

        self.ASPP_6 = _DenseAsppBlock(input_num=num_features + d_feature1 * 1, num1=d_feature0, num2=d_feature1,
                                      dilation_rate=6, drop_out=dropout0, bn_start=True, normalization=normalization)

        self.ASPP_12 = _DenseAsppBlock(input_num=num_features + d_feature1 * 2, num1=d_feature0, num2=d_feature1,
                                       dilation_rate=12, drop_out=dropout0, bn_start=True, normalization=normalization)

        self.ASPP_18 = _DenseAsppBlock(input_num=num_features + d_feature1 * 3, num1=d_feature0, num2=d_feature1,
                                       dilation_rate=18, drop_out=dropout0, bn_start=True, normalization=normalization)

        self.ASPP_24 = _DenseAsppBlock(input_num=num_features + d_feature1 * 4, num1=d_feature0, num2=d_feature1,
                                       dilation_rate=24, drop_out=dropout0, bn_start=True, normalization=normalization)
        num_features = num_features + 5 * d_feature1

        self.classification = nn.Sequential(
            nn.Dropout2d(p=dropout1),
            nn.Conv2d(in_channels=num_features, out_channels=num_classes, kernel_size=1, padding=0),
            nn.Upsample(scale_factor=8, mode='bilinear', align_corners=True),
        )

    def forward(self, _input):
        feature = self.features(_input)

        aspp3 = self.ASPP_3(feature)
        feature = torch.cat((aspp3, feature), dim=1)

        aspp6 = self.ASPP_6(feature)
        feature = torch.cat((aspp6, feature), dim=1)

        aspp12 = self.ASPP_12(feature)
        feature = torch.cat((aspp12, feature), dim=1)

        aspp18 = self.ASPP_18(feature)
        feature = torch.cat((aspp18, feature), dim=1)

        aspp24 = self.ASPP_24(feature)
        feature = torch.cat((aspp24, feature), dim=1)

        cls = self.classification(feature)

        return cls


class _DenseAsppBlock(nn.Sequential):
    """ ConvNet block for building DenseASPP. """

    def __init__(self, input_num, num1, num2, dilation_rate, drop_out, bn_start=True, normalization='bn'):
        super(_DenseAsppBlock, self).__init__()
        if bn_start:
            self.add_module('norm_1', BatchNorm2d(
                input_num, momentum=0.0003) if normalization == 'bn' else group_norm(input_num, eps=0.0003)),

        self.add_module('relu_1', nn.ReLU(inplace=True)),
        self.add_module('conv_1', nn.Conv2d(in_channels=input_num, out_channels=num1, kernel_size=1)),

        self.add_module('norm_2', BatchNorm2d(
            num1, momentum=0.0003) if normalization == 'bn' else group_norm(num1, eps=0.0003)),
        self.add_module('relu_2', nn.ReLU(inplace=True)),
        self.add_module('conv_2', nn.Conv2d(in_channels=num1, out_channels=num2, kernel_size=3,
                                            dilation=dilation_rate, padding=dilation_rate)),

        self.drop_rate = drop_out

    def forward(self, _input):
        feature = super(_DenseAsppBlock, self).forward(_input)

        if self.drop_rate > 0:
            feature = F.dropout2d(feature, p=self.drop_rate, training=self.training)

        return feature


class _DenseLayer(nn.Sequential):
    def __init__(self, num_input_features, growth_rate, bn_size, drop_rate, dilation_rate=1, normalization='bn'):
        super(_DenseLayer, self).__init__()
        self.add_module('norm_1', BatchNorm2d(
            num_input_features) if normalization == 'bn' else group_norm(num_input_features))
        self.add_module('relu_1', nn.ReLU(inplace=True))
        self.add_module('conv_1', nn.Conv2d(num_input_features, bn_size *
                                            growth_rate, kernel_size=1, stride=1, bias=False))
        self.add_module('norm_2', BatchNorm2d(
            bn_size * growth_rate) if normalization == 'bn' else group_norm(bn_size * growth_rate)),
        self.add_module('relu_2', nn.ReLU(inplace=True))
        self.add_module('conv_2', nn.Conv2d(bn_size * growth_rate, growth_rate,
                                            kernel_size=3, stride=1, dilation=dilation_rate, padding=dilation_rate,
                                            bias=False))
        self.drop_rate = drop_rate

    def forward(self, x):
        new_features = super(_DenseLayer, self).forward(x)
        if self.drop_rate > 0:
            new_features = F.dropout(new_features, p=self.drop_rate, training=self.training)
        return torch.cat([x, new_features], 1)


class _DenseBlock(nn.Sequential):
    def __init__(self,
                 num_layers,
                 num_input_features,
                 bn_size, growth_rate,
                 drop_rate,
                 dilation_rate=1,
                 normalization='bn'):
        super(_DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(num_input_features + i * growth_rate, growth_rate,
                                bn_size, drop_rate, dilation_rate=dilation_rate, normalization=normalization)
            self.add_module('denselayer%d' % (i + 1), layer)


class _Transition(nn.Sequential):
    def __init__(self, num_input_features, num_output_features, stride=2, normalization='bn'):
        super(_Transition, self).__init__()
        self.add_module('norm', BatchNorm2d(
            num_input_features) if normalization == 'bn' else group_norm(num_input_features))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv2d(num_input_features, num_output_features, kernel_size=1, stride=1, bias=False))
        if stride == 2:
            self.add_module('pool', nn.AvgPool2d(kernel_size=2, stride=stride))


class DenseASPP121(DenseASPP):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseASPP121, self).__init__(
            image_channels, num_classes, output_stride=8, version=121, normalization=normalization
        )


class DenseASPP161(DenseASPP):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseASPP161, self).__init__(
            image_channels, num_classes, output_stride=8, version=161, normalization=normalization
        )


class DenseASPP169(DenseASPP):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseASPP169, self).__init__(
            image_channels, num_classes, output_stride=8, version=169, normalization=normalization
        )


class DenseASPP201(DenseASPP):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseASPP201, self).__init__(
            image_channels, num_classes, output_stride=8, version=201, normalization=normalization
        )
