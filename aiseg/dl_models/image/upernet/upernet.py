import torch
import torch.nn as nn
import torch.nn.functional as F

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import batch_norm_to_group_norm
from aiseg.dl_models.utils import group_norm
from aiseg.dl_models.utils import load_backbone


def up_and_add(x, y):
    return F.interpolate(x, size=(y.size(2), y.size(3)), mode='bilinear', align_corners=True) + y


class PSPModule(nn.Module):
    # In the original inmplementation they use precise RoI pooling 
    # Instead of using adaptative average pooling
    def __init__(self, in_channels, bin_sizes=[1, 2, 4, 6], norm_layer=nn.BatchNorm2d):
        super(PSPModule, self).__init__()
        self.norm_layer = norm_layer
        out_channels = in_channels // len(bin_sizes)
        self.stages = nn.ModuleList([self._make_stages(in_channels, out_channels, b_s)
                                     for b_s in bin_sizes])
        self.bottleneck = nn.Sequential(
            nn.Conv2d(in_channels + (out_channels * len(bin_sizes)), in_channels,
                      kernel_size=3, padding=1, bias=False),
            self.norm_layer(in_channels),
            nn.ReLU(inplace=True),
            nn.Dropout2d(0.1)
        )

    def _make_stages(self, in_channels, out_channels, bin_sz):
        prior = nn.AdaptiveAvgPool2d(output_size=bin_sz)
        conv = nn.Conv2d(in_channels, out_channels, kernel_size=1, bias=False)
        bn = self.norm_layer(out_channels)
        relu = nn.ReLU(inplace=True)
        return nn.Sequential(prior, conv, bn, relu)

    def forward(self, features):
        h, w = features.size()[2], features.size()[3]
        pyramids = [features]
        pyramids.extend([F.interpolate(stage(features), size=(h, w), mode='bilinear',
                                       align_corners=True) for stage in self.stages])
        output = self.bottleneck(torch.cat(pyramids, dim=1))
        return output


class ResNet(nn.Module):
    def __init__(self, loaded_model, backbone_channels, output_stride=16):
        super(ResNet, self).__init__()

        self.initial = nn.Sequential(*list(loaded_model.children())[:4])
        self.layer1 = loaded_model.layer1
        self.layer2 = loaded_model.layer2
        self.layer3 = loaded_model.layer3
        self.layer4 = loaded_model.layer4

        if output_stride == 16:
            s3, s4, d3, d4 = (2, 1, 1, 2)
        elif output_stride == 8:
            s3, s4, d3, d4 = (1, 1, 2, 4)

        if output_stride == 8:
            for n, m in self.layer3.named_modules():
                if 'conv1' in n and (backbone_channels == 512):  # resnet 18 or 34
                    m.dilation, m.padding, m.stride = (d3, d3), (d3, d3), (s3, s3)
                elif 'conv2' in n:
                    m.dilation, m.padding, m.stride = (d3, d3), (d3, d3), (s3, s3)
                elif 'downsample.0' in n:
                    m.stride = (s3, s3)

        for n, m in self.layer4.named_modules():
            if 'conv1' in n and backbone_channels == 512:
                m.dilation, m.padding, m.stride = (d4, d4), (d4, d4), (s4, s4)
            elif 'conv2' in n:
                m.dilation, m.padding, m.stride = (d4, d4), (d4, d4), (s4, s4)
            elif 'downsample.0' in n:
                m.stride = (s4, s4)

    def forward(self, x):
        x = self.initial(x)
        x1 = self.layer1(x)
        x2 = self.layer2(x1)
        x3 = self.layer3(x2)
        x4 = self.layer4(x3)

        return [x1, x2, x3, x4]


class FPNFuse(nn.Module):
    def __init__(self, feature_channels, norm_layer=nn.BatchNorm2d):
        super(FPNFuse, self).__init__()
        fpn_out = feature_channels[0]
        self.conv1x1 = nn.ModuleList([nn.Conv2d(ft_size, fpn_out, kernel_size=1)
                                      for ft_size in feature_channels[1:]])
        self.smooth_conv = nn.ModuleList([nn.Conv2d(fpn_out, fpn_out, kernel_size=3, padding=1)]
                                         * (len(feature_channels) - 1))
        self.conv_fusion = nn.Sequential(
            nn.Conv2d(len(feature_channels) * fpn_out, fpn_out, kernel_size=3, padding=1, bias=False),
            norm_layer(fpn_out),
            nn.ReLU(inplace=True)
        )

    def forward(self, features):
        features[1:] = [conv1x1(feature) for feature, conv1x1 in zip(features[1:], self.conv1x1)]
        p = [up_and_add(features[i], features[i - 1]) for i in reversed(range(1, len(features)))]
        p = [smooth_conv(x) for smooth_conv, x in zip(self.smooth_conv, p)]
        p = list(reversed(p))
        p.append(features[-1])  # P = [P1, P2, P3, P4]
        h, w = p[0].size(2), p[0].size(3)
        p[1:] = [F.interpolate(feature, size=(h, w), mode='bilinear', align_corners=True) for feature in p[1:]]

        x = self.conv_fusion(torch.cat(p, dim=1))
        return x


class UPerNet(BaseModel):
    # Implementing only the object path
    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone='resnet101',
                 pretrained=True,
                 normalization='bn',
                 **kwargs):
        super(UPerNet, self).__init__(image_channels, num_classes)
        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        loaded_model, backbone_channels = load_backbone(backbone, pretrained)
        if norm_layer != nn.BatchNorm2d:
            loaded_model = batch_norm_to_group_norm(loaded_model)

        if image_channels != 3:
            loaded_model.conv1 = nn.Conv2d(image_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)

        # resnet 18/34: [64, 128, 256, 512]
        # others: [256, 512, 1024, 2048]
        feature_channels = list(reversed([backbone_channels // (2**i) for i in range(4)]))

        self.backbone = ResNet(loaded_model, backbone_channels)

        self.PPN = PSPModule(backbone_channels, norm_layer=norm_layer)
        self.FPN = FPNFuse(feature_channels, norm_layer=norm_layer)
        self.head = nn.Conv2d(feature_channels[0], num_classes, kernel_size=3, padding=1)

    def forward(self, x):
        input_size = (x.size()[2], x.size()[3])

        features = self.backbone(x)
        features[-1] = self.PPN(features[-1])
        x = self.head(self.FPN(features))

        x = F.interpolate(x, size=input_size, mode='bilinear', align_corners=True)
        return x
