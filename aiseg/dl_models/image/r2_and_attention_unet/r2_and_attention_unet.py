import torch
import torch.nn as nn

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import group_norm


class ConvBlock(nn.Module):
    def __init__(self, ch_in, ch_out, norm_layer=nn.BatchNorm2d):
        super(ConvBlock, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(ch_in, ch_out, kernel_size=3, stride=1, padding=1, bias=True),
            norm_layer(ch_out),
            nn.ReLU(inplace=True),
            nn.Conv2d(ch_out, ch_out, kernel_size=3, stride=1, padding=1, bias=True),
            norm_layer(ch_out),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv(x)
        return x


class UpConv(nn.Module):
    def __init__(self, ch_in, ch_out, norm_layer=nn.BatchNorm2d):
        super(UpConv, self).__init__()
        self.up = nn.Sequential(
            nn.Upsample(scale_factor=2),
            nn.Conv2d(ch_in, ch_out, kernel_size=3, stride=1, padding=1, bias=True),
            norm_layer(ch_out),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.up(x)
        return x


class RecurrentBlock(nn.Module):
    def __init__(self, ch_out, t=2, norm_layer=nn.BatchNorm2d):
        super(RecurrentBlock, self).__init__()
        self.t = t
        self.ch_out = ch_out
        self.conv = nn.Sequential(
            nn.Conv2d(ch_out, ch_out, kernel_size=3, stride=1, padding=1, bias=True),
            norm_layer(ch_out),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x1 = x  # for t = 0
        for i in range(self.t):

            if i == 0:
                x1 = self.conv(x)

            x1 = self.conv(x + x1)
        return x1


class RCNNBlock(nn.Module):
    def __init__(self, ch_in, ch_out, t=2, norm_layer=nn.BatchNorm2d):
        super(RCNNBlock, self).__init__()
        self.RCNN = nn.Sequential(
            RecurrentBlock(ch_out, t=t, norm_layer=norm_layer),
            RecurrentBlock(ch_out, t=t, norm_layer=norm_layer)
        )
        self.Conv_1x1 = nn.Conv2d(ch_in, ch_out, kernel_size=1, stride=1, padding=0)

    def forward(self, x):
        x = self.Conv_1x1(x)
        x1 = self.RCNN(x)
        return x + x1


class SingleConv(nn.Module):
    def __init__(self, ch_in, ch_out, norm_layer=nn.BatchNorm2d):
        super(SingleConv, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(ch_in, ch_out, kernel_size=3, stride=1, padding=1, bias=True),
            norm_layer(ch_out),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv(x)
        return x


class AttentionBlock(nn.Module):
    def __init__(self, F_g, F_l, F_int, norm_layer=nn.BatchNorm2d):
        super(AttentionBlock, self).__init__()
        self.W_g = nn.Sequential(
            nn.Conv2d(F_g, F_int, kernel_size=1, stride=1, padding=0, bias=True),
            norm_layer(F_int)
        )

        self.W_x = nn.Sequential(
            nn.Conv2d(F_l, F_int, kernel_size=1, stride=1, padding=0, bias=True),
            norm_layer(F_int)
        )

        self.psi = nn.Sequential(
            nn.Conv2d(F_int, 1, kernel_size=1, stride=1, padding=0, bias=True),
            norm_layer(1),
            nn.Sigmoid()
        )

        self.relu = nn.ReLU(inplace=True)

    def forward(self, g, x):
        g1 = self.W_g(g)
        x1 = self.W_x(x)
        psi = self.relu(g1 + x1)
        psi = self.psi(psi)

        return x * psi


class R2UNet(BaseModel):
    # This is no time related recurrent network!
    # If you want to do time series analysis, see at: unet/stateful_unet.py
    def __init__(self, image_channels, num_classes, t=2, normalization='bn', **kwargs):
        super(R2UNet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1
        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.Maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.Upsample = nn.Upsample(scale_factor=2)

        self.RRCNN1 = RCNNBlock(ch_in=image_channels, ch_out=64, t=t, norm_layer=norm_layer)

        self.RRCNN2 = RCNNBlock(ch_in=64, ch_out=128, t=t, norm_layer=norm_layer)

        self.RRCNN3 = RCNNBlock(ch_in=128, ch_out=256, t=t, norm_layer=norm_layer)

        self.RRCNN4 = RCNNBlock(ch_in=256, ch_out=512, t=t, norm_layer=norm_layer)

        self.RRCNN5 = RCNNBlock(ch_in=512, ch_out=1024, t=t, norm_layer=norm_layer)

        self.Up5 = UpConv(ch_in=1024, ch_out=512, norm_layer=norm_layer)
        self.Up_RRCNN5 = RCNNBlock(ch_in=1024, ch_out=512, t=t, norm_layer=norm_layer)

        self.Up4 = UpConv(ch_in=512, ch_out=256, norm_layer=norm_layer)
        self.Up_RRCNN4 = RCNNBlock(ch_in=512, ch_out=256, t=t, norm_layer=norm_layer)

        self.Up3 = UpConv(ch_in=256, ch_out=128, norm_layer=norm_layer)
        self.Up_RRCNN3 = RCNNBlock(ch_in=256, ch_out=128, t=t, norm_layer=norm_layer)

        self.Up2 = UpConv(ch_in=128, ch_out=64, norm_layer=norm_layer)
        self.Up_RRCNN2 = RCNNBlock(ch_in=128, ch_out=64, t=t, norm_layer=norm_layer)

        self.Conv_1x1 = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, padding=0)

    def forward(self, x):
        # encoding path
        x1 = self.RRCNN1(x)

        x2 = self.Maxpool(x1)
        x2 = self.RRCNN2(x2)

        x3 = self.Maxpool(x2)
        x3 = self.RRCNN3(x3)

        x4 = self.Maxpool(x3)
        x4 = self.RRCNN4(x4)

        x5 = self.Maxpool(x4)
        x5 = self.RRCNN5(x5)

        # decoding + concat path
        d5 = self.Up5(x5)
        d5 = torch.cat((x4, d5), dim=1)
        d5 = self.Up_RRCNN5(d5)

        d4 = self.Up4(d5)
        d4 = torch.cat((x3, d4), dim=1)
        d4 = self.Up_RRCNN4(d4)

        d3 = self.Up3(d4)
        d3 = torch.cat((x2, d3), dim=1)
        d3 = self.Up_RRCNN3(d3)

        d2 = self.Up2(d3)
        d2 = torch.cat((x1, d2), dim=1)
        d2 = self.Up_RRCNN2(d2)

        d1 = self.Conv_1x1(d2)

        return d1


class AttentionUNet(BaseModel):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(AttentionUNet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.Maxpool = nn.MaxPool2d(kernel_size=2, stride=2)

        self.Conv1 = ConvBlock(ch_in=image_channels, ch_out=64, norm_layer=norm_layer)
        self.Conv2 = ConvBlock(ch_in=64, ch_out=128, norm_layer=norm_layer)
        self.Conv3 = ConvBlock(ch_in=128, ch_out=256, norm_layer=norm_layer)
        self.Conv4 = ConvBlock(ch_in=256, ch_out=512, norm_layer=norm_layer)
        self.Conv5 = ConvBlock(ch_in=512, ch_out=1024, norm_layer=norm_layer)

        self.Up5 = UpConv(ch_in=1024, ch_out=512, norm_layer=norm_layer)
        self.Att5 = AttentionBlock(F_g=512, F_l=512, F_int=256, norm_layer=norm_layer)
        self.Up_conv5 = ConvBlock(ch_in=1024, ch_out=512, norm_layer=norm_layer)

        self.Up4 = UpConv(ch_in=512, ch_out=256, norm_layer=norm_layer)
        self.Att4 = AttentionBlock(F_g=256, F_l=256, F_int=128, norm_layer=norm_layer)
        self.Up_conv4 = ConvBlock(ch_in=512, ch_out=256, norm_layer=norm_layer)

        self.Up3 = UpConv(ch_in=256, ch_out=128, norm_layer=norm_layer)
        self.Att3 = AttentionBlock(F_g=128, F_l=128, F_int=64, norm_layer=norm_layer)
        self.Up_conv3 = ConvBlock(ch_in=256, ch_out=128, norm_layer=norm_layer)

        self.Up2 = UpConv(ch_in=128, ch_out=64, norm_layer=norm_layer)
        self.Att2 = AttentionBlock(F_g=64, F_l=64, F_int=32, norm_layer=norm_layer)
        self.Up_conv2 = ConvBlock(ch_in=128, ch_out=64, norm_layer=norm_layer)

        self.Conv_1x1 = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, padding=0)

    def forward(self, x):
        # encoding path
        x1 = self.Conv1(x)

        x2 = self.Maxpool(x1)
        x2 = self.Conv2(x2)

        x3 = self.Maxpool(x2)
        x3 = self.Conv3(x3)

        x4 = self.Maxpool(x3)
        x4 = self.Conv4(x4)

        x5 = self.Maxpool(x4)
        x5 = self.Conv5(x5)

        # decoding + concat path
        d5 = self.Up5(x5)
        x4 = self.Att5(g=d5, x=x4)
        d5 = torch.cat((x4, d5), dim=1)
        d5 = self.Up_conv5(d5)

        d4 = self.Up4(d5)
        x3 = self.Att4(g=d4, x=x3)
        d4 = torch.cat((x3, d4), dim=1)
        d4 = self.Up_conv4(d4)

        d3 = self.Up3(d4)
        x2 = self.Att3(g=d3, x=x2)
        d3 = torch.cat((x2, d3), dim=1)
        d3 = self.Up_conv3(d3)

        d2 = self.Up2(d3)
        x1 = self.Att2(g=d2, x=x1)
        d2 = torch.cat((x1, d2), dim=1)
        d2 = self.Up_conv2(d2)

        d1 = self.Conv_1x1(d2)

        return d1


class R2AttentionUNet(BaseModel):
    def __init__(self, image_channels, num_classes, t=2, normalization='bn', **kwargs):
        super(R2AttentionUNet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.Maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.Upsample = nn.Upsample(scale_factor=2)

        self.RRCNN1 = RCNNBlock(ch_in=image_channels, ch_out=64, t=t, norm_layer=norm_layer)

        self.RRCNN2 = RCNNBlock(ch_in=64, ch_out=128, t=t, norm_layer=norm_layer)

        self.RRCNN3 = RCNNBlock(ch_in=128, ch_out=256, t=t, norm_layer=norm_layer)

        self.RRCNN4 = RCNNBlock(ch_in=256, ch_out=512, t=t, norm_layer=norm_layer)

        self.RRCNN5 = RCNNBlock(ch_in=512, ch_out=1024, t=t, norm_layer=norm_layer)

        self.Up5 = UpConv(ch_in=1024, ch_out=512, norm_layer=norm_layer)
        self.Att5 = AttentionBlock(F_g=512, F_l=512, F_int=256, norm_layer=norm_layer)
        self.Up_RRCNN5 = RCNNBlock(ch_in=1024, ch_out=512, t=t, norm_layer=norm_layer)

        self.Up4 = UpConv(ch_in=512, ch_out=256, norm_layer=norm_layer)
        self.Att4 = AttentionBlock(F_g=256, F_l=256, F_int=128, norm_layer=norm_layer)
        self.Up_RRCNN4 = RCNNBlock(ch_in=512, ch_out=256, t=t, norm_layer=norm_layer)

        self.Up3 = UpConv(ch_in=256, ch_out=128, norm_layer=norm_layer)
        self.Att3 = AttentionBlock(F_g=128, F_l=128, F_int=64, norm_layer=norm_layer)
        self.Up_RRCNN3 = RCNNBlock(ch_in=256, ch_out=128, t=t, norm_layer=norm_layer)

        self.Up2 = UpConv(ch_in=128, ch_out=64, norm_layer=norm_layer)
        self.Att2 = AttentionBlock(F_g=64, F_l=64, F_int=32, norm_layer=norm_layer)
        self.Up_RRCNN2 = RCNNBlock(ch_in=128, ch_out=64, t=t, norm_layer=norm_layer)

        self.Conv_1x1 = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, padding=0)

    def forward(self, x):
        # encoding path
        x1 = self.RRCNN1(x)

        x2 = self.Maxpool(x1)
        x2 = self.RRCNN2(x2)

        x3 = self.Maxpool(x2)
        x3 = self.RRCNN3(x3)

        x4 = self.Maxpool(x3)
        x4 = self.RRCNN4(x4)

        x5 = self.Maxpool(x4)
        x5 = self.RRCNN5(x5)

        # decoding + concat path
        d5 = self.Up5(x5)
        x4 = self.Att5(g=d5, x=x4)
        d5 = torch.cat((x4, d5), dim=1)
        d5 = self.Up_RRCNN5(d5)

        d4 = self.Up4(d5)
        x3 = self.Att4(g=d4, x=x3)
        d4 = torch.cat((x3, d4), dim=1)
        d4 = self.Up_RRCNN4(d4)

        d3 = self.Up3(d4)
        x2 = self.Att3(g=d3, x=x2)
        d3 = torch.cat((x2, d3), dim=1)
        d3 = self.Up_RRCNN3(d3)

        d2 = self.Up2(d3)
        x1 = self.Att2(g=d2, x=x1)
        d2 = torch.cat((x1, d2), dim=1)
        d2 = self.Up_RRCNN2(d2)

        d1 = self.Conv_1x1(d2)

        return d1
