"""
Changed on 18.01.2023 -> enabling multi GPU training without find_unused_parameters=True but keeping backward
compatibility of older networks:

The DDP takes the input and passes it to the local model, and then analyzes the output from the local model if
find_unused_parameters is set to True. This mode allows running backward on a subgraph of the model, and DDP finds out
which parameters are involved in the backward pass by traversing the autograd graph from the model output and marking
all unused parameters as ready for reduction. During the backward pass, the Reducer would only wait for unready
parameters, but it would still reduce all buckets. Marking a parameter gradient as ready does not help DDP skip buckets
as for now, but it will prevent DDP from waiting for absent gradients forever during the backward pass. Note that
traversing the autograd graph introduces extra overheads, so applications should only set find_unused_parameters to True
when necessary.

"""
from .image.danet.danet import DANet
from .image.danet.dran import Dran
from .image.dfanet.dfanet import DFANet
from .image.dfn.dfn import DFN
from .image.erfnet.erfnet import ERFNet
from .image.fastscnn.fastscnn import FastSCNN
from .image.gcn.gcn import GCN
from .image.gscnn.gscnn import GSCNN
from .image.icnet.icnet import ICNet
from .image.ocnet.ocnet import OCNet
from .image.psanet.psanet import PSANet
from .image.pspnet.pspnet import PSPNet
from .image.pspnet.pspnet import PSPDenseNet
from .image.unet.stateful_unet import SFUNet
from .image.unet.unet import UNet
from .image.unet.unet_resnet import UNetResNet
from .image.unet3plus.unet3plus import UNet3Plus
from .image.unet3plus.unet_3_plus_backbone import UNet3PlusBackboned

from ...constants import SUPPORTED_ARCHITECTURES

# placed here since that is no real constant but must be checked for consistency
# normally i would place that into model_constants.py but this would require a file manipulation
# if one only want to copy a model out of this project.
BACKWARD_COMPATIBLE_NETWORKS = [
    # 2D
    DANet,
    Dran,
    DFANet,
    DFN,
    ERFNet,
    FastSCNN,
    GCN,
    GSCNN,
    ICNet,
    OCNet,
    PSANet,
    PSPNet,
    PSPDenseNet,
    UNet,
    UNetResNet,
    UNet3Plus,
    UNet3PlusBackboned,
]

for model_class in BACKWARD_COMPATIBLE_NETWORKS:
    try:
        assert model_class.__name__ in SUPPORTED_ARCHITECTURES
    except AssertionError:
        raise ValueError(
            f'The class {model_class.__name__} is not in /aiseg/constants.SUPPORTED_ARCHITECTURES!\n'
            f'Please also check /aiseg/models/__init__.py -> SUPPORTED_MODEL_CLASSES')
