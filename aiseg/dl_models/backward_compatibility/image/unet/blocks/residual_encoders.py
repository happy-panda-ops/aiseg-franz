from .encoders import BaseEncoder
from .conv_relu_blocks import ResidualDoubleConvBlock


class ResidualEncoder(BaseEncoder):
    """
    out = Conv ReLU (x)
    residual = out
    residual_block = Conv ReLU Conv ReLU (out)
    out = residual + residual_block
    out = ReLU(out)
    out = MaxPool(2x2)
    """
    def __init__(self, input_channels, output_channels, is_3d=False):
        super(ResidualEncoder, self).__init__(input_channels, output_channels, is_3d, ResidualDoubleConvBlock)
