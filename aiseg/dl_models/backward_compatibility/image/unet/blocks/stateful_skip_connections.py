"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from .stateful_units import StatefulConvGRU
from .stateful_units import StatefulConvLSTM
from aiseg.dl_models.utils import get_group_norm_groups


class StatefulSkipConBase(torch.nn.Module):
    def __init__(self, in_channels, hidden_channels, c_rnn_unit, normalization='gn', device='cuda'):
        super(StatefulSkipConBase, self).__init__()

        self.rnn_layer = c_rnn_unit(in_channels, hidden_channels=hidden_channels, kernel_size=(3, 3), device=device)
        if normalization == 'bn':
            self.norm = torch.nn.BatchNorm2d(hidden_channels[-1])
        else:
            self.norm = torch.nn.GroupNorm(get_group_norm_groups(hidden_channels[-1]), hidden_channels[-1])

    def forward(self, x, reset_state):
        """
        Args:
            x: input of shape [B, C, H, W]
            reset_state: whether or not to reset state

        Returns: down sampled tensor, skip connection

        """
        skip_connection = self.rnn_layer(x, reset_state)
        skip_connection = self.norm(skip_connection)
        return skip_connection


class StatefulLSTMSkipCon(StatefulSkipConBase):
    """
    Out channels of skip connection: hidden_channels[-1]
    Hidden channels: [in_channels of first cell, out channels of second cell out channels of third cell, ....]
    """
    def __init__(self, in_channels, hidden_channels, normalization='gn', device='cuda'):
        super(StatefulLSTMSkipCon, self).__init__(in_channels, hidden_channels, StatefulConvLSTM, normalization, device)


class StatefulGRUSkipCon(StatefulSkipConBase):
    """
    Out channels of skip connection: hidden_channels[-1]
    Hidden channels: [in_channels of first cell, out channels of second cell out channels of third cell, ....]
    """
    def __init__(self, in_channels, hidden_channels, normalization='gn', device='cuda'):
        super(StatefulGRUSkipCon, self).__init__(in_channels, hidden_channels, StatefulConvGRU, normalization, device)
