"""
UNet 3+ paper: https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from aiseg.dl_models.base_model import BaseModel
from .nn_blocks import ConvReluBlock
from .nn_blocks import DoubleConvReluBlock


class ThreePlusEncoder(torch.nn.Module):
    """
    Encoder for standard UNet 3+
    """
    def __init__(self, input_channels, output_channels, is_3d, normalization='gn'):
        super().__init__()

        self.conv_block = DoubleConvReluBlock(input_channels, output_channels, is_3d=is_3d, normalization=normalization)
        pool = torch.nn.MaxPool3d if is_3d else torch.nn.MaxPool2d
        self.pool = pool(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.conv_block(x)
        pooled = self.pool(x)
        return x, pooled


class ThreePlusDecoder(torch.nn.Module):
    def __init__(self, center_out_channels, level, is_3d=False, normalization='gn', unet_depth=5):
        """
        ONLY for UNet3+ !WITHOUT! a backbone
        Suppose you have one center and 4 encoders.
        Suppose you want to decode at level 3
        Idea here is:
            Init by giving the current decoder level (3) then the operation list will be:
            [
                upscaling_center by 4
                upscaling level_4 by 2
                ConvNormReLu ConvNormReLu on level_3
                downsampling level_2 by 2
                downsampling level_1 by 4
            ]
        At forward, the encoder iterates through the layers:
            *args = [center, enc_4, enc_3, enc_2, enc_1]

        Args:
            center_out_channels: based on the depth. 5 -> 1024
            unet_depth: Depth incl. the center (center, enc4, enc3, enc2, enc1 -> 5)
        """
        super(ThreePlusDecoder, self).__init__()

        if is_3d:
            self.max_pool = torch.nn.MaxPool3d
        else:
            self.max_pool = torch.nn.MaxPool2d

        self.unet_depth = unet_depth  # number of encoder operations + center

        self.operation_list = torch.nn.ModuleList()

        up_sampling_mode = 'trilinear' if is_3d else 'bilinear'

        center_up_factor = int(2 ** (self.unet_depth - level))
        self.operation_list.append(
            torch.nn.Sequential(
                torch.nn.Upsample(scale_factor=center_up_factor, mode=up_sampling_mode, align_corners=True),
                DoubleConvReluBlock(center_out_channels, 64, normalization=normalization, is_3d=is_3d),
            )
        )

        # iterate from down level (level=4) to top level (enc 1, level = 1), regard the decoded in channels: 320
        # if level is 1, we need to upsample every decoded part (in_channel=320) --> scale will be: 16, 8, 4, 2
        for i in range(1, self.unet_depth - level):
            up_scaling_factor = int(2 ** (self.unet_depth - level - i))
            in_channels = 320
            self.operation_list.append(
                torch.nn.Sequential(
                    torch.nn.Upsample(scale_factor=up_scaling_factor, mode=up_sampling_mode, align_corners=True),
                    DoubleConvReluBlock(in_channels, 64, normalization=normalization, is_3d=is_3d),
                )
            )

        current_scale = int(2 ** (self.unet_depth - level))
        current_in_channels = center_out_channels // current_scale
        self.current_level_operation = torch.nn.Sequential(
            DoubleConvReluBlock(current_in_channels, 64, normalization=normalization, is_3d=is_3d)
        )
        self.operation_list.append(self.current_level_operation)

        # iterate from down level (center, level=5) to top level (enc 1, level = 1)
        # if level is 1, we need to upsample every decoded part --> scale will be: 16, 8, 4, 2
        for i in range(1, level):
            kernel_size = int(2 ** i)
            in_channels = current_in_channels // int(2 ** i)
            self.operation_list.append(
                torch.nn.Sequential(
                    self.max_pool(kernel_size),
                    DoubleConvReluBlock(in_channels, 64, normalization=normalization, is_3d=is_3d),
                )
            )

        self.final = ConvReluBlock(64 * self.unet_depth, 64 * self.unet_depth, is_3d=is_3d, normalization=normalization)

    def forward(self, *args):
        """
        Suppose you have 4 encoders and one center. Then:
        *args must be: center, enc_4, enc_3, enc_2, enc_1
        """
        if len(args) != self.unet_depth:
            raise ValueError('Only for UNet3+\n'
                             'Pass all encoded elements as follows: center, level_4, level_3, level_2, level_1\n'
                             'Assuming you have 4 encoders and one center.')
        out_list = []
        for idx, element in enumerate(args):
            out_list.append(self.operation_list[idx](element))

        x = torch.cat(out_list, dim=1)

        return self.final(x)


class UNet3Plus(BaseModel):
    """
    PyTorch U-Net+++
    UNet3+: https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 is_3d=False,
                 normalization='gn',
                 **kwargs):
        super().__init__(image_channels, num_classes)

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1 and image_channels != 3:
            raise ValueError('image channels must be 1 or 3 for gray or RGB')

        if num_classes == 2:
            num_classes = 1

        self.num_classes = num_classes
        self.dropout_2d = 0.2

        self.center_channels = 1024

        self.enc_1 = ThreePlusEncoder(
            image_channels, self.center_channels // 16, normalization=normalization, is_3d=is_3d
        )
        self.enc_2 = ThreePlusEncoder(
            self.center_channels // 16, self.center_channels // 8, normalization=normalization, is_3d=is_3d
        )
        self.enc_3 = ThreePlusEncoder(
            self.center_channels // 8, self.center_channels // 4, normalization=normalization, is_3d=is_3d
        )
        self.enc_4 = ThreePlusEncoder(
            self.center_channels // 4, self.center_channels // 2, normalization=normalization, is_3d=is_3d
        )

        self.center = DoubleConvReluBlock(
            self.center_channels // 2, self.center_channels, normalization=normalization, is_3d=is_3d
        )

        self.dec_4 = ThreePlusDecoder(self.center_channels,  level=4, is_3d=is_3d, normalization=normalization)
        self.dec_3 = ThreePlusDecoder(self.center_channels, level=3, is_3d=is_3d, normalization=normalization)
        self.dec_2 = ThreePlusDecoder(self.center_channels, level=2, is_3d=is_3d, normalization=normalization)
        self.dec_1 = ThreePlusDecoder(self.center_channels, level=1, is_3d=is_3d, normalization=normalization)

        if is_3d:
            self.final = torch.nn.Conv3d(320, num_classes, kernel_size=(3, 3, 3), padding=(1, 1, 1))
        else:
            self.final = torch.nn.Conv2d(320, num_classes, kernel_size=(3, 3), padding=(1, 1))

    def forward(self, x):
        enc_1, pool_1 = self.enc_1(x)
        enc_2, pool_2 = self.enc_2(pool_1)
        enc_3, pool_3 = self.enc_3(pool_2)
        enc_4, pool_4 = self.enc_4(pool_3)

        center = self.center(pool_4)

        dec_4 = self.dec_4(center, enc_4, enc_3, enc_2, enc_1)
        dec_3 = self.dec_3(center, dec_4, enc_3, enc_2, enc_1)
        dec_2 = self.dec_2(center, dec_4, dec_3, enc_2, enc_1)
        dec_1 = self.dec_1(center, dec_4, dec_3, dec_2, enc_1)

        x = self.final(dec_1)
        return x
