import torch
import torchvision
import warnings

from torchvision.models import ResNet18_Weights, ResNet34_Weights, ResNet50_Weights, ResNet101_Weights, \
    ResNet152_Weights, ResNeXt50_32X4D_Weights, ResNeXt101_32X8D_Weights
from torchvision.models import DenseNet201_Weights, DenseNet169_Weights, DenseNet161_Weights, DenseNet121_Weights

from aiseg.dl_models import model_constants


def load_backbone(backbone, pretrained, remove_unused_parameters=True):
    if backbone.startswith('resn'):
        if backbone == 'resnet18':
            weights = ResNet18_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnet18(weights=weights)
            backbone_channels = 512
        elif backbone == 'resnet34':
            weights = ResNet34_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnet34(weights=weights)
            backbone_channels = 512
        elif backbone == 'resnet50':
            weights = ResNet50_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnet50(weights=weights)
            backbone_channels = 2048
        elif backbone == 'resnet101':
            weights = ResNet101_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnet101(weights=weights)
            backbone_channels = 2048
        elif backbone == 'resnet152':
            weights = ResNet152_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnet152(weights=weights)
            backbone_channels = 2048
        elif backbone == 'resnext50':
            weights = ResNeXt50_32X4D_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnext50_32x4d(weights=weights)
            backbone_channels = 2048
        elif backbone == 'resnext101':
            weights = ResNeXt101_32X8D_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.resnext101_32x8d(weights=weights)
            backbone_channels = 2048
        else:
            raise NotImplementedError('only 18, 34, 101, 152 version of ResNet and 50, 101 of ResNext are implemented')

        if remove_unused_parameters:
            del encoder.fc

    elif backbone.startswith('densenet'):
        backbone_channels = None
        if backbone == 'densenet121':
            weights = DenseNet121_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.densenet121(weights=weights)
        elif backbone == 'densenet161':
            weights = DenseNet161_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.densenet161(weights=weights)
        elif backbone == 'densenet169':
            weights = DenseNet169_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.densenet169(weights=weights)
        elif backbone == 'densenet201':
            weights = DenseNet201_Weights.DEFAULT if pretrained else None
            encoder = torchvision.models.densenet201(weights=weights)
        else:
            raise NotImplementedError('only 121, 161, 169, 201 version of DenseNet are implemented')

    else:
        raise NotImplementedError('only 18, 34, 101, 152 version of ResNet and 50, 101 of ResNext are implemented')
    return encoder, backbone_channels


def batch_norm_to_group_norm(layer):
    """Iterates over a whole model (or layer of a model) and replaces every batch norm 2D with a group norm

    Args:
        layer: model or one layer of a model like resnet34.layer1 or Sequential(), ...
    """
    for name, module in layer.named_modules():
        if name:
            try:
                # name might be something like: model.layer1.sequential.0.conv1 --> this wont work. Except this case
                sub_layer = getattr(layer, name)
                if isinstance(sub_layer, torch.nn.BatchNorm2d):
                    num_channels = sub_layer.num_features
                    # first level of current layer or model contains a batch norm --> replacing.
                    layer._modules[name] = torch.nn.GroupNorm(get_group_norm_groups(num_channels), num_channels)
            except AttributeError:
                name = name.split('.')[0]
                sub_layer = getattr(layer, name)
                batch_norm_to_group_norm(sub_layer)
    return layer


def get_group_norm_groups(channels):
    """
    GroupNorm expects:
    num_groups: int
    num_channels: int

    The initial parameters can be extracted out of the original paper:
    https://arxiv.org/pdf/1803.08494.pdf
    The extraction can be found at the model_constants.py

    But in_channels of Conv2D is no constant so this function interpolates the "best fitting" values.

    The Table 3 in paper shows that the num groups has a higher impact on the results as long as channels per group are
    >= 8. This function adapts this behaviour and expects at least 16 channels to split into 2 groups! Otherwise you get
    a single group which equals a layer normalization.

    Args:
        channels: channels of the previous conv layer of the Group norm

    Returns: num_groups


    Examples:
        Output (Groups) for different channels:
        GROUP_NORM_LOOKUP = {
            16: 2,  # -> channels per group: 8
            32: 4,  # -> channels per group: 8
            64: 8,  # -> channels per group: 8
            128: 8,  # -> channels per group: 16
            256: 16,  # -> channels per group: 16
            320: 16,  # -> channels per group: 20
            512: 32,  # -> channels per group: 16
            1024: 32,  # -> channels per group: 32
            2048: 32,  # -> channels per group: 64
            4096: 64,   # -> channels per group: 64
            8192: 128,   # -> channels per group: 64
        }


    """
    if not isinstance(channels, int):
        raise ValueError('channels must be an integer!')

    if channels in model_constants.GROUP_NORM_LOOKUP.keys():
        return model_constants.GROUP_NORM_LOOKUP[channels]

    if channels == 1:
        warnings.warn(
            'Group norm with channels == 1 results to Instance Norm (num_groups=1, num_channels=1). '
            'Please read the paper:\n'
            'https://arxiv.org/pdf/1803.08494.pdf\n'
        )
        return 1

    if channels < 16:
        warnings.warn(
            'Group norm with 2 < channels < 16 results to LayerNorm (num_groups=1). Please read the paper:\n'
            'https://arxiv.org/pdf/1803.08494.pdf\n'
        )
        return 1
    if channels % 2 != 0:
        warnings.warn(
            f'GroupNorm Warning: Can not divide {channels} by at least 2 to split into groups.\n'
            f'Using LayerNorm!'
        )
        return 1

    in_channel_keys = sorted(model_constants.GROUP_NORM_LOOKUP.keys())

    groups = None

    for group_norm_channel in reversed(in_channel_keys):
        if channels >= group_norm_channel:
            groups = model_constants.GROUP_NORM_LOOKUP[group_norm_channel]
            break

    if groups is None:
        raise RuntimeError(f'Error at GroupNorm group calculation: got None as groups.')

    channels_per_group = channels / groups
    while not channels_per_group.is_integer() and not groups <= 1:
        groups = groups // 2
        channels_per_group = channels / groups

    return groups


def group_norm(channels, eps=None, **kwargs):
    if eps is not None:
        return torch.nn.GroupNorm(get_group_norm_groups(channels), channels, eps=eps)
    else:
        return torch.nn.GroupNorm(get_group_norm_groups(channels), channels)
