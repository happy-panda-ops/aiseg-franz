import torch
import torch.nn as nn

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import group_norm

"""
Implementation based on the paper:
https://arxiv.org/pdf/1707.01992.pdf
"""


class ConvInit(nn.Module):
    def __init__(self, in_channels, norm_layer=nn.BatchNorm3d):
        super(ConvInit, self).__init__()
        self.conv1 = nn.Conv3d(in_channels, 16, kernel_size=3, padding=1)
        self.norm = norm_layer(16)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        y1 = self.conv1(x)
        y2 = self.norm(y1)
        y2 = self.relu(y2)

        return y1, y2


class ConvRed(nn.Module):
    def __init__(self, in_channels, norm_layer=nn.BatchNorm3d):
        super(ConvRed, self).__init__()
        self.norm = norm_layer(in_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv = nn.Conv3d(in_channels, 16, kernel_size=3, padding=1)

    def forward(self, x):
        x = self.norm(x)
        x = self.relu(x)
        x = self.conv(x)
        return x


class DilatedConv2(nn.Module):
    def __init__(self, in_channels, norm_layer=nn.BatchNorm3d):
        super(DilatedConv2, self).__init__()
        self.norm = norm_layer(in_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv = nn.Conv3d(in_channels, 32, kernel_size=3, padding=2, dilation=2)

    def forward(self, x):
        x = self.norm(x)
        x = self.relu(x)
        x = self.conv(x)
        return x


class DilatedConv4(nn.Module):
    def __init__(self, in_channels, norm_layer=nn.BatchNorm3d):
        super(DilatedConv4, self).__init__()
        self.norm = norm_layer(in_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv = nn.Conv3d(in_channels, 64, kernel_size=3, padding=4, dilation=4)

    def forward(self, x):
        x = self.norm(x)
        x = self.relu(x)
        x = self.conv(x)
        return x


class Conv1x1x1(nn.Module):
    def __init__(self, in_channels, out_channels, norm_layer=nn.BatchNorm3d):
        super(Conv1x1x1, self).__init__()
        self.norm = norm_layer(in_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv = nn.Conv3d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        x = self.norm(x)
        x = self.relu(x)
        x = self.conv(x)
        return x


class HighResNet3D(BaseModel):
    def __init__(self,
                 image_channels,
                 num_classes,
                 normalization='gn',
                 shortcut_type="A",
                 dropout_layer=True,
                 **kwargs):
        super(HighResNet3D, self).__init__(image_channels, num_classes)

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1:
            raise ValueError('image channels must be 1. RGB for 3D is not supported')

        if num_classes == 2:
            num_classes = 1  # binary

        self.norm_layer = torch.nn.BatchNorm3d if normalization == 'bn' else group_norm

        self.shortcut_type = shortcut_type
        self.init_channels = 16
        self.reduced_channels = 16
        self.dil2_channels = 32
        self.dil4_channels = 64
        self.conv_out_channels = 80

        if self.shortcut_type == "B":
            self.res_pad_1 = Conv1x1x1(self.reduced_channels, self.dil2_channels, norm_layer=self.norm_layer)
            self.res_pad_2 = Conv1x1x1(self.dil2_channels, self.dil4_channels, norm_layer=self.norm_layer)

        self.conv_init = ConvInit(image_channels, norm_layer=self.norm_layer)

        self.red_blocks1 = self.create_red(self.init_channels)
        self.red_blocks2 = self.create_red(self.reduced_channels)
        self.red_blocks3 = self.create_red(self.reduced_channels)

        self.dil2block1 = self.create_dil2(self.reduced_channels)
        self.dil2block2 = self.create_dil2(self.dil2_channels)
        self.dil2block3 = self.create_dil2(self.dil2_channels)

        self.dil4block1 = self.create_dil4(self.dil2_channels)
        self.dil4block2 = self.create_dil4(self.dil4_channels)
        self.dil4block3 = self.create_dil4(self.dil4_channels)

        if dropout_layer:
            conv_out = nn.Conv3d(self.dil4_channels, self.conv_out_channels, kernel_size=1)
            drop3d = nn.Dropout3d()
            conv1x1x1 = Conv1x1x1(self.conv_out_channels, num_classes, norm_layer=self.norm_layer)
            self.conv_out = nn.Sequential(conv_out, drop3d, conv1x1x1)
        else:
            self.conv_out = Conv1x1x1(self.dil4_channels, num_classes, norm_layer=self.norm_layer)

    def shortcut_pad(self, x, desired_channels):
        if self.shortcut_type == 'A':
            batch_size, channels, dim0, dim1, dim2 = x.shape
            extra_channels = desired_channels - channels
            zero_channels = int(extra_channels / 2)
            zeros_half = x.new_zeros(batch_size, zero_channels, dim0, dim1, dim2)
            y = torch.cat((zeros_half, x, zeros_half), dim=1)
        elif self.shortcut_type == 'B':
            if desired_channels == self.dil2_channels:
                y = self.res_pad_1(x)
            elif desired_channels == self.dil4_channels:
                y = self.res_pad_2(x)
            else:
                raise NotImplementedError('HighResNet3D, shortcut type B got an error.')
        else:
            raise NotImplementedError(f'Shortcut type: {self.shortcut_type} not supported')
        return y

    def create_red(self, in_channels):
        conv_red_1 = ConvRed(in_channels, norm_layer=self.norm_layer)
        conv_red_2 = ConvRed(self.reduced_channels, norm_layer=self.norm_layer)
        return nn.Sequential(conv_red_1, conv_red_2)

    def create_dil2(self, in_channels):
        conv_dil2_1 = DilatedConv2(in_channels, norm_layer=self.norm_layer)
        conv_dil2_2 = DilatedConv2(self.dil2_channels, norm_layer=self.norm_layer)
        return nn.Sequential(conv_dil2_1, conv_dil2_2)

    def create_dil4(self, in_channels):
        conv_dil4_1 = DilatedConv4(in_channels, norm_layer=self.norm_layer)
        conv_dil4_2 = DilatedConv4(self.dil4_channels, norm_layer=self.norm_layer)
        return nn.Sequential(conv_dil4_1, conv_dil4_2)

    def red_forward(self, x):
        x, x_res = self.conv_init(x)
        x_red_1 = self.red_blocks1(x)
        x_red_2 = self.red_blocks2(x_red_1 + x_res)
        x_red_3 = self.red_blocks3(x_red_2 + x_red_1)
        return x_red_3, x_red_2

    def dilation2(self, x_red_3, x_red_2):
        x_dil2_1 = self.dil2block1(x_red_3 + x_red_2)
        # logging.info(x_dil2_1.shape ,x_red_3.shape )

        x_red_padded = self.shortcut_pad(x_red_3, self.dil2_channels)

        x_dil2_2 = self.dil2block2(x_dil2_1 + x_red_padded)
        x_dil2_3 = self.dil2block3(x_dil2_2 + x_dil2_1)
        return x_dil2_3, x_dil2_2

    def dilation4(self, x_dil2_3, x_dil2_2):
        x_dil4_1 = self.dil4block1(x_dil2_3 + x_dil2_2)
        x_dil2_padded = self.shortcut_pad(x_dil2_3, self.dil4_channels)
        x_dil4_2 = self.dil4block2(x_dil4_1 + x_dil2_padded)
        x_dil4_3 = self.dil4block3(x_dil4_2 + x_dil4_1)
        return x_dil4_3 + x_dil4_2

    def forward(self, x):
        x_red_3, x_red_2 = self.red_forward(x)
        x_dil2_3, x_dil2_2 = self.dilation2(x_red_3, x_red_2)
        x_dil4 = self.dilation4(x_dil2_3, x_dil2_2)
        y = self.conv_out(x_dil4)
        return y
