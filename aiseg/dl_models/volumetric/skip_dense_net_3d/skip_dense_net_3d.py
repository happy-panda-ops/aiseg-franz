from collections import OrderedDict

import torch
import torch.nn as nn

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.utils import group_norm

"""
Based on the implementation of https://github.com/tbuikr/3D-SkipDenseSeg
Paper here : https://arxiv.org/pdf/1709.03199.pdf
"""


class _DenseLayer(nn.Sequential):
    def __init__(self, num_input_features, growth_rate, bn_size, drop_rate, norm_layer=nn.BatchNorm3d):
        super(_DenseLayer, self).__init__()
        self.add_module('norm1', norm_layer(num_input_features)),
        self.add_module('relu1', nn.ReLU(inplace=True)),
        self.add_module('conv1', nn.Conv3d(num_input_features, bn_size *
                                           growth_rate, kernel_size=1, stride=1, bias=False)),
        self.add_module('norm2', norm_layer(bn_size * growth_rate)),
        self.add_module('relu2', nn.ReLU(inplace=True)),
        self.add_module('conv2', nn.Conv3d(bn_size * growth_rate, growth_rate,
                                           kernel_size=3, stride=1, padding=1, bias=False)),
        self.drop_layer = nn.Dropout3d(p=drop_rate)

        self.out_channels = num_input_features + growth_rate

    def forward(self, x):
        new_features = super(_DenseLayer, self).forward(x)
        new_features = self.drop_layer(new_features)
        return torch.cat([x, new_features], 1)


class _DenseBlock(nn.Sequential):
    def __init__(self, num_layers, num_input_features, bn_size, growth_rate, drop_rate, norm_layer=nn.BatchNorm3d):
        super(_DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(num_input_features + i * growth_rate, growth_rate, bn_size, drop_rate, norm_layer)
            self.add_module('denselayer%d' % (i + 1), layer)


class _Transition(nn.Sequential):
    def __init__(self, num_input_features, num_output_features, norm_layer=nn.BatchNorm3d):
        super(_Transition, self).__init__()
        self.add_module('norm', norm_layer(num_input_features))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv3d(num_input_features, num_output_features,
                                          kernel_size=1, stride=1, bias=False))

        self.add_module('pool_norm', norm_layer(num_output_features))
        self.add_module('pool_relu', nn.ReLU(inplace=True))
        self.add_module('pool', nn.Conv3d(num_output_features, num_output_features, kernel_size=2, stride=2))


class SkipDenseNet3D(BaseModel):
    """Densely Connected Convolutional Networks" <https://arxiv.org/pdf/1608.06993.pdf>`
    Paper here : https://arxiv.org/pdf/1709.03199.pdf

    Args:
        growth_rate (int) - how many filters to add each layer (`k` in paper)
        block_config (list of 4 ints) - how many layers in each pooling block
        num_init_features (int) - the number of filters to learn in the first convolution layer
        bn_size (int) - multiplicative factor for number of bottle neck layers
          (i.e. bn_size * k features in the bottleneck layer)
        drop_rate (float) - dropout rate after each dense layer
        num_classes (int) - number of classification classes
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 normalization='gn',
                 growth_rate=16,
                 block_config=(6, 12, 24, 16),
                 num_init_features=32,
                 drop_rate=0.0,
                 bn_size=4,
                 **kwargs):
        super(SkipDenseNet3D, self).__init__(image_channels, num_classes)
        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1:
            raise ValueError('image channels must be 1. RGB for 3D is not supported')

        if num_classes == 2:
            num_classes = 1  # binary

        norm_layer = torch.nn.BatchNorm3d if normalization == 'bn' else group_norm

        # First three convolutions
        self.features = nn.Sequential(OrderedDict([
            ('conv0', nn.Conv3d(image_channels, num_init_features, kernel_size=3, stride=1, padding=1, bias=False)),
            ('norm0', norm_layer(num_init_features)),
            ('relu0', nn.ReLU(inplace=True)),
            ('conv1', nn.Conv3d(num_init_features, num_init_features, kernel_size=3, stride=1, padding=1, bias=False)),
            ('norm1', norm_layer(num_init_features)),
            ('relu1', nn.ReLU(inplace=True)),
            ('conv2', nn.Conv3d(num_init_features, num_init_features, kernel_size=3, stride=1, padding=1, bias=False)),
        ]))
        self.features_bn = nn.Sequential(OrderedDict([
            ('norm2', norm_layer(num_init_features)),
            ('relu2', nn.ReLU(inplace=True)),
        ]))
        self.conv_pool_first = nn.Conv3d(
            num_init_features, num_init_features, kernel_size=2, stride=2, padding=0, bias=False
        )

        # Each dense block
        num_features = num_init_features
        self.dense_blocks = nn.ModuleList([])
        self.transit_blocks = nn.ModuleList([])
        self.upsampling_blocks = nn.ModuleList([])

        for i, num_layers in enumerate(block_config):
            block = _DenseBlock(
                num_layers=num_layers,
                num_input_features=num_features,
                bn_size=bn_size,
                growth_rate=growth_rate,
                drop_rate=drop_rate,
                norm_layer=norm_layer
            )

            self.dense_blocks.append(block)

            num_features = num_features + num_layers * growth_rate

            up_block = nn.ConvTranspose3d(
                num_features,
                num_classes,
                kernel_size=2 ** (i + 1) + 2,
                stride=2 ** (i + 1),
                padding=1,
                bias=False
            )

            self.upsampling_blocks.append(up_block)

            if i != len(block_config) - 1:
                trans = _Transition(
                    num_input_features=num_features, num_output_features=num_features // 2, norm_layer=norm_layer
                )
                self.transit_blocks.append(trans)
                # self.features.add_module('transition%d' % (i + 1), trans)
                num_features = num_features // 2

        # Final batch norm
        # self.features.add_module('norm5', nn.BatchNorm3d(num_features))

        # Linear layer
        # self.classifier = nn.Linear(num_features, num_classes)
        # self.bn4 = nn.BatchNorm3d(num_features)

        # ----------------------- classifier -----------------------
        self.bn_class = norm_layer(num_classes * 4 + num_init_features)
        self.relu_last = nn.ReLU(inplace=True)
        self.conv_class = nn.Conv3d(num_classes * 4 + num_init_features, num_classes, kernel_size=1, padding=0)
        # ----------------------------------------------------------

    def forward(self, x):
        first_three_features = self.features(x)
        first_three_features_bn = self.features_bn(first_three_features)
        out = self.conv_pool_first(first_three_features_bn)

        out = self.dense_blocks[0](out)
        up_block1 = self.upsampling_blocks[0](out)
        out = self.transit_blocks[0](out)

        out = self.dense_blocks[1](out)
        up_block2 = self.upsampling_blocks[1](out)
        out = self.transit_blocks[1](out)

        out = self.dense_blocks[2](out)
        up_block3 = self.upsampling_blocks[2](out)
        out = self.transit_blocks[2](out)

        out = self.dense_blocks[3](out)
        up_block4 = self.upsampling_blocks[3](out)

        out = torch.cat([up_block1, up_block2, up_block3, up_block4, first_three_features], 1)

        # ----------------------- classifier -----------------------
        out = self.conv_class(self.relu_last(self.bn_class(out)))
        # ----------------------------------------------------------
        return out
