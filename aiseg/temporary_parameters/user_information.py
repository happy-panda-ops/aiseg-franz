import logging

from aiseg.utils import get_time_remaining_string
from aiseg.utils import get_time_spent_string


def show_stats(history, total_epochs, start_epoch, start_time):
    epoch, train_loss, valid_loss, train_acc, valid_acc = history[-1][:5]

    human_time_spent = get_time_spent_string(start_time)
    human_time_remaining = get_time_remaining_string(
        start_time,
        total_count=total_epochs - start_epoch,
        current_idx=epoch - 1 - start_epoch,
    )
    percent_processed = int(round(epoch / total_epochs * 100))

    logging.info(
        f'Processed: {percent_processed}%, time spent: {human_time_spent}, time remaining: {human_time_remaining}'
    )

    logging.info(f'Training: Loss: {train_loss:.6f}, Accuracy: {train_acc:.6f}%')
    logging.info(f'Validation: Loss: {valid_loss:.6f}, Accuracy: {valid_acc:.6f}%')


def get_best_five(sorted_hist, amount=5):
    best_five = []
    for i in range(amount):
        best_five.append(sorted_hist[i])
    return best_five


def show_best_five_stats(history):
    """
    Shows the best 5 epochs based on the training history

    Args:
        history: list containing sub lists such as: [epoch + 1, train_loss, valid_loss, train_acc, valid_acc, test_acc]

    """
    if len(history) < 5:
        return

    best_train_loss = get_best_five(sorted(history, key=lambda x: x[1]))
    best_valid_loss = get_best_five(sorted(history, key=lambda x: x[2]))
    best_train_acc = get_best_five(sorted(history, key=lambda x: x[3], reverse=True))
    best_val_acc = get_best_five(sorted(history, key=lambda x: x[4], reverse=True))

    bests = [best_train_loss, best_valid_loss, best_train_acc, best_val_acc]

    # print will look like:
    header_string = '               | train_loss   | valid_loss   | train_acc    | valid_acc    |'
    #               '----------------------------------------------------------------------------
    #               'epoch / value: |      /       |      /       |      /       |      /       |

    separator = len(header_string) * '-'
    logging.info(
        'Information: Showing best 5 epochs regarding: training loss, '
        'validation loss, training accuracy, validation accuracy'
    )
    text = '\n##### RESULTS: #####\n'
    text += '\n' + header_string
    text += '\n' + separator

    for i in range(5):
        column_string = 'epoch / value: |'

        for idx, sorted_value_list in enumerate(bests):
            index_of_value = idx + 1
            epoch_nr = str(sorted_value_list[i][0])
            epoch_nr += (4 - len(epoch_nr)) * ' '
            value = str(sorted_value_list[i][index_of_value])[:5]

            column_string += ' ' + epoch_nr + ' / ' + value + ' |'
        text += '\n' + column_string
        text += '\n' + separator

    logging.info(text)
    return text
