from decimal import Decimal

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.safestring import mark_safe

from aiseg import constants

"""
This is created once to store the users online augmentation preferences.
"""


class OnlineImageAugParameters(models.Model):
    policy = models.CharField(
        verbose_name='Augmentation policy',
        choices=constants.AUGMENTATION_POLICIES,
        default=constants.AUGMENTATION_POLICIES[1][0],  # geo_first
        max_length=255,
        help_text=mark_safe(
            '<b>Select the augmentation policy</b>:<br>'
            '<small>'
            '<u>Random</u>: Randomly applies operations until max augmentations is reached'
            '<br>'
            '<u>Geometric first</u>: Depending on the max augmentations, first applying rotation and/or resizing, then '
            'other geometric operations and afterwards pixel manipulations.<br>'
            'Example: max aug = 3, all probabilities = 100 results to: 1. either rotating or resizing, 2. one other '
            'geometric operation, 3. one pixel manipulation.'
            '<br>'
            '<u>Pixel first</u>: Depending on max augmentations, first applying pixel manipulations, then geometric '
            'operations.'
            '<br>'
            '<u>Geometric only</u>: Only applying geometric manipulations. First resizing or/and rotating, then the '
            'rest.<br>'
            '<u>Pixel only</u>: Only applying pixel manipulating operations.'
            '</small>'
        ),
    )
    max_augmentations = models.PositiveSmallIntegerField(
        verbose_name='Max augmentations per image',
        default=5,
        help_text='How many augmentations are applied at max to a single image. 0 for all',
    )
    global_strength = models.PositiveSmallIntegerField(
        verbose_name='Global probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='A single image gets augmented if: random number between 0 and 100 <= global probability.',
    )
    online_aug_start_epoch = models.PositiveIntegerField(
        verbose_name='Online augmentation start epoch',
        default=1,
        validators=[MinValueValidator(1)],
        help_text='Epoch at which the augmentation is activated. Every previous epoch is not augmented. Set 1 to start '
                  'with first epoch.',
    )

    # further properties:
    vertical_flip_allowed = models.BooleanField(
        verbose_name='Vertical flip allowed',
        default=False,
        help_text='Whether flipping upside down is allowed.'
    )
    max_rot_angle = models.PositiveSmallIntegerField(
        verbose_name='Maximum rotation angle',
        default=30,
        help_text='Maximum angle, the image gets rotated (+- this angle).',
    )
    resizing_scale_lower = models.DecimalField(
        verbose_name='Resizing lower boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.8'),
        validators=[MinValueValidator(Decimal('0.25'))],
        help_text='<= 2.0! Explanation: See Resizing upper boundary.',
    )
    resizing_scale_upper = models.DecimalField(
        verbose_name='Resizing upper boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('1.2'),
        validators=[MaxValueValidator(Decimal('2.0'))],
        help_text='>= 0,25! During resizing, a random number between "Resizing lower boundary" and "Resizing upper'
                  ' boundary" is used as resizing multiplier (new_size = old_size x resizing_scale).',
    )
    squeeze_scale_lower = models.DecimalField(
        verbose_name='Squeezing lower boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.8'),
        validators=[MinValueValidator(Decimal('0.25'))],
        help_text='<= 2.0!  Explanation: See Squeezing upper boundary.',
    )
    squeeze_scale_upper = models.DecimalField(
        verbose_name='Squeezing upper boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('1.2'),
        validators=[MaxValueValidator(Decimal('2.0'))],
        help_text='>= 0,25! During squeezing, a random number between "Squeezing lower boundary" and "Squeezing upper'
                  ' boundary" is used as squeezing multiplier (new width and/or height = old_size x squeeze_scale).',
    )
    tilting_start_factor = models.DecimalField(
        verbose_name='Tilting start factor',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.25'),
        validators=[MinValueValidator(Decimal('0.0')), MaxValueValidator(Decimal('0.5'))],
        help_text='0 to 0.5! Random tilting selects to tilt backwards, left, right and forward. Suppose, we are tilting'
                  ' backwards (upper side of the image will be squeezed, lower side keeps the size), then this factor '
                  'relates to the x coordinate of the upper left corner coordinate of the tilted image. '
                  'x_top_left = tilting_start_factor * width_initial; x_top_right = tilting_end_factor * width_initial',
    )
    tilting_end_factor = models.DecimalField(
        verbose_name='Tilting end factor',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.75'),
        validators=[MinValueValidator(Decimal('0.5')), MaxValueValidator(Decimal('1.0'))],
        help_text='0.5 to 1.0!  Explanation: See Tilting start factor.',
    )

    # GEOMETRIC
    elastic_distortion_prob = models.PositiveSmallIntegerField(
        verbose_name='Elastic distortion probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly distorts the image.',
    )
    flip_prob = models.PositiveSmallIntegerField(
        verbose_name='Flipping probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Random flipping horizontally (and vertically if allowed).',
    )
    grid_distortion_prob = models.PositiveSmallIntegerField(
        verbose_name='Grid distorting probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Distorting the image along a grid.',
    )
    optical_distortion_prob = models.PositiveSmallIntegerField(
        verbose_name='Optical distorting probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Apply a random optical distortion.',
    )
    random_crop_prob = models.PositiveSmallIntegerField(
        verbose_name='Random cropping probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Crop somewhere into the image.',
    )
    resize_prob = models.PositiveSmallIntegerField(
        verbose_name='Resizing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Resizes the image. If the image gets too small, it will be filled by mirroring the image.',
    )
    rotate_by_angle_prob = models.PositiveSmallIntegerField(
        verbose_name='Rotating probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Rotate the image. This also crops into the image to prevent black regions.',
    )
    tilt_prob = models.PositiveSmallIntegerField(
        verbose_name='Tilting probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Tilt the image. This also crops into the image to prevent black regions.',
    )
    squeeze_prob = models.PositiveSmallIntegerField(
        verbose_name='Squeezing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Random squeeze the image in height and/or width.'
                  ' The squeezed regions are filled with mirrored input to fit the input size.',
    )
    grid_shuffle_prob = models.PositiveSmallIntegerField(
        verbose_name='Grid shuffling probability',
        default=0,
        validators=[MaxValueValidator(100)],
        help_text='Selects a n x n grid depending on the image size and realigns (shuffles) the patches of the image.',
    )

    # PIXEL
    brightness_prob = models.PositiveSmallIntegerField(
        verbose_name='Brightness changing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly changes the brightess of the image.',
    )
    contrast_prob = models.PositiveSmallIntegerField(
        verbose_name='Contrast changing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly changes the contrast of the image.',
    )
    gaussian_filter_prob = models.PositiveSmallIntegerField(
        verbose_name='Blurring probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Applies a gaussian filter (blur) to the image',
    )
    gaussian_noise_prob = models.PositiveSmallIntegerField(
        verbose_name='Gaussian noise probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds a gaussian noise to the image.',
    )
    random_erasing_prob = models.PositiveSmallIntegerField(
        verbose_name='Random erasing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly erases some regions of the image and replaces them with black, white or noisy patches.',
    )
    channel_shuffle_prob = models.PositiveSmallIntegerField(
        verbose_name='Channel shuffling probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Shuffles the r g and b channelrandomly. Only for rgb.',
    )
    color_to_hsv_prob = models.PositiveSmallIntegerField(
        verbose_name='Color to hsv probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Converts rgb to hsv. Only for rgb.',
    )
    iso_noise_prob = models.PositiveSmallIntegerField(
        verbose_name='Iso noise probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds iso noise to the image. Only for rgb.',
    )
    random_fog_prob = models.PositiveSmallIntegerField(
        verbose_name='Random fog probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Initial learning rate. Will be adapted by Adam optimizer. Only for rgb.',
    )
    random_rain_prob = models.PositiveSmallIntegerField(
        verbose_name='Random rain probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds random rain to the image. Only for rgb.',
    )
    random_shadow_prob = models.PositiveSmallIntegerField(
        verbose_name='Random shadow probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds a random shadow to the image. Only for rgb.',
    )
    random_snow_prob = models.PositiveSmallIntegerField(
        verbose_name='Random snow probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds "snow" to the image. Only for rgb.',
    )
    random_sun_flair_prob = models.PositiveSmallIntegerField(
        verbose_name='Random sun flair probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds an approximated sun flare to the image. Only for rgb.',
    )
    rgb_shift_prob = models.PositiveSmallIntegerField(
        verbose_name='RGB shifting probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Shifts all color channels. Only for rgb.',
    )
    solarize_prob = models.PositiveSmallIntegerField(
        verbose_name='solarize probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Manipulates the rgb channels. Only for rgb.',
    )
    to_gray_prob = models.PositiveSmallIntegerField(
        verbose_name='Converting to gray probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Converts the image to grayscale (channels are kept). Only for rgb.',
    )
    to_sepia_prob = models.PositiveSmallIntegerField(
        verbose_name='To sepia probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Converts the image to sepia. Only for rgb.',
    )


class OnlineVolumeAugParameters(models.Model):
    policy = models.CharField(
        verbose_name='Augmentation policy',
        choices=constants.AUGMENTATION_POLICIES,
        default=constants.AUGMENTATION_POLICIES[1][0],  # geo_first
        max_length=255,
        help_text=mark_safe(
            '<b>Select the augmentation policy:</b><br>'
            '<small>'
            '<u>Random</u>: Randomly applies operations until max augmentations is reached'
            '<br>'
            '<u>Geometric first</u>: Depending on the max augmentations, first applying rotation and/or resizing, then '
            'other geometric operations and afterwards pixel manipulations.<br>'
            'Example: max aug = 3, all probabilities = 100 results to: 1. either rotating or resizing, 2. one other '
            'geometric operation, 3. one pixel manipulation.'
            '<br>'
            '<u>Pixel first</u>: Depending on max augmentations, first applying pixel manipulations, then geometric '
            'operations.'
            '<br>'
            '<u>Geometric only</u>: Only applying geometric manipulations. First resizing or/and rotating, then the'
            ' rest.<br>'
            '<u>Pixel only</u>: Only applying pixel manipulating operations.'
            '</small>'
        ),
    )
    max_augmentations = models.PositiveSmallIntegerField(
        verbose_name='Max augmentations per volume',
        default=5,
        help_text='How many augmentations are applied at max to a single image. 0 for all',
    )
    global_strength = models.PositiveSmallIntegerField(
        verbose_name='Global probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='A single image gets augmented if: random number between 0 and 1 > than global probability.',
    )
    online_aug_start_epoch = models.PositiveIntegerField(
        verbose_name='Online augmentation start epoch',
        default=1,
        validators=[MinValueValidator(1)],
        help_text='Epoch at which the augmentation is activated. Every previous epoch is not augmented. Set 1 to start '
                  'with first epoch.',
    )

    # further properties:
    depth_flip_allowed = models.BooleanField(
        verbose_name='Z flip allowed',
        default=True,
        help_text='Whether flipping the volume in depth is allowed.'
    )
    xy_rot_allowed = models.BooleanField(
        verbose_name='Rotation around X and Y allowed',
        default=False,
        help_text='If the input depth != input height and width, this may cause loss of information. Recommendation: '
                  'Use offline augmentation (rotation on x and y only) first in that case.'
    )
    fixed_rotation_angle = models.BooleanField(
        verbose_name='Use fixed rotation angle',
        default=False,
        help_text='Converts the maximum rotation angle to a fixed angle. If "Rotation around X and Y allowed" and '
                  'this are activated, the random rotation rotates the volume randomly around all axes, not only one. '
                  'In this case: If you set the angle to 180, the rotation will be 90 or 180 for each axis to boost'
                  ' randomness.'
    )
    max_rot_angle = models.PositiveSmallIntegerField(
        verbose_name='Maximum rotation angle',
        default=180,
        help_text='Maximum angle, the volume gets rotated (+- this angle). If "Rotation around X and Y allowed" is '
                  'activated, the volume is rotated only around one of the three axes, as this can cause unwanted '
                  'changes to the information of the input and target. Use a fixed rotation angle '
                  '(such as 90, 180, ..) to allow for the rotation around multiple axes simultaneously.',
    )
    resizing_scale_lower = models.DecimalField(
        verbose_name='Resizing lower boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.8'),
        validators=[MinValueValidator(Decimal('0.25'))],
        help_text='>= 0,25! Explanation: See Resizing upper boundary.',
    )
    resizing_scale_upper = models.DecimalField(
        verbose_name='Resizing upper boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('1.2'),
        validators=[MaxValueValidator(Decimal('4.0'))],
        help_text='<= 4.0! During resizing, a random number between "Resizing lower boundary" and "Resizing upper'
                  ' boundary" is used as resizing multiplier (new_size = old_size x resizing_scale; '
                  'old_size is x, y and z).',
    )
    squeeze_scale_lower = models.DecimalField(
        verbose_name='Squeezing lower boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.8'),
        validators=[MinValueValidator(Decimal('0.25'))],
        help_text='>= 0,25! Explanation: see Squeezing upper boundary.',
    )
    squeeze_scale_upper = models.DecimalField(
        verbose_name='Squeezing upper boundary',
        decimal_places=2,
        max_digits=3,
        default=Decimal('1.2'),
        validators=[MaxValueValidator(Decimal('4.0'))],
        help_text='<= 4.0! During squeezing, a random number between "Squeezing lower boundary" and "Squeezing upper'
                  ' boundary" is used as squeezing multiplier (new width, height and/or depth = old_size x '
                  'squeeze_scale).',
    )
    tilting_start_factor = models.DecimalField(
        verbose_name='Tilting start factor',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.25'),
        validators=[MinValueValidator(Decimal('0.0')), MaxValueValidator(Decimal('0.5'))],
        help_text='0 to 0.5! Random tilting selects to tilt backwards, left, right and forward. Example: The coordinate'
                  ' system is as follows: when looking top down onto the volume, the upper left corner relates to the '
                  'origin of the volume, z pointing away, x to the right and y points down.'
                  'Suppose, we are tilting backwards (upper side of the volume will be squeezed, lower side keeps the'
                  ' size), then this factor relates to the x coordinate of the upper left corner coordinate of the '
                  'tilted volume. x_top_left = tilting_start_factor * width_initial; x_top_right = tilting_end_factor '
                  '* width_initial',
    )
    tilting_end_factor = models.DecimalField(
        verbose_name='Tilting end factor',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.75'),
        validators=[MinValueValidator(Decimal('0.5')), MaxValueValidator(Decimal('1.0'))],
        help_text='0.5 to 1.0! Explanation: Tilting start factor.',
    )

    # GEOMETRIC
    flip_prob = models.PositiveSmallIntegerField(
        verbose_name='Random flipping probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Flips the volume randomly in x y and z (if Z flip allowed). Crops and resizes the result afterwards '
                  'to fit the input size.',
    )
    random_crop_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Random cropping probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly crops inside the volume. Resizes or mirrors the result afterwards to fit the input size.',
    )
    random_resize_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Random resizing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly resizes the volume. Crops or mirrors the result afterwards to fit the input size.',
    )
    rotate_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Rotating probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text=mark_safe(
            'This function rotates the volume randomly around the axes. Coordinate system: see Tilting start factor.'
        ),
    )
    squeeze_prob = models.PositiveSmallIntegerField(
        verbose_name='Random squeezing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Squeezes the volume randomly in x, y and z. Resizes and crops afterwards to fit input size.',
    )
    tilt_prob = models.PositiveSmallIntegerField(
        verbose_name='Random tilting probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Tilts the volume to the left, right, top or bottom. Resizes and crops the result afterwards to fit '
                  'the input size.',
    )

    # VOXEL
    add_blur_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Random blurring probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Applies a gaussian filter with a random standard deviation (5 to 25) to the volume.',
    )
    add_noise_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Random noise probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds random noise per normal distribution to the model (standard deviation: 5 to 25).',
    )
    brightness_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Brightness changing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Manipulates the brightness of the volume.',
    )
    contrast_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Contrast manipulating probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly manipulates the contrast.',
    )
    random_erasing_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Random erasing probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Randomly erases some regions of the volume and fills with black white or noisy patches.',
    )
    sharpen_3d_with_blur_prob = models.PositiveSmallIntegerField(
        verbose_name='Sharpening probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Sharpens the volume.',
    )
    random_shadow_3d_prob = models.PositiveSmallIntegerField(
        verbose_name='Shadow probability',
        default=100,
        validators=[MaxValueValidator(100)],
        help_text='Adds a shadow to the volume.',
    )
