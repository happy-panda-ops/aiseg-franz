import inspect

import numpy as np
import os

from decimal import Decimal

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.safestring import mark_safe

from .datasets import TrainingDataset
from .. import constants
from ..data_augmentation.online_augmentation_pipelines import ImageAugmentationPipeline, VolumeAugmentationPipeline
from ..file_handling.utils import convert_np_array_to_binary, convert_binary_to_np_array
from ..training.metrics import MetricStorage
from ..utils import get_human_time_string_from_seconds, get_human_time_values_from_seconds


def upload_path(instance, filename):
    if not isinstance(instance, ModelAnalysis):
        raise ValueError('Instance must be a ModelAnalysis instance')
    path = f'analysis_{str(instance.pk).zfill(5)}'
    return os.path.join(path, filename)


def fill_or_delete_analysis(analysis):
    if not isinstance(analysis, ModelAnalysis):
        raise ValueError('Analysis must be a ModelAnalysis instance.')
    has_changed = False
    if not analysis.history:
        analysis.delete()
        has_changed = True
        return None, has_changed

    header, history = analysis.get_history()
    best_loss_epoch = sorted(history, key=lambda x: x[2])[0]
    if not analysis.validation_accuracy:
        analysis.training_loss = Decimal(best_loss_epoch[1])
        analysis.validation_loss = Decimal(best_loss_epoch[2])
        analysis.training_accuracy = Decimal(best_loss_epoch[3])
        analysis.validation_accuracy = Decimal(best_loss_epoch[4])
        analysis.save()
        has_changed = True
    if analysis.total_epochs != history.shape[0]:
        analysis.total_epochs = history.shape[0]
        analysis.save()
        has_changed = True

    if not analysis.best_epoch:
        analysis.best_epoch = best_loss_epoch[0]
        analysis.save()
        has_changed = True

    return analysis, has_changed


class ModelAnalysis(models.Model):
    created_datetime = models.DateTimeField(
        verbose_name='Created at',
        help_text='This is the datetime, when the model training process started.',
        editable=False,
    )
    architecture = models.CharField(
        verbose_name='Architecture',
        max_length=128,
        choices=constants.ARCHITECTURE_CHOICES,
        editable=False,
    )
    backbone = models.CharField(
        verbose_name='Backbone',
        max_length=128,
        blank=True,
        editable=False,
    )
    custom_name = models.CharField(
        verbose_name='Custom Name',
        max_length=128,
        blank=True,
    )
    normalization = models.CharField(
        # may not be used to filter - added too late
        verbose_name='Normalization',
        max_length=128,
        blank=True,
        choices=constants.NORMALIZATION_CHOICES,
    )
    optimizer_name = models.CharField(
        verbose_name='Optimizer',
        max_length=64,
        blank=False,
        choices=constants.OPTIMIZER_CHOICES,
        default='Adam',
    )
    init_learning_rate = models.DecimalField(
        verbose_name='Learning rate',
        decimal_places=6,
        max_digits=8,
        default=Decimal('0.001'),
        validators=[MaxValueValidator(Decimal('1.0')), MinValueValidator(Decimal('0.000001'))],
    )

    lr_scheduler = models.CharField(
        verbose_name='Learning rate scheduler',
        choices=constants.LR_SCHEDULER_CHOICES,
        default=constants.LR_SCHEDULER_LINEAR,
        max_length=128,
        help_text=mark_safe(
            'The scheduler adjusts the learning rate of the networks after each epoch. Basic concept: <br>'
            'Linear: 3 phases (see graph).<br>'
            '<small>Phase 1 (optional): The learning rate increases linearly during the warm up.<br>'
            'Phase 2: The learning rate will be constant until "Lr constant period" + "Lr warm up period" is '
            'reached.<br>'
            'Phase 3 (optional): The lr decays to 0 until the final epoch is reached.</small><br>'
            'Step: Step wise reduction with a step size of: "LR adjustment step size". Decays automatically.<br>'
            'Cosine and warm restart: Cyclically adjusts the learning rate using a cosine function. Decays '
            f'automatically until {constants.MINIMUM_LR}, then resets (warm restart) or increases (cosine) to lr.<br>'
            '<small>See also: <a href=https://www.kaggle.com/code/billiemage/understand-lr-scheduler-with-simple-'
            'examples class="form-a">Understand lr_scheduler with simple examples</a></small>'
        ),
    )
    lr_adjust_step_size = models.PositiveIntegerField(
        # activated/deactivated in frontend based on the selected scheduler
        verbose_name='LR adjustment step size',
        default=0,
        help_text='Lr adjusts after this many epochs when using the "Step" or "Cosine" scheduler, otherwise this '
                  'parameter is not used.',
    )
    lr_warm_up_period = models.PositiveIntegerField(
        verbose_name='Lr warm up period',
        default=0,
        help_text='For linear scheduler only. Number of epochs that the initial learning rate will warm-up '
                  '(slowly increase) linearly. After this epoch, the set learning rate will be used. '
                  'Warm-up in general can be beneficial. No warm up or 2 to 5 epochs should be enough.<br>'
                  '<small>I refer to the paper <a class="form-a" href="https://arxiv.org/pdf/1908.03265.pdf">On the '
                  'Variance of the Adaptive Learning Rate and Beyond</a>. Shorter explanation: '
                  '<a class="form-a" href="https://stackoverflow.com/a/55942518/13439260">What does "learning rate '
                  'warm-up" mean?</a></small>',
    )
    lr_constant_period = models.PositiveIntegerField(
        verbose_name='Lr constant period',
        default=100,
        help_text='For linear scheduler only. After the warm-up, the learning rate will be constant for this many '
                  'epochs. Afterwards, the learning rate decays for "Lr decay period" epochs.',
    )

    lr_decay_period = models.PositiveIntegerField(
        verbose_name='Lr decay period',
        default=0,
        help_text='For linear scheduler only. Calculated automatically. '
                  'After the constant learning rate period, the lr will decrease linear to 0 for this many epochs.',
    )

    fine_tuning_epoch = models.PositiveIntegerField(
        verbose_name='Fine-tuning epoch',
        default=0,
        help_text='Set 0 to deactivate. Must be > 0 and <= Total epochs. Deactivate online augmentation after this '
                  'epoch (if it was active before).',
    )

    pretrained = models.BooleanField(
        # may not be used to filter - added too late
        verbose_name='Pretrained',
        blank=True,
        null=True,
    )
    dataset = models.ForeignKey(
        TrainingDataset,
        verbose_name='Training dataset',
        on_delete=models.SET_NULL,
        null=True,
    )

    total_epochs = models.PositiveIntegerField(
        verbose_name='Epochs trained',
    )
    best_epoch = models.PositiveIntegerField(
        verbose_name='Best epoch',
        null=True,
        default=None,
    )
    testing_epoch = models.PositiveIntegerField(
        verbose_name='Epoch for testing',
        help_text='Is used for testing. If test is executed again with a different epoch, this will be updated '
                  'accordingly.',
        null=True,
        default=None,
    )

    batch_size = models.PositiveIntegerField(
        verbose_name='Batch size',
    )
    channels = models.PositiveSmallIntegerField(
        verbose_name='Input channels',
    )
    classes = models.PositiveIntegerField(
        verbose_name='Output channels',
    )
    input_height = models.PositiveIntegerField(
        verbose_name='Input height',
    )
    input_width = models.PositiveIntegerField(
        verbose_name='Input width',
    )
    input_depth = models.PositiveIntegerField(
        verbose_name='Input depth',
        null=True,
        default=None,
    )
    # Why like this and not using the db model? I work on a hpc as well without a database. This simply made life easier
    # in the first place and one can extract all infos out of the checkpoint. There, kwargs are stored as dict.
    # TODO Maybe change to two new FK pointing to online image/volume aug. Import must be adjusted accordingly
    online_aug_kwargs_string = models.TextField(
        verbose_name='Online Augmentation Parameters',
        blank=True,
        default='',
    )

    storage_size = models.DecimalField(
        verbose_name='Storage size',
        help_text='in MB',
        decimal_places=2,
        max_digits=11,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0.00'))]
    )
    inference_time = models.DecimalField(
        verbose_name='Inference time',
        help_text='Time needed in seconds to predict 1024 images of size: 512x512 or 1 volume of size 1024x512x512',
        max_digits=11,
        decimal_places=2,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0.00'))]
    )
    training_accuracy = models.DecimalField(
        verbose_name='Training accuracy',
        help_text='in %, related to the best epoch.',
        max_digits=7,
        decimal_places=4,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('100.00'))]
    )
    validation_accuracy = models.DecimalField(
        verbose_name='Validation accuracy',
        help_text='in %, related to the best epoch.',
        max_digits=7,
        decimal_places=4,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('100.00'))]
    )
    test_accuracy = models.DecimalField(
        verbose_name='Test accuracy',
        help_text='in %, related to the testing epoch.',
        max_digits=7,
        decimal_places=4,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('100.00'))]
    )
    training_loss = models.DecimalField(
        verbose_name='Training loss',
        help_text='related to the best epoch.',
        max_digits=7,
        decimal_places=5,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0')), MaxValueValidator(Decimal('99'))]
    )
    validation_loss = models.DecimalField(
        verbose_name='Validation loss',
        help_text='related to the best epoch.',
        max_digits=7,
        decimal_places=5,
        blank=True,
        null=True,
        validators=[MinValueValidator(Decimal('0')), MaxValueValidator(Decimal('99'))]
    )
    history = models.BinaryField()
    history_header = models.TextField(
        verbose_name='Training history header',
        blank=True,
    )

    training_duration_10_epochs = models.PositiveIntegerField(
        verbose_name='Training duration (s)',
        help_text='Training duration in seconds per 10 epochs.',
        default=0,
    )

    testing_all_pixels = models.PositiveBigIntegerField(
        verbose_name='All pixels used for testing',
        blank=True,
        null=True,
    )
    testing_true_positives_ratio = models.DecimalField(
        verbose_name='True positives ratio',
        help_text='related to the testing epoch (also called sensitivity or recall).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )
    testing_true_negatives_ratio = models.DecimalField(
        verbose_name='True negatives ratio',
        help_text='related to the testing epoch (also called specificity).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )
    testing_false_positives_ratio = models.DecimalField(
        verbose_name='False positives ratio',
        help_text='related to the testing epoch (also called fallout).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )
    testing_false_negatives_ratio = models.DecimalField(
        verbose_name='False negatives ratio',
        help_text='related to the testing epoch (1 - sensitivity).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )
    testing_precision = models.DecimalField(
        verbose_name='Precision',
        help_text='related to the testing epoch (also called Positive Predicted Value).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )
    testing_mean_iou = models.DecimalField(
        verbose_name='Mean IoU',
        help_text='related to the testing epoch (also called jaccard index.).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )
    testing_dice_coefficient = models.DecimalField(
        verbose_name='DICE',
        help_text='related to the testing epoch (also called overlap index or F1-measure).',
        blank=True,
        null=True,
        decimal_places=12,
        max_digits=13,
        validators=[MinValueValidator(Decimal('0.00')), MaxValueValidator(Decimal('1.00'))]
    )

    encoded_class_weights = models.BinaryField(blank=True, null=True)

    # stored like: ndarray([value class 0, value class 1, ..., value class n (classes - 1)])
    # class_suited_for_calculation:
    # stored as numpy array with values = 0 or 1. 1 means: class occurred during testing and calculation was possible,
    # 0 means: class did not occur during testing. Example: ndarray([1, 1, 1, 0, 1, 1])
    testing_class_suited_for_calculation = models.BinaryField(blank=True, null=True)
    testing_true_positives_ratio_per_class = models.BinaryField(blank=True, null=True)
    testing_true_negatives_ratio_per_class = models.BinaryField(blank=True, null=True)
    testing_false_positives_ratio_per_class = models.BinaryField(blank=True, null=True)
    testing_false_negatives_ratio_per_class = models.BinaryField(blank=True, null=True)
    testing_precision_per_class = models.BinaryField(blank=True, null=True)
    testing_mean_iou_per_class = models.BinaryField(blank=True, null=True)  # actually iou
    testing_dice_coefficient_per_class = models.BinaryField(blank=True, null=True)
    testing_pixels_per_class = models.BinaryField(blank=True, null=True)
    testing_accuracy_per_class = models.BinaryField(blank=True, null=True)

    @property
    def class_weights(self):
        if self.encoded_class_weights is not None:
            return convert_binary_to_np_array(self.encoded_class_weights)
        else:
            return np.ones(self.classes)

    def save_history(self, history, header=None):
        if header is None:
            header = f'epochs, train_loss, val_loss, train_acc, val_acc, augmentation_strengths'
        if not isinstance(history, (list, np.ndarray)):
            raise ValueError('History must be an np array or a list')

        if not isinstance(history, np.ndarray):
            history = np.array(history)

        self.history = convert_np_array_to_binary(history)
        self.history_header = header
        self.save()

    def get_history(self):
        if self.history:
            return self.history_header, convert_binary_to_np_array(self.history)
        else:
            return None, None

    def get_online_aug_kwargs_as_dict(self):
        if not (self.online_aug_kwargs_string.startswith('{') and self.online_aug_kwargs_string.endswith('}')):
            # why? example: eval(os.system('rm -rf /')) would delete everything from your computer.
            raise ValueError(f'Cannot eval string to dict! Unsafe! String to evaluate: {self.online_aug_kwargs_string}')
        return eval(self.online_aug_kwargs_string)

    def get_total_training_duration_string(self):
        if self.training_duration_10_epochs:
            total_time = self.total_epochs / 10 * self.training_duration_10_epochs
            return get_human_time_string_from_seconds(total_time, localize=False)
        return None

    def get_total_training_duration_values(self):
        if self.training_duration_10_epochs:
            total_time = self.total_epochs / 10 * self.training_duration_10_epochs
            hours, minutes, seconds = get_human_time_values_from_seconds(total_time)
            days = 0
            if hours > 24:
                days = hours // 24
                hours = hours - days * 24
            return days, hours, minutes, seconds
        return None

    def save_test_storage(self, storage, testing_epoch):
        if isinstance(storage, MetricStorage):
            self.testing_epoch = testing_epoch
            self.test_accuracy = Decimal(storage.overall_accuracy * 100)
            self.testing_all_pixels = storage.all_pixels

            self.testing_class_suited_for_calculation = convert_np_array_to_binary(storage.class_suited_for_calculation)
            self.testing_accuracy_per_class = convert_np_array_to_binary(storage.accuracy_per_class)
            self.testing_true_positives_ratio_per_class = convert_np_array_to_binary(
                storage.true_positives_ratio_per_class
            )
            self.testing_true_negatives_ratio_per_class = convert_np_array_to_binary(
                storage.true_negatives_ratio_per_class
            )
            self.testing_false_positives_ratio_per_class = convert_np_array_to_binary(
                storage.false_positives_ratio_per_class
            )
            self.testing_false_negatives_ratio_per_class = convert_np_array_to_binary(
                storage.false_negatives_ratio_per_class
            )
            self.testing_precision_per_class = convert_np_array_to_binary(storage.precision_per_class)
            self.testing_mean_iou_per_class = convert_np_array_to_binary(storage.mean_iou_per_class)
            self.testing_dice_coefficient_per_class = convert_np_array_to_binary(storage.dice_coefficient_per_class)
            self.testing_pixels_per_class = convert_np_array_to_binary(storage.pixels_per_class)

            self.testing_true_positives_ratio = Decimal(storage.true_positives_ratio)
            self.testing_true_negatives_ratio = Decimal(storage.true_negatives_ratio)
            self.testing_false_positives_ratio = Decimal(storage.false_positives_ratio)
            self.testing_false_negatives_ratio = Decimal(storage.false_negatives_ratio)
            self.testing_precision = Decimal(storage.precision)
            self.testing_mean_iou = Decimal(storage.mean_iou)
            self.testing_dice_coefficient = Decimal(storage.dice_coefficient)

            self.save()
        else:
            raise ValueError('storage must be a MetricStorage instance!')

    def get_online_aug_parameter_list(self, improve_readability=False):
        """

        Args:
            improve_readability: converts parameters like 'tilting_prob' to tilting probability, adds units if needed
             and more.

        Returns: list of tuples like: [(arg, value)]

        """
        if not self.online_aug_kwargs_string:
            # old strings are stored with ''
            return []
        if not (self.online_aug_kwargs_string.startswith('{') and self.online_aug_kwargs_string.endswith('}')):
            # why? example: eval(os.system('rm -rf /')) would delete everything from your computer.
            raise ValueError(f'Cannot eval string to dict! Unsafe! String to evaluate: {self.online_aug_kwargs_string}')

        kwargs = eval(self.online_aug_kwargs_string)
        if not kwargs:
            return []

        if self.architecture in constants.TWO_D_NETWORKS:
            pipe_class = ImageAugmentationPipeline
        else:
            pipe_class = VolumeAugmentationPipeline

        pipe_args = inspect.getfullargspec(pipe_class).args

        try:
            parameter_list = [('Aug start epoch', kwargs['online_aug_start_epoch'])]
        except KeyError:
            # old models
            parameter_list = [('Aug start epoch', 0)]

        if improve_readability:
            for arg in pipe_args:
                if arg == 'self':
                    continue
                if arg not in kwargs.keys():
                    continue

                name = arg.replace('_', ' ')

                if 'prob' in name:
                    name = name.replace('prob', 'probability')
                try:
                    value = kwargs[arg]
                    if 'prob' in arg or arg == 'global_strength':
                        value = f'{value:.0f}%'
                    if isinstance(value, bool):
                        if value:
                            value = 'Yes'
                        else:
                            value = 'No'
                    if isinstance(value, list):
                        values_list = [f'{val:.2f}' for val in value]
                        value = f'{", ".join(values_list)}'

                    if 'scale' in 'arg':
                        name += ' (min, max)'

                    if arg == 'tilting_start_end_factor':
                        name = 'tilting (start, end)'

                    if arg == 'max_rot_angle':
                        name = 'max rotation angle'
                        value = mark_safe(f'&#177; {value}°')

                    if 'policy' in arg:
                        value = constants.AUGMENTATION_POLICIES_DICT[value]
                    if not isinstance(value, str):
                        value = str(value)

                except KeyError:
                    value = ''

                name = name.capitalize()
                parameter_list.append((name, value))
        else:
            for arg in pipe_args:
                if arg == 'self':
                    continue
                try:
                    parameter_list.append((arg, kwargs[arg]))
                except KeyError:
                    parameter_list.append((arg, None))
        return parameter_list
