import torch

from decimal import Decimal

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.safestring import mark_safe

from aiseg import constants

"""
This is created once to store the users training input over time.
"""


class TrainingParameters(models.Model):
    architecture = models.CharField(
        verbose_name='Architecture',
        max_length=128,
        choices=constants.ARCHITECTURE_CHOICES,
        default=constants.UNet_NAME,
        help_text='Choose a neural network.'
    )
    backbone = models.CharField(
        verbose_name='Backbone',
        max_length=128,
        blank=True,
        choices=constants.BACKBONE_MODEL_CHOICES,  # dynamically selected at frontend
        help_text='Choose a backbone if supported.'
    )
    pretrained = models.BooleanField(
        verbose_name='Pretrained',
        default=True,
        help_text='Only for rgb and if batch normalization is used. Otherwise training from scratch.'
    )
    dataset = models.CharField(
        verbose_name='Dataset name',
        max_length=256,
        default='',
        blank=True,
    )
    norm_on_dataset = models.BooleanField(
        verbose_name='Normalize on dataset',
        default=True,
        help_text='If True, the mean and std of the dataset will be used for normalization (recommended).'
    )
    batch_size = models.PositiveIntegerField(
        verbose_name='Batch size',
        default=16,
        help_text='Images/Volumes per forward pass during training.',
    )
    channels = models.PositiveSmallIntegerField(
        verbose_name='Input channels',
        default=1,
        choices=[(1, 1), (3, 3)],
        help_text='1 for gray, 3 for rgb.'
    )
    classes = models.PositiveIntegerField(
        verbose_name='Output classes',
        default=2,
        validators=[MinValueValidator(2)],
        help_text='Classes to be segmented. Binary classification: 2, multiclass: > 2.',
    )
    epochs = models.PositiveIntegerField(
        verbose_name='Total epochs',
        validators=[MinValueValidator(1)],
        default=100,
        help_text='Number of maximal epochs to run.',
    )
    save_every = models.PositiveIntegerField(
        verbose_name='Save every',
        validators=[MinValueValidator(0)],
        default=0,
        help_text='Save a checkpoint every x epochs. Best checkpoints regarding validation loss and accuracy are '
                  'always saved. Set to 0 to store only the last and the best 2 checkpoints (val loss and accuracy).',
    )
    input_size = models.PositiveIntegerField(
        verbose_name='Input size',
        validators=[MinValueValidator(32)],
        default=256,
        help_text='height and width (h = w) in pixels/voxels',
    )
    input_depth = models.PositiveIntegerField(
        verbose_name='Input depth',
        default=0,
        help_text='For 3D networks: depth of the input block. Dimension is: (input_size x '
                  'input_size x input_depth) = (h, w, d)',
    )
    reproduce = models.BooleanField(
        verbose_name='Reproducible',
        default=False,
        help_text=f'Slower! Recommended for research.',
    )
    normalization = models.CharField(
        verbose_name='Normalization Layer',
        choices=constants.NORMALIZATION_CHOICES,
        default=constants.GROUP_NORM,
        max_length=12,
        help_text=mark_safe('Recommended: Group Normalization ('
                            '<a class="form-a" href="https://arxiv.org/abs/1803.08494">paper</a>).'),
    )
    optimizer_name = models.CharField(
        verbose_name='Optimizer',
        max_length=64,
        blank=False,
        choices=constants.OPTIMIZER_CHOICES,
        default='AdamW',
        help_text='Select an optimizer. Use AdamW if unsure.'
    )
    learning_rate = models.DecimalField(
        verbose_name='Learning rate',
        decimal_places=4,
        max_digits=6,
        default=Decimal('0.001'),
        validators=[MaxValueValidator(Decimal('1.0')), MinValueValidator(Decimal('0.000001'))],
        help_text='Initial learning rate. Will be adapted by the optimizer.',
    )

    device_choices = [('cpu', 'CPU')]
    default_device = 'cpu'
    device_help_text = 'No supported graphics card found.'
    if torch.cuda.is_available():
        device_count = torch.cuda.device_count()
        if device_count == 1:
            device_choices.append(('cuda:0', 'GPU'))
            default_device = 'cuda:0'
        else:
            for i in range(device_count):
                device_choices.append((f'cuda:{i}', f'GPU:{i}'))
            default_device = 'cuda'

        device_choices.append(('cuda', 'MultiGPU'))  # is set to disabled (frontend) if only 1 gpu is available
        device_help_text = 'Device to process on (GPU recommended).'

    device = models.CharField(
        verbose_name='Device',
        choices=device_choices,
        default=default_device,
        max_length=7,
        help_text=device_help_text,
    )
    online_aug = models.BooleanField(
        verbose_name='Use online augmentation',
        default=False,
        help_text='Training data will be augmented during the training.',
    )

    auto_weight = models.BooleanField(
        verbose_name='Auto weight classes',
        default=True,
        help_text='Recommended. Weights each class with respect to the occurrence in the dataset.',
    )

    num_gpus = models.PositiveIntegerField(
        verbose_name='Parallel GPUs',
        default=None,
        validators=[MinValueValidator(2)],
        help_text='Set device to MultiGPU to enable. Number of gpus used during training. Must be >=2.',
        blank=True,
        null=True,
    )

    lr_scheduler = models.CharField(
        verbose_name='Learning rate scheduler',
        choices=constants.LR_SCHEDULER_CHOICES,
        default=constants.LR_SCHEDULER_LINEAR,
        max_length=128,
        help_text=mark_safe(
            'The scheduler adjusts the learning rate of the networks after each epoch. Basic concept: <br>'
            'Linear: 3 phases (see graph).<br>'
            '<small>Phase 1 (optional): The learning rate increases linearly during the warm up.<br>'
            'Phase 2: The learning rate will be constant until "Lr constant period" + "Lr warm up period" is '
            'reached.<br>'
            'Phase 3 (optional): The lr decays to 0 until the final epoch is reached.</small><br>'
            'Step: Step wise reduction with a step size of: "LR adjustment step size". Decays automatically.<br>'
            'Cosine and warm restart: Cyclically adjusts the learning rate using a cosine function. Decays '
            f'automatically until {constants.MINIMUM_LR}, then resets (warm restart) or increases (cosine) to lr.<br>'
            '<small>See also: <a href=https://www.kaggle.com/code/billiemage/understand-lr-scheduler-with-simple-'
            'examples class="form-a">Understand lr_scheduler with simple examples</a></small>'
        ),
    )
    lr_adjust_step_size = models.PositiveIntegerField(
        # activated/deactivated in frontend based on the selected scheduler
        verbose_name='LR step size',
        default=0,
        help_text=mark_safe(
            f'In case of <b>"Step" scheduler</b>: Lr is divided by 2 after this many epochs.<br>In case of '
            f'<b>"Cosine warm restart"</b>: Lr decays in a cosine fashion to {constants.MINIMUM_LR} and will then be '
            f'reset to the initial lr value. The number of epochs to do so is represented by this value. This value '
            f'will be adjusted after each cycle by multiplying this value by 2. '
            f'Example: <a href=https://www.kaggle.com/code/isbhargav/guide-to-pytorch-learning-rate-scheduling#9.'
            f'CosineAnnealingWarmRestarts class="form-a">click here and see last image</a>.<br>In case of <b>'
            f'"Cosine"</b>: LR cycles using the cosine function. Example: '
            f'step size = 10: The lr decays to {constants.MINIMUM_LR} in the first 10 epochs. It then increases to the '
            f'initial lr from epoch 11 to 20. It then again decays and so on. Example image: '
            f'<a href=https://www.kaggle.com/code/isbhargav/guide-to-pytorch-learning-rate-scheduling#6.-'
            f'CosineAnnealingLR class="form-a">click here</a>.'
        ),
    )
    lr_warm_up_period = models.PositiveIntegerField(
        verbose_name='Lr warm up period',
        default=0,
        help_text='For linear scheduler only. Number of epochs that the initial learning rate will warm-up '
                  '(slowly increase) linearly. After this epoch, the set learning rate will be used. '
                  'Warm-up in general can be beneficial. No warm up or 2 to 5 epochs should be enough.<br>'
                  '<small>I refer to the paper <a class="form-a" href="https://arxiv.org/pdf/1908.03265.pdf">On the '
                  'Variance of the Adaptive Learning Rate and Beyond</a>. Shorter explanation: '
                  '<a class="form-a" href="https://stackoverflow.com/a/55942518/13439260">What does "learning rate '
                  'warm-up" mean?</a></small>',
    )
    lr_constant_period = models.PositiveIntegerField(
        verbose_name='Lr constant period',
        default=100,
        help_text='For linear scheduler only. After the warm-up, the learning rate will be constant for this many '
                  'epochs. Afterwards, the learning rate decays for "Lr decay period" epochs.',
    )

    lr_decay_period = models.PositiveIntegerField(
        verbose_name='Lr decay period',
        default=0,
        help_text='For linear scheduler only. Calculated automatically. '
                  'After the constant learning rate period, the lr will decrease linear to 0 for this many epochs.',
    )

    fine_tuning_epoch = models.PositiveIntegerField(
        verbose_name='Fine-tuning epoch',
        default=0,
        help_text='Set 0 to deactivate. Must be > 0 and <= Total epochs. Deactivates online augmentation after this '
                  'epoch (if it was active before).',
    )
