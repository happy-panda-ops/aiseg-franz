"""roving_surface_extractor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from aiseg import views

app_name = 'aiseg'
urlpatterns = [
    path('', views.index, name='index'),
    path('analysis/clean_up/', views.clean_model_analyses, name='analyses_clean_up'),
    path('analysis/comparison/', views.model_comparison, name='model_comparison'),
    path('analyses/export/', views.export, name='export'),
    path('analysis/trained_model_overview/', views.trained_model_overview, name='trained_model_overview'),
    path('analysis/trained_model/<str:analysis_pk>/', views.trained_model_analysis, name='trained_model_analysis'),
    path('analysis/trained_model/delete/<str:analysis_pk>/', views.delete_model_analysis, name='delete_model_analysis'),
    path('analysis/trained_model_import/', views.trained_model_import, name='trained_model_import'),
    path('augmentation/', views.augmentation, name='augmentation'),
    path('augmentation/image/', views.image_augmentation, name='image_augmentation'),
    path('augmentation/volume/', views.volume_augmentation, name='volume_augmentation'),
    path('datasets/', views.datasets_overview, name='datasets_overview'),
    path('datasets/delete/<str:dataset_pk>/', views.delete_dataset, name='delete_dataset'),
    path('datasets/<str:dataset_pk>/', views.edit_dataset, name='edit_dataset'),
    path('inference/', views.inference, name='inference'),
    path('logs/', views.logs, name='logs'),
    path('monitor/<str:log_pk>/', views.monitor, name='monitor'),
    path('configs/', views.configurations_overview, name='configurations_overview'),
    path(
        'configs/train/delete/<str:configuration_pk>/',
        views.delete_training_configuration,
        name='delete_training_configuration'
    ),
    path(
        'configs/inference/delete/<str:configuration_pk>/',
        views.delete_inference_configuration,
        name='delete_inference_configuration'
    ),
    path(
        'configs/train/<str:configuration_pk>/',
        views.edit_training_configuration,
        name='edit_training_configuration'
    ),
    path(
        'configs/inference/<str:configuration_pk>/',
        views.edit_inference_configuration,
        name='edit_inference_configuration'
    ),
    path('training/', views.overview, name='training_overview'),
    path('training/new_training/', views.new_training, name='new_training'),
    path('training/continuing/', views.continuing, name='continuing'),
    path('training/online_augmentation/image/', views.online_image_aug, name='online_image_aug'),
    path('training/online_augmentation/volume/', views.online_volume_aug, name='online_volume_aug'),
    path('training/online_augmentation/examples/', views.online_aug_examples, name='online_aug_examples'),
]
