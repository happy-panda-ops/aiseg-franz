var backbone_choices;
var epochs_field;
var lr_warm_up_period_field;
var lr_constant_period_field;
var fine_tuning_epoch_field;
var lr_scheduler_field;
var lr_decay_field;
var lr_adjust_step_size_field;

function postCalculationRequestRequest() {
    document.getElementById("js-optimal-para-request-button").disabled = true;
    document.getElementById("js-run-button").disabled = true;
    try {
        document.getElementById("js-run-kill-button").disabled = true;
    }
    catch (TypeError) {}

    let backendForm = document.getElementById("input_form");

    let hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = 'calculate_optimal_parameters';
    hiddenField.value = 'true';
    backendForm.appendChild(hiddenField);

    backendForm.submit();
}

function updateBackboneSelect() {
    let architecture_select = document.getElementById("id_architecture");
    let backbone_select = document.getElementById("id_backbone");
    let initial_value = backbone_select.value;
    backbone_select.length = 0;

    let selected_architecture = architecture_select.options[architecture_select.selectedIndex].value;
    let backbone_list = backbone_choices[selected_architecture];

    for (let i = 0; i < backbone_list.length; i++) {
        let option = document.createElement("option");
        option.value = backbone_list[i];
        if (option.value === initial_value) {
            option.setAttribute('selected', '')
        }
        if (backbone_list[i]){
            option.text = backbone_list[i];
        } else {
            option.text = '-------------';
        }
        backbone_select.appendChild(option);
    }
}

function updateRequestButton(blocked_devices) {
    let device_select = document.getElementById("id_device");
    let selected_device = device_select.value;
    let request_button = document.getElementById("js-optimal-para-request-button");
    let calc_button_text = document.getElementsByClassName('js-data-holder')[0].dataset.calc_button_text;
    let calc_button_blocked_text = document.getElementsByClassName('js-data-holder')[0].dataset.calc_button_blocked_text;

    if (blocked_devices.includes(selected_device)) {
        request_button.innerHTML = calc_button_blocked_text;
        if (request_button.className.split(' ').includes('btn-primary-form')) {
            request_button.className = request_button.className.replace('btn-primary-form', 'btn-log-running');
        }
    } else {
        request_button.innerHTML = calc_button_text;
        if (request_button.className.split(' ').includes('btn-log-running')) {
            request_button.className = request_button.className.replace('btn-log-running', 'btn-primary-form');
        }
    }
}

function set_disabled(field) {
    if (!field.hasAttribute("disabled")) {
        field.setAttribute('disabled', '');
    }
}

function set_active(field) {
    if (field.hasAttribute("disabled")) {
        field.removeAttribute('disabled');
    }
}

function update_lr_scheduling_fields(is_scheduler_change=false, is_epoch_update=false) {
    if (lr_scheduler_field.value === 'LinearLR') {
        set_active(lr_constant_period_field);
        set_active(lr_warm_up_period_field);
        set_disabled(lr_adjust_step_size_field);

        if (is_scheduler_change) {
            lr_constant_period_field.value = epochs_field.value;
            lr_warm_up_period_field.value = "0";
            lr_adjust_step_size_field.value = "0";
        }

        lr_decay_field.value = parseInt(epochs_field.value) - (parseInt(lr_warm_up_period_field.value) + parseInt(lr_constant_period_field.value));
        if (lr_decay_field.value < 0) {
            lr_decay_field.style.backgroundColor = "var(--danger-red)";
            lr_decay_field.style.color = "var(--gray-light)";
        } else {
            lr_decay_field.style.backgroundColor = "";
            lr_decay_field.style.color = "";
        }
    } else {
        if (is_scheduler_change) {
            lr_decay_field.value = '0';
            lr_constant_period_field.value = epochs_field.value;
            lr_warm_up_period_field.value = "0";
        } else if (is_epoch_update) {
            lr_constant_period_field.value = epochs_field.value;
        }

        lr_decay_field.style.backgroundColor = "";
        lr_decay_field.style.color = "";
        set_active(lr_adjust_step_size_field);
        set_disabled(lr_constant_period_field);
        set_disabled(lr_warm_up_period_field);
    }
}

function validate_fine_tuning_epoch() {
    let epochs_field = document.getElementById("id_epochs");
    let fine_tuning_field = document.getElementById("id_fine_tuning_epoch");

    if (parseInt(fine_tuning_field.value) > parseInt(epochs_field.value)) {
        fine_tuning_field.style.backgroundColor = "var(--danger-red)";
        fine_tuning_field.style.color = "var(--gray-light)";
    } else {
        fine_tuning_field.style.backgroundColor = "";
        fine_tuning_field.style.color = "";
    }
}


window.addEventListener('load', function load(event) {
    let request_button = document.getElementById("js-optimal-para-request-button");
    let architecture_select = document.getElementById("id_architecture");
    let device_select = document.getElementById("id_device");
    backbone_choices = JSON.parse(document.getElementsByClassName('js-data-holder')[0].dataset.backbone_dict);
    let predicted_ram = document.getElementsByClassName('js-data-holder')[0].dataset.predicted_ram;
    let blocked_devices = document.getElementsByClassName('js-data-holder')[0].dataset.blocked_devices;
    blocked_devices = blocked_devices.replace(/ /g, '').replace(/'/g, '').replace(/\[/g, '').replace(/]/g, '').split(',')

    epochs_field = document.getElementById("id_epochs");
    lr_warm_up_period_field = document.getElementById("id_lr_warm_up_period");
    lr_constant_period_field = document.getElementById("id_lr_constant_period");
    fine_tuning_epoch_field = document.getElementById("id_fine_tuning_epoch");
    lr_scheduler_field = document.getElementById("id_lr_scheduler");
    lr_decay_field = document.getElementById("id_lr_decay_period");
    lr_adjust_step_size_field = document.getElementById("id_lr_adjust_step_size");

    update_lr_scheduling_fields();

    if (predicted_ram !== 'None') {
        let success_modal = new bootstrap.Modal(document.getElementById('js-ram-calc-success-modal'), {})
        success_modal.toggle()
    }

    updateBackboneSelect();
    updateRequestButton(blocked_devices);

    request_button.addEventListener("click", function () {
        postCalculationRequestRequest();
    });

    architecture_select.addEventListener("change", function () {
        updateBackboneSelect();
    });

    device_select.addEventListener("change", function () {
        updateRequestButton(blocked_devices);
    });

    epochs_field.addEventListener("change", function() {
        update_lr_scheduling_fields(false, true);
        validate_fine_tuning_epoch();
    })

    lr_warm_up_period_field.addEventListener("change", function() {
        update_lr_scheduling_fields();
    })

    lr_constant_period_field.addEventListener("change", function() {
        update_lr_scheduling_fields();
    })

    fine_tuning_epoch_field.addEventListener("change", function() {
        validate_fine_tuning_epoch();
    })

    lr_scheduler_field.addEventListener("change", function() {
        update_lr_scheduling_fields(true, false);
    })

});
