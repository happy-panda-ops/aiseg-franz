var best_epoch;
var train_loss;
var valid_loss;
var train_acc;
var valid_acc;
var tick_interval;
var train_data_visible;

var LOSS_MAX = null;

// slightly customized theme
function set_gray_theme() {
    Highcharts.theme = {
        colors: [
            '#9dc1cf', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
            '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'
        ],
        chart: {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                stops: [
                    [0, '#3e3e40'],
                    [1, '#2a2a2b']
                ]
            },
            style: {
                fontFamily: '\'Unica One\', sans-serif'
            },
            plotBorderColor: '#606063'
        },
        title: {
            style: {
                color: '#E0E0E3',
                fontSize: '20px'
            }
        },
        subtitle: {
            style: {
                color: '#84868b',
                textTransform: 'uppercase'
            }
        },
        xAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: '#E0E0E3'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            title: {
                style: {
                    color: '#A0A0A3'

                }
            }
        },
        yAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: '#E0E0E3'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            tickWidth: 1,
            title: {
                style: {
                    color: '#A0A0A3'
                }
            }
        },
        tooltip: {
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            style: {
                color: '#F0F0F0'
            }
        },
        plotOptions: {
            series: {
                dataLabels: {
                    color: '#F0F0F3',
                    style: {
                        fontSize: '13px'
                    }
                },
                marker: {
                    lineColor: '#333'
                }
            },
            boxplot: {
                fillColor: '#505053'
            },
            candlestick: {
                lineColor: 'white'
            },
            errorbar: {
                color: 'white'
            }
        },
        legend: {
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            itemStyle: {
                color: '#E0E0E3'
            },
            itemHoverStyle: {
                color: '#FFF'
            },
            itemHiddenStyle: {
                color: '#606063'
            },
            title: {
                style: {
                    color: '#C0C0C0'
                }
            }
        },
        credits: {
            style: {
                color: '#666'
            }
        },
        labels: {
            style: {
                color: '#707073'
            }
        },

        drilldown: {
            activeAxisLabelStyle: {
                color: '#F0F0F3'
            },
            activeDataLabelStyle: {
                color: '#F0F0F3'
            }
        },

        navigation: {
            buttonOptions: {
                symbolStroke: '#DDDDDD',
                theme: {
                    fill: '#505053'
                }
            }
        },

        // scroll charts
        rangeSelector: {
            buttonTheme: {
                fill: '#505053',
                stroke: '#000000',
                style: {
                    color: '#CCC'
                },
                states: {
                    hover: {
                        fill: '#707073',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    },
                    select: {
                        fill: '#000003',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    }
                }
            },
            inputBoxBorderColor: '#505053',
            inputStyle: {
                backgroundColor: '#333',
                color: 'silver'
            },
            labelStyle: {
                color: 'silver'
            }
        },

        navigator: {
            handles: {
                backgroundColor: '#666',
                borderColor: '#AAA'
            },
            outlineColor: '#CCC',
            maskFill: 'rgba(255,255,255,0.1)',
            series: {
                color: '#7798BF',
                lineColor: '#A6C7ED'
            },
            xAxis: {
                gridLineColor: '#505053'
            }
        },

        scrollbar: {
            barBackgroundColor: '#808083',
            barBorderColor: '#808083',
            buttonArrowColor: '#CCC',
            buttonBackgroundColor: '#606063',
            buttonBorderColor: '#606063',
            rifleColor: '#FFF',
            trackBackgroundColor: '#404043',
            trackBorderColor: '#404043'
        },
    };

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);
}


function createLossGraph() {
    let loss_chart = Highcharts.chart('js-loss-graph', {
        chart: {
            type: 'line',
            zoomType: 'x',
        },
        plotOptions: {
            line: {
                pointStart: 1,
                lineWidth: 2,
                marker: {
                    enabled: false,
                },
            },
        },
        title: {
            text: 'Loss',
        },
        xAxis: {
            title: {
                text: 'Epoch',
            },
            crosshair: true,
            gridLineWidth: 1,
            min: 0,
            tickInterval: tick_interval,
        },
        yAxis: {
            title: {
                text: 'Loss',
            },
            min: 0,
            max: LOSS_MAX,
        },
        tooltip: {
            formatter: function() {
                return 'Epoch: <b>' + this.x + '</b><br>' + this.series.name + ': <b>' + this.y + '</b>';
            },
        },
        series: [{
            name: 'Training Loss',
            data: train_loss,
            visible: train_data_visible,
        }, {
            name: 'Validation Loss',
            data: valid_loss,
        }],
        annotations: [{
            draggable: '',
            labelOptions: {
                backgroundColor: 'rgba(255,255,255,0.5)',
            },
//            labels: [{
//                point: {
//                    xAxis: 0,
//                    yAxis: 0,
//                    x: best_epoch,
//                    y: valid_loss[best_epoch - 1][1],
//                },
//                text: 'Best Epoch (' + best_epoch + ')'
//            }],
        }],
    });
}


function createAccuracyGraph() {
    let accuracy_chart = Highcharts.chart('js-accuracy-graph', {
        chart: {
            type: 'line',
            zoomType: 'x',
        },
        title: {
            text: 'Accuracy',
        },
        xAxis: {
            title: {
                text: 'Epoch',
            },
            min: 0,
            crosshair: true,
            gridLineWidth: 1,
            labels: {
                overflow: 'justify'
            },
            tickInterval: tick_interval,
        },
        yAxis: {
            title: {
                text: 'Accuracy in %',
            },
            max: 100,
        },
        tooltip: {
            formatter: function() {
                return 'Epoch: <b>' + this.x + '</b><br>' + this.series.name + ': <b>' + this.y + ' %</b>';
            },
        },
        plotOptions: {
            line: {
                lineWidth: 2,
                marker: {
                    enabled: false,
                },
            },
        },
        series: [{
            name: 'Training Accuracy',
            data: train_acc,
            visible: train_data_visible,
        }, {
            name: 'Validation Accuracy',
            data: valid_acc,
        }],
         annotations: [{
            draggable: '',
            labelOptions: {
                backgroundColor: 'rgba(255,255,255,0.5)',
                verticalAlign: 'top',
                y: 15
            },
//            labels: [{
//                point: {
//                    xAxis: 0,
//                    yAxis: 0,
//                    x: best_epoch,
//                    y: valid_acc[best_epoch - 1][1],
//                },
//                text: 'Best Epoch (' + best_epoch + ')'
//            }],
        }],
    });
}


window.addEventListener('load', function load(event) {
    // enable tooltips:
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    let history_data_holder = document.getElementsByClassName('js-data-holder')[0];

    // best_epoch = JSON.parse(history_data_holder.dataset.best_epoch);
    train_loss = JSON.parse(history_data_holder.dataset.train_loss);
    valid_loss = JSON.parse(history_data_holder.dataset.valid_loss);
    train_acc = JSON.parse(history_data_holder.dataset.train_acc);
    valid_acc = JSON.parse(history_data_holder.dataset.valid_acc);
    train_data_visible = history_data_holder.dataset.train_data_active
    let max_loss = history_data_holder.dataset.max_loss_val;

    if (max_loss > 3) {
        LOSS_MAX = 3;
    }

    if (train_loss.length <= 5) {
        tick_interval = 1;
    } else {
        tick_interval = undefined;
    }

    if (train_data_visible === 'True') {
        train_data_visible = true;
    } else {
        train_data_visible = false;
    }

    set_gray_theme();
    createAccuracyGraph();
    createLossGraph();

})
