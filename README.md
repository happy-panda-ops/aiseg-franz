# AiSeg - Easy Pytorch Segmentation

| Features                                                                                                                                                                                                                                                                                                                             | Interface                                                                     |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| - **User Interface** (see right)<br>- **35 2D and 9 3D supported models** (more with different backbones)<br>- **Need a model?** Simply [copy](#copy) it (cite the authors and this project!)<br>- **Data augmentation** (online and offline) <br>- **Training**<br>- **Inference**<br>- **Analysis**<br>- **Easy installation**<br> | <img src="/readme_images/comparison_graphs.png" alt="interface" width="500"/> |

# Table of Contents:

---

1. [**Background**](#background)
2. [**Installation**](#install)
3. [**Tutorial**](#tutorial)
4. [**Models**](#archi)
    - [Overview](#overview)
        - [2D](#overview2d)
        - [3D](#overview3d)
5. [**Data preparation**](#data)
6. [**Augmentation**](#augmentation)
7. [**Citation**](#citation)

<a name="background"></a>
# Background
<details>
<summary><b><u>Click to expand</u></b></summary>

---
#### Project info
This project is still under development!

You want to use this project or code of it for commercial use? Please contact 
[Franz Wagner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/photogrammetrie/die-professur/beschaeftigte/m-sc-franz-wagner?set_language=en)

#### Why it was created
This project was initially created in the scope of [CRC/TRR 280](https://www.sfbtrr280.de/en/) to segment cement-based 
composites captured with a microtomograph. One question was: "Which model works best for this task?". So I implemented 
"some" state-of-the art networks and now this project contains one of the largest collection of2D and 3D segmentation 
models (UPDATE: found [PaddleSeg](https://github.com/PaddlePaddle/PaddleSeg). It contains more but no such UI). 
Further, this project has an optimized (in terms of best practice) training, inference and augmentation module as 
well as an UI.

#### About me
A former software developer, now working at the TU Dresden as research associate (and PhD student) at the chair of 
photogrammetry.
</details>

<a name="install"></a>
# Installation

<details>
<summary><b><u>Click to expand</u></b></summary>

**Hardware requirements:**
- at least 2 core CPU (4 or more recommended)
- at least 12GB RAM (16 or more recommended)
- NVIDIA GPU (CUDA >= 11.7) with at least 4GB RAM for training, 2GB for inference (Recommended: training: 8GB or more, inference: 4GB or more)

**Windows:**
1. Install Python 3.9 (https://www.python.org/ftp/python/3.9.13/python-3.9.13-amd64.exe)
2. execute the `windows_installer.bat` (this creates a virtual environment inside this project)

**Linux (Ubuntu)**
1. Use Python 3.9, ensure pip and venv are installed!
2. open a terminal and cd to this project
3. call: `source linux_installer.sh` this creates a folder called virtual_env at the same level as this file.

**Uninstall?** Simply delete the project.

<small>If you want to load a checkpoint trained with python <3.9, you need to install Microsoft Visual C++ >=14.0 and 
call `pip install backports-zoneinfo`</small>
</details>

# Start the UI
<details>
<summary><b><u>Click to expand</u></b></summary>

**Windows**
1. double-click on the `start_server.bat`

**Linux**
1. open a terminal and cd to `../ai_seg/`
2. call: `source start_server.sh`

<a name="tutorial"></a>
</details>

# Tutorial

<details>
<summary><b><u>General</u></b></summary>

**Basic layout**

<img src="/readme_images/tutorial/index.jpg" alt="index" width="800"/>

**The navigation bar explained**

<img src="/readme_images/tutorial/navbar.jpg" alt="index" width="800"/>
</details>

<details>
<summary><b><u>Training</u></b></summary>

<img src="/readme_images/tutorial/training/index_training.jpg" alt="interface" width="800"/>

**Click on Training or use a checkpoint to continue the training**

<img src="/readme_images/tutorial/training/overview.jpg" alt="tr_overview" width="800"/>

**The training form**

<img src="/readme_images/tutorial/training/training/training_form.jpg" alt="tr_form" width="800"/>

1. **Select your Dataset**
2. **Select the Architecture** (UNetResNet with ResNeXt 50 backbone is a good start)
3. **Normalization** if batch size is <=16, select Group Normalization, check their [paper](https://arxiv.org/pdf/1803.08494.pdf])! However, this drops pretrained.
4. **Input channels** 3 for rgb, 1 for gray
5. **Output classes** 2 for binary
6. **Pretrained** True is in most cases superior! Weights pretrained on Imagenet
7. **Use online augmentation** see at augmentation.
8. **Load or calculate optimal parameters** 
   - Calculates optimal parameters for your system. 
   - Tries to use a batch size of >=16 by reducing the input size
   - However: 
     - **larger input size is superior**! 
     - If you think, it is too small: increase input size and
     - use Group Normalization ([paper](https://arxiv.org/pdf/1803.08494.pdf]))
     - Set largest possible input size and a batch size of at least 2
   - Calculate with fixed depth: only for 3D. Calculation does use your set depth.
9. **Or set a custom input size**
   - some models expect input sizes of 2^n, where n >= 5
   - some 3D models (Like UNet 3D) expect input depths which are dividable by 2 at least 4 times 
     - smallest input depth is therefore: 16 (16/(2^4)=1)
   - Larger input sizes are superior!

Parallel GPUs: Only activated on a multi GPU machine.

**Monitoring** (all log outputs are stored at: `aiseg/logs/`)
- live logging is showed after you hit run:

<img src="/readme_images/tutorial/training/training/training_run_with_log.jpg" alt="tr_logging" width="800"/>

</details>

<details>
<summary><b><u>Inference</u></b></summary>

<img src="/readme_images/tutorial/inference/index_inference.jpg" alt="tr_logging" width="800"/>

**The inference form**

<img src="/readme_images/tutorial/inference/inference.jpg" alt="tr_logging" width="800"/>

1. **Pass the path to your checkpoint**
2. **Pass the path to the data you want to segment**
3. **Select overlapping (50%: every pixel is segmented at least twice)**
4. **Choose a weighting strategy if overlap is active**
    - Sum: Each segmentation has a weight of 1
    - Linear: Border pixels have a weight of 1, increasing linear by 1 to the center (like a pyramid in 2D)
    - Gauss: 2D/3D gaussian bell where border pixels have a weight of ~1/3 to that of the center

</details>

<details>
<summary><b><u>Augmentation</u></b></summary>

<details>
<summary><b><u>Online Augmentation</u></b></summary>

**Select online augmentation** during training initialization and prepare the expected input size (and depth for 3D)

For the possible augmentations, please see the appendix in [https://doi.org/10.1016/j.jag.2023.103305](https://doi.org/10.1016/j.jag.2023.103305)

<img src="/readme_images/tutorial/augmentation/online_aug/online_aug_active.jpg" alt="onl_aug_init" width="800"/>

**A new view:**

<img src="/readme_images/tutorial/augmentation/online_aug/online_aug_interface.jpg" alt="onl_aug_form" width="800"/>

1. **Set your parameters** This is a very important step. You definitely should create some examples!
2. **Check your parameters by clicking on Show examples (top right)**:

**See examples of your set parameters**

<img src="/readme_images/tutorial/augmentation/online_aug/online_aug_examples.jpg" alt="onl_aug_examples" width="800"/>
</details>

<details>
<summary><b><u>Offline Augmentation</u></b></summary>

<img src="/readme_images/tutorial/augmentation/index_augmentation.jpg" alt="aug_index" width="800"/>

**Set your parameters, choose the augmentation methods and run:**

<img src="/readme_images/tutorial/augmentation/offline/image_aug_form.jpg" alt="offl_aug" width="800"/>
</details>
</details>


<details>
<summary><b><u>Model Analysis</u></b></summary>

<img src="/readme_images/tutorial/analysis/index_analysis.jpg" alt="analysis_index" width="800"/>

<details>
<summary><b><u>Single Model</u></b></summary>

**Overview of your trained models** on top you can export or clean up those used for hyperparameter testing

<img src="/readme_images/tutorial/analysis/overview.jpg" alt="analysis_overview" width="800"/>

**Select a model and see the analysis**

<img src="/readme_images/tutorial/analysis/analysis_1.jpg" alt="analysis_1" width="800"/>

**Check out the used online augmentation parameters if used**

<img src="/readme_images/tutorial/analysis/analysis_2.jpg" alt="analysis_2" width="800"/>

**Check out the single class performance of your network**

<img src="/readme_images/tutorial/analysis/analysis_3.jpg" alt="analysis_3" width="800"/>

**See fancy graphs of your training run** you can also export the model data (bottom right)

<img src="/readme_images/tutorial/analysis/analysis_4.jpg" alt="analysis_4" width="800"/>

**Export: exports to a folder**

<img src="/readme_images/tutorial/analysis/export.jpg" alt="export_single" width="800"/>
</details>

<details>
<summary><b><u>Model Comparison</u></b></summary>

**Comparison**

<img src="/readme_images/tutorial/comparison/comparison_1_annotated.jpg" alt="comparison_1" width="800"/>

**Select all networks** might be too much

<img src="/readme_images/tutorial/comparison/comparison_2.jpg" alt="comparison_2" width="800"/>

**Show only the bets X networks** activate sorting by a datasets (bottom left)

<img src="/readme_images/tutorial/comparison/comparison_3.jpg" alt="comparison_3" width="800"/>

**Search for your networks of interest**

<img src="/readme_images/tutorial/comparison/comparison_4.jpg" alt="comparison_4" width="800"/>

**Click on a network to activate/deactivate it**

<img src="/readme_images/tutorial/comparison/comparison_5.jpg" alt="comparison_5" width="800"/>
</details>

</details>

<details>
<summary><b><u>Misc</u></b></summary>

<details>
<summary><b><u>Import Models</u></b> trained on a different machine</summary>

**searches also in subdirectories if wanted** (an existing dataset is needed!)

<img src="/readme_images/tutorial/misc/import_1.jpg" alt="import" width="800"/>

1. select or create a dataset. All imported nets are related to this dataset!
2. pass the path to your folder containing the checkpoint(s)
3. only interested in analysis? import only the last checkpoint of your folder

</details>

<details>
<summary><b><u>Datasets</u></b></summary>

- **Will be created/related automatically if a new training is started**
- **A dataset is related to a single folder on the device!** you can set a custom name for convenience

**Show datasets**

<img src="/readme_images/tutorial/misc/datasets.jpg" alt="dataset" width="800"/>

**Or create a new one**

<img src="/readme_images/tutorial/misc/dataset_new.jpg" alt="create_dataset" width="800"/>
</details>

<details>
<summary><b><u>Configurations</u></b></summary>

**Left: calculated training configurations | Right: optimal inference configurations**

<img src="/readme_images/tutorial/misc/configurations.jpg" alt="configs" width="800"/>
</details>

<details>
<summary><b><u>Logs</u></b></summary>

- **Logs are created automatically**
- **They are stored at: `aiseg/logs`**
- **every execution has their own log**

<img src="/readme_images/tutorial/misc/logs_1.jpg" alt="logs" width="800"/>

**Select a log, and you get to the monitoring**

<img src="/readme_images/tutorial/training/training/training_run_with_log.jpg" alt="example_log" width="800"/>
</details>

<details>
<summary><b><u>Monitor</u></b></summary>

**Every execution can be monitored live, like a training:**

<img src="/readme_images/tutorial/training/training/training_run_with_log.jpg" alt="example_log_tr" width="800"/>

**Or a testing**

<img src="/readme_images/tutorial/misc/logs_example_testing.jpg" alt="example_log_tr" width="800"/>

**Or a parameter calculation**

<img src="/readme_images/tutorial/misc/logs_example_para_calc.jpg" alt="example_log_tr" width="800"/>

**And so on**
</details>

</details>

<a name="archi"></a>
# Models

<details>
<summary><b><u>Click to expand</u></b></summary>

<a name="overview"></a>
**Overview**
- If **source is given**, the model is not implemented by myself but copied, bug freed and the code is changed more or 
  less to fit into this project (GroupNorm was also added).
- If **paper is given**, I reimplemented the network or due to bugs in the original implementation and/or I heavily 
  refactored a network code.
- If **a backbone** is given, you can pass one of them during the training guide.

<a name="overview2d"></a>
### 2D Networks

<details>
<summary><b><u>Click to show 2D networks</u></b></summary>

| Model Name                     | Possible Backbones                                                        | Source                                                                                                                                                      |
|--------------------------------|---------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Attention UNet                 |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation) [paper](https://doi.org/10.48550/arXiv.1804.03999)                                   |
| BiSeNet                        |                                                                           | [source: zllrunning](https://github.com/zllrunning/face-parsing.PyTorch) [paper](https://doi.org/10.48550/arXiv.1808.00897)                                 |
| BiSeNetV2                      |                                                                           | [source: CoinCheung](https://github.com/CoinCheung/BiSeNet) [paper](https://doi.org/10.48550/arXiv.2004.02147)                                              |
| DANet                          | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: junfu1115](https://github.com/junfu1115/DANet) [paper](https://doi.org/10.48550/arXiv.1809.02983)                                                  |
| DRAN                           | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: junfu1115](https://github.com/junfu1115/DANet) [paper](https://doi.org/10.48550/ARXIV.2109.14525)                                                  |
| DeepLab V3+                    | drn, xception, resnet101                                                  | [source: jfzhang95](https://github.com/jfzhang95/pytorch-deeplab-xception) [paper](https://doi.org/10.48550/arXiv.1802.02611)                               |
| DenseASPP 121/161/169/201      |                                                                           | [source: DeepMotionAIResearch](https://github.com/DeepMotionAIResearch/DenseASPP) [paper](https://doi.org/10.1109/CVPR.2018.00388)                          |
| DenseNet 57/67/103             |                                                                           | [source: ilsang](https://github.com/ilsang/PyTorch-SE-Segmentation) [paper](https://doi.org/10.48550/arXiv.1608.06993)                                      |
| DFANet                         |                                                                           | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) [paper](https://doi.org/10.48550/arXiv.1904.02216)                        |
| DFN                            |                                                                           | [source: ycszen](https://github.com/ycszen/TorchSeg) [paper](https://doi.org/10.48550/arXiv.1804.09337)                                                     |
| DUC - HDC                      |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1702.08502)                                   |
| ENet                           |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1606.02147)                                   |
| ERFNet                         |                                                                           | [source: Eromera](https://github.com/Eromera/erfnet_pytorch) [paper](https://doi.org/10.1109/TITS.2017.2750080)                                             |
| ESPNet                         |                                                                           | [source: sacmehta](https://github.com/sacmehta/ESPNet) [paper](https://doi.org/10.48550/arXiv.1803.06815)                                                   |
| ExtremeC3Net                   |                                                                           | [source: clovaai](https://github.com/clovaai/ext_portrait_segmentation) [paper](https://doi.org/10.48550/arXiv.1908.03093)                                  |
| Fast-SCNN                      |                                                                           | [source: DeepVoltaire](https://github.com/DeepVoltaire/Fast-SCNN) [paper](https://doi.org/10.48550/arXiv.1902.04502)                                        |
| FCN 8/16/32                    |                                                                           | [source: wkentaro](https://github.com/wkentaro/pytorch-fcn) [paper](https://doi.org/10.48550/arXiv.1605.06211)                                              |
| GCN                            | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1703.02719)                                   |
| GSCNN                          | wider_resnet38_a2 (not changeable)                                        | [source: nv-tlabs](https://github.com/nv-tlabs/GSCNN) [paper](https://doi.org/10.48550/arXiv.1907.05740)                                                    |
| HRNet                          |                                                                           | [source: HRNet](https://github.com/HRNet/HRNet-Semantic-Segmentation) [paper](https://doi.org/10.48550/arXiv.1908.07919)                                    |
| ICNet                          | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) [paper](https://doi.org/10.48550/arXiv.1704.08545)                        |
| LadderNet                      |                                                                           | [source: juntang-zhuang](https://github.com/juntang-zhuang/LadderNet) [paper](https://doi.org/10.48550/arXiv.1810.07810)                                    |
| LEDNet                         |                                                                           | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) [paper](https://doi.org/10.48550/arXiv.1905.02423)                        |
| OCNet                          | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) [paper](https://doi.org/10.48550/arXiv.1809.00916)                        |
| PSANet (currently not working) | resnet50, resnet101, resnet152, resnext50, resnext101                     | [paper](https://arxiv.org/ftp/arxiv/papers/2102/2102.07880.pdf) [paper](https://doi.org/10.48550/arXiv.2102.07880)                                          |
| PSPNet                         | resnet50, resnet101, resnet152, resnext50, resnext101                     | Refactored, added better decoding [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1612.01105) |
| PSPDenseNet                    | densenet121, densenet161, densenet169, densenet201                        | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1612.01105)                                   |
| R2-Attention UNet              |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation) combination of Attention and R2 UNet - no paper                                      |
| R2UNet                         |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation) [paper](https://doi.org/10.48550/arXiv.1802.06955)                                   |
| SegNet                         | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1505.07293)                                   |
| SINet                          |                                                                           | [source: clovaai](https://github.com/clovaai/ext_portrait_segmentation) [paper](https://doi.org/10.48550/arXiv.1911.09099)                                  |
| UNet                           | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | Build by myself. [paper](https://doi.org/10.48550/arXiv.1505.04597)                                                                                         |
| UNet ++                        |                                                                           | [source: 4uiiurz1](https://github.com/4uiiurz1/pytorch-nested-unet) [paper](https://doi.org/10.48550/arXiv.1807.10165)                                      |
| UNet 3+                        | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | Customized, no code was provided. [paper](https://doi.org/10.1109/ICASSP40776.2020.9053405)                                                                 |
| UPerNet                        | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) [paper](https://doi.org/10.48550/arXiv.1807.10221)                                   |

</details>

<a name="overview3d"></a>
### 3D Networks

<details>
<summary><b><u>Click to show 3D networks</u></b></summary>

| Model Name       | Possible Backbones                                                | Source                                                                                                                         |
|------------------|-------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| DenseVoxNet      |                                                                   | [source: black0017](https://github.com/black0017/MedicalZooPytorch) [paper](https://doi.org/10.48550/arXiv.1708.00573)         |
| High ResNet 3D   |                                                                   | [source: black0017](https://github.com/black0017/MedicalZooPytorch) [paper](https://doi.org/10.1007/978-3-319-59050-9_28)      |
| Med 3D           | resnet10 resnet18 resnet34 resnet50 resnet101 resnet152 resnet200 | [source: black0017](https://github.com/black0017/MedicalZooPytorch) [paper](https://doi.org/10.48550/arXiv.1904.00625)         |
| SkipDenseSeg     |                                                                   | Bug freed [source: tbuikr](https://github.com/tbuikr/3D-SkipDenseSeg) [paper](https://doi.org/10.1016/j.bspc.2019.101613)      |
| Residual UNet 3D |                                                                   | [source: wolny](https://github.com/wolny/pytorch-3dunet) [paper](https://doi.org/10.48550/arXiv.1706.00120)                    |
| UNet 3D          |                                                                   | [source: wolny](https://github.com/wolny/pytorch-3dunet) [paper](https://doi.org/10.1007/978-3-319-46723-8_49)                 |
| VNet             |                                                                   | [source: black0017](https://github.com/black0017/MedicalZooPytorch) [paper](https://doi.org/10.48550/arXiv.1606.04797)         |
| VNet Light       |                                                                   | [source: black0017](https://github.com/black0017/MedicalZooPytorch) [paper](https://doi.org/10.1109/ICASSP40776.2020.9053454)  |

</details>

<a name="copy"></a>
### How to copy:
To **use them in your project**: copy the base_model.py, the model_constants.py, the utils.py and of course
the related model folder. 

Also: **Cite the authors and the project!** That's it.

Attention: binary classification of background and foreground expects num_classes=2! Change that if needed.

</details>

<a name="data"></a>
# Data preparation

<details>
<summary><b><u>Click to expand</u></b></summary>

**Example images below!** (expand 2D or 3D Data)

Create a folder at the same level as `aiseg/` called `segmentation_datasets` and prepare your data as follows:
- **Images and masks** must have the **same name** (not extension, e.g.: image: img_0001.jpg mask: img_0001.png)!
- If(recommended) you have validation data, put your it inside here: `your_dataset_folder/validation`
- If(recommended) you have test data, put your it inside here: `your_dataset_folder/test`
- If you have no explicit validation data, the validation data will be extracted from the training data. To reproduce 
  the separation, set reproduce to True during the guide. I recommend preselecting 
  the validation data to enable training continuation from checkpoints without reproduce=True. 

Here is an example of the folder structure (with validation and test data):

## Setup: 
Model zoo will be created during training and contains your models.

<img src="/readme_images/folder_structure/01_base_dir.PNG" alt="base_dir">

## Put Datasets inside segmentation_datasets

<img src="/readme_images/folder_structure/02_segmentation_datasets.PNG" alt="base_dir">

## Setup your data:

**2D Data**
<details>
<summary><b><u>Click to show folder structure</u></b></summary>

| Datasets                                                                                           | 2D Structure                                                                            | Example Images                                                            |
|----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---------------------------------------------------------------------------|
| <img src="/readme_images/folder_structure/02_segmentation_datasets_select_2d.PNG" alt="structure"> | <img src="/readme_images/folder_structure/03_dataset_structure_2d.PNG" alt="dataset2d"> | <img src="/readme_images/folder_structure/04_images_2d.PNG" alt="images"> |

```
aiseg/
segmentation_datasets/
    └───2D_dataset
        └───images
        │   img_0001.jpg
        │   img_0002.jpg
        │   ...
        │
        └───masks
        │   mask_0001.tif
        │   mask_0002.tif
        │   ...
        │ 
        └───validation
        │    └───images
        │    │   val_img_0001.png
        │    │   val_img_0002.png
        │    │   ...
        │    │
        │    └───masks
        │        val_mask_0001.png
        │        val_mask_0002.png
        │        ...
        │
        └───test (optional, highly recommended)
             └───images
             │   test_img_0001.png
             │   test_img_0002.png
             │   ...
             │
             └───masks
                 test_mask_0001.png
                 test_mask_0002.png
                 ...
```
</details>

**3D Data**
<details>
<summary><b><u>Click to show folder structure</u></b></summary>

| Datasets                                                                                           | 3D Structure                                                                            | Images folder contains sub folders with the volumes                                         |
|----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| <img src="/readme_images/folder_structure/02_segmentation_datasets_select_3d.PNG" alt="structure"> | <img src="/readme_images/folder_structure/06_dataset_structure_3d.PNG" alt="dataset2d"> | <img src="/readme_images/folder_structure/08_dataset_structure_images_3d.PNG" alt="images"> |


```
aiseg/
segmentation_datasets/
    └───3D_or_time_sequence_dataset
        └───images
        │   └───sequence_or_vol_1
        │   │   img_0001.bmp
        │   │   img_0002.bmp
        │   │   ...
        │   └───sequence_or_vol_2
        │   │   img_0001.jpg
        │   │   img_0002.jpg
        │   │   ...
        │   └───sequence_or_vol_n
        │       img_0001.png
        │       img_0002.png
        │       ...
        └───masks
        │   └───sequence_or_vol_1
        │   │   mask_0001.png
        │   │   mask_0002.png
        │   │   ...
        │   └───sequence_or_vol_2
        │   │   mask_0001.png
        │   │   mask_0002.png
        │   │   ...
        │   └───sequence_or_vol_n
        │       mask_0001.png
        │       mask_0002.png
        │       ...
        │
        └───validation
        │       └───images
        │       │   val_sequence_or_vol_1
        │       │   val_sequence_or_vol_2
        │       │   ...
        │       │
        │       └───masks
        │           val_sequence_or_vol_1
        │           val_sequence_or_vol_2
        │           ...
        │
        │
        │
        └───test (optional, but highly recommended)
                └───images
                │   test_sequence_or_vol_1
                │   test_sequence_or_vol_2
                │   ...
                │
                └───masks
                    test_sequence_or_vol_1
                    test_sequence_or_vol_2
                    ...
```
</details>
</details>

<a name="augmentation"></a>
# Offline Augmentation

(see at tutorial for online augmentation)

<details>
<summary><b><u>Click to expand</u></b></summary>

**What it does?**

The term offline augmentation refers to the manipulation of the input data before the training has started. Hereby, 
the operator retains control over the dataset. The augmented images and masks can be inspected by the operator 
beforehand to check if the augmentations are reasonable, as they are notoriously domain depended. An example of a 
classification problem: If the number 6 were rotated by 180°, a 9 would result, but it would still be labeled as a 6.
One disadvantage of this approach is the much larger required storage space.

The augmentation uses many own procedures combined with the great [albumentations](https://pypi.org/project/albumentations/) 
library. Check them out!

**Should I use online or offline augmentation?**

It was found that offline augmentation is superior on fewer data (up to 1k training images), while online augmentation 
is advantageous for a larger dataset (like Cityscapes), as it becomes impractical to store many augmented images on 
the hard drive. Checkout [this paper (link missing, file in review)]().

**How to augment an existing dataset**
1. select augmentation and choose only the images and masks of the training dataset! Do not touch the validation or test data!

The new training data does now consist of:
- original images and masks
- augmented images and masks

### Impact example offline augmentation:
<details>
<summary><b><u>Click to expand</u></b></summary>

This image shows 100 2D neural networks trained to segment water in rivers. Orange: with augmentation, 
Blue: without augmentation.

<img src="/readme_images/validation_accuracy_augmentation_vs_no_aug.PNG" alt="augmentation_impact">

</details>

#### usable 2D Operations (offline):
<details>
<summary><b><u>Click to expand</u></b></summary>

<img src="/readme_images/tutorial/augmentation/offline/img_augmentations.JPG" alt="augmentation_impact">

</details>

#### usable 3D Operations (offline):
<details>
<summary><b><u>Click to expand</u></b></summary>

<img src="/readme_images/tutorial/augmentation/offline/volume_augmentations.JPG" alt="augmentation_impact">

</details>
</details>

<a name="citation"></a>
# Citation
If you use this software or part of its code for your research:

```
Please cite using the GitLab link.
```

Contact: [Franz Wagner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/photogrammetrie/die-professur/beschaeftigte/m-sc-franz-wagner?set_language=en)

Papers using this project:
- Abstract: [Towards automatic real-time water level estimation using surveillance cameras](https://doi.org/10.5194/egusphere-egu22-3225)
- Article: [Image-Based Modelling and Analysis of Carbon-Fibre Reinforced Concrete Shell Structures](https://www.researchgate.net/publication/365051068_Image-based_Modelling_and_Analysis_of_Carbon-Fibre_Reinforced_Concrete_Shell_Structures)
- Article: [River Water Segmentation: A Comparative Study of Offline and Online Augmentation using 32 CNNs](https://doi.org/10.1016/j.jag.2023.103305)
- Article: [A Comparative Study of Deep Architectures for Voxel Segmentation in Volume Images](https://doi.org/10.5194/isprs-archives-XLVIII-1-W2-2023-1667-2023)
- Article: [Analysis of Thin Carbon Reinforced Concrete Structures through Microtomography and Machine Learning](https://doi.org/10.3390/buildings13092399)
- Article: [Damage Analysis and Quality Control of Carbon-Reinforced Concrete Beams Based on In Situ Computed Tomography Tests](https://doi.org/10.3390/buildings13102669)
